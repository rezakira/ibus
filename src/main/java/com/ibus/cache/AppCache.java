package com.ibus.cache;

import java.util.HashMap;
import java.util.Map;

import javax.security.auth.Subject;

import com.ibus.constant.CacheType;

import co.id.rokubi.model.Menu;
import co.id.rokubi.model.Role;
import co.id.rokubi.model.Setting;

public class AppCache {

	private static Map<String, Setting> settingMap;
	private static Map<String, Menu> menuMap;
	private static Map<String, Subject> subjectMap;
	private static Map<String, Role> roleMap;

	static {
		settingMap = new HashMap<String, Setting>();
		menuMap = new HashMap<String, Menu>();
		subjectMap = new HashMap<String, Subject>();
		roleMap = new HashMap<String, Role>();
	}

	public static Map<String, Setting> getSettingMap() {
		return settingMap;
	}

	public static Map<String, Menu> getMenuMap() {
		return menuMap;
	}

	public static Map<String, Subject> getSubjectMap() {
		return subjectMap;
	}

	public static Map<String, Role> getRoleMap() {
		return roleMap;
	}

	public static void add(ICacheable cacheableObj, CacheType type) {

		if (type.equals(CacheType.SETTING)) {
			settingMap.put(cacheableObj.getId(), (Setting) cacheableObj.get());

		} else if (type.equals(CacheType.MENU)) {
			menuMap.put(cacheableObj.getId(), (Menu) cacheableObj.get());
		} else if (type.equals(CacheType.ROLE)) {
			roleMap.put(cacheableObj.getId(), (Role) cacheableObj.get());
		}

	
	}

}
