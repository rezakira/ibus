package com.ibus.cache;

public interface ICacheable {
	public Object get ();
	public String getId();
}
