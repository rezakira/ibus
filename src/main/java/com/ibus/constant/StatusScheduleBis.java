package com.ibus.constant;

public enum StatusScheduleBis {
	BELUM_BERANGKAT("B"), 
	DIJALAN("D"), 
	SUDAH_SAMPAI("S");
	
	private String value;
	
	private StatusScheduleBis(String value) {
		this.value = value;
	}

	public String getValue() {
		return value;
	}
}
