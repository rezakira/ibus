package com.ibus.constant;

public enum RoleType {
	
	MEMBER("MBM"), 
	ADMINISTRATOR("ADM"), 
	OWNER("OWN"), 
	OPERATOR("OPT");
	
	private String value;
	
	private RoleType(String value) {
		this.value = value;
	}

	public String getValue() {
		return value;
	}

}
