package com.ibus.constant;

public enum StatusOrder {
	BOOKING("1"), 
	DIBAYAR("2"), 
	DIBATALKAN("3"), 
	DITOLAK("4"),
	SELESAI("5");
	
	private String value;
	
	private StatusOrder(String value) {
		this.value = value;
	}

	public String getValue() {
		return value;
	}
}
