package com.ibus.constant;

public enum StatusBus {
	BOOKED("1"), 
	RESERVED("2"), 
	AVAILABLE("3");
	
	private String value;
	
	private StatusBus(String value) {
		this.value = value;
	}

	public String getValue() {
		return value;
	}
}
