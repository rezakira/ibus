package com.ibus.constant;

import java.math.BigDecimal;
//import java.math.MathContext;
import java.math.RoundingMode;

import com.ibus.util.NumberUtil;

public class FormulaCalculator {

	//private MathContext mc = new MathContext(2); // 2 precision

	private BigDecimal valueCapaian;

	private BigDecimal valueTarget;

	// --------- OBLIK 	-----------//
	public BigDecimal countPersentaseKetersediaanObatVaksin (BigDecimal avObatVaksin){
		//		MathContext mc = new MathContext(2);
		//		BigDecimal value =
		//				new BigDecimal(100).divide(new BigDecimal(144), 2, BigDecimal.ROUND_HALF_UP);
		//		BigDecimal temp =  value.multiply(avObatVaksin, mc);	
		//		return temp;
		return avObatVaksin;
	}	

	public BigDecimal countPersentasePenggunaanObatGenerik (BigDecimal sampleRs, BigDecimal samplePkm){

		//		BigDecimal temp =  sampleRs.add(samplePkm);
		//
		//		BigDecimal value = temp.divide(new BigDecimal(108), 2, BigDecimal.ROUND_HALF_UP);
		//
		//		return (value.multiply(new BigDecimal(100)));
		return NumberUtil.getAverage(sampleRs, samplePkm);
	}

	public BigDecimal countPersentaseInstalasi (BigDecimal sigIfStandard, BigDecimal sigIf){
		//		BigDecimal value =
		//				sigIfStandard.divide(sigIf,2, BigDecimal.ROUND_HALF_UP);
		//		return value.multiply(new BigDecimal(100));
		return NumberUtil.getPercent(sigIfStandard, sigIf);
	}
	// --------- 		-----------//

	// --------- KEGIATAN PENINGKATAN PELAYANAN KEFARMASIAN (YANFAR)	-----------//
	public BigDecimal countInstalasiFarmasiRumahSakitPemerintah (BigDecimal sigRspStandard, BigDecimal sigRsp){
		/*BigDecimal value =
				sigRspStandard.divide(sigRsp,2, BigDecimal.ROUND_HALF_UP);
		return value.multiply(new BigDecimal(100));*/

		return NumberUtil.getPercent(sigRspStandard, sigRsp);
	}

	public BigDecimal countPuskesmasPerawatan (BigDecimal sigPkmStandard, BigDecimal sigPkm){
		//		BigDecimal value =
		//				sigPkmStandard.divide(sigPkm,2, BigDecimal.ROUND_HALF_UP);
		//		return value.multiply(new BigDecimal(100));

		return NumberUtil.getPercent(sigPkmStandard, sigPkm);
	}	

	//belum fix!!!!
	public BigDecimal countPersentasePenggunaanObatRasional (BigDecimal sigIspaAntibiotik,
			BigDecimal sigDiareAntibiotik, BigDecimal sigMialgiaInjeksi, BigDecimal rerataItemObat){
		//		// rumusnya 
		//		// 1-(pctPor/4) * (4/1.4) 
		//
		//		// (pctPor/4)
		//		BigDecimal a = pctPor.divide(new BigDecimal(4),2, BigDecimal.ROUND_HALF_UP);
		//
		//		// 1-a
		//		BigDecimal b = new BigDecimal(1).subtract(a);
		//
		//		// (4/1.4) 
		//		BigDecimal c = new BigDecimal(4).divide(new BigDecimal(1.4),2, BigDecimal.ROUND_HALF_UP);
		//
		//		BigDecimal d = b.multiply(c);
		//
		//		BigDecimal valueIspa =
		//				sigIspaAntibiotik.divide(sigIspa,2, BigDecimal.ROUND_HALF_UP);
		//
		//		BigDecimal valueDiare =
		//				sigDiareAntibiotik.divide(sigDiare,2, BigDecimal.ROUND_HALF_UP);
		//
		//		BigDecimal valueMyalgia =
		//				sigMialgiaInjeksi.divide(sigMyalgia,2, BigDecimal.ROUND_HALF_UP);
		//
		//		//BigDecimal result = null;
		//
		//		BigDecimal result = (new BigDecimal(100).subtract(valueIspa)).multiply(new BigDecimal(100/80));		
		//
		//		result = result.add((new BigDecimal(100).subtract(valueDiare)).multiply(new BigDecimal(100/92)));
		//
		//		result = result.add((new BigDecimal(100).subtract(valueMyalgia)).multiply(new BigDecimal(100/99)));
		//
		//		result = result.add(d);
		//
		//		result = result.divide(new BigDecimal(6.21),2, BigDecimal.ROUND_HALF_UP);
		//
		//		return result;
		//BigDecimal paIspa = (NumberUtil.ONE_HUNDRED.subtract(sigIspaAntibiotik)).multiply(NumberUtil.ONE_HUNDRED.divide(NumberUtil.EIGHTY,2,RoundingMode.HALF_UP));
		//BigDecimal paDiare = (NumberUtil.ONE_HUNDRED.subtract(sigDiareAntibiotik,MathContext.UNLIMITED)).multiply((NumberUtil.ONE_HUNDRED.divide(NumberUtil.NINTY_TWO, 2, RoundingMode.HALF_UP)));
		//BigDecimal paMyalgia = (NumberUtil.ONE_HUNDRED.subtract(sigMialgiaInjeksi,MathContext.UNLIMITED)).multiply((NumberUtil.ONE_HUNDRED.divide(NumberUtil.NINTY_NINE, 2, RoundingMode.HALF_UP)));
		//BigDecimal rt = BigDecimal.ONE.subtract(rerataItemObat.divide(NumberUtil.FOUR, 2, RoundingMode.HALF_UP), MathContext.UNLIMITED).multiply(NumberUtil.FOUR.divide(new BigDecimal(1.4), 2, RoundingMode.HALF_UP), MathContext.UNLIMITED);
		//return NumberUtil.getAverage(paIspa, paDiare, paMyalgia, rt);

		// rumus di excel -> =( S*1.25 + T*1.09 + U*1.01 + V*2.86) / 6.2
//		BigDecimal s = NumberUtil.ONE_HUNDRED.subtract(sigIspaAntibiotik);
//		BigDecimal t = NumberUtil.ONE_HUNDRED.subtract(sigDiareAntibiotik);
//		BigDecimal u = checkMyalgia (sigMialgiaInjeksi);
//		BigDecimal v = checkRerataItemObat (rerataItemObat);
//		
//		s	=	s.multiply(new BigDecimal(1.25));
//		t	=	t.multiply(new BigDecimal(1.09));
//		u	=	u.multiply(new BigDecimal(1.01));
//		v	=	v.multiply(new BigDecimal(2.86));
//
//		return (s.add(t).add(u).add(v)).divide(NumberUtil.SIX_POINT_TWENTY_ONE, 2, RoundingMode.HALF_UP);
		
		BigDecimal result = sigIspaAntibiotik.add(sigDiareAntibiotik);
		result = result.add(sigMialgiaInjeksi);
		result = result.add(rerataItemObat);
		return result.divide(new BigDecimal(4), RoundingMode.HALF_UP);
	}
	
//	private BigDecimal checkMyalgia (BigDecimal sigMialgiaInjeksi){
//		//=IF(L16<1,100,	IF(L16<2,90,	IF(L16<3,80,	IF(L16<4,70,	IF(L16<5,60,	IF(L16<6,50,	IF(L16<7,40,	IF(L16<8,30,30))))))))
//		if (sigMialgiaInjeksi.compareTo(new BigDecimal (1)) == -1)
//			return new BigDecimal (100);
//		
//		else if (sigMialgiaInjeksi.compareTo(new BigDecimal (2)) == -1)
//			return new BigDecimal (90);		
//		
//		else if (sigMialgiaInjeksi.compareTo(new BigDecimal (3)) == -1)
//			return new BigDecimal (80);		
//		
//		else if (sigMialgiaInjeksi.compareTo(new BigDecimal (4)) == -1)
//			return new BigDecimal (70);		
//		
//		else if (sigMialgiaInjeksi.compareTo(new BigDecimal (5)) == -1)
//			return new BigDecimal (60);		
//		
//		else if (sigMialgiaInjeksi.compareTo(new BigDecimal (6)) == -1)
//			return new BigDecimal (50);		
//		
//		else if (sigMialgiaInjeksi.compareTo(new BigDecimal (7)) == -1)
//			return new BigDecimal (40);		
//		
//		else if (sigMialgiaInjeksi.compareTo(new BigDecimal (8)) == -1)
//			return new BigDecimal (30);					
//		else
//			return new BigDecimal (30);
//	}
	
//	private BigDecimal checkRerataItemObat (BigDecimal rerataItemObat){
//		//=IF(M16<=2.6,100,	IF(M16<=2.8,88.9,	IF(M16<=3,77.8,
//		//IF(M16<=3.2,66.7,	IF(M16<=3.4,55.6,	IF(M16<=3.6,44.4,
//		//IF(M16<=3.8,33.3,	IF(M16<=4,33.3,33.3))))))))
//		if (rerataItemObat.compareTo(new BigDecimal (2.6)) == -1)
//			return new BigDecimal (100);
//		
//		else if (rerataItemObat.compareTo(new BigDecimal (2.8)) == -1)
//			return new BigDecimal (88.9);		
//		
//		else if (rerataItemObat.compareTo(new BigDecimal (3)) == -1)
//			return new BigDecimal (77.8);		
//		
//		else if (rerataItemObat.compareTo(new BigDecimal (3.2)) == -1)
//			return new BigDecimal (66.7);		
//		
//		else if (rerataItemObat.compareTo(new BigDecimal (3.4)) == -1)
//			return new BigDecimal (55.6);		
//		
//		else if (rerataItemObat.compareTo(new BigDecimal (3.6)) == -1)
//			return new BigDecimal (44.4);		
//		
//		else if (rerataItemObat.compareTo(new BigDecimal (3.8)) == -1)
//			return new BigDecimal (33.3);		
//		
//		else if (rerataItemObat.compareTo(new BigDecimal (4)) == -1)
//			return new BigDecimal (33.3);					
//		else
//			return new BigDecimal (33.3);
//	}	

	// --------- 		-----------//

	// --------- KEGIATAN PENINGKATAN PRODUKSI DAN DISTRIBUSI ALAT KESEHATAN 	-----------//
	public BigDecimal countPersentaseSaranaProduksiAlkes (BigDecimal sigSaranaProdCpakb, BigDecimal sigSaranaProd){
		//		BigDecimal value =
		//				sigSaranaProdCpakb.divide(sigSaranaProd,2, BigDecimal.ROUND_HALF_UP);
		//		return value.multiply(new BigDecimal(100));	

		return NumberUtil.getPercent(sigSaranaProdCpakb, sigSaranaProd);
	}

	public BigDecimal countPersentaseSaranaDistribusiAlkesPkrt (BigDecimal sigSaranaDistSyrt, BigDecimal sigSaranaDist){
		//		BigDecimal value =
		//				sigSaranaDistSyrt.divide(sigSaranaDist,2, BigDecimal.ROUND_HALF_UP);
		//		return value.multiply(new BigDecimal(100));	

		return NumberUtil.getPercent(sigSaranaDistSyrt, sigSaranaDist);
	}	

	public BigDecimal countPersentaseProdukAlkes (BigDecimal sigPkrtSyarat, BigDecimal sigPkrt){
		//		BigDecimal value =
		//				sigPkrtSyarat.divide(sigPkrt,2, BigDecimal.ROUND_HALF_UP);
		//		return value.multiply(new BigDecimal(100));		

		return NumberUtil.getPercent(sigPkrtSyarat, sigPkrt);
	}		
	
	public BigDecimal countPersentasePremarketGrp (BigDecimal sigPmSelesai, BigDecimal sigPmMasuk){
		return NumberUtil.getPercent(sigPmSelesai, sigPmMasuk);   
	}	
	
	// --------- 		-----------//

	// --------- SETDITJEN 	-----------//
	public BigDecimal countPersentaseDokumenAnggaran (BigDecimal sigAnggaranDisetujui, BigDecimal sigAnggaranDiusulkan){
		//		BigDecimal value =
		//				sigAnggaranDisetujui.divide(sigAnggaranDiusulkan,2, BigDecimal.ROUND_HALF_UP);
		//		return value.multiply(new BigDecimal(100));	

		return NumberUtil.getPercent(sigAnggaranDisetujui, sigAnggaranDiusulkan);
	}

	public BigDecimal countPersentaseDukunganManajemen (BigDecimal sigDekonDilaksanakan, BigDecimal sigOutputDekon){
		//		BigDecimal value =
		//				sigDekonDilaksanakan.divide(sigOutputDekon,2, BigDecimal.ROUND_HALF_UP);
		//		return value.multiply(new BigDecimal(100));

		return NumberUtil.getPercent(sigDekonDilaksanakan, sigOutputDekon);
	}	
	/*
	 * 
	 * FORMULA NOMER 3 langsung bind ke value capaian
	 * 
	 * 
	 * */

	// --------- 		-----------//

	// --------- FM gak ada formula, semua langsung bind 	-----------//

	// --------- 	-----------//

	public BigDecimal getValueCapaian() {
		return valueCapaian;
	}

	public void setValueCapaian(BigDecimal valueCapaian) {
		this.valueCapaian = valueCapaian;
	}

	public BigDecimal getValueTarget() {
		return valueTarget;
	}

	public void setValueTarget(BigDecimal valueTarget) {
		this.valueTarget = valueTarget;
	}

	// --------- OBLIK 	-----------//
	// --------- 		-----------//
}