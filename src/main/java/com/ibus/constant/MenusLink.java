/**
 * 
 */
package com.ibus.constant;

import java.util.HashMap;
import java.util.Map;

import co.id.rokubi.model.Link;

/**
 * @author Radian
 *
 */
public class MenusLink  extends co.id.rokubi.constant.MenusLink {
	public static Map<String, Link> link = new HashMap<String, Link>();
	
	static{
		link = co.id.rokubi.constant.MenusLink.link;
		
//		// main menu
//		link.put("EXPPK", new Link("", "iconsweets-notebook"));
//		link.put("EXPPM", new Link("", "iconsweets-chart8"));
//		link.put("REPRT", new Link("", "iconsweets-usercomment"));
//		
//		link.put("EXPOT", new Link("", "iconsweets-suitcase"));
		
		// sub menu
		link.put("LIBUS", new Link("/data-master/list_bus.page", ""));
		link.put("LISDS", new Link("/data-master/list_rute_from.page", ""));
		link.put("LISSB", new Link("/data-master/list_schedule_bus.page", ""));
		link.put("LIORD", new Link("/transaction/list_order.page", ""));
		link.put("LIBPM", new Link("/berita_promo/list_berita_promo.page", ""));
		link.put("LISOT", new Link("/data-master/list_schedule_on_time.page", ""));
		
		
		
	}
}
