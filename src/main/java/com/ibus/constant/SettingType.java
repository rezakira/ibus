package com.ibus.constant;

public enum SettingType {
	TIME_ZONE("TIMZN"), 
	LANGUAGE("LANGE"), 
	SCHOOL_NAME("SCHNM"), 
	SCHOOL_ADDRESS("SCHAD"), 
	SCHOOL_PIC("SCHPC"), 
	SCHOOL_PIC_EMAIL("SCHEM"), 
	SCHOOL_PIC_PHONE("SCHPH");

	private String value;

	private SettingType(String value) {
		this.value = value;
	}

	public String getValue() {
		return value;
	}

}
