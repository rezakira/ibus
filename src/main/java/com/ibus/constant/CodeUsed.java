package com.ibus.constant;

public enum CodeUsed {
	PAJAK_KELUARAN("PK"), 
	PAJAK_MASUKKAN("PM"),
	DOKUMEN_LAIN("DL"),
	KODE_BRG_JASA("BJ"),
	LAWAN_TRANSAKSI("LW"),
	RETUR_DOKUMEN_LAIN("RDL"),
	RETUR_PAJAK_KELUARAN("RPK"),
	RETUR_PAJAK_MASUKKAN("RPM");

	private String value;

	private CodeUsed(String value) {
		this.value = value;
	}

	public String getValue() {
		return value;
	}
}
