package com.ibus.constant;

public enum CacheType {
	SETTING, MENU, SUBJECT, ROLE, CALENDAR;
}
