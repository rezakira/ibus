package com.ibus.constant;

public enum StatusBeritaPromo {
	DRAFT("0"),  
	PUBLISH("1");
	
	private String value;
	
	private StatusBeritaPromo(String value) {
		this.value = value;
	}

	public String getValue() {
		return value;
	}
}
