package com.ibus.dao;

import com.ibus.model.ETicket;
import java.util.List;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Result;
import org.apache.ibatis.annotations.Results;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.type.JdbcType;

public interface ETicketMapper {
    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table e-ticket
     *
     * @mbggenerated Mon Aug 29 15:27:30 ICT 2016
     */
    @Insert({
        "insert into e_ticket (ID, ID_ORDER, ",
        "DATE_ADDED, DATE_MODIFIED, ",
        "PATH)",
        "values (#{id,jdbcType=VARCHAR}, #{idOrder,jdbcType=VARCHAR}, ",
        "#{dateAdded,jdbcType=TIMESTAMP}, #{dateModified,jdbcType=TIMESTAMP}, ",
        "#{path,jdbcType=VARCHAR})"
    })
    int insert(ETicket record);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table e-ticket
     *
     * @mbggenerated Mon Aug 29 15:27:30 ICT 2016
     */
    @Select({
        "select",
        "ID, ID_ORDER, DATE_ADDED, DATE_MODIFIED, PATH",
        "from e_ticket"
    })
    @Results({
        @Result(column="ID", property="id", jdbcType=JdbcType.VARCHAR),
        @Result(column="ID_ORDER", property="idOrder", jdbcType=JdbcType.VARCHAR),
        @Result(column="DATE_ADDED", property="dateAdded", jdbcType=JdbcType.TIMESTAMP),
        @Result(column="DATE_MODIFIED", property="dateModified", jdbcType=JdbcType.TIMESTAMP),
        @Result(column="PATH", property="path", jdbcType=JdbcType.VARCHAR)
    })
    List<ETicket> selectAll();
    
    @Select({
        "select",
        "ID, ID_ORDER, DATE_ADDED, DATE_MODIFIED, PATH",
        "from e_ticket",
        "where ID_ORDER = #{id,jdbcType=VARCHAR}"
    })
    @Results({
        @Result(column="ID", property="id", jdbcType=JdbcType.VARCHAR),
        @Result(column="ID_ORDER", property="idOrder", jdbcType=JdbcType.VARCHAR),
        @Result(column="DATE_ADDED", property="dateAdded", jdbcType=JdbcType.TIMESTAMP),
        @Result(column="DATE_MODIFIED", property="dateModified", jdbcType=JdbcType.TIMESTAMP),
        @Result(column="PATH", property="path", jdbcType=JdbcType.VARCHAR)
    })
    ETicket selectByIDOrder(String id);
    
    @Select({
        "select",
        "ID, ID_ORDER, DATE_ADDED, DATE_MODIFIED, PATH",
        "from e_ticket",
        "where ID = #{id,jdbcType=VARCHAR}"
    })
    @Results({
        @Result(column="ID", property="id", jdbcType=JdbcType.VARCHAR),
        @Result(column="ID_ORDER", property="idOrder", jdbcType=JdbcType.VARCHAR),
        @Result(column="DATE_ADDED", property="dateAdded", jdbcType=JdbcType.TIMESTAMP),
        @Result(column="DATE_MODIFIED", property="dateModified", jdbcType=JdbcType.TIMESTAMP),
        @Result(column="PATH", property="path", jdbcType=JdbcType.VARCHAR)
    })
    ETicket selectByID(String id);
}