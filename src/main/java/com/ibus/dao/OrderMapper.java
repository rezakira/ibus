package com.ibus.dao;

import com.ibus.model.Order;
import com.ibus.model.SitOrder;

import java.util.List;

import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Result;
import org.apache.ibatis.annotations.Results;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;
import org.apache.ibatis.type.JdbcType;

public interface OrderMapper {
    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table order
     *
     * @mbggenerated Thu Aug 04 13:17:57 ICT 2016
     */
    @Delete({
        "delete from order_transaction",
        "where ID_ORDER = #{idOrder,jdbcType=VARCHAR}"
    })
    int deleteByPrimaryKey(String idOrder);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table order
     *
     * @mbggenerated Thu Aug 04 13:17:57 ICT 2016
     */
    @Insert({
        "insert into order_transaction (ID_ORDER, USER_ID, ",
        "SCHEDULE_ID, ID_RUTE_FROM, ",
        "ID_RUTE_TO, PRICE_TICKET_UNIT, ",
        "DISCOUNT, TOTAL_PERSON, ",
        "TOTAL_PRICE, STATUS, ",
        "QRCODE,ORDER_DATE,DATE_ADDED,DATE_MODIFIED)",
        "values (#{idOrder,jdbcType=VARCHAR}, #{userId,jdbcType=VARCHAR}, ",
        "#{scheduleId,jdbcType=BIGINT}, #{idRuteFrom,jdbcType=BIGINT}, ",
        "#{idRuteTo,jdbcType=BIGINT}, #{priceTicketUnit,jdbcType=INTEGER}, ",
        "#{discount,jdbcType=REAL}, #{totalPerson,jdbcType=INTEGER}, ",
        "#{totalPrice,jdbcType=INTEGER}, #{status,jdbcType=VARCHAR}, ",
        "#{qrcode,jdbcType=LONGVARCHAR},#{orderDate,jdbcType=TIMESTAMP},#{dateAdded,jdbcType=TIMESTAMP},#{dateModified,jdbcType=TIMESTAMP})"
    })
    int insert(Order record);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table order
     *
     * @mbggenerated Thu Aug 04 13:17:57 ICT 2016
     */
    @Select({
        "select",
        "ID_ORDER, USER_ID, SCHEDULE_ID, ID_RUTE_FROM, ID_RUTE_TO, PRICE_TICKET_UNIT, ",
        "DISCOUNT, TOTAL_PERSON, TOTAL_PRICE, STATUS, QRCODE,ORDER_DATE,DATE_ADDED,DATE_MODIFIED",
        "from order_transaction",
        "where ID_ORDER = #{idOrder,jdbcType=VARCHAR}"
    })
    @Results({
        @Result(column="ID_ORDER", property="idOrder", jdbcType=JdbcType.VARCHAR, id=true),
        @Result(column="USER_ID", property="userId", jdbcType=JdbcType.VARCHAR),
        @Result(column="SCHEDULE_ID", property="scheduleId", jdbcType=JdbcType.BIGINT),
        @Result(column="ID_RUTE_FROM", property="idRuteFrom", jdbcType=JdbcType.BIGINT),
        @Result(column="ID_RUTE_TO", property="idRuteTo", jdbcType=JdbcType.BIGINT),
        @Result(column="PRICE_TICKET_UNIT", property="priceTicketUnit", jdbcType=JdbcType.INTEGER),
        @Result(column="DISCOUNT", property="discount", jdbcType=JdbcType.REAL),
        @Result(column="TOTAL_PERSON", property="totalPerson", jdbcType=JdbcType.INTEGER),
        @Result(column="TOTAL_PRICE", property="totalPrice", jdbcType=JdbcType.INTEGER),
        @Result(column="STATUS", property="status", jdbcType=JdbcType.VARCHAR),
        @Result(column="QRCODE", property="qrcode", jdbcType=JdbcType.LONGVARCHAR),
        @Result(column="DATE_ADDED", property="dateAdded", jdbcType=JdbcType.TIMESTAMP),
        @Result(column="DATE_MODIFIED", property="dateModified", jdbcType=JdbcType.TIMESTAMP),
        @Result(column="ORDER_DATE", property="orderDate", jdbcType=JdbcType.TIMESTAMP)
    })
    Order selectByPrimaryKey(String idOrder);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table order
     *
     * @mbggenerated Thu Aug 04 13:17:57 ICT 2016
     */
    @Select({
        "select",
        "ID_ORDER, USER_ID, SCHEDULE_ID, ID_RUTE_FROM, ID_RUTE_TO, PRICE_TICKET_UNIT, ",
        "DISCOUNT, TOTAL_PERSON, TOTAL_PRICE, STATUS, QRCODE,ORDER_DATE,DATE_ADDED,DATE_MODIFIED",
        "from order_transaction"
    })
    @Results({
        @Result(column="ID_ORDER", property="idOrder", jdbcType=JdbcType.VARCHAR, id=true),
        @Result(column="USER_ID", property="userId", jdbcType=JdbcType.VARCHAR),
        @Result(column="SCHEDULE_ID", property="scheduleId", jdbcType=JdbcType.BIGINT),
        @Result(column="ID_RUTE_FROM", property="idRuteFrom", jdbcType=JdbcType.BIGINT),
        @Result(column="ID_RUTE_TO", property="idRuteTo", jdbcType=JdbcType.BIGINT),
        @Result(column="PRICE_TICKET_UNIT", property="priceTicketUnit", jdbcType=JdbcType.INTEGER),
        @Result(column="DISCOUNT", property="discount", jdbcType=JdbcType.REAL),
        @Result(column="TOTAL_PERSON", property="totalPerson", jdbcType=JdbcType.INTEGER),
        @Result(column="TOTAL_PRICE", property="totalPrice", jdbcType=JdbcType.INTEGER),
        @Result(column="STATUS", property="status", jdbcType=JdbcType.VARCHAR),
        @Result(column="QRCODE", property="qrcode", jdbcType=JdbcType.LONGVARCHAR),
        @Result(column="DATE_ADDED", property="dateAdded", jdbcType=JdbcType.TIMESTAMP),
        @Result(column="DATE_MODIFIED", property="dateModified", jdbcType=JdbcType.TIMESTAMP),
        @Result(column="ORDER_DATE", property="orderDate", jdbcType=JdbcType.TIMESTAMP)
    })
    List<Order> selectAll();

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table order
     *
     * @mbggenerated Thu Aug 04 13:17:57 ICT 2016
     */
    @Update({
        "update order_transaction",
        "set USER_ID = #{userId,jdbcType=VARCHAR},",
          "SCHEDULE_ID = #{scheduleId,jdbcType=BIGINT},",
          "ID_RUTE_FROM = #{idRuteFrom,jdbcType=BIGINT},",
          "ID_RUTE_TO = #{idRuteTo,jdbcType=BIGINT},",
          "PRICE_TICKET_UNIT = #{priceTicketUnit,jdbcType=INTEGER},",
          "DISCOUNT = #{discount,jdbcType=REAL},",
          "TOTAL_PERSON = #{totalPerson,jdbcType=INTEGER},",
          "TOTAL_PRICE = #{totalPrice,jdbcType=INTEGER},",
          "STATUS = #{status,jdbcType=VARCHAR},",
          "QRCODE = #{qrcode,jdbcType=LONGVARCHAR},",
          "DATE_ADDED = #{dateAdded,jdbcType=TIMESTAMP},",
          "DATE_MODIFIED = #{dateModified,jdbcType=TIMESTAMP},",
          "ORDER_DATE = #{orderDate,jdbcType=TIMESTAMP}",
        "where ID_ORDER = #{idOrder,jdbcType=VARCHAR}"
    })
    int updateByPrimaryKey(Order record);
    
    
    @Select({
    	"select",
        "ID_ORDER, USER_ID, SCHEDULE_ID, ID_RUTE_FROM, ID_RUTE_TO, PRICE_TICKET_UNIT, ",
        "DISCOUNT, TOTAL_PERSON, TOTAL_PRICE, STATUS, QRCODE,ORDER_DATE,DATE_ADDED,DATE_MODIFIED",
        "from order_transaction",
        "${sql}",
        "ORDER BY DATE_MODIFIED DESC",
        "limit #{limit,jdbcType=VARCHAR}" , "offset #{offset,jdbcType=VARCHAR}"
    })
    @Results({
    	 @Result(column="ID_ORDER", property="idOrder", jdbcType=JdbcType.VARCHAR, id=true),
         @Result(column="USER_ID", property="userId", jdbcType=JdbcType.VARCHAR),
         @Result(column="SCHEDULE_ID", property="scheduleId", jdbcType=JdbcType.BIGINT),
         @Result(column="ID_RUTE_FROM", property="idRuteFrom", jdbcType=JdbcType.BIGINT),
         @Result(column="ID_RUTE_TO", property="idRuteTo", jdbcType=JdbcType.BIGINT),
         @Result(column="PRICE_TICKET_UNIT", property="priceTicketUnit", jdbcType=JdbcType.INTEGER),
         @Result(column="DISCOUNT", property="discount", jdbcType=JdbcType.REAL),
         @Result(column="TOTAL_PERSON", property="totalPerson", jdbcType=JdbcType.INTEGER),
         @Result(column="TOTAL_PRICE", property="totalPrice", jdbcType=JdbcType.INTEGER),
         @Result(column="STATUS", property="status", jdbcType=JdbcType.VARCHAR),
         @Result(column="QRCODE", property="qrcode", jdbcType=JdbcType.LONGVARCHAR),
         @Result(column="DATE_ADDED", property="dateAdded", jdbcType=JdbcType.TIMESTAMP),
         @Result(column="DATE_MODIFIED", property="dateModified", jdbcType=JdbcType.TIMESTAMP),
         @Result(column="ORDER_DATE", property="orderDate", jdbcType=JdbcType.TIMESTAMP)
    })
    List<Order> selectAllByPage(
    		@Param("limit")Integer limit,
    		@Param("offset")Integer offset,
    		@Param("sql")String sql);
    

	@Select({
	"select",
	"count(1)",
	"from order_transaction",
	"${sql}"
	})
	@Result(javaType=java.lang.Integer.class)
	Integer selectCountAll(@Param("sql")String sql);
	
	 	@Select({
	        "select",
	        "ID_ORDER, USER_ID, SCHEDULE_ID, ID_RUTE_FROM, ID_RUTE_TO, PRICE_TICKET_UNIT, ",
	        "DISCOUNT, TOTAL_PERSON, TOTAL_PRICE, STATUS, QRCODE,ORDER_DATE,DATE_ADDED,DATE_MODIFIED",
	        "from order_transaction",
	        "${sql}"
	    })
	    @Results({
	        @Result(column="ID_ORDER", property="idOrder", jdbcType=JdbcType.VARCHAR, id=true),
	        @Result(column="USER_ID", property="userId", jdbcType=JdbcType.VARCHAR),
	        @Result(column="SCHEDULE_ID", property="scheduleId", jdbcType=JdbcType.BIGINT),
	        @Result(column="ID_RUTE_FROM", property="idRuteFrom", jdbcType=JdbcType.BIGINT),
	        @Result(column="ID_RUTE_TO", property="idRuteTo", jdbcType=JdbcType.BIGINT),
	        @Result(column="PRICE_TICKET_UNIT", property="priceTicketUnit", jdbcType=JdbcType.INTEGER),
	        @Result(column="DISCOUNT", property="discount", jdbcType=JdbcType.REAL),
	        @Result(column="TOTAL_PERSON", property="totalPerson", jdbcType=JdbcType.INTEGER),
	        @Result(column="TOTAL_PRICE", property="totalPrice", jdbcType=JdbcType.INTEGER),
	        @Result(column="STATUS", property="status", jdbcType=JdbcType.VARCHAR),
	        @Result(column="QRCODE", property="qrcode", jdbcType=JdbcType.LONGVARCHAR),
	        @Result(column="DATE_ADDED", property="dateAdded", jdbcType=JdbcType.TIMESTAMP),
	        @Result(column="DATE_MODIFIED", property="dateModified", jdbcType=JdbcType.TIMESTAMP),
	        @Result(column="ORDER_DATE", property="orderDate", jdbcType=JdbcType.TIMESTAMP)
	    })
	    List<Order> selectAllwithParam(@Param("sql")String sql);
	 	
	 	
	 	@Select({
	    	"select",
	        "ID_SIT_ORDER, NO_KURSI, SCHEDULE_OT_REF_ID, DATE_ADDED, DATE_MODIFIED, ID_ORDER_REF, ",
	        "STATUS",
	        "from sit_order",
	        "where ID_ORDER_REF = #{id,jdbcType=VARCHAR}",
	        "${sql}",
	        "ORDER BY DATE_MODIFIED DESC",
	        "limit #{limit,jdbcType=VARCHAR}" , "offset #{offset,jdbcType=VARCHAR}"
	    })
	    @Results({
	    	@Result(column="ID_SIT_ORDER", property="idSitOrder", jdbcType=JdbcType.VARCHAR, id=true),
	        @Result(column="NO_KURSI", property="noKursi", jdbcType=JdbcType.INTEGER),
	        @Result(column="SCHEDULE_OT_REF_ID", property="scheduleOtRefId", jdbcType=JdbcType.BIGINT),
	        @Result(column="DATE_ADDED", property="dateAdded", jdbcType=JdbcType.TIMESTAMP),
	        @Result(column="DATE_MODIFIED", property="dateModified", jdbcType=JdbcType.TIMESTAMP),
	        @Result(column="ID_ORDER_REF", property="idOrderRef", jdbcType=JdbcType.VARCHAR),
	        @Result(column="STATUS", property="status", jdbcType=JdbcType.VARCHAR)
	    })
	    List<SitOrder> selectAllSit(
	    		@Param("id")String id,
	    		@Param("limit")Integer limit,
	    		@Param("offset")Integer offset,
	    		@Param("sql")String sql);
		
		@Select({ "select", 
			"count(1)",
			"from sit_order",
			"where ID_ORDER_REF = #{id,jdbcType=VARCHAR}",
			"${sql}" })
		@Result(javaType = java.lang.Integer.class)
		Integer selectCountAllSit(@Param("id")String id, @Param("sql") String sql);
	

}