package com.ibus.dao;

import com.ibus.model.RuteTo;

import java.util.List;

import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Result;
import org.apache.ibatis.annotations.Results;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;
import org.apache.ibatis.type.JdbcType;

public interface RuteToMapper {
    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table rute_to
     *
     * @mbggenerated Thu Aug 04 10:17:21 ICT 2016
     */
    @Delete({
        "delete from rute_to",
        "where ID_RUTE_TO = #{idRuteTo,jdbcType=BIGINT}"
    })
    int deleteByPrimaryKey(Long idRuteTo);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table rute_to
     *
     * @mbggenerated Thu Aug 04 10:17:21 ICT 2016
     */
    @Insert({
        "insert into rute_to (ID_RUTE_TO, ID_RUTE_FROM_REF, ",
        "NAME_RUTE_TO, DATE_ADDED, ",
        "DATE_MODIFIED, STATUS, ",
        "PRICE_TICKET_UNIT, DESC_RUTE_TO)",
        "values (#{idRuteTo,jdbcType=BIGINT}, #{idRuteFromRef,jdbcType=BIGINT}, ",
        "#{nameRuteTo,jdbcType=VARCHAR}, #{dateAdded,jdbcType=TIMESTAMP}, ",
        "#{dateModified,jdbcType=TIMESTAMP}, #{status,jdbcType=VARCHAR}, ",
        "#{priceTicketUnit,jdbcType=INTEGER}, #{descRuteTo,jdbcType=LONGVARCHAR})"
    })
    int insert(RuteTo record);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table rute_to
     *
     * @mbggenerated Thu Aug 04 10:17:21 ICT 2016
     */
    @Select({
        "select",
        "ID_RUTE_TO, ID_RUTE_FROM_REF, NAME_RUTE_TO, DATE_ADDED, DATE_MODIFIED, STATUS, ",
        "PRICE_TICKET_UNIT, DESC_RUTE_TO",
        "from rute_to",
        "where ID_RUTE_TO = #{idRuteTo,jdbcType=BIGINT}"
    })
    @Results({
        @Result(column="ID_RUTE_TO", property="idRuteTo", jdbcType=JdbcType.BIGINT, id=true),
        @Result(column="ID_RUTE_FROM_REF", property="idRuteFromRef", jdbcType=JdbcType.BIGINT),
        @Result(column="NAME_RUTE_TO", property="nameRuteTo", jdbcType=JdbcType.VARCHAR),
        @Result(column="DATE_ADDED", property="dateAdded", jdbcType=JdbcType.TIMESTAMP),
        @Result(column="DATE_MODIFIED", property="dateModified", jdbcType=JdbcType.TIMESTAMP),
        @Result(column="STATUS", property="status", jdbcType=JdbcType.VARCHAR),
        @Result(column="PRICE_TICKET_UNIT", property="priceTicketUnit", jdbcType=JdbcType.INTEGER),
        @Result(column="DESC_RUTE_TO", property="descRuteTo", jdbcType=JdbcType.LONGVARCHAR)
    })
    RuteTo selectByPrimaryKey(Long idRuteTo);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table rute_to
     *
     * @mbggenerated Thu Aug 04 10:17:21 ICT 2016
     */
    @Select({
        "select",
        "ID_RUTE_TO, ID_RUTE_FROM_REF, NAME_RUTE_TO, DATE_ADDED, DATE_MODIFIED, STATUS, ",
        "PRICE_TICKET_UNIT, DESC_RUTE_TO",
        "from rute_to"
    })
    @Results({
        @Result(column="ID_RUTE_TO", property="idRuteTo", jdbcType=JdbcType.BIGINT, id=true),
        @Result(column="ID_RUTE_FROM_REF", property="idRuteFromRef", jdbcType=JdbcType.BIGINT),
        @Result(column="NAME_RUTE_TO", property="nameRuteTo", jdbcType=JdbcType.VARCHAR),
        @Result(column="DATE_ADDED", property="dateAdded", jdbcType=JdbcType.TIMESTAMP),
        @Result(column="DATE_MODIFIED", property="dateModified", jdbcType=JdbcType.TIMESTAMP),
        @Result(column="STATUS", property="status", jdbcType=JdbcType.VARCHAR),
        @Result(column="PRICE_TICKET_UNIT", property="priceTicketUnit", jdbcType=JdbcType.INTEGER),
        @Result(column="DESC_RUTE_TO", property="descRuteTo", jdbcType=JdbcType.LONGVARCHAR)
    })
    List<RuteTo> selectAll();

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table rute_to
     *
     * @mbggenerated Thu Aug 04 10:17:21 ICT 2016
     */
    @Update({
        "update rute_to",
        "set ID_RUTE_FROM_REF = #{idRuteFromRef,jdbcType=BIGINT},",
          "NAME_RUTE_TO = #{nameRuteTo,jdbcType=VARCHAR},",
          "DATE_ADDED = #{dateAdded,jdbcType=TIMESTAMP},",
          "DATE_MODIFIED = #{dateModified,jdbcType=TIMESTAMP},",
          "STATUS = #{status,jdbcType=VARCHAR},",
          "PRICE_TICKET_UNIT = #{priceTicketUnit,jdbcType=INTEGER},",
          "DESC_RUTE_TO = #{descRuteTo,jdbcType=LONGVARCHAR}",
        "where ID_RUTE_TO = #{idRuteTo,jdbcType=BIGINT}"
    })
    int updateByPrimaryKey(RuteTo record);
    
    
    @Select({
        "select",
        "ID_RUTE_TO, ID_RUTE_FROM_REF, NAME_RUTE_TO, DATE_ADDED, DATE_MODIFIED, STATUS, ",
        "PRICE_TICKET_UNIT, DESC_RUTE_TO",
        "from rute_to",
        "${sql}",
        "ORDER BY DATE_MODIFIED DESC",
        "limit #{limit,jdbcType=VARCHAR}" , "offset #{offset,jdbcType=VARCHAR}"
    })
    @Results({
    	@Result(column="ID_RUTE_TO", property="idRuteTo", jdbcType=JdbcType.BIGINT, id=true),
        @Result(column="ID_RUTE_FROM_REF", property="idRuteFromRef", jdbcType=JdbcType.BIGINT),
        @Result(column="NAME_RUTE_TO", property="nameRuteTo", jdbcType=JdbcType.VARCHAR),
        @Result(column="DATE_ADDED", property="dateAdded", jdbcType=JdbcType.TIMESTAMP),
        @Result(column="DATE_MODIFIED", property="dateModified", jdbcType=JdbcType.TIMESTAMP),
        @Result(column="STATUS", property="status", jdbcType=JdbcType.VARCHAR),
        @Result(column="PRICE_TICKET_UNIT", property="priceTicketUnit", jdbcType=JdbcType.INTEGER),
        @Result(column="DESC_RUTE_TO", property="descRuteTo", jdbcType=JdbcType.LONGVARCHAR)
    })
    List<RuteTo> selectAllByPage(
    		@Param("limit")Integer limit,
    		@Param("offset")Integer offset,
    		@Param("sql")String sql);
    

	@Select({
	"select",
	"count(1)",
	"from rute_to",
	"${sql}"
	})
	@Result(javaType=java.lang.Integer.class)
	Integer selectCountAll(@Param("sql")String sql);
	
	@Select({
  		"select",
  		"nextval('sq_rute_to')",
  		})
  	@Result(javaType=java.lang.Integer.class)
  	Integer selectNext();
    
    @Select({
  		"select",
  		"currval('sq_rute_to')",
  		})
  	@Result(javaType=java.lang.Integer.class)
  	Integer selectCurrent();
    
    
    @Delete({
        "delete from rute_to",
        "where ID_RUTE_FROM_REF = #{id,jdbcType=BIGINT}"
    })
    int deleteByRuteFromRefID(Long id);
    
    @Select({
        "select",
        "ID_RUTE_TO, ID_RUTE_FROM_REF, NAME_RUTE_TO, DATE_ADDED, DATE_MODIFIED, STATUS, ",
        "PRICE_TICKET_UNIT, DESC_RUTE_TO",
        "from rute_to",
        "${sql}"
    })
    @Results({
        @Result(column="ID_RUTE_TO", property="idRuteTo", jdbcType=JdbcType.BIGINT, id=true),
        @Result(column="ID_RUTE_FROM_REF", property="idRuteFromRef", jdbcType=JdbcType.BIGINT),
        @Result(column="NAME_RUTE_TO", property="nameRuteTo", jdbcType=JdbcType.VARCHAR),
        @Result(column="DATE_ADDED", property="dateAdded", jdbcType=JdbcType.TIMESTAMP),
        @Result(column="DATE_MODIFIED", property="dateModified", jdbcType=JdbcType.TIMESTAMP),
        @Result(column="STATUS", property="status", jdbcType=JdbcType.VARCHAR),
        @Result(column="PRICE_TICKET_UNIT", property="priceTicketUnit", jdbcType=JdbcType.INTEGER),
        @Result(column="DESC_RUTE_TO", property="descRuteTo", jdbcType=JdbcType.LONGVARCHAR)
    })
    List<RuteTo> selectAllwithParam(@Param("sql")String sql);
}