package com.ibus.service;

import java.util.List;
import java.util.Map;

import co.id.rokubi.exception.InternalServiceException;
import co.id.rokubi.exception.InvalidParamException;
import co.id.rokubi.model.Page;

import com.ibus.controller.bean.SitOrderBean;
import com.ibus.model.ETicket;
import com.ibus.model.Order;
import com.ibus.model.SitOrder;

public interface TransactionManagementService {
	
	List<Order> getAllOrderByParam(Map<String, String> params) throws InvalidParamException, InternalServiceException;
	Order getOrderDetailByID(String id) throws InvalidParamException, InternalServiceException;
	Order saveOrder(Order obj) throws InvalidParamException, InternalServiceException;
	Order updateOrder(Order obj) throws InvalidParamException, InternalServiceException;
	void deleteOrderByID(String id) throws InvalidParamException, InternalServiceException;
	Page findPageOrder(Integer offset, Integer limit,String filter) throws InvalidParamException, InternalServiceException;
	String QRCodeGenerator(Order obj) throws InvalidParamException, InternalServiceException;
	Page findPageSit(String id, Integer offset, Integer limit,String filter);
	
	ETicket getETicketDetailByIDOrder(String id) throws InvalidParamException, InternalServiceException;
	ETicket getETicketDetailByID(String id) throws InvalidParamException, InternalServiceException;
	
	List<SitOrder> getAllSitOrderByParam(Map<String, String> params) throws InvalidParamException, InternalServiceException;
	SitOrder getSitOrderDetailByID(String id) throws InvalidParamException, InternalServiceException;
	SitOrder saveSitOrder(SitOrder obj) throws InvalidParamException, InternalServiceException;
	SitOrder updateSitOrder(SitOrder obj) throws InvalidParamException, InternalServiceException;
	void deleteSitOrderByID(String id) throws InvalidParamException, InternalServiceException;
	List<SitOrderBean> findPageSitOrder(String orderId) throws InvalidParamException, InternalServiceException;
	
	Long checkSitOrder(String orderId) throws InvalidParamException, InternalServiceException;

}
