package com.ibus.service;

public interface IService {
	public void logTrace(Object message);
	public void logTrace(Object message, Throwable t);
	public void logDebug(Object message);
	public void logDebug(Object message, Throwable t);
	public void logInfo(Object message);
	public void logInfo(Object message, Throwable t);
	public void logWarn(Object message);
	public void logWarn(Object message, Throwable t);
	public void logError(Object message);
	public void logError(Object message, Throwable t);
	public void logFatal(Object message);
	public void logFatal(Object message, Throwable t);
}
