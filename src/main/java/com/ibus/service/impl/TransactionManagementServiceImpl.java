package com.ibus.service.impl;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.UUID;

import net.glxn.qrgen.QRCode;
import net.glxn.qrgen.image.ImageType;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.ibus.constant.StatusBus;
import com.ibus.controller.bean.SitOrderBean;
import com.ibus.dao.BusMapper;
import com.ibus.dao.ETicketMapper;
import com.ibus.dao.OrderMapper;
import com.ibus.dao.RuteFromMapper;
import com.ibus.dao.RuteToMapper;
import com.ibus.dao.ScheduleBusMapper;
import com.ibus.dao.ScheduleOnTimeMapper;
import com.ibus.dao.SitOrderMapper;
import com.ibus.model.Bus;
import com.ibus.model.ETicket;
import com.ibus.model.Order;
import com.ibus.model.ScheduleOnTime;
import com.ibus.model.SitOrder;
import com.ibus.service.TransactionManagementService;

import co.id.rokubi.dao.UserMapper;
import co.id.rokubi.exception.InternalServiceException;
import co.id.rokubi.exception.InvalidParamException;
import co.id.rokubi.model.Page;
import co.id.rokubi.service.AppManagementService;
import co.id.rokubi.service.impl.AbstractService;

@Service
public class TransactionManagementServiceImpl extends AbstractService implements
		TransactionManagementService {

	@Autowired
	private OrderMapper orderMapper;
	

	@Autowired
	private ETicketMapper eTicketMapper;

	@Autowired
	private AppManagementService appManagementService;

	@Override
	public String getServiceName() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<Order> getAllOrderByParam(Map<String, String> params)
			throws InvalidParamException, InternalServiceException {
		// TODO Auto-generated method stub
		List<Order> results = new ArrayList<Order>();
		String sql = "";
		try {

			if (params.size() > 0) {
				sql += "where ";
				int counter = 0;
				for (Map.Entry<String, String> entry : params.entrySet()) {

					// SUBMITTED_BY = '"+getCurrentUser()+"'
					sql += entry.getKey() + " = '" + entry.getValue() + "' ";
					counter++;

					if (counter == params.size()) {

					} else {
						sql += " AND ";
					}
				}
				System.out.println("sql command:" + sql);
			}
			
			results = orderMapper.selectAllwithParam(sql);

		} catch (Exception ex) {
			logError("error:", ex);
			InternalServiceException internalException = new InternalServiceException(
					appManagementService.getMessage("error.service"));
			internalException.getMessageObj().setDescription(ex.getMessage());
			throw internalException;
		}

		return results;
	}

	@Autowired
	private UserMapper userMapper;
	@Autowired
	private ScheduleBusMapper scheduleMapper;
	@Autowired
	private RuteFromMapper ruteFromMapper;
	@Autowired
	private RuteToMapper ruteToMapper;

	@Override
	public Order getOrderDetailByID(String id) throws InvalidParamException,
			InternalServiceException {
		// TODO Auto-generated method stub
		Order result = new Order();
		try {
			result = orderMapper.selectByPrimaryKey(id);
			result.setUserRef(userMapper.selectByPrimaryKey(result.getUserId()));
			result.setSchedule(scheduleMapper.selectByPrimaryKey(result
					.getScheduleId()));
			result.setRuteFrom(ruteFromMapper.selectByPrimaryKey(result
					.getIdRuteFrom()));
			result.setRuteTo(ruteToMapper.selectByPrimaryKey(result
					.getIdRuteTo()));

		} catch (Exception ex) {
			logError("error:", ex);
			InternalServiceException internalException = new InternalServiceException(
					appManagementService.getMessage("error.service"));
			internalException.getMessageObj().setDescription(ex.getMessage());
			throw internalException;
		}

		return result;
	}

	private String getRandomOrder(int length) {
		final String charset = "0123456789abcdefghijklmnopqrstuvwxyz";

		Random rand = new Random(System.currentTimeMillis());
		StringBuilder sb = new StringBuilder();
		for (int i = 0; i < length; i++) {
			int pos = rand.nextInt(charset.length());
			sb.append(charset.charAt(pos));
		}
		return sb.toString();
	}

	@Override
	public Order saveOrder(Order obj) throws InvalidParamException,
			InternalServiceException {
		// TODO Auto-generated method stub
		try {

			obj.setIdOrder(getRandomOrder(8));

			obj.setDateAdded(new Date());
			obj.setDateModified(new Date());
			if(obj.getStatus().equalsIgnoreCase("2"))
			{
				String idQRCode = QRCodeGenerator(obj);
				obj.setQrcode(idQRCode);
			}
			orderMapper.insert(obj);

		} catch (Exception ex) {
			logError("error:", ex);
			InternalServiceException internalException = new InternalServiceException(
					appManagementService.getMessage("error.service"));
			internalException.getMessageObj().setDescription(ex.getMessage());
			throw internalException;
		}

		return obj;
	}

	@Override
	public Order updateOrder(Order obj) throws InvalidParamException,
			InternalServiceException {
		// TODO Auto-generated method stub
		try {

			obj.setDateModified(new Date());
			orderMapper.updateByPrimaryKey(obj);

		} catch (Exception ex) {
			logError("error:", ex);
			InternalServiceException internalException = new InternalServiceException(
					appManagementService.getMessage("error.service"));
			internalException.getMessageObj().setDescription(ex.getMessage());
			throw internalException;
		}

		return obj;
	}

	@Override
	public void deleteOrderByID(String id) throws InvalidParamException,
			InternalServiceException {
		// TODO Auto-generated method stub
		try {

			orderMapper.deleteByPrimaryKey(id);

		} catch (Exception ex) {
			logError("error:", ex);
			InternalServiceException internalException = new InternalServiceException(
					appManagementService.getMessage("error.service"));
			internalException.getMessageObj().setDescription(ex.getMessage());
			throw internalException;
		}

	}

	@Override
	public Page findPageOrder(Integer offset, Integer limit, String filter)
			throws InvalidParamException, InternalServiceException {
		// TODO Auto-generated method stub
		Page page = new Page();

		String sql = "";
		// "where SUBMITTED_BY = '"+getCurrentUser()+"' AND CODE_USED = '"+CodeUsed.PAJAK_KELUARAN.getValue()+"' AND ID_USE_FAKTUR like '%"+filter+"%' ";

		page.setLimit(limit);
		page.setCurrentPage(Page.calculatePage(offset, limit));
		List<Order> results = orderMapper.selectAllByPage(limit, offset, sql);
		for (Order order : results) {
			order.setUserRef(userMapper.selectByPrimaryKey(order.getUserId()));
			order.setSchedule(scheduleMapper.selectByPrimaryKey(order
					.getScheduleId()));
			order.setRuteFrom(ruteFromMapper.selectByPrimaryKey(order
					.getIdRuteFrom()));
			order.setRuteTo(ruteToMapper.selectByPrimaryKey(order.getIdRuteTo()));
		}
		page.setObj(results);
		page.setTotalRow(orderMapper.selectCountAll(sql));

		return page;
	}

	@Autowired
	private SitOrderMapper sitOrderMapper;

	@Override
	public List<SitOrder> getAllSitOrderByParam(Map<String, String> params)
			throws InvalidParamException, InternalServiceException {
		// TODO Auto-generated method stub
		List<SitOrder> results = new ArrayList<SitOrder>();
		String sql = "";
		try {

			if (params.size() > 0) {
				sql += "where ";
				int counter = 0;
				for (Map.Entry<String, String> entry : params.entrySet()) {

					// SUBMITTED_BY = '"+getCurrentUser()+"'
					sql += entry.getKey() + " = '" + entry.getValue() + "' ";
					counter++;

					if (counter == params.size()) {

					} else {
						sql += " AND ";
					}
				}
				System.out.println("sql command:" + sql);
			}

			results = sitOrderMapper.selectAllwithParams(sql);

		} catch (Exception ex) {
			logError("error:", ex);
			InternalServiceException internalException = new InternalServiceException(
					appManagementService.getMessage("error.service"));
			internalException.getMessageObj().setDescription(ex.getMessage());
			throw internalException;
		}

		return results;
	}

	@Override
	public SitOrder getSitOrderDetailByID(String id)
			throws InvalidParamException, InternalServiceException {
		// TODO Auto-generated method stub
		SitOrder result = new SitOrder();
		try {
			result = sitOrderMapper.selectByPrimaryKey(id);

		} catch (Exception ex) {
			logError("error:", ex);
			InternalServiceException internalException = new InternalServiceException(
					appManagementService.getMessage("error.service"));
			internalException.getMessageObj().setDescription(ex.getMessage());
			throw internalException;
		}

		return result;
	}

	@Override
	public SitOrder saveSitOrder(SitOrder obj) throws InvalidParamException,
			InternalServiceException {
		// TODO Auto-generated method stub
		try {
			obj.setDateAdded(new Date());
			obj.setDateModified(new Date());
			sitOrderMapper.insert(obj);

		} catch (Exception ex) {
			logError("error:", ex);
			InternalServiceException internalException = new InternalServiceException(
					appManagementService.getMessage("error.service"));
			internalException.getMessageObj().setDescription(ex.getMessage());
			throw internalException;
		}

		return obj;
	}

	@Override
	public SitOrder updateSitOrder(SitOrder obj) throws InvalidParamException,
			InternalServiceException {
		// TODO Auto-generated method stub
		try {

			obj.setDateAdded(new Date());
			obj.setDateModified(new Date());
			sitOrderMapper.updateByPrimaryKey(obj);

		} catch (Exception ex) {
			logError("error:", ex);
			InternalServiceException internalException = new InternalServiceException(
					appManagementService.getMessage("error.service"));
			internalException.getMessageObj().setDescription(ex.getMessage());
			throw internalException;
		}

		return obj;
	}

	@Override
	public void deleteSitOrderByID(String id) throws InvalidParamException,
			InternalServiceException {
		// TODO Auto-generated method stub
		try {

			sitOrderMapper.deleteByPrimaryKey(id);

		} catch (Exception ex) {
			logError("error:", ex);
			InternalServiceException internalException = new InternalServiceException(
					appManagementService.getMessage("error.service"));
			internalException.getMessageObj().setDescription(ex.getMessage());
			throw internalException;
		}
	}

	@Autowired
	private ScheduleOnTimeMapper scheduleOnTimeMapper;

	@Autowired
	private BusMapper busMapper;

	@Override
	public List<SitOrderBean> findPageSitOrder(String orderId)
			throws InvalidParamException, InternalServiceException {
		// TODO Auto-generated method stub
		List<SitOrderBean> listSitOrderBean = new ArrayList<SitOrderBean>();
		List<SitOrder> list_sit_order = new ArrayList<SitOrder>();
		List<ScheduleOnTime> scheduleOT = new ArrayList<ScheduleOnTime>();
		DateFormat df = new SimpleDateFormat("yyyy/MM/dd");
		SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss a");
        //String formattedDate = formatter.format(value);
		int jumlah_kursi = 50; // default

		Order orderDetails = orderMapper.selectByPrimaryKey(orderId);
		Map<String, String> params = new HashMap<>();

		// get jumlah kursi dari jadwal yang ada
		params.put("ID_SCHEDULE", orderDetails.getScheduleId().toString());
		params.put("RUTE_FROM_ID_REF", orderDetails.getIdRuteFrom().toString());
		params.put("RUTE_TO_ID_REF", orderDetails.getIdRuteTo().toString());
		params.put("ACTUAL_DATE", df.format(orderDetails.getOrderDate()));

		String sql = "";
		if (params.size() > 0) {
			sql += "where ";
			int counter = 0;
			for (Map.Entry<String, String> entry : params.entrySet()) {

				sql += entry.getKey() + " = '" + entry.getValue() + "' ";
				counter++;

				if (counter == params.size()) {

				} else {
					sql += " AND ";
				}
			}
			System.out.println("sql command:" + sql);
		}

		scheduleOT = scheduleOnTimeMapper.selectAllByParam(sql);
		if (scheduleOT.size() > 0) {
			Long scheduleOTRefId = scheduleOT.get(0).getIdOnTime();
			Bus busDetails = busMapper.selectByPrimaryKey(scheduleOT.get(0)
					.getIdBus());
			jumlah_kursi = busDetails.getCountOfChair();
			params = new HashMap<>();

			// get jumlah kursi dari jadwal yang ada
			params.put("ID_SCHEDULE", orderDetails.getScheduleId().toString());
			params.put("ID_RUTE_FROM", orderDetails.getIdRuteFrom().toString());
			params.put("ID_RUTE_TO", orderDetails.getIdRuteTo().toString());
			params.put("ORDER_DATE", df.format(orderDetails.getOrderDate()));
			
			sql = "";
			if (params.size() > 0) {
				sql += "where ";
				int counter = 0;
				for (Map.Entry<String, String> entry : params.entrySet()) {

					sql += entry.getKey() + " = '" + entry.getValue() + "' ";
					counter++;

					if (counter == params.size()) {

					} else {
						sql += " AND ";
					}
				}
				System.out.println("sql command:" + sql);
			}
			
			list_sit_order = sitOrderMapper.selectAllwithParams(sql);
			
			for (int counter = 1; counter <= jumlah_kursi; counter++) {
				SitOrderBean sit_ = new SitOrderBean();
				boolean found = false;
				
				for (SitOrder sitOrder_ : list_sit_order) {
					if(sitOrder_.getNoKursi().equals(counter))
					{
						found = true;
						sit_.setIdSitOrder(sitOrder_.getIdSitOrder());
						sit_.setNoKursi(sitOrder_.getNoKursi());
						sit_.setLastUpdate(sitOrder_.getDateModified());
						sit_.setStatus(sitOrder_.getStatus());
						sit_.setDateStringLastUpdate(formatter.format(sit_.getLastUpdate()));
						break;
					}
				}
				
				if(!found)
				{
					sit_.setIdSitOrder(orderDetails.getScheduleId().toString()+"-"
							+orderDetails.getIdRuteFrom().toString()+"-"
							+orderDetails.getIdRuteTo().toString()+"-"
							+df.format(orderDetails.getOrderDate())+"-"
							+counter);
					sit_.setNoKursi(counter);
					sit_.setLastUpdate(new Date());
					sit_.setStatus(StatusBus.AVAILABLE.getValue());
					sit_.setDateStringLastUpdate(formatter.format(sit_.getLastUpdate()));
				}
				
				
				sit_.setScheduleOT(scheduleOTRefId);
				listSitOrderBean.add(sit_);
				
				
			}
		}
		else
		{
			
			params = new HashMap<>();

			
			params.put("ID_SCHEDULE", orderDetails.getScheduleId().toString());
			params.put("ID_RUTE_FROM", orderDetails.getIdRuteFrom().toString());
			params.put("ID_RUTE_TO", orderDetails.getIdRuteTo().toString());
			params.put("ORDER_DATE", df.format(orderDetails.getOrderDate()));
			
			sql = "";
			if (params.size() > 0) {
				sql += "where ";
				int counter = 0;
				for (Map.Entry<String, String> entry : params.entrySet()) {

					sql += entry.getKey() + " = '" + entry.getValue() + "' ";
					counter++;

					if (counter == params.size()) {

					} else {
						sql += " AND ";
					}
				}
				System.out.println("sql command:" + sql);
			}
			
			list_sit_order = sitOrderMapper.selectAllwithParams(sql);
			
			
			for (int counter = 1; counter <= jumlah_kursi; counter++) {
				SitOrderBean sit_ = new SitOrderBean();
				boolean found = false;
				
				for (SitOrder sitOrder_ : list_sit_order) {
					if(sitOrder_.getNoKursi().equals(counter))
					{
						found = true;
						sit_.setIdSitOrder(sitOrder_.getIdSitOrder());
						sit_.setNoKursi(sitOrder_.getNoKursi());
						sit_.setLastUpdate(sitOrder_.getDateModified());
						sit_.setStatus(sitOrder_.getStatus());
						sit_.setDateStringLastUpdate(formatter.format(sit_.getLastUpdate()));
						break;
					}
				}
				
				if(!found)
				{
					sit_.setIdSitOrder(orderDetails.getScheduleId().toString()+"-"
							+orderDetails.getIdRuteFrom().toString()+"-"
							+orderDetails.getIdRuteTo().toString()+"-"
							+df.format(orderDetails.getOrderDate())+"-"
							+counter);
					sit_.setNoKursi(counter);
					sit_.setLastUpdate(new Date());
					sit_.setStatus(StatusBus.AVAILABLE.getValue());
					sit_.setDateStringLastUpdate(formatter.format(sit_.getLastUpdate()));
				}
				
				sit_.setScheduleOT(Long.valueOf(0));
				listSitOrderBean.add(sit_);
				
				
			}
		}

		return listSitOrderBean;

	}

	@Override
	public Long checkSitOrder(String orderId) throws InvalidParamException,
			InternalServiceException {
		// TODO Auto-generated method stub
		DateFormat df = new SimpleDateFormat("yyyy/MM/dd");
		Order orderDetails = orderMapper.selectByPrimaryKey(orderId);
		Map<String, String> params = new HashMap<>();
		List<ScheduleOnTime> scheduleOT = new ArrayList<ScheduleOnTime>();
		// get jumlah kursi dari jadwal yang ada
		params.put("ID_SCHEDULE", orderDetails.getScheduleId().toString());
		params.put("RUTE_FROM_ID_REF", orderDetails.getIdRuteFrom().toString());
		params.put("RUTE_TO_ID_REF", orderDetails.getIdRuteTo().toString());
		params.put("ACTUAL_DATE", df.format(orderDetails.getOrderDate()));

		String sql = "";
		if (params.size() > 0) {
			sql += "where ";
			int counter = 0;
			for (Map.Entry<String, String> entry : params.entrySet()) {

				sql += entry.getKey() + " = '" + entry.getValue() + "' ";
				counter++;

				if (counter == params.size()) {

				} else {
					sql += " AND ";
				}
			}
			System.out.println("sql command:" + sql);
		}

		scheduleOT = scheduleOnTimeMapper.selectAllByParam(sql);
		if (scheduleOT.size() > 0) {
			return scheduleOT.get(0).getIdOnTime();
		}
		else
		{
			return Long.valueOf(0);
		}
	}
	
	@Value("${user.pic.dir}")
	private String userPicDir;
	
	@Override
	public String QRCodeGenerator(Order obj) throws InvalidParamException,
			InternalServiceException {
		// TODO Auto-generated method stub
		
		String FILE_PIC_NAME = "image_QR_Code" + "_"
				+ getRandomFile(10);
		String dir = userPicDir + File.separatorChar
				+ "image_QR_Code" + File.separatorChar;
		String suffix = ".png";
		String filePath = dir + FILE_PIC_NAME + suffix;
		UUID uuid = UUID.randomUUID();
		String string = uuid.toString();
		String[] parts = string.split("-");
		String id = parts[0] + parts[1] + parts[2] + parts[3] + parts[4];
		String myCodeText = id.toString();
		
		ETicket eTicket = new ETicket();
		
		eTicket.setId(id);
		eTicket.setDateAdded(new Date());
		eTicket.setDateModified(new Date());
		eTicket.setIdOrder(obj.getIdOrder());
		eTicket.setPath(FILE_PIC_NAME+suffix);
		
		ByteArrayOutputStream out = QRCode.from(id).to(ImageType.PNG).stream();
		
		try {
		    FileOutputStream fout = new FileOutputStream(new File(filePath));

		    fout.write(out.toByteArray());

		    fout.flush();
		    fout.close();

		} catch (FileNotFoundException e) {
		    // Do Logging
		} catch (IOException e) {
		    // Do Logging
		} 
		System.out.println("\n\nYou have successfully created QR Code.");
		eTicketMapper.insert(eTicket);
		return myCodeText;
	}

	private String getRandomFile(int length) {
		final String charset = "0123456789abcdefghijklmnopqrstuvwxyz";

		Random rand = new Random(System.currentTimeMillis());
		StringBuilder sb = new StringBuilder();
		for (int i = 0; i < length; i++) {
			int pos = rand.nextInt(charset.length());
			sb.append(charset.charAt(pos));
		}
		return sb.toString();
	}
	
	@Override
	public Page findPageSit(String id, Integer offset, Integer limit, String filter) {
		// TODO Auto-generated method stub
		Page page = new Page();

		String sql = "";
		// "where SUBMITTED_BY = '"+getCurrentUser()+"' AND CODE_USED = '"+CodeUsed.PAJAK_KELUARAN.getValue()+"' AND ID_USE_FAKTUR like '%"+filter+"%' ";

		page.setLimit(limit);
		page.setCurrentPage(Page.calculatePage(offset, limit));
		page.setObj(orderMapper.selectAllSit(id, limit, offset, sql));
		page.setTotalRow(orderMapper.selectCountAllSit(id, sql));
		// System.out.println("total row:"+page.getTotalRow());
		return page;
	}

	@Override
	public ETicket getETicketDetailByIDOrder(String id) throws InvalidParamException,
			InternalServiceException {
		// TODO Auto-generated method stub
		ETicket result = new ETicket();
		try {
			result = eTicketMapper.selectByIDOrder(id);

		} catch (Exception ex) {
			logError("error:", ex);
			InternalServiceException internalException = new InternalServiceException(
					appManagementService.getMessage("error.service"));
			internalException.getMessageObj().setDescription(ex.getMessage());
			throw internalException;
		}

		return result;
	}

	@Override
	public ETicket getETicketDetailByID(String id)
			throws InvalidParamException, InternalServiceException {
		// TODO Auto-generated method stub
		ETicket result = new ETicket();
		try {
			result = eTicketMapper.selectByID(id);

		} catch (Exception ex) {
			logError("error:", ex);
			InternalServiceException internalException = new InternalServiceException(
					appManagementService.getMessage("error.service"));
			internalException.getMessageObj().setDescription(ex.getMessage());
			throw internalException;
		}

		return result;
	}

}
