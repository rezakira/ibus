package com.ibus.service.impl;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import co.id.rokubi.exception.InternalServiceException;
import co.id.rokubi.exception.InvalidParamException;
import co.id.rokubi.model.Page;
import co.id.rokubi.service.AppManagementService;
import co.id.rokubi.service.impl.AbstractService;
import co.id.rokubi.util.CommonUtil;

import com.ibus.dao.BeritaPromoMapper;
import com.ibus.dao.PromoBerlakuMapper;
import com.ibus.model.BeritaPromo;
import com.ibus.model.PromoBerlaku;
import com.ibus.service.BeritaPromoManagementService;

@Service
public class BeritaPromoManagementServiceImpl extends AbstractService implements
		BeritaPromoManagementService {
	
	@Autowired
	BeritaPromoMapper beritaPromoMapper;
	@Autowired
	PromoBerlakuMapper promoBerlakuMapper;
	@Autowired
	AppManagementService appManagementService;
	
	@Override
	public String getServiceName() {
		// TODO Auto-generated method stub
		return null;
	}

	
	@Override
	public List<BeritaPromo> getAllBeritaPromoByParam(Map<String, String> params)
			throws InvalidParamException, InternalServiceException {
		// TODO Auto-generated method stub
		List<BeritaPromo> results = new ArrayList<BeritaPromo>();
		String sql = "";
		try {

			if (params.size() > 0) {
				sql += "where ";
				int counter = 0;
				for (Map.Entry<String, String> entry : params.entrySet()) {

					// SUBMITTED_BY = '"+getCurrentUser()+"'
					sql += entry.getKey() + " = '" + entry.getValue() + "' ";
					counter++;

					if (counter == params.size()) {

					} else {
						sql += " AND ";
					}
				}
				System.out.println("sql command:" + sql);
			}
			
			results = beritaPromoMapper.selectAllwithParam(sql);
			for (BeritaPromo beritaPromo : results) {
				beritaPromo.setPathImageUrl(
						(CommonUtil.isNotNullOrEmpty(beritaPromo.getImageUrl())?pathImageBeritaPromo+beritaPromo.getImageUrl():
							beritaPromo.getImageUrl()));
				
			}

		} catch (Exception ex) {
			logError("error:", ex);

			InternalServiceException internalException = new InternalServiceException(
					appManagementService.getMessage("error.service"));
			internalException.getMessageObj().setDescription(ex.getMessage());
			throw internalException;
		}

		return results;
	}

	
	@Value("${berita.pic.path}")
	private String pathImageBeritaPromo;
	
	
	@Override
	public BeritaPromo getBeritaPromoDetailByID(Long id)
			throws InvalidParamException, InternalServiceException {
		// TODO Auto-generated method stub
		BeritaPromo result = new BeritaPromo();
		try {
			result = beritaPromoMapper.selectByPrimaryKey(id);
			result.setPathImageUrl(
					(CommonUtil.isNotNullOrEmpty(result.getImageUrl())?pathImageBeritaPromo+result.getImageUrl():
						result.getImageUrl()));


		} catch (Exception ex) {
			logError("error:", ex);
			InternalServiceException internalException = new InternalServiceException(
					appManagementService.getMessage("error.service"));
			internalException.getMessageObj().setDescription(ex.getMessage());
			throw internalException;
		}

		return result;
	}

	@Override
	public BeritaPromo saveBeritaPromo(BeritaPromo obj)
			throws InvalidParamException, InternalServiceException {
		// TODO Auto-generated method stub
		try {
				if(checkIsDateWithinRange(obj.getTimeStart(),obj.getTimeEnd(),obj.getContentType()) && obj.getContentType().equalsIgnoreCase("1"))
				{
					//promo is exist
					
				}
				else
				{
					//promo not exist
					
					obj.setIdBeritaPromo(beritaPromoMapper.selectNext().longValue());
					obj.setStatusPilihanPromo("0");
					obj.setDateAdded(new Date());
					obj.setDateModified(new Date());				
					beritaPromoMapper.insert(obj);
				}
		} catch (Exception ex) {
			logError("error:", ex);
			InternalServiceException internalException = new InternalServiceException(
					appManagementService.getMessage("error.service"));
			internalException.getMessageObj().setDescription(ex.getMessage());
			throw internalException;
		}

		return obj;
	}

	@Override
	public BeritaPromo updateBeritaPromo(BeritaPromo obj)
			throws InvalidParamException, InternalServiceException {
		// TODO Auto-generated method stub
		try {		
			obj.setDateModified(new Date());
			beritaPromoMapper.updateByPrimaryKey(obj);		
		} catch (Exception ex) {
			logError("error:", ex);
			InternalServiceException internalException = new InternalServiceException(
					appManagementService.getMessage("error.service"));
			internalException.getMessageObj().setDescription(ex.getMessage());
			throw internalException;
		}
		
		return obj;
	}

	@Override
	public void deleteBeritaPromoByID(Long id_long) throws InvalidParamException,
			InternalServiceException {
		// TODO Auto-generated method stub
		try {
			
			beritaPromoMapper.deleteByPrimaryKey(id_long);
			
		} catch (Exception ex) {
			logError("error:", ex);
			InternalServiceException internalException = new InternalServiceException(
					appManagementService.getMessage("error.service"));
			internalException.getMessageObj().setDescription(ex.getMessage());
			throw internalException;
		}
	}

	@Override
	public Page findPageBeritaPromo(Integer offset, Integer limit, String filter) {
		// TODO Auto-generated method stub
		Page page = new Page();
		
		String sql = "";
				//"where SUBMITTED_BY = '"+getCurrentUser()+"' AND CODE_USED = '"+CodeUsed.PAJAK_KELUARAN.getValue()+"' AND ID_USE_FAKTUR like '%"+filter+"%' ";
		
		page.setLimit(limit);
		page.setCurrentPage(Page.calculatePage(offset, limit));
		page.setObj(beritaPromoMapper.selectAllByPage(limit, offset,sql));
		page.setTotalRow(beritaPromoMapper.selectCountAll(sql));
		//System.out.println("total row:"+page.getTotalRow());
		return page;
	}


	@Override
	public boolean checkIsDateWithinRange(Date timeStart, Date timeEnd, String contentType)
			throws InvalidParamException, InternalServiceException {
		// TODO Auto-generated method stub
		List<BeritaPromo> result = beritaPromoMapper.selectAll();
		int countExist = 0;
		int countExistEndGreater = 0;
		for (BeritaPromo beritaPromo : result) {
			Date timeStartInDB = beritaPromo.getTimeStart();
			Date timeEndInDB = beritaPromo.getTimeEnd();
			
			String contentTypeInDB = beritaPromo.getContentType();
			
			Calendar calendarTimeStart = Calendar.getInstance();
			Calendar calendarTimeEnd = Calendar.getInstance();
			calendarTimeStart.setTime(timeStart);
			calendarTimeEnd.setTime(timeEnd);
			
			Calendar calendarTimeStartInDB = Calendar.getInstance();
			Calendar calendarTimeEndInDB = Calendar.getInstance();
			calendarTimeStartInDB.setTime(timeStartInDB);
			calendarTimeEndInDB.setTime(timeEndInDB);
			
			if(timeStart.after(timeStartInDB) && timeStart.before(timeEndInDB) && contentType.equalsIgnoreCase("1") && contentTypeInDB.equalsIgnoreCase("1") )
			{
				countExist = countExist + 1;
			}
			else if(timeEnd.after(timeStartInDB) && timeEnd.before(timeEndInDB) && contentType.equalsIgnoreCase("1") && contentTypeInDB.equalsIgnoreCase("1"))
			{
				countExist = countExist + 1;
			}
			else if(timeStart.equals(timeStartInDB) && contentType.equalsIgnoreCase("1") && contentTypeInDB.equalsIgnoreCase("1"))
			{
				countExist = countExist + 1;
			}
			else if(timeStart.equals(timeEndInDB) && contentType.equalsIgnoreCase("1") && contentTypeInDB.equalsIgnoreCase("1"))
			{
				countExist = countExist + 1;
			}
			else if(timeEnd.equals(timeStartInDB) && contentType.equalsIgnoreCase("1") && contentTypeInDB.equalsIgnoreCase("1"))
			{
				countExist = countExist + 1;
			}
			else if(timeEnd.equals(timeEndInDB) && contentType.equalsIgnoreCase("1") && contentTypeInDB.equalsIgnoreCase("1"))
			{
				countExist = countExist + 1;
			}
			else if(calendarTimeStart.getTimeInMillis() >= calendarTimeStartInDB.getTimeInMillis() && calendarTimeEnd.getTimeInMillis() <= calendarTimeEndInDB.getTimeInMillis())
			{
				countExist = countExist + 1;
			}
			else if(calendarTimeStart.getTimeInMillis() <= calendarTimeStartInDB.getTimeInMillis() && calendarTimeEnd.getTimeInMillis() >= calendarTimeEndInDB.getTimeInMillis())
			{
				countExistEndGreater = countExistEndGreater + 1;
			}
		}
		System.out.println("ini countexist promo");
		System.out.println(countExist);
		System.out.println("ini countexistendgreater promo");
		System.out.println(countExistEndGreater);
		if(countExist > 0)
		{
			return true;
		}
		else if(countExistEndGreater > 0)
		{
			return true;
		}
		else
		{
			return false;
		}
	}


	@Override
	public List<PromoBerlaku> getAllPromoBerlakuByParam(String param)
			throws InvalidParamException, InternalServiceException {
		// TODO Auto-generated method stub
		List<PromoBerlaku> results = new ArrayList<PromoBerlaku>();
		try {

			results = promoBerlakuMapper.selectAll();

		} catch (Exception ex) {
			logError("error:", ex);

			InternalServiceException internalException = new InternalServiceException(
					appManagementService.getMessage("error.service"));
			internalException.getMessageObj().setDescription(ex.getMessage());
			throw internalException;
		}

		return results;
	}


	@Override
	public PromoBerlaku getPromoBerlakuDetailByID(Long id)
			throws InvalidParamException, InternalServiceException {
		// TODO Auto-generated method stub
		PromoBerlaku result = new PromoBerlaku();
		try {
			result = promoBerlakuMapper.selectByPrimaryKey(id);

		} catch (Exception ex) {
			logError("error:", ex);
			InternalServiceException internalException = new InternalServiceException(
					appManagementService.getMessage("error.service"));
			internalException.getMessageObj().setDescription(ex.getMessage());
			throw internalException;
		}

		return result;
	}


	@Override
	public PromoBerlaku savePromoBerlaku(PromoBerlaku obj)
			throws InvalidParamException, InternalServiceException {
		// TODO Auto-generated method stub
		try {

			obj.setIdPromoBerlaku(promoBerlakuMapper.selectNext().longValue());
			obj.setDateAdded(new Date());
			obj.setDateModified(new Date());

			promoBerlakuMapper.insert(obj);
			promoBerlakuMapper.updateStatusPilihanPromoByPrimaryKey(obj);

		} catch (Exception ex) {
			logError("error:", ex);
			InternalServiceException internalException = new InternalServiceException(
					appManagementService.getMessage("error.service"));
			internalException.getMessageObj().setDescription(ex.getMessage());
			throw internalException;
		}

		return obj;
	}


	@Override
	public void deletePromoBerlakuByID(Long id) throws InvalidParamException,
			InternalServiceException {
		// TODO Auto-generated method stub
		try {
			
			promoBerlakuMapper.deleteByPrimaryKey(id);
			
		} catch (Exception ex) {
			logError("error:", ex);
			InternalServiceException internalException = new InternalServiceException(
					appManagementService.getMessage("error.service"));
			internalException.getMessageObj().setDescription(ex.getMessage());
			throw internalException;
		}
	}


	@Override
	public Page findPagePromoBerlaku(Integer offset, Integer limit,
			String filter) {
		// TODO Auto-generated method stub
		Page page = new Page();
		
		String sql = "";
				//"where SUBMITTED_BY = '"+getCurrentUser()+"' AND CODE_USED = '"+CodeUsed.PAJAK_KELUARAN.getValue()+"' AND ID_USE_FAKTUR like '%"+filter+"%' ";
		
		page.setLimit(limit);
		page.setCurrentPage(Page.calculatePage(offset, limit));
		page.setObj(promoBerlakuMapper.selectAllByPage(limit, offset,sql));
		page.setTotalRow(promoBerlakuMapper.selectCountAll(sql));
		//System.out.println("total row:"+page.getTotalRow());
		return page;
	}
}
