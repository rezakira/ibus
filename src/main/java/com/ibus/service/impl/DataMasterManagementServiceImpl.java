package com.ibus.service.impl;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ibus.constant.StatusBus;
import com.ibus.controller.bean.SitOrderBean;
import com.ibus.dao.BusMapper;
import com.ibus.dao.OrderMapper;
import com.ibus.dao.RuteFromMapper;
import com.ibus.dao.RuteToMapper;
import com.ibus.dao.ScheduleBusMapper;
import com.ibus.dao.ScheduleOnTimeMapper;
import com.ibus.dao.SitOrderMapper;
import com.ibus.dao.ViewSitOrderMapper;
import com.ibus.model.Bus;
import com.ibus.model.RuteFrom;
import com.ibus.model.RuteTo;
import com.ibus.model.ScheduleBus;
import com.ibus.model.ScheduleOnTime;
import com.ibus.model.SitOrder;
import com.ibus.model.ViewSitOrder;
import com.ibus.service.DataMasterManagementService;

import co.id.rokubi.exception.InternalServiceException;
import co.id.rokubi.exception.InvalidParamException;
import co.id.rokubi.model.Page;
import co.id.rokubi.service.AppManagementService;
import co.id.rokubi.service.impl.AbstractService;
import co.id.rokubi.util.DateUtil;

@Service
public class DataMasterManagementServiceImpl extends AbstractService implements
		DataMasterManagementService {

	@Autowired
	private BusMapper busMapper;

	@Autowired
	private OrderMapper orderMapper;

	@Autowired
	private SitOrderMapper sitOrderMapper;
	
	@Autowired
	private ViewSitOrderMapper viewSitOrderMapper;
	
	@Autowired
	private RuteFromMapper ruteFromMapper;

	@Autowired
	private RuteToMapper ruteToMapper;

	@Autowired
	private ScheduleBusMapper scheduleBusMapper;

	@Autowired
	private ScheduleOnTimeMapper scheduleOnTimeMapper;

	@Autowired
	private AppManagementService appManagementService;

	@Override
	public String getServiceName() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<Bus> getAllBusByParam(String param)
			throws InvalidParamException, InternalServiceException {
		// TODO Auto-generated method stub
		List<Bus> results = new ArrayList<Bus>();
		try {
			results = busMapper.selectAll();

		} catch (Exception ex) {
			logError("error:", ex);
			InternalServiceException internalException = new InternalServiceException(
					appManagementService.getMessage("error.service"));
			internalException.getMessageObj().setDescription(ex.getMessage());
			throw internalException;
		}

		return results;
	}

	@Override
	public Bus getBusDetailByID(Long id) throws InvalidParamException,
			InternalServiceException {
		// TODO Auto-generated method stub
		Bus result = new Bus();
		try {
			result = busMapper.selectByPrimaryKey(id);

		} catch (Exception ex) {
			logError("error:", ex);
			InternalServiceException internalException = new InternalServiceException(
					appManagementService.getMessage("error.service"));
			internalException.getMessageObj().setDescription(ex.getMessage());
			throw internalException;
		}

		return result;
	}

	@Override
	public Bus saveBus(Bus obj) throws InvalidParamException,
			InternalServiceException {
		// TODO Auto-generated method stub
		try {

			obj.setIdBus(busMapper.selectNext().longValue());
			obj.setDateAdded(new Date());
			obj.setDateModified(new Date());

			busMapper.insert(obj);

		} catch (Exception ex) {
			logError("error:", ex);
			InternalServiceException internalException = new InternalServiceException(
					appManagementService.getMessage("error.service"));
			internalException.getMessageObj().setDescription(ex.getMessage());
			throw internalException;
		}

		return obj;
	}

	@Override
	public Bus updateBus(Bus obj) throws InvalidParamException,
			InternalServiceException {
		// TODO Auto-generated method stub
		try {

			obj.setDateModified(new Date());

			busMapper.updateByPrimaryKey(obj);

		} catch (Exception ex) {
			logError("error:", ex);
			InternalServiceException internalException = new InternalServiceException(
					appManagementService.getMessage("error.service"));
			internalException.getMessageObj().setDescription(ex.getMessage());
			throw internalException;
		}

		return obj;
	}

	@Override
	public void deleteBusByID(Long id) throws InvalidParamException,
			InternalServiceException {
		// TODO Auto-generated method stub
		try {

			busMapper.deleteByPrimaryKey(id);

		} catch (Exception ex) {
			logError("error:", ex);
			InternalServiceException internalException = new InternalServiceException(
					appManagementService.getMessage("error.service"));
			internalException.getMessageObj().setDescription(ex.getMessage());
			throw internalException;
		}

	}

	@Override
	public Page findPageBus(Integer offset, Integer limit, String filter) {
		// TODO Auto-generated method stub
		Page page = new Page();

		String sql = "";
		// "where SUBMITTED_BY = '"+getCurrentUser()+"' AND CODE_USED = '"+CodeUsed.PAJAK_KELUARAN.getValue()+"' AND ID_USE_FAKTUR like '%"+filter+"%' ";

		page.setLimit(limit);
		page.setCurrentPage(Page.calculatePage(offset, limit));
		page.setObj(busMapper.selectAllByPage(limit, offset, sql));
		page.setTotalRow(busMapper.selectCountAll(sql));
		// System.out.println("total row:"+page.getTotalRow());
		return page;
	}

	@Override
	public List<RuteFrom> getAllRuteFromByParam(Map<String, String> params)
			throws InvalidParamException, InternalServiceException {
		// TODO Auto-generated method stub
		List<RuteFrom> results = new ArrayList<RuteFrom>();
		String sql = "";
		try {

			if (params.size() > 0) {
				sql += "where ";
				int counter = 0;
				for (Map.Entry<String, String> entry : params.entrySet()) {

					// SUBMITTED_BY = '"+getCurrentUser()+"'
					sql += entry.getKey() + " = '" + entry.getValue() + "' ";
					counter++;

					if (counter == params.size()) {

					} else {
						sql += " AND ";
					}
				}
				System.out.println("sql command:" + sql);
			}

			results = ruteFromMapper.selectAllwithParam(sql);

		} catch (Exception ex) {
			logError("error:", ex);
			InternalServiceException internalException = new InternalServiceException(
					appManagementService.getMessage("error.service"));
			internalException.getMessageObj().setDescription(ex.getMessage());
			throw internalException;
		}

		return results;
	}

	@Override
	public RuteFrom getRuteFromDetailByID(Long id)
			throws InvalidParamException, InternalServiceException {
		// TODO Auto-generated method stub

		RuteFrom result = new RuteFrom();
		try {

			result = ruteFromMapper.selectByPrimaryKey(id);

		} catch (Exception ex) {
			logError("error:", ex);
			InternalServiceException internalException = new InternalServiceException(
					appManagementService.getMessage("error.service"));
			internalException.getMessageObj().setDescription(ex.getMessage());
			throw internalException;
		}

		return result;
	}

	@Override
	public RuteFrom saveRuteFrom(RuteFrom obj) throws InvalidParamException,
			InternalServiceException {
		// TODO Auto-generated method stub
		try {

			obj.setIdRuteFrom(ruteFromMapper.selectNext().longValue());
			obj.setDateAdded(new Date());
			obj.setDateModified(new Date());

			ruteFromMapper.insert(obj);

		} catch (Exception ex) {
			logError("error:", ex);
			InternalServiceException internalException = new InternalServiceException(
					appManagementService.getMessage("error.service"));
			internalException.getMessageObj().setDescription(ex.getMessage());
			throw internalException;
		}

		return obj;

	}

	@Override
	public RuteFrom updateRuteFrom(RuteFrom obj) throws InvalidParamException,
			InternalServiceException {
		// TODO Auto-generated method stub
		try {

			obj.setDateModified(new Date());
			ruteFromMapper.updateByPrimaryKey(obj);

		} catch (Exception ex) {
			logError("error:", ex);
			InternalServiceException internalException = new InternalServiceException(
					appManagementService.getMessage("error.service"));
			internalException.getMessageObj().setDescription(ex.getMessage());
			throw internalException;
		}

		return obj;
	}

	@Override
	public void deleteRuteFromByID(Long id) throws InvalidParamException,
			InternalServiceException {
		// TODO Auto-generated method stub

		try {

			ruteFromMapper.deleteByPrimaryKey(id);

			ruteToMapper.deleteByRuteFromRefID(id);

		} catch (Exception ex) {
			logError("error:", ex);
			InternalServiceException internalException = new InternalServiceException(
					appManagementService.getMessage("error.service"));
			internalException.getMessageObj().setDescription(ex.getMessage());
			throw internalException;
		}

	}

	@Override
	public List<RuteTo> getAllRuteToByParam(Map<String, String> params)
			throws InvalidParamException, InternalServiceException {
		// TODO Auto-generated method stub
		List<RuteTo> results = new ArrayList<RuteTo>();
		String sql = "";
		try {

			if (params.size() > 0) {
				sql += "where ";
				int counter = 0;
				for (Map.Entry<String, String> entry : params.entrySet()) {

					// SUBMITTED_BY = '"+getCurrentUser()+"'
					sql += entry.getKey() + " = '" + entry.getValue() + "' ";
					counter++;

					if (counter == params.size()) {

					} else {
						sql += " AND ";
					}
				}
				System.out.println("sql command:" + sql);
			}

			results = ruteToMapper.selectAllwithParam(sql);

		} catch (Exception ex) {
			logError("error:", ex);
			InternalServiceException internalException = new InternalServiceException(
					appManagementService.getMessage("error.service"));
			internalException.getMessageObj().setDescription(ex.getMessage());
			throw internalException;
		}

		return results;
	}

	@Override
	public RuteTo getRuteToDetailByID(Long id) throws InvalidParamException,
			InternalServiceException {
		// TODO Auto-generated method stub
		RuteTo result = new RuteTo();
		try {

			result = ruteToMapper.selectByPrimaryKey(id);

		} catch (Exception ex) {
			logError("error:", ex);
			InternalServiceException internalException = new InternalServiceException(
					appManagementService.getMessage("error.service"));
			internalException.getMessageObj().setDescription(ex.getMessage());
			throw internalException;
		}

		return result;
	}

	@Override
	public RuteTo saveRuteTo(RuteTo obj) throws InvalidParamException,
			InternalServiceException {
		// TODO Auto-generated method stub
		try {

			obj.setIdRuteTo(ruteToMapper.selectNext().longValue());
			obj.setDateAdded(new Date());
			obj.setDateModified(new Date());

			ruteToMapper.insert(obj);

		} catch (Exception ex) {
			logError("error:", ex);
			InternalServiceException internalException = new InternalServiceException(
					appManagementService.getMessage("error.service"));
			internalException.getMessageObj().setDescription(ex.getMessage());
			throw internalException;
		}

		return obj;
	}

	@Override
	public RuteTo updateRuteTo(RuteTo obj) throws InvalidParamException,
			InternalServiceException {
		// TODO Auto-generated method stub
		try {

			obj.setDateModified(new Date());

			ruteToMapper.updateByPrimaryKey(obj);

		} catch (Exception ex) {
			logError("error:", ex);
			InternalServiceException internalException = new InternalServiceException(
					appManagementService.getMessage("error.service"));
			internalException.getMessageObj().setDescription(ex.getMessage());
			throw internalException;
		}

		return obj;
	}

	@Override
	public void deleteRuteToByID(Long id) throws InvalidParamException,
			InternalServiceException {
		// TODO Auto-generated method stub
		try {

			ruteToMapper.deleteByPrimaryKey(id);

		} catch (Exception ex) {
			logError("error:", ex);
			InternalServiceException internalException = new InternalServiceException(
					appManagementService.getMessage("error.service"));
			internalException.getMessageObj().setDescription(ex.getMessage());
			throw internalException;
		}

	}

	@Override
	public List<ScheduleBus> getAllScheduleBusByParam(Map<String, String> params)
			throws InvalidParamException, InternalServiceException {
		// TODO Auto-generated method stub
		List<ScheduleBus> results = new ArrayList<ScheduleBus>();
		String sql = "";
		try {

			if (params.size() > 0) {
				sql += "where ";
				int counter = 0;
				for (Map.Entry<String, String> entry : params.entrySet()) {

					// SUBMITTED_BY = '"+getCurrentUser()+"'
					sql += entry.getKey() + " = '" + entry.getValue() + "' ";
					counter++;

					if (counter == params.size()) {

					} else {
						sql += " AND ";
					}
				}
				System.out.println("sql command:" + sql);
			}

			results = scheduleBusMapper.selectAllByParam(sql);

		} catch (Exception ex) {
			logError("error:", ex);
			InternalServiceException internalException = new InternalServiceException(
					appManagementService.getMessage("error.service"));
			internalException.getMessageObj().setDescription(ex.getMessage());
			throw internalException;
		}

		return results;
	}

	@Override
	public ScheduleBus getScheduleBusDetailByID(Long id)
			throws InvalidParamException, InternalServiceException {
		// TODO Auto-generated method stub
		ScheduleBus result = new ScheduleBus();
		try {
			result = scheduleBusMapper.selectByPrimaryKey(id);
		} catch (Exception ex) {
			logError("error:", ex);
			InternalServiceException internalException = new InternalServiceException(
					appManagementService.getMessage("error.service"));
			internalException.getMessageObj().setDescription(ex.getMessage());
			throw internalException;
		}

		return result;
	}

	@Override
	public ScheduleBus saveScheduleBus(ScheduleBus obj)
			throws InvalidParamException, InternalServiceException {
		// TODO Auto-generated method stub
		try {
			obj.setDateAdded(new Date());
			obj.setDateModified(new Date());
			obj.setIdSchedule(scheduleBusMapper.selectNext().longValue());
			scheduleBusMapper.insert(obj);

		} catch (Exception ex) {
			logError("error:", ex);
			InternalServiceException internalException = new InternalServiceException(
					appManagementService.getMessage("error.service"));
			internalException.getMessageObj().setDescription(ex.getMessage());
			throw internalException;
		}

		return obj;
	}

	@Override
	public ScheduleBus updateScheduleBus(ScheduleBus obj)
			throws InvalidParamException, InternalServiceException {
		// TODO Auto-generated method stub
		try {

			obj.setDateModified(new Date());

			scheduleBusMapper.updateByPrimaryKey(obj);

		} catch (Exception ex) {
			logError("error:", ex);
			InternalServiceException internalException = new InternalServiceException(
					appManagementService.getMessage("error.service"));
			internalException.getMessageObj().setDescription(ex.getMessage());
			throw internalException;
		}

		return obj;
	}

	@Override
	public void deleteScheduleBusByID(Long id) throws InvalidParamException,
			InternalServiceException {
		// TODO Auto-generated method stub
		try {

			scheduleBusMapper.deleteByPrimaryKey(id);

		} catch (Exception ex) {
			logError("error:", ex);
			InternalServiceException internalException = new InternalServiceException(
					appManagementService.getMessage("error.service"));
			internalException.getMessageObj().setDescription(ex.getMessage());
			throw internalException;
		}

	}

	@Override
	public Page findPageScheduleBus(Integer offset, Integer limit, String filter) {
		// TODO Auto-generated method stub
		Page page = new Page();

		String sql = "";
		// "where SUBMITTED_BY = '"+getCurrentUser()+"' AND CODE_USED = '"+CodeUsed.PAJAK_KELUARAN.getValue()+"' AND ID_USE_FAKTUR like '%"+filter+"%' ";

		page.setLimit(limit);
		page.setCurrentPage(Page.calculatePage(offset, limit));
		page.setObj(scheduleBusMapper.selectAllByPage(limit, offset, sql));
		page.setTotalRow(scheduleBusMapper.selectCountAll(sql));
		// System.out.println("total row:"+page.getTotalRow());
		return page;
	}

	@Override
	public List<ScheduleOnTime> getAllScheduleOnTimeByParam(String param)
			throws InvalidParamException, InternalServiceException {
		// TODO Auto-generated method stub
		List<ScheduleOnTime> results = new ArrayList<ScheduleOnTime>();
		try {
			results = scheduleOnTimeMapper.selectAll();

		} catch (Exception ex) {
			logError("error:", ex);
			InternalServiceException internalException = new InternalServiceException(
					appManagementService.getMessage("error.service"));
			internalException.getMessageObj().setDescription(ex.getMessage());
			throw internalException;
		}

		return results;
	}

	@Override
	public ScheduleOnTime getScheduleOnTimeDetailByID(Long id)
			throws InvalidParamException, InternalServiceException {
		// TODO Auto-generated method stub
		ScheduleOnTime result = new ScheduleOnTime();
		try {
			result = scheduleOnTimeMapper.selectByPrimaryKey(id);
		} catch (Exception ex) {
			logError("error:", ex);
			InternalServiceException internalException = new InternalServiceException(
					appManagementService.getMessage("error.service"));
			internalException.getMessageObj().setDescription(ex.getMessage());
			throw internalException;
		}

		return result;
	}

	@Override
	public ScheduleOnTime saveScheduleOnTime(ScheduleOnTime obj)
			throws InvalidParamException, InternalServiceException {
		// TODO Auto-generated method stub
		try {
			if(checkExistScheduleOnTime(obj.getIdBus(), obj.getActualDate(), obj.getIdSchedule(), obj.getRuteFromIdRef(), obj.getRuteToIdRef())) {
				// not exist
				
			}
			else{
				//exist
				obj.setDateAdded(new Date());
				obj.setDateModified(new Date());
				obj.setIdOnTime(scheduleOnTimeMapper.selectNext().longValue());
				scheduleOnTimeMapper.insert(obj);
				String actualDate = DateUtil.convertDatetoString2(obj.getActualDate());
				List<ViewSitOrder> result = viewSitOrderMapper.selectByParams(obj.getIdSchedule(), obj.getRuteFromIdRef(), obj.getRuteToIdRef(), actualDate);
				for (ViewSitOrder viewSitOrder : result) {
					String idSitOrder = viewSitOrder.getIdSitOrder();
					SitOrder sitOrder = sitOrderMapper.selectByPrimaryKey(idSitOrder);
					System.out.println(viewSitOrder.getIdSitOrder());
					System.out.println(sitOrder.getIdSitOrder());
					System.out.println(sitOrder.getScheduleOtRefId());
					sitOrder.setScheduleOtRefId(obj.getIdOnTime());
					System.out.println(sitOrder.getScheduleOtRefId());
					sitOrderMapper.updateByPrimaryKey(sitOrder);
				}
			}
			
		} catch (Exception ex) {
			logError("error:", ex);
			InternalServiceException internalException = new InternalServiceException(
					appManagementService.getMessage("error.service"));
			internalException.getMessageObj().setDescription(ex.getMessage());
			throw internalException;
		}
		return obj;

	}

	@Override
	public ScheduleOnTime updateScheduleOnTime(ScheduleOnTime obj)
			throws InvalidParamException, InternalServiceException {
		// TODO Auto-generated method stub
		try {

			obj.setDateModified(new Date());

			scheduleOnTimeMapper.updateByPrimaryKey(obj);

		} catch (Exception ex) {
			logError("error:", ex);
			InternalServiceException internalException = new InternalServiceException(
					appManagementService.getMessage("error.service"));
			internalException.getMessageObj().setDescription(ex.getMessage());
			throw internalException;
		}

		return obj;
	}

	@Override
	public void deleteScheduleOnTimeByID(Long id) throws InvalidParamException,
			InternalServiceException {
		// TODO Auto-generated method stub
		try {

			scheduleOnTimeMapper.deleteByPrimaryKey(id);

		} catch (Exception ex) {
			logError("error:", ex);
			InternalServiceException internalException = new InternalServiceException(
					appManagementService.getMessage("error.service"));
			internalException.getMessageObj().setDescription(ex.getMessage());
			throw internalException;
		}
	}

	@Override
	public Page findPageScheduleOnTime(Integer offset, Integer limit,
			String filter) {
		// TODO Auto-generated method stub
		Page page = new Page();

		String sql = "";
		// "where SUBMITTED_BY = '"+getCurrentUser()+"' AND CODE_USED = '"+CodeUsed.PAJAK_KELUARAN.getValue()+"' AND ID_USE_FAKTUR like '%"+filter+"%' ";

		page.setLimit(limit);
		page.setCurrentPage(Page.calculatePage(offset, limit));
		page.setObj(scheduleOnTimeMapper.selectAllByPage(limit, offset, sql));
		page.setTotalRow(scheduleOnTimeMapper.selectCountAll(sql));
		// System.out.println("total row:"+page.getTotalRow());
		return page;
	}

	@Override
	public Page findPageRuteFrom(Integer offset, Integer limit, String filter) {
		// TODO Auto-generated method stub
		Page page = new Page();

		String sql = "";
		// "where SUBMITTED_BY = '"+getCurrentUser()+"' AND CODE_USED = '"+CodeUsed.PAJAK_KELUARAN.getValue()+"' AND ID_USE_FAKTUR like '%"+filter+"%' ";

		page.setLimit(limit);
		page.setCurrentPage(Page.calculatePage(offset, limit));
		page.setObj(ruteFromMapper.selectAllByPage(limit, offset, sql));
		page.setTotalRow(ruteFromMapper.selectCountAll(sql));

		return page;
	}

	@Override
	public Page findPageRuteTo(Integer offset, Integer limit, String filter,
			Long ruteFromRef) {
		// TODO Auto-generated method stub
		Page page = new Page();

		String sql = "where ID_RUTE_FROM_REF = '" + ruteFromRef + "' ";
		// "where SUBMITTED_BY = '"+getCurrentUser()+"' AND CODE_USED = '"+CodeUsed.PAJAK_KELUARAN.getValue()+"' AND ID_USE_FAKTUR like '%"+filter+"%' ";

		page.setLimit(limit);
		page.setCurrentPage(Page.calculatePage(offset, limit));

		List<RuteTo> results = ruteToMapper.selectAllByPage(limit, offset, sql);
		for (RuteTo ruteTo : results) {
			ruteTo.setRuteFromRef(ruteFromMapper.selectByPrimaryKey(ruteTo
					.getIdRuteFromRef()));
		}

		page.setObj(results);
		page.setTotalRow(ruteToMapper.selectCountAll(sql));

		return page;
	}

	@Override
	public List<Bus> getAllBusByScheduleOnTimeParam(String param)
			throws InvalidParamException, InternalServiceException {
		// TODO Auto-generated method stub
		List<Bus> results = new ArrayList<Bus>();
		try {
			results = busMapper.selectBusSchedule();

		} catch (Exception ex) {
			logError("error:", ex);
			InternalServiceException internalException = new InternalServiceException(
					appManagementService.getMessage("error.service"));
			internalException.getMessageObj().setDescription(ex.getMessage());
			throw internalException;
		}

		return results;
	}

	@Override
	public boolean checkExistScheduleOnTime(Long idBus, Date actualDate, Long idSchedule,
			Long ruteFromIdRef, Long ruteToIdRef) throws InvalidParamException,
			InternalServiceException {
		// TODO Auto-generated method stub
		int count = scheduleOnTimeMapper.selectCountByScheduleOnTimeExist(idBus, actualDate, idSchedule, ruteFromIdRef, ruteToIdRef);
		if (count > 0) {
			return true;
		}
		else
		{
			return false;
		}
	}
	
	@Override
	public List<SitOrderBean> findPageSitOrder(Long idOnTime)
			throws InvalidParamException, InternalServiceException {
		// TODO Auto-generated method stub
		List<SitOrderBean> listSitOrderBean = new ArrayList<SitOrderBean>();
		List<SitOrder> list_sit_order = new ArrayList<SitOrder>();
		List<ScheduleOnTime> scheduleOT = new ArrayList<ScheduleOnTime>();
		DateFormat df = new SimpleDateFormat("yyyy/MM/dd");
		SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss a");
        //String formattedDate = formatter.format(value);
		int jumlah_kursi = 50; // default

		ScheduleOnTime scheduleOnTimeDetails = scheduleOnTimeMapper.selectByPrimaryKey(idOnTime);
		Map<String, String> params = new HashMap<>();

		// get jumlah kursi dari jadwal yang ada
		params.put("ID_SCHEDULE", scheduleOnTimeDetails.getIdSchedule().toString());
		params.put("RUTE_FROM_ID_REF", scheduleOnTimeDetails.getRuteFromIdRef().toString());
		params.put("RUTE_TO_ID_REF", scheduleOnTimeDetails.getRuteToIdRef().toString());
		params.put("ACTUAL_DATE", df.format(scheduleOnTimeDetails.getActualDate()));

		String sql = "";
		if (params.size() > 0) {
			sql += "where ";
			int counter = 0;
			for (Map.Entry<String, String> entry : params.entrySet()) {

				sql += entry.getKey() + " = '" + entry.getValue() + "' ";
				counter++;

				if (counter == params.size()) {

				} else {
					sql += " AND ";
				}
			}
			System.out.println("sql command:" + sql);
		}

		scheduleOT = scheduleOnTimeMapper.selectAllByParam(sql);
		if (scheduleOT.size() > 0) {
			Long scheduleOTRefId = scheduleOT.get(0).getIdOnTime();
			Bus busDetails = busMapper.selectByPrimaryKey(scheduleOT.get(0)
					.getIdBus());
			jumlah_kursi = busDetails.getCountOfChair();
			params = new HashMap<>();

			// get jumlah kursi dari jadwal yang ada
			params.put("ID_SCHEDULE", scheduleOnTimeDetails.getIdSchedule().toString());
			params.put("ID_RUTE_FROM", scheduleOnTimeDetails.getRuteFromIdRef().toString());
			params.put("ID_RUTE_TO", scheduleOnTimeDetails.getRuteToIdRef().toString());
			params.put("ORDER_DATE", df.format(scheduleOnTimeDetails.getActualDate()));
			
			sql = "";
			if (params.size() > 0) {
				sql += "where ";
				int counter = 0;
				for (Map.Entry<String, String> entry : params.entrySet()) {

					sql += entry.getKey() + " = '" + entry.getValue() + "' ";
					counter++;

					if (counter == params.size()) {

					} else {
						sql += " AND ";
					}
				}
				System.out.println("sql command:" + sql);
			}
			
			list_sit_order = sitOrderMapper.selectAllwithParams(sql);
			
			for (int counter = 1; counter <= jumlah_kursi; counter++) {
				SitOrderBean sit_ = new SitOrderBean();
				boolean found = false;
				
				for (SitOrder sitOrder_ : list_sit_order) {
					if(sitOrder_.getNoKursi().equals(counter))
					{
						found = true;
						sit_.setIdSitOrder(sitOrder_.getIdSitOrder());
						sit_.setNoKursi(sitOrder_.getNoKursi());
						sit_.setLastUpdate(sitOrder_.getDateModified());
						sit_.setStatus(sitOrder_.getStatus());
						sit_.setDateStringLastUpdate(formatter.format(sit_.getLastUpdate()));
						break;
					}
				}
				
				if(!found)
				{
					sit_.setIdSitOrder(scheduleOnTimeDetails.getIdSchedule().toString()+"-"
							+scheduleOnTimeDetails.getRuteFromIdRef().toString()+"-"
							+scheduleOnTimeDetails.getRuteToIdRef().toString()+"-"
							+df.format(scheduleOnTimeDetails.getActualDate())+"-"
							+counter);
					sit_.setNoKursi(counter);
					sit_.setLastUpdate(new Date());
					sit_.setStatus(StatusBus.AVAILABLE.getValue());
					sit_.setDateStringLastUpdate(formatter.format(sit_.getLastUpdate()));
				}
				
				
				sit_.setScheduleOT(scheduleOTRefId);
				listSitOrderBean.add(sit_);
				
				
			}
		}
		else
		{
			
			params = new HashMap<>();

			
			params.put("ID_SCHEDULE", scheduleOnTimeDetails.getIdSchedule().toString());
			params.put("ID_RUTE_FROM", scheduleOnTimeDetails.getRuteFromIdRef().toString());
			params.put("ID_RUTE_TO", scheduleOnTimeDetails.getRuteToIdRef().toString());
			params.put("ORDER_DATE", df.format(scheduleOnTimeDetails.getActualDate()));
			
			sql = "";
			if (params.size() > 0) {
				sql += "where ";
				int counter = 0;
				for (Map.Entry<String, String> entry : params.entrySet()) {

					sql += entry.getKey() + " = '" + entry.getValue() + "' ";
					counter++;

					if (counter == params.size()) {

					} else {
						sql += " AND ";
					}
				}
				System.out.println("sql command:" + sql);
			}
			
			list_sit_order = sitOrderMapper.selectAllwithParams(sql);
			
			
			for (int counter = 1; counter <= jumlah_kursi; counter++) {
				SitOrderBean sit_ = new SitOrderBean();
				boolean found = false;
				
				for (SitOrder sitOrder_ : list_sit_order) {
					if(sitOrder_.getNoKursi().equals(counter))
					{
						found = true;
						sit_.setIdSitOrder(sitOrder_.getIdSitOrder());
						sit_.setNoKursi(sitOrder_.getNoKursi());
						sit_.setLastUpdate(sitOrder_.getDateModified());
						sit_.setStatus(sitOrder_.getStatus());
						sit_.setDateStringLastUpdate(formatter.format(sit_.getLastUpdate()));
						break;
					}
				}
				
				if(!found)
				{
					sit_.setIdSitOrder(scheduleOnTimeDetails.getIdSchedule().toString()+"-"
							+scheduleOnTimeDetails.getRuteFromIdRef().toString()+"-"
							+scheduleOnTimeDetails.getRuteToIdRef().toString()+"-"
							+df.format(scheduleOnTimeDetails.getActualDate())+"-"
							+counter);
					sit_.setNoKursi(counter);
					sit_.setLastUpdate(new Date());
					sit_.setStatus(StatusBus.AVAILABLE.getValue());
					sit_.setDateStringLastUpdate(formatter.format(sit_.getLastUpdate()));
				}
				
				sit_.setScheduleOT(Long.valueOf(0));
				listSitOrderBean.add(sit_);
				
				
			}
		}

		return listSitOrderBean;

	}

}
