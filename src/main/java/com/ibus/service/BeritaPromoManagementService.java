package com.ibus.service;

import java.util.Date;
import java.util.List;
import java.util.Map;

import co.id.rokubi.exception.InternalServiceException;
import co.id.rokubi.exception.InvalidParamException;
import co.id.rokubi.model.Page;
import co.id.rokubi.service.IService;

import com.ibus.model.BeritaPromo;
import com.ibus.model.PromoBerlaku;

public interface BeritaPromoManagementService extends IService {
	
	List<BeritaPromo> getAllBeritaPromoByParam(Map<String, String> params) throws InvalidParamException, InternalServiceException;
	BeritaPromo getBeritaPromoDetailByID(Long id) throws InvalidParamException, InternalServiceException;
	BeritaPromo saveBeritaPromo(BeritaPromo obj) throws InvalidParamException, InternalServiceException;
	BeritaPromo updateBeritaPromo(BeritaPromo obj) throws InvalidParamException, InternalServiceException;
	void deleteBeritaPromoByID(Long id) throws InvalidParamException, InternalServiceException;
	Page findPageBeritaPromo(Integer offset, Integer limit,String filter);
	boolean checkIsDateWithinRange(Date timeStart, Date timeEnd, String contentType)throws InvalidParamException, InternalServiceException;

	List<PromoBerlaku> getAllPromoBerlakuByParam(String param) throws InvalidParamException, InternalServiceException;
	PromoBerlaku getPromoBerlakuDetailByID(Long id) throws InvalidParamException, InternalServiceException;
	PromoBerlaku savePromoBerlaku(PromoBerlaku obj) throws InvalidParamException, InternalServiceException;
	void deletePromoBerlakuByID(Long id) throws InvalidParamException, InternalServiceException;
	Page findPagePromoBerlaku(Integer offset, Integer limit,String filter);
}
