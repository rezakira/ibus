package com.ibus.service;

import java.util.Date;
import java.util.List;
import java.util.Map;

import com.ibus.controller.bean.SitOrderBean;
import com.ibus.model.Bus;
import com.ibus.model.RuteFrom;
import com.ibus.model.RuteTo;
import com.ibus.model.ScheduleBus;
import com.ibus.model.ScheduleOnTime;

import co.id.rokubi.exception.InternalServiceException;
import co.id.rokubi.exception.InvalidParamException;
import co.id.rokubi.model.Page;
import co.id.rokubi.service.IService;


public interface DataMasterManagementService extends IService {
	
	List<Bus> getAllBusByParam(String param) throws InvalidParamException, InternalServiceException;
	List<Bus> getAllBusByScheduleOnTimeParam(String param) throws InvalidParamException, InternalServiceException;
	Bus getBusDetailByID(Long id) throws InvalidParamException, InternalServiceException;
	Bus saveBus(Bus obj) throws InvalidParamException, InternalServiceException;
	Bus updateBus(Bus obj) throws InvalidParamException, InternalServiceException;
	void deleteBusByID(Long id) throws InvalidParamException, InternalServiceException;
	Page findPageBus(Integer offset, Integer limit,String filter);
	
	List<RuteFrom> getAllRuteFromByParam(Map<String, String> params) throws InvalidParamException, InternalServiceException;
	RuteFrom getRuteFromDetailByID(Long id) throws InvalidParamException, InternalServiceException;
	RuteFrom saveRuteFrom(RuteFrom obj) throws InvalidParamException, InternalServiceException;
	RuteFrom updateRuteFrom(RuteFrom obj) throws InvalidParamException, InternalServiceException;
	void deleteRuteFromByID(Long id) throws InvalidParamException, InternalServiceException;
	Page findPageRuteFrom(Integer offset, Integer limit,String filter);
	
	List<RuteTo> getAllRuteToByParam(Map<String, String> params) throws InvalidParamException, InternalServiceException;
	RuteTo getRuteToDetailByID(Long id) throws InvalidParamException, InternalServiceException;
	RuteTo saveRuteTo(RuteTo obj) throws InvalidParamException, InternalServiceException;
	RuteTo updateRuteTo(RuteTo obj) throws InvalidParamException, InternalServiceException;
	void deleteRuteToByID(Long id) throws InvalidParamException, InternalServiceException;
	Page findPageRuteTo(Integer offset, Integer limit,String filter,Long ruteFromRef);
	
	List<ScheduleBus> getAllScheduleBusByParam(Map<String, String> params) throws InvalidParamException, InternalServiceException;
	ScheduleBus getScheduleBusDetailByID(Long id) throws InvalidParamException, InternalServiceException;
	ScheduleBus saveScheduleBus(ScheduleBus obj) throws InvalidParamException, InternalServiceException;
	ScheduleBus updateScheduleBus(ScheduleBus obj) throws InvalidParamException, InternalServiceException;
	void deleteScheduleBusByID(Long id) throws InvalidParamException, InternalServiceException;
	Page findPageScheduleBus(Integer offset, Integer limit,String filter);
	

	
	List<ScheduleOnTime> getAllScheduleOnTimeByParam(String param) throws InvalidParamException, InternalServiceException;
	ScheduleOnTime getScheduleOnTimeDetailByID(Long id) throws InvalidParamException, InternalServiceException;
	ScheduleOnTime saveScheduleOnTime(ScheduleOnTime obj) throws InvalidParamException, InternalServiceException;
	ScheduleOnTime updateScheduleOnTime(ScheduleOnTime obj) throws InvalidParamException, InternalServiceException;
	void deleteScheduleOnTimeByID(Long id) throws InvalidParamException, InternalServiceException;
	Page findPageScheduleOnTime(Integer offset, Integer limit,String filter);
	boolean checkExistScheduleOnTime(Long idBus, Date actualDate,Long idSchedule ,Long ruteFromIdRef,Long ruteToIdRef) throws InvalidParamException, InternalServiceException;
	List<SitOrderBean> findPageSitOrder(Long idOnTime) throws InvalidParamException, InternalServiceException;
	
}
