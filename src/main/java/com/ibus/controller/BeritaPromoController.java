package com.ibus.controller;

import java.io.File;
import java.io.FileOutputStream;
import java.security.Principal;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;

import com.ibus.controller.bean.BeritaPromoBean;
import com.ibus.model.BeritaPromo;
import com.ibus.model.PromoBerlaku;
import com.ibus.service.BeritaPromoManagementService;
import com.ibus.service.DataMasterManagementService;

import co.id.rokubi.controller.AbstractController;
import co.id.rokubi.exception.InternalServiceException;
import co.id.rokubi.exception.InvalidParamException;
import co.id.rokubi.model.JsonResponse;
import co.id.rokubi.model.Page;
import co.id.rokubi.model.TabelData;
import co.id.rokubi.util.CommonUtil;
import co.id.rokubi.util.DateUtil;
import co.id.rokubi.util.FileUtil;
import co.id.rokubi.util.ImageUtil;

@Controller
@RequestMapping("/berita_promo")
public class BeritaPromoController extends AbstractController {

	@Autowired
	private BeritaPromoManagementService beritaPromoManagementService;
	@Autowired
	private DataMasterManagementService dataMasterManagementService;
	
	private static final String LIST_BERITA_PROMO = "berita_promo/list_berita_promo";
	private static final String ADD_BERITA_PROMO = "berita_promo/add_berita_promo";
	private static final String UPDATE_BERITA_PROMO = "berita_promo/update_berita_promo";
	private static final String DETAIL_BERITA_PROMO = "berita_promo/detail_berita_promo";

	@RequestMapping(value = "list_berita_promo.page", method = RequestMethod.GET)
	public ModelAndView listBeritaPromoPage(ModelMap model,
			final HttpServletRequest request, Principal principal,
			HttpSession httpSession) throws InvalidParamException,
			InternalServiceException {
		ModelAndView modelView = new ModelAndView(LIST_BERITA_PROMO);
		return modelView;
	}

	@RequestMapping(value = "add_berita_promo.page", method = RequestMethod.GET)
	public ModelAndView addBeritaPromoPage(ModelMap model,
			final HttpServletRequest request, Principal principal,
			HttpSession httpSession) throws InvalidParamException,
			InternalServiceException {
		ModelAndView modelView = new ModelAndView(ADD_BERITA_PROMO);

		return modelView;
	}
	
	@RequestMapping(value = "image_berita_promo/{image_url}", method = RequestMethod.GET)
	public String imageBeritaPromoPage(@PathVariable String image_url,ModelMap model,
			final HttpServletRequest request, Principal principal,
			HttpSession httpSession) throws InvalidParamException,
			InternalServiceException {
		String link = "http://localhost:8080/img_ibus/image_berita_promo/"+image_url;
		System.out.println(link);

		return link;
	}

	@RequestMapping(value = "update_berita_promo.page/{id_berita_promo}", method = RequestMethod.GET)
	public ModelAndView updateBeritaPromoPage(
			@PathVariable Long id_berita_promo, ModelMap model,
			final HttpServletRequest request, Principal principal,
			HttpSession httpSession) throws InvalidParamException,
			InternalServiceException {
		ModelAndView modelView = new ModelAndView(UPDATE_BERITA_PROMO);
		modelView.addObject("beritaPromoDetails", beritaPromoManagementService
				.getBeritaPromoDetailByID(id_berita_promo));

		return modelView;
	}
	
	@RequestMapping(value = "detail_berita_promo.page/{id_berita_promo}", method = RequestMethod.GET)
	public ModelAndView detailBeritaPromoPage(
			@PathVariable Long id_berita_promo, ModelMap model,
			final HttpServletRequest request, Principal principal,
			HttpSession httpSession) throws InvalidParamException,
			InternalServiceException {
		ModelAndView modelView = new ModelAndView(DETAIL_BERITA_PROMO);
		modelView.addObject("beritaPromoDetails", beritaPromoManagementService
				.getBeritaPromoDetailByID(id_berita_promo));
		Map<String, String> params = new HashMap<>();
		modelView.addObject("list_rute_from", dataMasterManagementService.getAllRuteFromByParam(params));
		modelView.addObject("list_promo_berlaku", beritaPromoManagementService.getPromoBerlakuDetailByID(id_berita_promo));
		return modelView;
	}

	@RequestMapping(value = "/berita_promo/data-table.json", method = RequestMethod.GET)
	@ResponseBody
	public TabelData listBeritaPromoDataTable(
			@RequestParam("sEcho") String echo,
			@RequestParam("iDisplayStart") Integer offset,
			@RequestParam("iDisplayLength") Integer limit,
			@RequestParam(value = "sSearch", required = false) String searchByName) {

		TabelData tabelData = new TabelData();
		tabelData.setsEcho(echo);
		// System.out.println("keyString:"+searchByName);
		Page page = beritaPromoManagementService.findPageBeritaPromo(offset,
				limit, searchByName);

		tabelData.setsEcho(echo);
		tabelData.setiTotalDisplayRecords(page.getTotalRow());
		tabelData.setiTotalRecords(10);
		tabelData.setAaData(page.getObj());
		return tabelData;
	}

	@Value("${user.pic.dir}")
	private String userPicDir;

	private String getRandomFile(int length) {
		final String charset = "0123456789abcdefghijklmnopqrstuvwxyz";

		Random rand = new Random(System.currentTimeMillis());
		StringBuilder sb = new StringBuilder();
		for (int i = 0; i < length; i++) {
			int pos = rand.nextInt(charset.length());
			sb.append(charset.charAt(pos));
		}
		return sb.toString();
	}

	@Transactional
	@RequestMapping(value = "saveBeritaPromo.json", method = RequestMethod.POST)
	@ResponseBody
	public JsonResponse actionSaveBeritaPromo(
			@ModelAttribute("berita_promo") BeritaPromoBean beritaPromo,
			BindingResult result) throws InvalidParamException,
			InternalServiceException {

		JsonResponse response = new JsonResponse();
		try {
			
			
			Calendar c = GregorianCalendar.getInstance();
			c.set(Calendar.HOUR_OF_DAY, 0);
			c.set(Calendar.AM_PM, 0);
			c.set(Calendar.HOUR, 0);
			c.set(Calendar.MINUTE, 0);
			c.set(Calendar.SECOND, 0);
			c.set(Calendar.MILLISECOND, 0);
			Date todayDate = c.getTime();
			System.out.println(todayDate);
			System.out.println(beritaPromo.getTimeStart());
			System.out.println("Date Time Start:"+ beritaPromo.getDateTimeStart());
			System.out.println("Date Time End:"+ beritaPromo.getDateTimeEnd());
			beritaPromo.setTimeStart(DateUtil.convertStringtoDate2(beritaPromo.getDateTimeStart()));
			beritaPromo.setTimeEnd(DateUtil.convertStringtoDate2(beritaPromo.getDateTimeEnd()));
			System.out.println(beritaPromo.getTimeStart());
			System.out.println(beritaPromo.getTimeEnd());
			
			if(beritaPromo.getTimeStart().before(beritaPromo.getTimeEnd()) && beritaPromo.getTimeStart().after(todayDate) || beritaPromo.getTimeStart().equals(todayDate)){
				
				BeritaPromo saveBeritaPromo = new BeritaPromo();

				// save picture file
				MultipartFile file = beritaPromo.getImage();
				if (CommonUtil.isNotNullOrEmpty(file)) {
					final String FILE_PIC_NAME = "image_berita_promo" + "_"
							+ getRandomFile(10);

					try {
						if (file.getSize() > 0) {
							String dir = userPicDir + File.separatorChar
									+ "image_berita_promo" + File.separatorChar;
							File newFile = new File(dir);
							if (!newFile.exists())
								newFile.mkdirs();

							Pattern p = Pattern.compile("\\.[A-Za-z]*$");
							Matcher m = p.matcher(file.getOriginalFilename());
							String suffix = m.find() ? m.group() : ".jpg";
							String fileDir = dir + FILE_PIC_NAME + suffix;
							newFile = new File(fileDir);
							if (newFile.exists())
								newFile.delete();
							FileUtil.fastChannelCopy(file.getInputStream(),
									new FileOutputStream(fileDir));
//							String fileResultName = ImageUtil.resizeImageWithHint(
//									dir, FILE_PIC_NAME, suffix, 200, 200);
							
							String fileResultName = ImageUtil.compressImage(dir, FILE_PIC_NAME, suffix);

							saveBeritaPromo.setImageUrl(fileResultName);
						}
						else if(file.getSize() == 0){
							//jika file size 0 atau tidak ada file baru yang akan di upload
							System.out.print("masuk else if size file = 0\n");
							saveBeritaPromo.setImageUrl("");
							System.out.println(saveBeritaPromo.getImageUrl());
						}
					} catch (Exception e) {
						logError("saving user error:", e);
						InternalServiceException ise = new InternalServiceException(
								"Saving Error");
						ise.getMessageObj().setDescription(e.getMessage());
						throw ise;
					}
				} else {
					saveBeritaPromo.setImageUrl("");
				}

				saveBeritaPromo.setIdBeritaPromo(beritaPromo.getIdBeritaPromo());
				saveBeritaPromo.setTitle(beritaPromo.getTitle());
				saveBeritaPromo.setContentType(beritaPromo.getContentType());
				saveBeritaPromo.setTimeStart(beritaPromo.getTimeStart());
				saveBeritaPromo.setTimeEnd(beritaPromo.getTimeEnd());
				saveBeritaPromo.setStatus(beritaPromo.getStatus());
				saveBeritaPromo.setDateAdded(beritaPromo.getDateAdded());
				saveBeritaPromo.setDateModified(beritaPromo.getDateModified());
				saveBeritaPromo.setContent(beritaPromo.getContent());
				
				if(beritaPromo.getContentType().equalsIgnoreCase("0"))
				{
					saveBeritaPromo.setPotonganHarga(0);
				}
				else if(beritaPromo.getContentType().equalsIgnoreCase("1"))
				{
					saveBeritaPromo.setPotonganHarga(beritaPromo.getPotonganHarga());
				}
				
				beritaPromo.setImage(null);

				saveBeritaPromo = beritaPromoManagementService.saveBeritaPromo(saveBeritaPromo);
				
				if(CommonUtil.isNotNullOrEmpty(saveBeritaPromo.getIdBeritaPromo()))
				{
					response.setMessage(JsonResponse.SUCCESS, "Success.");
					response.setObj(beritaPromo);
				}
				else
				{
					response.setMessage(JsonResponse.VALIDATE_ERROR,
							"Promo sudah ada.");
				}
			}
			else
			{
				response.setMessage(JsonResponse.VALIDATE_ERROR, "Tanggal mulai harus lebih awal dari tanggal berakhir atau tanggal mulai harus sama dengan tanggal sekarang atau lebih!");
			}
		} catch (Exception e) {
			// logger.error(e.getMessage());
			response.setMessage(JsonResponse.VALIDATE_ERROR,
					"Internal Server error. Please contact your administrator");
		}

		return response;
	}

	@Transactional
	@RequestMapping(value = "updateBeritaPromo.json", method = RequestMethod.POST)
	@ResponseBody
	public JsonResponse actionUpdateBeritaPromo(
			@ModelAttribute("beritaPromo") BeritaPromoBean beritaPromo,
			BindingResult result) throws InvalidParamException,
			InternalServiceException {

		JsonResponse response = new JsonResponse();

		try {
			
			System.out.println(beritaPromo.getTimeStart());
			System.out.println("Date Time Start:"+ beritaPromo.getDateTimeStart());
			System.out.println("Date Time End:"+ beritaPromo.getDateTimeEnd());
			beritaPromo.setTimeStart(DateUtil.convertStringtoDate2(beritaPromo.getDateTimeStart()));
			beritaPromo.setTimeEnd(DateUtil.convertStringtoDate2(beritaPromo.getDateTimeEnd()));
			System.out.println(beritaPromo.getTimeStart());
			System.out.println(beritaPromo.getTimeEnd());
			if (CommonUtil.isNotNullOrEmpty(beritaPromo.getDateTimeStart()))
			{
				beritaPromo.setTimeStart(DateUtil.convertStringtoDate2(beritaPromo.getDateTimeStart()));
			}
			
			if (CommonUtil.isNotNullOrEmpty(beritaPromo.getDateTimeStart()))
			{
				beritaPromo.setTimeEnd(DateUtil.convertStringtoDate2(beritaPromo.getDateTimeEnd()));
			}
			
			System.out.println(beritaPromo.getTimeStart());
			System.out.println(beritaPromo.getTimeEnd());
			BeritaPromo beritaPromoOldData = beritaPromoManagementService
					.getBeritaPromoDetailByID(beritaPromo.getIdBeritaPromo());

			BeritaPromo updateBeritaPromo = new BeritaPromo();

			// save picture file
			MultipartFile file = beritaPromo.getImage();
			
			System.out.print("before if common util\n");
			if (CommonUtil.isNotNullOrEmpty(file)) {
				System.out.print("after if common util\n");
				final String FILE_PIC_NAME = "image_berita_promo" + "_"
						+ getRandomFile(10);
				System.out.print(FILE_PIC_NAME);
				System.out.print("\n");
				try {
					System.out.print("masuk ke try\n");
					if (file.getSize() > 0) {
						System.out.print("after if get size\n");
						String dir = userPicDir + File.separatorChar
								+ "image_berita_promo" + File.separatorChar;
						File newFile = new File(dir);
						if (!newFile.exists())
							newFile.mkdirs();

						Pattern p = Pattern.compile("\\.[A-Za-z]*$");
						Matcher m = p.matcher(file.getOriginalFilename());
						String suffix = m.find() ? m.group() : ".jpg";
						String fileDir = dir + FILE_PIC_NAME + suffix;
						newFile = new File(fileDir);
						if (newFile.exists())
							newFile.delete();
						
						//delete old file
						String oldFileDir = dir + beritaPromoOldData.getImageUrl(); 
						System.out.print(oldFileDir);
						File oldFile = new File(oldFileDir);
						
						oldFile.delete();
						
						FileUtil.fastChannelCopy(file.getInputStream(),
								new FileOutputStream(fileDir));
						String fileResultName = ImageUtil.resizeImageWithHint(
								dir, FILE_PIC_NAME, suffix, 200, 200);

						updateBeritaPromo.setImageUrl(fileResultName);
					}
					else if(file.getSize() == 0){
						//jika file size 0 atau tidak ada file baru yang akan di upload
						System.out.print("masuk else if size file = 0\n");
						updateBeritaPromo.setImageUrl(beritaPromoOldData.getImageUrl());
						System.out.print(updateBeritaPromo.getImageUrl());
						System.out.print("\n");
						System.out.print(beritaPromoOldData.getImageUrl());
					}
				} catch (Exception e) {
					logError("saving user error:", e);
					InternalServiceException ise = new InternalServiceException(
							"Saving Error");
					ise.getMessageObj().setDescription(e.getMessage());
					throw ise;
				}
			} else {
				System.out.print("masuk else common util\n");
				updateBeritaPromo.setImageUrl("");
			}
			updateBeritaPromo.setIdBeritaPromo(beritaPromo.getIdBeritaPromo());
			updateBeritaPromo.setTitle(beritaPromo.getTitle());
			updateBeritaPromo.setContentType(beritaPromo.getContentType());
			updateBeritaPromo.setTimeStart(beritaPromo.getTimeStart());
			updateBeritaPromo.setTimeEnd(beritaPromo.getTimeEnd());
			updateBeritaPromo.setStatus(beritaPromo.getStatus());
			updateBeritaPromo.setDateAdded(beritaPromoOldData.getDateAdded());
			updateBeritaPromo.setDateModified(beritaPromo.getDateModified());
			updateBeritaPromo.setContent(beritaPromo.getContent());
			updateBeritaPromo.setPotonganHarga(beritaPromo.getPotonganHarga());
			updateBeritaPromo.setStatusPilihanPromo(beritaPromo.getStatusPilihanPromo());
			if(beritaPromo.getStatusPilihanPromo().equalsIgnoreCase("0"))
			{
				System.out.println("semua jurusan");
				System.out.println(updateBeritaPromo.getStatusPilihanPromo());					
			}
			else if(beritaPromo.getStatusPilihanPromo().equalsIgnoreCase("1"))
			{
				System.out.println("pilihan jurusan");
				System.out.println(updateBeritaPromo.getStatusPilihanPromo());	
			}
			beritaPromoManagementService.updateBeritaPromo(updateBeritaPromo);
			beritaPromo.setImage(null);

			response.setMessage(JsonResponse.SUCCESS, "Success.");
			response.setObj(beritaPromo);
		} catch (Exception e) {
			// logger.error(e.getMessage());
			response.setMessage(JsonResponse.VALIDATE_ERROR,
					"Internal Server error. Please contact your administrator");
		}

		return response;
	}

	@Transactional
	@RequestMapping(value = "deleteBeritaPromo.json", method = RequestMethod.POST)
	@ResponseBody
	public JsonResponse actionDeleteBeritaPromo(@RequestParam("id_long") Long id_long)
			throws InvalidParamException, InternalServiceException {

		JsonResponse response = new JsonResponse();
		try {
			beritaPromoManagementService.deleteBeritaPromoByID(id_long);
			response.setMessage(JsonResponse.SUCCESS, "Success.");
		} catch (Exception e) {
			// logger.error(e.getMessage());
			response.setMessage(JsonResponse.VALIDATE_ERROR,
					"Internal Server error. Please contact your administrator");
		}

		return response;
	}
	
	//Promo Berlaku//
	
	private static final String LIST_PROMO_BERLAKU = "promo_berlaku/list_promo_berlaku";
	private static final String ADD_PROMO_BERLAKU = "promo_berlaku/add_promo_berlaku";

	@RequestMapping(value = "list_promo_berlaku.page", method = RequestMethod.GET)
	public ModelAndView listBusPage(ModelMap model,
			final HttpServletRequest request, Principal principal,
			HttpSession httpSession) throws InvalidParamException,
			InternalServiceException {
		ModelAndView modelView = new ModelAndView(LIST_PROMO_BERLAKU);
		return modelView;
	}

	@RequestMapping(value = "add_promo_berlaku.page", method = RequestMethod.GET)
	public ModelAndView addBusPage(ModelMap model,
			final HttpServletRequest request, Principal principal,
			HttpSession httpSession) throws InvalidParamException,
			InternalServiceException {
		ModelAndView modelView = new ModelAndView(ADD_PROMO_BERLAKU);
		return modelView;
	}

	@RequestMapping(value = "/promo_berlaku/data-table.json", method = RequestMethod.GET)
	@ResponseBody
	public TabelData listPromoBerlakuDataTable(
			@RequestParam("sEcho") String echo,
			@RequestParam("iDisplayStart") Integer offset,
			@RequestParam("iDisplayLength") Integer limit,
			@RequestParam(value = "sSearch", required = false) String searchByName) {

		TabelData tabelData = new TabelData();
		tabelData.setsEcho(echo);
		// System.out.println("keyString:"+searchByName);
		Page page = beritaPromoManagementService.findPagePromoBerlaku(offset, limit,
				searchByName);

		tabelData.setsEcho(echo);
		tabelData.setiTotalDisplayRecords(page.getTotalRow());
		tabelData.setiTotalRecords(10);
		tabelData.setAaData(page.getObj());
		return tabelData;
	}

	@Transactional
	@RequestMapping(value = "savePromoBerlaku.json", method = RequestMethod.POST)
	@ResponseBody
	public JsonResponse actionSavePromoBerlaku(@ModelAttribute("promo_berlaku") PromoBerlaku promoBerlaku,
			BindingResult result) throws InvalidParamException,
			InternalServiceException {

		JsonResponse response = new JsonResponse();
		try {

			beritaPromoManagementService.savePromoBerlaku(promoBerlaku);
			response.setMessage(JsonResponse.SUCCESS, "Success.");
		} catch (Exception e) {
			// logger.error(e.getMessage());
			response.setMessage(JsonResponse.VALIDATE_ERROR,
					"Internal Server error. Please contact your administrator");
		}

		return response;
	}

	@Transactional
	@RequestMapping(value = "deletePromoBerlaku.json", method = RequestMethod.POST)
	@ResponseBody
	public JsonResponse actionDeletePromoBerlaku(@RequestParam("id_long") Long id_long)
			throws InvalidParamException, InternalServiceException {

		JsonResponse response = new JsonResponse();
		try {
			beritaPromoManagementService.deletePromoBerlakuByID(id_long);
			response.setMessage(JsonResponse.SUCCESS, "Success.");
		} catch (Exception e) {
			// logger.error(e.getMessage());
			response.setMessage(JsonResponse.VALIDATE_ERROR,
					"Internal Server error. Please contact your administrator");
		}

		return response;
	}
}