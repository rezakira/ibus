package com.ibus.controller;

import java.security.Principal;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.ibus.constant.RoleType;
import com.ibus.constant.StatusBus;
import com.ibus.model.ETicket;
import com.ibus.model.Order;
import com.ibus.model.RuteFrom;
import com.ibus.model.RuteTo;
import com.ibus.model.ScheduleBus;
import com.ibus.model.SitOrder;
import com.ibus.service.DataMasterManagementService;
import com.ibus.service.TransactionManagementService;

import co.id.rokubi.constant.StatusType;
import co.id.rokubi.controller.AbstractController;
import co.id.rokubi.exception.InternalServiceException;
import co.id.rokubi.exception.InvalidParamException;
import co.id.rokubi.model.JsonResponse;
import co.id.rokubi.model.Page;
import co.id.rokubi.model.TabelData;
import co.id.rokubi.service.AppManagementService;
import co.id.rokubi.service.UserManagementService;

import com.ibus.controller.bean.SitOrderBean;
import com.ibus.dao.ETicketMapper;
import com.ibus.dao.OrderMapper;
import com.ibus.dao.RuteFromMapper;
import com.ibus.dao.RuteToMapper;
import com.ibus.dao.ScheduleBusMapper;
import com.ibus.dao.ScheduleOnTimeMapper;


@Controller
@RequestMapping("/transaction")
public class TransactionController extends AbstractController {

	
	@Autowired
	private TransactionManagementService transactionManagementService;
	
	@Autowired
	private UserManagementService userManagementService;
	
	@Autowired
	private DataMasterManagementService dataMasterManagementService;
	
	@Autowired
	private AppManagementService appManagementService;
	
	@Autowired
	private OrderMapper orderMapper;
	
	@Autowired
	private ETicketMapper eTicketMapper;
	
	@Autowired
	private ScheduleBusMapper scheduleBusMapper;
	
	@Autowired
	private RuteFromMapper ruteFromMapper;
	
	@Autowired
	private RuteToMapper ruteToMapper;
	
	/* Order Management */

	private static final String LIST_ORDER = "order/list_order";
	private static final String ADD_ORDER = "order/add_order";
	private static final String UPDATE_ORDER = "order/update_order";
	private static final String CHECK_QR_CODE = "order/check_qr_code";
	private static final String DETAIL_ORDER = "order/detail_order";
	
	@RequestMapping(value = "list_order.page", method = RequestMethod.GET)
	public ModelAndView listOrderPage(ModelMap model,
			final HttpServletRequest request, Principal principal,
			HttpSession httpSession) throws InvalidParamException,
			InternalServiceException {
		ModelAndView modelView = new ModelAndView(LIST_ORDER);
		return modelView;
	}
	
	@RequestMapping(value = "check_qr_code.page", method = RequestMethod.GET)
	public ModelAndView checkQrCodePage(ModelMap model,
			final HttpServletRequest request, Principal principal,
			HttpSession httpSession) throws InvalidParamException,
			InternalServiceException {
		ModelAndView modelView = new ModelAndView(CHECK_QR_CODE);
		
		return modelView;
	}

	@RequestMapping(value = "add_order.page", method = RequestMethod.GET)
	public ModelAndView addOrderPage(ModelMap model,
			final HttpServletRequest request, Principal principal,
			HttpSession httpSession) throws InvalidParamException,
			InternalServiceException {
		ModelAndView modelView = new ModelAndView(ADD_ORDER);
		modelView.addObject("list_member", userManagementService.findUserByRoleId(RoleType.MEMBER.getValue()));
		
		Map<String, String> params = new HashMap<>();
		params.put("STATUS", StatusType.ACTIVE.getValue());
		
		modelView.addObject("list_schedule", dataMasterManagementService.getAllScheduleBusByParam(params));
		modelView.addObject("list_rute_from", dataMasterManagementService.getAllRuteFromByParam(params));
		//modelView.addObject("list_rute_to", dataMasterManagementService.getAllRuteToByParam(params));
		
		return modelView;
	}

	@Autowired
	private ScheduleOnTimeMapper scheduleOnTimeMapper;
	
	@RequestMapping(value = "update_order.page/{id_order}", method = RequestMethod.GET)
	public ModelAndView updateOrderPage(
			@PathVariable String id_order,
			ModelMap model, final HttpServletRequest request,
			Principal principal, HttpSession httpSession)
			throws InvalidParamException, InternalServiceException {
		ModelAndView modelView = new ModelAndView(UPDATE_ORDER);
		modelView.addObject("orderDetails", transactionManagementService.getOrderDetailByID(id_order));
		modelView.addObject("list_member", userManagementService.findUserByRoleId(RoleType.MEMBER.getValue()));
		modelView.addObject("eTicketDetails", transactionManagementService.getETicketDetailByIDOrder(id_order));
		
		Map<String, String> params = new HashMap<>();
		params.put("STATUS", StatusType.ACTIVE.getValue());
		
		modelView.addObject("list_schedule", dataMasterManagementService.getAllScheduleBusByParam(params));
		modelView.addObject("list_rute_from", dataMasterManagementService.getAllRuteFromByParam(params));
		
		return modelView;
	}
	
	@RequestMapping(value = "/order/data-table.json", method = RequestMethod.GET)
	@ResponseBody
	public TabelData listOrderDataTable(
			@RequestParam("sEcho") String echo,
			@RequestParam("iDisplayStart") Integer offset,
			@RequestParam("iDisplayLength") Integer limit,
			@RequestParam(value = "sSearch", required = false) String searchByName) throws InvalidParamException, InternalServiceException {

		TabelData tabelData = new TabelData();
		tabelData.setsEcho(echo);
		// System.out.println("keyString:"+searchByName);
		Page page = transactionManagementService.findPageOrder(offset, limit, searchByName);

		tabelData.setsEcho(echo);
		tabelData.setiTotalDisplayRecords(page.getTotalRow());
		tabelData.setiTotalRecords(10);
		tabelData.setAaData(page.getObj());
		return tabelData;
	}
	
	
	@Transactional
	@RequestMapping(value = "saveOrder.json", method = RequestMethod.POST)
	@ResponseBody
	public JsonResponse actionSaveOrder(@ModelAttribute("order") Order order,
			BindingResult result) throws InvalidParamException,
			InternalServiceException {

		JsonResponse response = new JsonResponse();
		List<SitOrderBean> list_sit_order = new ArrayList<SitOrderBean>();
		
		try {
			
			order = transactionManagementService.saveOrder(order);
			list_sit_order = transactionManagementService.findPageSitOrder(order.getIdOrder());
			order.setListSit(list_sit_order);
			
			
			response.setMessage(JsonResponse.SUCCESS, "Success.");
			response.setObj(order);
			
		} catch (Exception e) {
			// logger.error(e.getMessage());
			response.setMessage(JsonResponse.VALIDATE_ERROR,
					"Internal Server error. Please contact your administrator");
		}

		return response;
	}

	@Transactional
	@RequestMapping(value = "updateOrder.json", method = RequestMethod.POST)
	@ResponseBody
	public JsonResponse actionUpdateOrder(@ModelAttribute("order") Order order,
			BindingResult result) throws InvalidParamException,
			InternalServiceException {

		JsonResponse response = new JsonResponse();
		try {
			Order orderOldData = transactionManagementService.getOrderDetailByID(order.getIdOrder());

			order.setDateAdded(orderOldData.getDateAdded());
			order.setQrcode(orderOldData.getQrcode());
			
			order = transactionManagementService.updateOrder(order);
			response.setMessage(JsonResponse.SUCCESS, "Success.");
			response.setObj(order);
		} catch (Exception e) {
			// logger.error(e.getMessage());
			response.setMessage(JsonResponse.VALIDATE_ERROR,
					"Internal Server error. Please contact your administrator");
		}

		return response;
	}
	
	

	
	
	@Transactional
	@RequestMapping(value = "saveSitOrder.json", method = RequestMethod.POST)
	@ResponseBody
	public JsonResponse actionSaveSitOrder(@ModelAttribute("sit_order") SitOrder sit_order,
			BindingResult result) throws InvalidParamException,
			InternalServiceException {

		JsonResponse response = new JsonResponse();
		try {
			sit_order.setStatus(StatusBus.BOOKED.getValue());
			sit_order = transactionManagementService.saveSitOrder(sit_order);
			response.setMessage(JsonResponse.SUCCESS, "Success.");
			response.setObj(sit_order);
			
		} catch (Exception e) {
			// logger.error(e.getMessage());
			response.setMessage(JsonResponse.VALIDATE_ERROR,
					"Internal Server error. Please contact your administrator");
		}

		return response;
	}

	@Transactional
	@RequestMapping(value = "updateSitOrder.json", method = RequestMethod.POST)
	@ResponseBody
	public JsonResponse actionUpdateSitOrder(@ModelAttribute("sit_order") SitOrder sit_order,
			BindingResult result) throws InvalidParamException,
			InternalServiceException {

		JsonResponse response = new JsonResponse();
		try {
			
			sit_order = transactionManagementService.updateSitOrder(sit_order);
			response.setMessage(JsonResponse.SUCCESS, "Success.");
			response.setObj(sit_order);
			
		} catch (Exception e) {
			// logger.error(e.getMessage());
			response.setMessage(JsonResponse.VALIDATE_ERROR,
					"Internal Server error. Please contact your administrator");
		}

		return response;
	}
	
	
	@Transactional
	@RequestMapping(value = "removeSitOrder.json", method = RequestMethod.POST)
	@ResponseBody
	public JsonResponse actionRemoveSitOrder(@RequestParam("id_sit_order") String id_sit_order) 
			throws InvalidParamException,
			InternalServiceException {

		JsonResponse response = new JsonResponse();
		try {
			
			transactionManagementService.deleteSitOrderByID(id_sit_order);
			response.setMessage(JsonResponse.SUCCESS, "Success.");
			
		} catch (Exception e) {
			// logger.error(e.getMessage());
			response.setMessage(JsonResponse.VALIDATE_ERROR,
					"Internal Server error. Please contact your administrator");
		}

		return response;
	}
	
	@RequestMapping(value = "/order/data-table-sit.json/{id_order}", method = RequestMethod.GET)
	@ResponseBody
	public TabelData listDetailScheduleOnTimeTable(
			@PathVariable String id_order,
			@RequestParam("sEcho") String echo,
			@RequestParam("iDisplayStart") Integer offset,
			@RequestParam("iDisplayLength") Integer limit,
			@RequestParam(value = "sSearch", required = false) String searchByName) {

		TabelData tabelData = new TabelData();
		tabelData.setsEcho(echo);
		// System.out.println("keyString:"+searchByName);
		Page page = transactionManagementService.findPageSit(id_order, offset, limit,
				searchByName);

		tabelData.setsEcho(echo);
		tabelData.setiTotalDisplayRecords(page.getTotalRow());
		tabelData.setiTotalRecords(10);
		tabelData.setAaData(page.getObj());

		return tabelData;
	}
	
	@Transactional
	@RequestMapping(value = "checkqr.json", method = RequestMethod.POST)
	@ResponseBody
	public JsonResponse actionCheckQR() 
			throws InvalidParamException,
			InternalServiceException {

		JsonResponse response = new JsonResponse();
		try {
			
			response.setMessage(JsonResponse.SUCCESS, "Success.");
			
		} catch (Exception e) {
			// logger.error(e.getMessage());
			response.setMessage(JsonResponse.VALIDATE_ERROR,
					"Internal Server error. Please contact your administrator");
		}

		return response;
	}
	
	@RequestMapping(value = "order/detail_order.page/{uuid}", method = RequestMethod.GET)
	public ModelAndView detailOrderPage(
			@PathVariable String uuid,
			ModelMap model, final HttpServletRequest request,
			Principal principal, HttpSession httpSession)
			throws InvalidParamException, InternalServiceException {
		ModelAndView modelView = new ModelAndView(DETAIL_ORDER);
		ETicket eTicket = eTicketMapper.selectByID(uuid);
		String idOrder = eTicket.getIdOrder();
		Order order = orderMapper.selectByPrimaryKey(idOrder);
		modelView.addObject("orderDetails", order);
		ScheduleBus scheduleBus = scheduleBusMapper.selectByPrimaryKey(order.getScheduleId());
		modelView.addObject("scheduleDetails", scheduleBus);
		RuteFrom ruteFrom = ruteFromMapper.selectByPrimaryKey(order.getIdRuteFrom());
		modelView.addObject("ruteFromDetails", ruteFrom);
		RuteTo ruteTo = ruteToMapper.selectByPrimaryKey(order.getIdRuteTo());
		modelView.addObject("ruteToDetails", ruteTo);
		Date orderDate = order.getOrderDate();
		DateFormat df = new SimpleDateFormat("yyyy/MM/dd");
		String stringOrderDate = df.format(orderDate);
		modelView.addObject("orderDateDetails", stringOrderDate);
		return modelView;
	}
	
	@Transactional
	@RequestMapping(value = "proceed_order.json", method = RequestMethod.POST)
	@ResponseBody
	public JsonResponse actionProceedOrder(@ModelAttribute("order") Order order,
			BindingResult result) throws InvalidParamException,
			InternalServiceException {

		JsonResponse response = new JsonResponse();
		try {
			Order orderOldData = transactionManagementService.getOrderDetailByID(order.getIdOrder());

			order.setDateAdded(orderOldData.getDateAdded());
			order.setQrcode(orderOldData.getQrcode());
			order.setStatus("5");
			order.setPriceTicketUnit(orderOldData.getPriceTicketUnit());
			order = transactionManagementService.updateOrder(order);
			response.setMessage(JsonResponse.SUCCESS, "Success.");
			response.setObj(order);
		} catch (Exception e) {
			// logger.error(e.getMessage());
			response.setMessage(JsonResponse.VALIDATE_ERROR,
					"Internal Server error. Please contact your administrator");
		}

		return response;
	}

}
