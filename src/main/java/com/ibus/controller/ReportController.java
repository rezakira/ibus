package com.ibus.controller;

import java.security.Principal;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import co.id.rokubi.controller.AbstractController;
import co.id.rokubi.exception.InternalServiceException;
import co.id.rokubi.exception.InvalidParamException;

@Controller
@RequestMapping("/report")
public class ReportController extends AbstractController {
	private static final String REPORT_DEFECT = "help/userGuide"; //path to view
	
	
	/**
	 * access http://localhost/fine/report/report-defect.page
	 * @param model
	 * @param request
	 * @param principal
	 * @param httpSession
	 * @return
	 * @throws InvalidParamException
	 * @throws InternalServiceException
	 */
	@RequestMapping(value = "report-defect.page", method = RequestMethod.GET)	
	public ModelAndView direktoratListPage(ModelMap model, final HttpServletRequest request,
			Principal principal, HttpSession httpSession) throws InvalidParamException, InternalServiceException {

		ModelAndView modelView = new ModelAndView(REPORT_DEFECT);

		return modelView;
	}
	
}
