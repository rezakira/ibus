package com.ibus.controller;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;









import com.ibus.constant.StatusBeritaPromo;
import com.ibus.constant.StatusBus;
import com.ibus.constant.StatusOrder;
import com.ibus.controller.bean.Destination;
import com.ibus.controller.bean.SitOrderBean;
import com.ibus.model.BeritaPromo;
import com.ibus.model.Order;
import com.ibus.model.RuteFrom;
import com.ibus.model.RuteTo;
import com.ibus.model.ScheduleBus;
import com.ibus.model.ScheduleOnTime;
import com.ibus.model.SitOrder;
import com.ibus.service.BeritaPromoManagementService;
import com.ibus.service.DataMasterManagementService;
import com.ibus.service.TransactionManagementService;

import co.id.rokubi.constant.StatusType;
import co.id.rokubi.controller.AbstractController;
import co.id.rokubi.controller.bean.Chair;
import co.id.rokubi.controller.bean.ReservationSitBean;
import co.id.rokubi.controller.bean.ReservationTicketBean;
import co.id.rokubi.controller.bean.UserLogin;
import co.id.rokubi.controller.bean.UserRegister;
import co.id.rokubi.exception.InternalServiceException;
import co.id.rokubi.exception.InvalidParamException;
import co.id.rokubi.model.JsonResponse;
import co.id.rokubi.model.User;
import co.id.rokubi.service.UserManagementService;
import co.id.rokubi.util.CommonUtil;

@Controller
@RequestMapping("/mobile-api")
public class MobileServiceController extends AbstractController {

		
	@Autowired
	private UserManagementService userManagementService;
	
	@RequestMapping(value = "register-member.json" , method = RequestMethod.POST)
	@ResponseBody
	public JsonResponse registrationMember(
			@RequestBody UserRegister userRegister) throws InvalidParamException, InternalServiceException
	{
		 JsonResponse response = new JsonResponse();
		 
		 if(userManagementService.isUserExist(userRegister.getUsername()))
		 {
			 response = new JsonResponse(JsonResponse.VALIDATE_ERROR, "Register Gagal , User sudah terdaftar di sistem !!");
			 return response;
		 }
		 
		 try
		 {
			 
			User user = new User();
			user.setUsername(userRegister.getUsername());
			user.setFirstName(userRegister.getFirstName());
			user.setLastName(userRegister.getLastName());
			user.setEmail(userRegister.getEmail());
			user.setPassword(userRegister.getPassword());
			
			 
			user.setRoleId("MBM"); //Member
			//user.setUserNo(user.getUserNo());
			
			user.setIsAccountNonExpired(true);
			user.setIsAccountNonLocked(true);
			user.setIsCredentialsNonExpired(true);
			
			//public member langsung aktif
			user.setIsEnabled(true);
			user.setStatus("A");
			
			user.setCreateDate(new Date());
			user.setPublishDate(new Date());
			user.setPublisher("admin");
			user.setImageUrl(null);
			
			userManagementService.saveUser(user, null);
			response = new JsonResponse(JsonResponse.SUCCESS, "Success");
		 }
		 catch(Exception ex)
		 {
			 response = new JsonResponse(JsonResponse.VALIDATE_ERROR, "Failed");
		 }
		 
		 return response;
	}
	
	

	@RequestMapping(value = "login-member.json" , method = RequestMethod.POST)
	@ResponseBody
	public JsonResponse loginMember(
			@RequestBody UserLogin userLogin) throws InvalidParamException, InternalServiceException
	{
		JsonResponse response = new JsonResponse();
		User userGet = new User();
	
		
		
		try {
			
			if(CommonUtil.isNullOrEmpty(userLogin.getUsername()))
			{
				response = new JsonResponse(JsonResponse.VALIDATE_ERROR, 
						"Username Kosong,Anda Harus mengisi Username");
				return response;
			}
			
			if(CommonUtil.isNullOrEmpty(userLogin.getPassword()))
			{
				response = new JsonResponse(JsonResponse.VALIDATE_ERROR,  
						"Password Anda Kosong,Anda Harus mengisi password");
				return response;
			}


			//existing user
			if(userManagementService.isUserExist(userLogin.getUsername()) == false)
			{
				response = new JsonResponse(JsonResponse.VALIDATE_ERROR, 
						"Username Tidak Terdaftar Pada Sistem");
				return response;
			}
			else
			{
				userGet = userManagementService.findUserByUsername(userLogin.getUsername());
			}
			
	
			if(CommonUtil.isNullOrEmpty(userGet.getStatus()))
			{
				response.setMessage(JsonResponse.VALIDATE_ERROR,"Account Anda Belum Di Autorisasi");
				return response;
			}
			else
			if(userGet.getStatus().equals("D"))
			{
				response.setMessage(JsonResponse.VALIDATE_ERROR,"Account Anda Belum Di Autorisasi");
				return response;
			}
			
			if(userManagementService.checkValidateUser(userLogin.getUsername(), userLogin.getPassword()))
			{
				response.setMessage(JsonResponse.SUCCESS, "Success.");
				response.setObj(userLogin.getUsername());
			}
			else
			{
				response.setMessage(JsonResponse.VALIDATE_ERROR, "Error Validasi,Username/Password Salah !!");
			}
			
			
		} catch (Exception e) {
			logError(e.getMessage());
			response.setMessage(JsonResponse.VALIDATE_ERROR,
					"Internal Server error. Please contact your administrator");
		}
		logInfo("finish :loginMember");
		return response;
	}
	

	
	

	@Autowired
	private TransactionManagementService trsManagementService;
	
	@RequestMapping(value = "reservation-ticket.json" , method = RequestMethod.POST)
	@ResponseBody
	public JsonResponse reservationTicket(
			@RequestBody ReservationTicketBean reservationTicketBean) throws InvalidParamException, InternalServiceException
	{
		JsonResponse response = new JsonResponse();
		User userGet = new User();
		Order order = new Order();
		

		try {
			
			if(CommonUtil.isNullOrEmpty(reservationTicketBean.getUser_id()))
			{
				response = new JsonResponse(JsonResponse.VALIDATE_ERROR, 
						"Username Kosong,Anda Harus mengisi Username");
				return response;
			}
			

			//existing user
			if(userManagementService.isUserExist(reservationTicketBean.getUser_id()) == false)
			{
				response = new JsonResponse(JsonResponse.VALIDATE_ERROR, 
						"Username Tidak Terdaftar Pada Sistem");
				return response;
			}
			else
			{
				userGet = userManagementService.findUserByUsername(reservationTicketBean.getUser_id());
			}
			
	
			if(CommonUtil.isNullOrEmpty(userGet.getStatus()))
			{
				response.setMessage(JsonResponse.VALIDATE_ERROR,"Account Anda Belum Di Autorisasi");
				return response;
			}
			else
			if(userGet.getStatus().equals("D"))
			{
				response.setMessage(JsonResponse.VALIDATE_ERROR,"Account Anda Belum Di Autorisasi");
				return response;
			}
			
			
			//Save Order
			Date currentTime = new Date();
			
			order.setIdRuteFrom(Long.valueOf(reservationTicketBean.getFrom_destination()));
			order.setIdRuteTo(Long.valueOf(reservationTicketBean.getTo_destination()));
			DateFormat format = new SimpleDateFormat("yyyy/MM/dd");
			order.setOrderDate(format.parse(reservationTicketBean.getDate_reservation()));
			order.setScheduleId(Long.valueOf(reservationTicketBean.getTime_reservation()));
			order.setStatus(StatusOrder.BOOKING.getValue());
			order.setTotalPerson(reservationTicketBean.getCount_passanger());
			order.setUserId(reservationTicketBean.getUser_id());
			
			//Set Pricce
			RuteTo ruteTo = dataMasterManagementService.getRuteToDetailByID(order.getIdRuteTo());
			order.setDiscount((float) 0);
			order.setPriceTicketUnit(ruteTo.getPriceTicketUnit());
			order.setQrcode("");
			order.setTotalPrice((order.getTotalPerson()*order.getPriceTicketUnit()));
			
			order = trsManagementService.saveOrder(order);
			reservationTicketBean.setBooked_id(order.getIdOrder());
			reservationTicketBean.setAdded(currentTime);
			reservationTicketBean.setModifed(currentTime);
			
			
			
			//sit generator
			List<SitOrderBean> list_sit_order = new ArrayList<SitOrderBean>();
			list_sit_order = trsManagementService.findPageSitOrder(order.getIdOrder());
			ScheduleOnTime busOntime = null;
			Long idBus = Long.valueOf(0);
			
			List<Chair> list_sit_available = new ArrayList<Chair>();
			if(CommonUtil.isNotNullOrEmpty(list_sit_order.get(0).getScheduleOT()))
			{
				busOntime = dataMasterManagementService.getScheduleOnTimeDetailByID(list_sit_order.get(0).getScheduleOT());
				idBus = busOntime.getIdBus();
			}

			for (SitOrderBean obj : list_sit_order) {
				
				Chair chair = new Chair();
				chair.setAvailable(Integer.valueOf(obj.getStatus())); //0 or 1
				chair.setBis_id_ref(String.valueOf(idBus));
				chair.setSit_id(obj.getIdSitOrder());
				chair.setTime_schedule_id(String.valueOf(obj.getScheduleOT()));
				
				list_sit_available.add(chair);
			}
				
			
			
			HashMap< String, Object> results = new HashMap<String,Object>();
			results.put("review_reservation", reservationTicketBean);
			results.put("list_sit_available", list_sit_available);
			
			
			response.setMessage(JsonResponse.SUCCESS,"Success");
			response.setObj(results);
			
		} catch (Exception e) {
			logError(e.getMessage());
			response.setMessage(JsonResponse.VALIDATE_ERROR,
					"Internal Server error. Please contact your administrator");
		}
		
		logInfo("finish :reservationTicket");
		return response;
	}
	
	
	@RequestMapping(value = "reservation-sit.json" , method = RequestMethod.POST)
	@ResponseBody
	public JsonResponse reservationSit(
			@RequestBody ReservationSitBean reservationSit) throws InvalidParamException, InternalServiceException
	{
		JsonResponse response = new JsonResponse();

		try {
			Date currentTime = new Date();
			reservationSit.setAdded(currentTime);
			reservationSit.setModifed(currentTime);
			
			List<String> listSitOrder = reservationSit.getList_sit_booked();
			for (int i = 0; i < listSitOrder.size(); i++) {
				SitOrder sitOrder = new SitOrder();
				sitOrder.setStatus(StatusBus.BOOKED.getValue());
				sitOrder.setIdOrderRef(reservationSit.getBooked_id());
				sitOrder.setIdSitOrder(listSitOrder.get(i));
				String[] parts = sitOrder.getIdSitOrder().split("-");
				sitOrder.setNoKursi(Integer.valueOf(parts[4]));
				sitOrder.setScheduleOtRefId(trsManagementService.checkSitOrder(reservationSit.getBooked_id()));
				
				trsManagementService.saveSitOrder(sitOrder);
			}
			
			response.setMessage(JsonResponse.SUCCESS,"Success");
			response.setObj(reservationSit);
		} catch (Exception e) {
			logError(e.getMessage());
			response.setMessage(JsonResponse.VALIDATE_ERROR,
					"Internal Server error. Please contact your administrator");
		}
		
		logInfo("finish :reservationSit");
		return response;
	}
	
	
	
	@RequestMapping(value = "get-information-reservation.json" , method = RequestMethod.GET)
	@ResponseBody
	public JsonResponse getInformationReservation() throws InvalidParamException, InternalServiceException
	{
		JsonResponse response = new JsonResponse();
	
		try {
			
			List<Destination> list_from = new ArrayList<Destination>();
			
			Map<String, String> params = new HashMap<>();
			params.put("STATUS", StatusType.ACTIVE.getValue());
			
			List<RuteFrom> result_rute_from = 
					dataMasterManagementService.getAllRuteFromByParam(params);
			for (RuteFrom ruteFrom : result_rute_from) {
				list_from.add(new Destination(ruteFrom.getIdRuteFrom().intValue(), ruteFrom.getNameRuteFrom()));
			}
						
			List<Destination> list_to = new ArrayList<Destination>();

			
			List<Destination> list_time_schedule = new ArrayList<Destination>();
			List<ScheduleBus> result_schedule = 
					dataMasterManagementService.getAllScheduleBusByParam(params);
			for (ScheduleBus scheduleBus : result_schedule) {
				Destination temp = new Destination();
				
				temp.setId(scheduleBus.getIdSchedule().intValue());
				temp.setValue(scheduleBus.getTimeStart()+"-"+scheduleBus.getTimeEnd());
				list_time_schedule.add(temp);
			}
			
			
			
			HashMap< String, Object> results = new HashMap<String,Object>();
			results.put("list_from", list_from);
			results.put("list_to", list_to);
			results.put("list_time_schedule", list_time_schedule);
			
			
			response.setMessage(JsonResponse.SUCCESS,"Success");
			response.setObj(results);
			
			
		} catch (Exception e) {
			logError(e.getMessage());
			response.setMessage(JsonResponse.VALIDATE_ERROR,
					"Internal Server error. Please contact your administrator");
		}
		
		logInfo("finish :getInformationReservation");
		return response;
	}
	
	@Autowired
	private DataMasterManagementService dataMasterManagementService;
	
	@RequestMapping(value = "get-information-to-destination.json" , method = RequestMethod.GET)
	@ResponseBody
	public JsonResponse getInformationToDestination(
			@RequestParam("id_from") String id_from) throws InvalidParamException, InternalServiceException
	{
		JsonResponse response = new JsonResponse();
	
		try {
			
			Map<String, String> params = new HashMap<>();
			params.put("STATUS", StatusType.ACTIVE.getValue());
			params.put("ID_RUTE_FROM_REF", id_from);
			
			List<Destination> tempList = new ArrayList<Destination>();
			List<RuteTo> result = 
					dataMasterManagementService.getAllRuteToByParam(params);
			for (RuteTo ruteTo : result) {
				tempList.add(new Destination(ruteTo.getIdRuteTo().intValue(), ruteTo.getNameRuteTo()));
			}
			
			response.setMessage(JsonResponse.SUCCESS,"Success");
			response.setObj(tempList);
			
			
		} catch (Exception e) {
			logError(e.getMessage());
			response.setMessage(JsonResponse.VALIDATE_ERROR,
					"Internal Server error. Please contact your administrator");
		}
		
		logInfo("finish :getInformationReservation");
		return response;
	}
	
	
	@Autowired
	private BeritaPromoManagementService beritaPromoManagementService;
	
	@RequestMapping(value = "get-all-berita-promo.json" , method = RequestMethod.GET)
	@ResponseBody
	public JsonResponse getAllBeritaPromo() throws InvalidParamException, InternalServiceException
	{
		
		JsonResponse response = new JsonResponse();
		List<BeritaPromo> listBeritaPromo = new ArrayList<>();
		Map<String, String> params = new HashMap<>();
		params.put("STATUS", StatusBeritaPromo.PUBLISH.getValue());
		try {
			listBeritaPromo = beritaPromoManagementService.getAllBeritaPromoByParam(params);
			
			response.setMessage(JsonResponse.SUCCESS,"Success");
			response.setObj(listBeritaPromo);
			
			
		} catch (Exception e) {
			logError(e.getMessage());
			response.setMessage(JsonResponse.VALIDATE_ERROR,
					"Internal Server error. Please contact your administrator");
		}
		
		logInfo("finish :getAllBeritaPromo");
		return response;
	}
	
	@RequestMapping(value = "get-berita-promo-detail.json" , method = RequestMethod.GET)
	@ResponseBody
	public JsonResponse getBeritaPromoDetail(@RequestParam("id") String id) throws InvalidParamException, InternalServiceException
	{
		
		JsonResponse response = new JsonResponse();
		BeritaPromo beritaPromo = new BeritaPromo();
		try {
			beritaPromo = beritaPromoManagementService.getBeritaPromoDetailByID(Long.valueOf(id));
			
			response.setMessage(JsonResponse.SUCCESS,"Success");
			response.setObj(beritaPromo);
			
			
		} catch (Exception e) {
			logError(e.getMessage());
			response.setMessage(JsonResponse.VALIDATE_ERROR,
					"Internal Server error. Please contact your administrator");
		}
		
		logInfo("finish :getBeritaPromoDetail");
		return response;
	}
	
	@RequestMapping(value = "get-all-order.json" , method = RequestMethod.GET)
	@ResponseBody
	public JsonResponse getAllOrder(@RequestParam("user_id") String user_id) throws InvalidParamException, InternalServiceException
	{
		
		JsonResponse response = new JsonResponse();
		List<Order> listOrder = new ArrayList<Order>();
		Map<String, String> params = new HashMap<>();
		params.put("USER_ID", user_id);
		
		try {
			listOrder = trsManagementService.getAllOrderByParam(params);
			
			response.setMessage(JsonResponse.SUCCESS,"Success");
			response.setObj(listOrder);
			
			
		} catch (Exception e) {
			logError(e.getMessage());
			response.setMessage(JsonResponse.VALIDATE_ERROR,
					"Internal Server error. Please contact your administrator");
		}
		
		logInfo("finish :getAllOrder");
		return response;
	}
	
	
	@RequestMapping(value = "get-order-detail.json" , method = RequestMethod.GET)
	@ResponseBody
	public JsonResponse getOrderDetail(@RequestParam("order_id") String order_id) throws InvalidParamException, InternalServiceException
	{
		
		JsonResponse response = new JsonResponse();
		Order orderDetail = new Order();
		try {
			orderDetail = trsManagementService.getOrderDetailByID(order_id);
			
			response.setMessage(JsonResponse.SUCCESS,"Success");
			response.setObj(orderDetail);
			
			
		} catch (Exception e) {
			logError(e.getMessage());
			response.setMessage(JsonResponse.VALIDATE_ERROR,
					"Internal Server error. Please contact your administrator");
		}
		
		logInfo("finish :getOrderDetail");
		return response;
	}
}
