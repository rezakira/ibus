package com.ibus.controller.bean;

import java.util.Date;

import com.ibus.constant.StatusBus;

public class SitOrderBean {

	private String idSitOrder;
	private Integer noKursi;
	private String status;
	private Date lastUpdate;
	private String dateStringLastUpdate;
	private String statusString;
	private Long scheduleOT;

	public Long getScheduleOT() {
		return scheduleOT;
	}

	public void setScheduleOT(Long scheduleOT) {
		this.scheduleOT = scheduleOT;
	}

	public String getStatusString() {
		if (status.equals(StatusBus.AVAILABLE.getValue())) {
			statusString = "Available";
		} else if (status.equals(StatusBus.RESERVED.getValue())) {
			statusString = "Reserved";
		} else if (status.equals(StatusBus.BOOKED.getValue())) {
			statusString = "Booked";
		}
		return statusString;
	}

	public void setStatusString(String statusString) {
		this.statusString = statusString;
	}

	public String getDateStringLastUpdate() {
		return dateStringLastUpdate;
	}

	public void setDateStringLastUpdate(String dateStringLastUpdate) {
		this.dateStringLastUpdate = dateStringLastUpdate;
	}

	public String getIdSitOrder() {
		return idSitOrder;
	}

	public void setIdSitOrder(String idSitOrder) {
		this.idSitOrder = idSitOrder;
	}

	public Integer getNoKursi() {
		return noKursi;
	}

	public void setNoKursi(Integer noKursi) {
		this.noKursi = noKursi;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public Date getLastUpdate() {
		return lastUpdate;
	}

	public void setLastUpdate(Date lastUpdate) {
		this.lastUpdate = lastUpdate;
	}

}
