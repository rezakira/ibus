package com.ibus.controller.bean;

import java.math.BigDecimal;

public class DashboardBean {
	 
	private String 	namaIndikator;
	private Integer q;
		
	private BigDecimal valYear;
	private BigDecimal valYear_1;
	private BigDecimal valYear_2;
	private BigDecimal valYear_3;
	private BigDecimal valYear_4;
	
	private BigDecimal lastValTarget;
	
	public String getNamaIndikator() {
		return namaIndikator;
	}
	public void setNamaIndikator(String namaIndikator) {
		this.namaIndikator = namaIndikator;
	}
	public Integer getQ() {
		return q;
	}
	public void setQ(Integer q) {
		this.q = q;
	}
	public BigDecimal getValYear() {
		return valYear;
	}
	public void setValYear(BigDecimal valYear) {
		this.valYear = valYear;
	}
	public BigDecimal getValYear_1() {
		return valYear_1;
	}
	public void setValYear_1(BigDecimal valYear_1) {
		this.valYear_1 = valYear_1;
	}
	public BigDecimal getValYear_2() {
		return valYear_2;
	}
	public void setValYear_2(BigDecimal valYear_2) {
		this.valYear_2 = valYear_2;
	}
	public BigDecimal getValYear_3() {
		return valYear_3;
	}
	public void setValYear_3(BigDecimal valYear_3) {
		this.valYear_3 = valYear_3;
	}
	public BigDecimal getValYear_4() {
		return valYear_4;
	}
	public void setValYear_4(BigDecimal valYear_4) {
		this.valYear_4 = valYear_4;
	}
	public BigDecimal getLastValTarget() {
		return lastValTarget;
	}
	public void setLastValTarget(BigDecimal lastValTarget) {
		this.lastValTarget = lastValTarget;
	}
}