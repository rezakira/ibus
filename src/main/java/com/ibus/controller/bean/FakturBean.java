package com.ibus.controller.bean;

import org.springframework.web.multipart.MultipartFile;

public class FakturBean {
	private MultipartFile file_template;

	public MultipartFile getFile_template() {
		return file_template;
	}

	public void setFile_template(MultipartFile file_template) {
		this.file_template = file_template;
	}

	
}
