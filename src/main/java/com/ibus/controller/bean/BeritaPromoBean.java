package com.ibus.controller.bean;

import org.springframework.web.multipart.MultipartFile;

import com.ibus.model.BeritaPromo;;

/**
 * @author Radian
 *
 */
public class BeritaPromoBean extends BeritaPromo{

	private MultipartFile image;

	/**
	 * @return the image
	 */
	public MultipartFile getImage() {
		return image;
	}

	/**
	 * @param image the image to set
	 */
	public void setImage(MultipartFile image) {
		this.image = image;
	}

}
