package com.ibus.controller.bean;

public class Destination {
	
	private Integer id;
	private String value;
	
	public Destination()
	{
		
	}
	
	public Destination(Integer id,String value)
	{
		this.id = id;
		this.value = value;
	}
	
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getValue() {
		return value;
	}
	public void setValue(String value) {
		this.value = value;
	}
	

}
