package com.ibus.controller;

import java.security.Principal;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;


import co.id.rokubi.controller.AbstractController;
import co.id.rokubi.exception.InternalServiceException;
import co.id.rokubi.exception.InvalidParamException;

@Controller
@RequestMapping("/help")
public class HelpController extends AbstractController {

	private static final String HELP = "help/userGuide";

	@RequestMapping(value = "userGuide.page", method = RequestMethod.GET)
	public ModelAndView ReportPage(ModelMap model,
			final HttpServletRequest request, Principal principal,
			HttpSession httpSession) throws InvalidParamException,
			InternalServiceException {
		ModelAndView modelView = new ModelAndView(HELP);
		return modelView;
	}

	private static final String LAPOR = "help/lapor-kerusakan";

	@RequestMapping(value = "lapor-kerusakan.page", method = RequestMethod.GET)
	public ModelAndView Report(ModelMap model,
			final HttpServletRequest request, Principal principal,
			HttpSession httpSession) throws InvalidParamException,
			InternalServiceException {
		ModelAndView modelView = new ModelAndView(LAPOR);
		return modelView;
	}

	@Value("${email.sender.lk}")
	private String EMAIL_SENDER_LK;
	
	@Value("${email.receiver.lk}")
	private String EMAIL_RECEIVER_LK;
	
	@Value("${username.sender.lk}")
	private String USERNAME_SENDER_LK;
	
	@Value("${password.sender.lk}")
	private String PASSWORD_SENDER_LK;
	
	
}
