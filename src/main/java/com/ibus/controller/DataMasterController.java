package com.ibus.controller;

import java.security.Principal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import co.id.rokubi.constant.StatusType;
import co.id.rokubi.controller.AbstractController;
import co.id.rokubi.exception.InternalServiceException;
import co.id.rokubi.exception.InvalidParamException;
import co.id.rokubi.model.JsonResponse;
import co.id.rokubi.model.Page;
import co.id.rokubi.model.TabelData;
import co.id.rokubi.util.CommonUtil;

import com.ibus.controller.bean.Destination;
import com.ibus.model.Bus;
import com.ibus.model.RuteFrom;
import com.ibus.model.RuteTo;
import com.ibus.model.ScheduleBus;
import com.ibus.model.ScheduleOnTime;
import com.ibus.service.DataMasterManagementService;


@Controller
@RequestMapping("/data-master")
public class DataMasterController extends AbstractController {

	@Autowired
	private DataMasterManagementService dataMasterManagement;

	/* Bus Management */

	private static final String LIST_BUS = "bus/list_bus";
	private static final String ADD_BUS = "bus/add_bus";
	private static final String UPDATE_BUS = "bus/update_bus";

	@RequestMapping(value = "list_bus.page", method = RequestMethod.GET)
	public ModelAndView listBusPage(ModelMap model,
			final HttpServletRequest request, Principal principal,
			HttpSession httpSession) throws InvalidParamException,
			InternalServiceException {
		ModelAndView modelView = new ModelAndView(LIST_BUS);
		return modelView;
	}

	@RequestMapping(value = "add_bus.page", method = RequestMethod.GET)
	public ModelAndView addBusPage(ModelMap model,
			final HttpServletRequest request, Principal principal,
			HttpSession httpSession) throws InvalidParamException,
			InternalServiceException {
		ModelAndView modelView = new ModelAndView(ADD_BUS);
		return modelView;
	}

	@RequestMapping(value = "update_bus.page/{id_bus}", method = RequestMethod.GET)
	public ModelAndView updateBusPage(@PathVariable Long id_bus,
			ModelMap model, final HttpServletRequest request,
			Principal principal, HttpSession httpSession)
			throws InvalidParamException, InternalServiceException {
		ModelAndView modelView = new ModelAndView(UPDATE_BUS);
		modelView.addObject("busDetails",
				dataMasterManagement.getBusDetailByID(id_bus));
		return modelView;
	}

	@RequestMapping(value = "/bus/data-table.json", method = RequestMethod.GET)
	@ResponseBody
	public TabelData listBusDataTable(
			@RequestParam("sEcho") String echo,
			@RequestParam("iDisplayStart") Integer offset,
			@RequestParam("iDisplayLength") Integer limit,
			@RequestParam(value = "sSearch", required = false) String searchByName) {

		TabelData tabelData = new TabelData();
		tabelData.setsEcho(echo);
		// System.out.println("keyString:"+searchByName);
		Page page = dataMasterManagement.findPageBus(offset, limit,
				searchByName);

		tabelData.setsEcho(echo);
		tabelData.setiTotalDisplayRecords(page.getTotalRow());
		tabelData.setiTotalRecords(10);
		tabelData.setAaData(page.getObj());
		return tabelData;
	}

	@Transactional
	@RequestMapping(value = "saveBus.json", method = RequestMethod.POST)
	@ResponseBody
	public JsonResponse actionSaveBus(@ModelAttribute("bus") Bus bus,
			BindingResult result) throws InvalidParamException,
			InternalServiceException {

		JsonResponse response = new JsonResponse();
		try {

			dataMasterManagement.saveBus(bus);
			response.setMessage(JsonResponse.SUCCESS, "Success.");
		} catch (Exception e) {
			// logger.error(e.getMessage());
			response.setMessage(JsonResponse.VALIDATE_ERROR,
					"Internal Server error. Please contact your administrator");
		}

		return response;
	}

	@Transactional
	@RequestMapping(value = "updateBus.json", method = RequestMethod.POST)
	@ResponseBody
	public JsonResponse actionUpdateBus(@ModelAttribute("bus") Bus bus,
			BindingResult result) throws InvalidParamException,
			InternalServiceException {

		JsonResponse response = new JsonResponse();
		try {
			Bus busOldData = dataMasterManagement.getBusDetailByID(bus
					.getIdBus());

			bus.setDateAdded(busOldData.getDateAdded());
			dataMasterManagement.updateBus(bus);
			response.setMessage(JsonResponse.SUCCESS, "Success.");
		} catch (Exception e) {
			// logger.error(e.getMessage());
			response.setMessage(JsonResponse.VALIDATE_ERROR,
					"Internal Server error. Please contact your administrator");
		}

		return response;
	}

	@Transactional
	@RequestMapping(value = "deleteBus.json", method = RequestMethod.POST)
	@ResponseBody
	public JsonResponse actionDeleteBus(@RequestParam("id_long") Long id_long)
			throws InvalidParamException, InternalServiceException {

		JsonResponse response = new JsonResponse();
		try {
			dataMasterManagement.deleteBusByID(id_long);
			response.setMessage(JsonResponse.SUCCESS, "Success.");
		} catch (Exception e) {
			// logger.error(e.getMessage());
			response.setMessage(JsonResponse.VALIDATE_ERROR,
					"Internal Server error. Please contact your administrator");
		}

		return response;
	}

	/* Schedule Bus */

	private static final String LIST_SCHEDULE_BUS = "schedule_bus/list_schedule_bus";
	private static final String ADD_SCHEDULE_BUS = "schedule_bus/add_schedule_bus";
	private static final String UPDATE_SCHEDULE_BUS = "schedule_bus/update_schedule_bus";

	@RequestMapping(value = "list_schedule_bus.page", method = RequestMethod.GET)
	public ModelAndView listScheduleBusPage(ModelMap model,
			final HttpServletRequest request, Principal principal,
			HttpSession httpSession) throws InvalidParamException,
			InternalServiceException {
		ModelAndView modelView = new ModelAndView(LIST_SCHEDULE_BUS);
		return modelView;
	}

	@RequestMapping(value = "add_schedule_bus.page", method = RequestMethod.GET)
	public ModelAndView addScheduleBusPage(ModelMap model,
			final HttpServletRequest request, Principal principal,
			HttpSession httpSession) throws InvalidParamException,
			InternalServiceException {
		ModelAndView modelView = new ModelAndView(ADD_SCHEDULE_BUS);
		return modelView;
	}

	@RequestMapping(value = "update_schedule_bus.page/{id_schedule_bus}", method = RequestMethod.GET)
	public ModelAndView updateScheduleBusPage(
			@PathVariable Long id_schedule_bus, ModelMap model,
			final HttpServletRequest request, Principal principal,
			HttpSession httpSession) throws InvalidParamException,
			InternalServiceException {
		ModelAndView modelView = new ModelAndView(UPDATE_SCHEDULE_BUS);
		modelView.addObject("scheduleBusDetails",
				dataMasterManagement.getScheduleBusDetailByID(id_schedule_bus));
		return modelView;
	}

	@RequestMapping(value = "/schedule_bus/data-table.json", method = RequestMethod.GET)
	@ResponseBody
	public TabelData listScheduleBusDataTable(
			@RequestParam("sEcho") String echo,
			@RequestParam("iDisplayStart") Integer offset,
			@RequestParam("iDisplayLength") Integer limit,
			@RequestParam(value = "sSearch", required = false) String searchByName) {

		TabelData tabelData = new TabelData();
		tabelData.setsEcho(echo);
		// System.out.println("keyString:"+searchByName);
		Page page = dataMasterManagement.findPageScheduleBus(offset, limit,
				searchByName);

		tabelData.setsEcho(echo);
		tabelData.setiTotalDisplayRecords(page.getTotalRow());
		tabelData.setiTotalRecords(10);
		tabelData.setAaData(page.getObj());
		return tabelData;
	}

	@Transactional
	@RequestMapping(value = "saveScheduleBus.json", method = RequestMethod.POST)
	@ResponseBody
	public JsonResponse actionSaveScheduleBus(
			@ModelAttribute("schedule_bus") ScheduleBus scheduleBus,
			BindingResult result) throws InvalidParamException,
			InternalServiceException {

		JsonResponse response = new JsonResponse();
		try {

			dataMasterManagement.saveScheduleBus(scheduleBus);
			response.setMessage(JsonResponse.SUCCESS, "Success.");
		} catch (Exception e) {
			// logger.error(e.getMessage());
			response.setMessage(JsonResponse.VALIDATE_ERROR,
					"Internal Server error. Please contact your administrator");
		}
		return response;
	}

	@Transactional
	@RequestMapping(value = "updateScheduleBus.json", method = RequestMethod.POST)
	@ResponseBody
	public JsonResponse actionUpdateScheduleBus(
			@ModelAttribute("schedule_bus") ScheduleBus scheduleBus,
			BindingResult result) throws InvalidParamException,
			InternalServiceException {

		JsonResponse response = new JsonResponse();
		try {
			ScheduleBus scheduleBusOldData = dataMasterManagement
					.getScheduleBusDetailByID(scheduleBus.getIdSchedule());

			scheduleBus.setDateAdded(scheduleBusOldData.getDateAdded());
			dataMasterManagement.updateScheduleBus(scheduleBus);
			response.setMessage(JsonResponse.SUCCESS, "Success.");
		} catch (Exception e) {
			// logger.error(e.getMessage());
			response.setMessage(JsonResponse.VALIDATE_ERROR,
					"Internal Server error. Please contact your administrator");
		}

		return response;
	}

	@Transactional
	@RequestMapping(value = "deleteScheduleBus.json", method = RequestMethod.POST)
	@ResponseBody
	public JsonResponse actionDeleteScheduleBus(
			@RequestParam("id_long") Long id_long)
			throws InvalidParamException, InternalServiceException {

		JsonResponse response = new JsonResponse();
		try {
			dataMasterManagement.deleteScheduleBusByID(id_long);
			response.setMessage(JsonResponse.SUCCESS, "Success.");
		} catch (Exception e) {
			// logger.error(e.getMessage());
			response.setMessage(JsonResponse.VALIDATE_ERROR,
					"Internal Server error. Please contact your administrator");
		}

		return response;
	}

	/* Rute Management */
	private static final String LIST_RUTE_FROM = "rute/list_rute_from";
	private static final String ADD_RUTE_FROM = "rute/add_rute_from";
	private static final String UPDATE_RUTE_FROM = "rute/update_rute_from";
	private static final String LIST_RUTE_TO = "rute/list_rute_to";
	private static final String ADD_RUTE_TO = "rute/add_rute_to";
	private static final String UPDATE_RUTE_TO = "rute/update_rute_to";

	@RequestMapping(value = "list_rute_from.page", method = RequestMethod.GET)
	public ModelAndView listRuteFromPage(ModelMap model,
			final HttpServletRequest request, Principal principal,
			HttpSession httpSession) throws InvalidParamException,
			InternalServiceException {
		ModelAndView modelView = new ModelAndView(LIST_RUTE_FROM);
		return modelView;
	}

	@RequestMapping(value = "add_rute_from.page", method = RequestMethod.GET)
	public ModelAndView addRuteFromPage(ModelMap model,
			final HttpServletRequest request, Principal principal,
			HttpSession httpSession) throws InvalidParamException,
			InternalServiceException {
		ModelAndView modelView = new ModelAndView(ADD_RUTE_FROM);
		return modelView;
	}

	@RequestMapping(value = "update_rute_from.page/{id_rute_from}", method = RequestMethod.GET)
	public ModelAndView updateRuteFromPage(
			@PathVariable Long id_rute_from,
			ModelMap model,
			final HttpServletRequest request, Principal principal,
			HttpSession httpSession) throws InvalidParamException,
			InternalServiceException {
		ModelAndView modelView = new ModelAndView(UPDATE_RUTE_FROM);
		modelView.addObject("ruteFromDetails", dataMasterManagement.getRuteFromDetailByID(id_rute_from));
		return modelView;
	}

	@RequestMapping(value = "list_rute_to.page/{id_rute_from}", method = RequestMethod.GET)
	public ModelAndView listRuteToPage(
			@PathVariable Long id_rute_from,
			ModelMap model,
			final HttpServletRequest request, Principal principal,
			HttpSession httpSession) throws InvalidParamException,
			InternalServiceException {
		ModelAndView modelView = new ModelAndView(LIST_RUTE_TO);
		modelView.addObject("id_rute_from", id_rute_from);
		
		return modelView;
	}

	@RequestMapping(value = "add_rute_to.page/{id_rute_from}", method = RequestMethod.GET)
	public ModelAndView addRuteToPage(
			@PathVariable Long id_rute_from,
			ModelMap model,
			final HttpServletRequest request, Principal principal,
			HttpSession httpSession) throws InvalidParamException,
			InternalServiceException {
		ModelAndView modelView = new ModelAndView(ADD_RUTE_TO);
		modelView.addObject("id_rute_from", id_rute_from);
		return modelView;
	}

	@RequestMapping(value = "update_rute_to.page/{id_rute_to}", method = RequestMethod.GET)
	public ModelAndView updateRuteToPage(
			@PathVariable Long id_rute_to,
			ModelMap model,
			final HttpServletRequest request, Principal principal,
			HttpSession httpSession) throws InvalidParamException,
			InternalServiceException {
		ModelAndView modelView = new ModelAndView(UPDATE_RUTE_TO);
		modelView.addObject("ruteToDetails", dataMasterManagement.getRuteToDetailByID(id_rute_to));
		return modelView;
	}
	
	@RequestMapping(value = "/rute_from/data-table.json", method = RequestMethod.GET)
	@ResponseBody
	public TabelData listRuteFromTable(
			@RequestParam("sEcho") String echo,
			@RequestParam("iDisplayStart") Integer offset,
			@RequestParam("iDisplayLength") Integer limit,
			@RequestParam(value = "sSearch", required = false) String searchByName) {

		TabelData tabelData = new TabelData();
		tabelData.setsEcho(echo);
		// System.out.println("keyString:"+searchByName);
		Page page = dataMasterManagement.findPageRuteFrom(offset, limit,
				searchByName);

		tabelData.setsEcho(echo);
		tabelData.setiTotalDisplayRecords(page.getTotalRow());
		tabelData.setiTotalRecords(10);
		tabelData.setAaData(page.getObj());
		return tabelData;
	}
	
	
	@RequestMapping(value = "/rute_to/data-table.json/{id_rute_from}", method = RequestMethod.GET)
	@ResponseBody
	public TabelData listRuteToTable(
			@PathVariable Long id_rute_from,
			@RequestParam("sEcho") String echo,
			@RequestParam("iDisplayStart") Integer offset,
			@RequestParam("iDisplayLength") Integer limit,
			@RequestParam(value = "sSearch", required = false) String searchByName) {

		TabelData tabelData = new TabelData();
		tabelData.setsEcho(echo);
		// System.out.println("keyString:"+searchByName);
		Page page = dataMasterManagement.findPageRuteTo(offset, limit,
				searchByName,id_rute_from);

		tabelData.setsEcho(echo);
		tabelData.setiTotalDisplayRecords(page.getTotalRow());
		tabelData.setiTotalRecords(10);
		tabelData.setAaData(page.getObj());
		return tabelData;
	}
	
	
	@Transactional
	@RequestMapping(value = "saveRuteFrom.json", method = RequestMethod.POST)
	@ResponseBody
	public JsonResponse actionSaveRuteFrom(
			@ModelAttribute("ruteFrom") RuteFrom ruteFrom,
			BindingResult result) throws InvalidParamException,
			InternalServiceException {

		JsonResponse response = new JsonResponse();
		try {

			dataMasterManagement.saveRuteFrom(ruteFrom);
			response.setMessage(JsonResponse.SUCCESS, "Success.");
		} catch (Exception e) {
			// logger.error(e.getMessage());
			response.setMessage(JsonResponse.VALIDATE_ERROR,
					"Internal Server error. Please contact your administrator");
		}
		return response;
	}
	
	
	@Transactional
	@RequestMapping(value = "updateRuteFrom.json", method = RequestMethod.POST)
	@ResponseBody
	public JsonResponse actionUpdateRuteFrom(
			@ModelAttribute("ruteFrom") RuteFrom ruteFrom,
			BindingResult result) throws InvalidParamException,
			InternalServiceException {

		JsonResponse response = new JsonResponse();
		try {
			RuteFrom ruteFromOld = dataMasterManagement.
					getRuteFromDetailByID(ruteFrom.getIdRuteFrom());
			
			ruteFrom.setDateAdded(ruteFromOld.getDateAdded());
			
			dataMasterManagement.updateRuteFrom(ruteFrom);
			response.setMessage(JsonResponse.SUCCESS, "Success.");
		} catch (Exception e) {
			// logger.error(e.getMessage());
			response.setMessage(JsonResponse.VALIDATE_ERROR,
					"Internal Server error. Please contact your administrator");
		}
		return response;
	}
	
	
	@Transactional
	@RequestMapping(value = "deleteRuteFrom.json", method = RequestMethod.POST)
	@ResponseBody
	public JsonResponse actionDeleteRuteFrom(@RequestParam("id") Long id)
			throws InvalidParamException, InternalServiceException {

		JsonResponse response = new JsonResponse();
		try {
			dataMasterManagement.deleteRuteFromByID(id);
			response.setMessage(JsonResponse.SUCCESS, "Success.");
		} catch (Exception e) {
			// logger.error(e.getMessage());
			response.setMessage(JsonResponse.VALIDATE_ERROR,
					"Internal Server error. Please contact your administrator");
		}

		return response;
	}
	
	
	@Transactional
	@RequestMapping(value = "saveRuteTo.json", method = RequestMethod.POST)
	@ResponseBody
	public JsonResponse actionSaveRuteTo(
			@ModelAttribute("ruteTo") RuteTo ruteTo,
			BindingResult result) throws InvalidParamException,
			InternalServiceException {

		JsonResponse response = new JsonResponse();
		try {

			dataMasterManagement.saveRuteTo(ruteTo);
			response.setMessage(JsonResponse.SUCCESS, "Success.");
		} catch (Exception e) {
			// logger.error(e.getMessage());
			response.setMessage(JsonResponse.VALIDATE_ERROR,
					"Internal Server error. Please contact your administrator");
		}
		return response;
	}
	
	
	@Transactional
	@RequestMapping(value = "updateRuteTo.json", method = RequestMethod.POST)
	@ResponseBody
	public JsonResponse actionUpdateRuteTo(
			@ModelAttribute("ruteTo") RuteTo ruteTo,
			BindingResult result) throws InvalidParamException,
			InternalServiceException {

		JsonResponse response = new JsonResponse();
		try {
			RuteTo ruteToOld = dataMasterManagement.
					getRuteToDetailByID(ruteTo.getIdRuteTo());
			ruteTo.setDateAdded(ruteToOld.getDateAdded());
			
			dataMasterManagement.updateRuteTo(ruteTo);
			
			response.setMessage(JsonResponse.SUCCESS, "Success.");
		} catch (Exception e) {
			// logger.error(e.getMessage());
			response.setMessage(JsonResponse.VALIDATE_ERROR,
					"Internal Server error. Please contact your administrator");
		}
		return response;
	}
	
	
	@Transactional
	@RequestMapping(value = "deleteRuteTo.json", method = RequestMethod.POST)
	@ResponseBody
	public JsonResponse actionDeleteRuteTo(@RequestParam("id") Long id)
			throws InvalidParamException, InternalServiceException {

		JsonResponse response = new JsonResponse();
		try {
			dataMasterManagement.deleteRuteToByID(id);
			response.setMessage(JsonResponse.SUCCESS, "Success.");
		} catch (Exception e) {
			// logger.error(e.getMessage());
			response.setMessage(JsonResponse.VALIDATE_ERROR,
					"Internal Server error. Please contact your administrator");
		}

		return response;
	}
	
	@RequestMapping(value = "get-information-to-destination.json" , method = RequestMethod.GET)
	@ResponseBody
	public JsonResponse getInformationToDestination(
			@RequestParam("id_from") String id_from) throws InvalidParamException, InternalServiceException
	{
		JsonResponse response = new JsonResponse();
	
		try {
			
			Map<String, String> params = new HashMap<>();
			params.put("STATUS", StatusType.ACTIVE.getValue());
			params.put("ID_RUTE_FROM_REF", id_from);
			
			List<Destination> tempList = new ArrayList<Destination>();
			List<RuteTo> result = 
					dataMasterManagement.getAllRuteToByParam(params);
			for (RuteTo ruteTo : result) {
				tempList.add(new Destination(ruteTo.getIdRuteTo().intValue(), ruteTo.getNameRuteTo()));
			}
			
			response.setMessage(JsonResponse.SUCCESS,"Success");
			response.setObj(tempList);
			
			
		} catch (Exception e) {
			logError(e.getMessage());
			response.setMessage(JsonResponse.VALIDATE_ERROR,
					"Internal Server error. Please contact your administrator");
		}
		
		logInfo("finish :getInformationReservation");
		return response;
	}
	
	@RequestMapping(value = "get-destination-detail.json" , method = RequestMethod.GET)
	@ResponseBody
	public JsonResponse getDestinationDetail(
			@RequestParam("id") Long id) throws InvalidParamException, InternalServiceException
	{
		JsonResponse response = new JsonResponse();
	
		try {
			
			
			response.setMessage(JsonResponse.SUCCESS,"Success");
			response.setObj(dataMasterManagement.getRuteToDetailByID(id));
			
		} catch (Exception e) {
			logError(e.getMessage());
			response.setMessage(JsonResponse.VALIDATE_ERROR,
					"Internal Server error. Please contact your administrator");
		}
		
		logInfo("finish :getInformationReservation");
		return response;
	}
	
/* Schedule On Time Management */
	
	private static final String LIST_SCHEDULE_ON_TIME = "schedule_on_time/list_schedule_on_time";
	private static final String ADD_SCHEDULE_ON_TIME = "schedule_on_time/add_schedule_on_time";
	private static final String UPDATE_SCHEDULE_ON_TIME = "schedule_on_time/update_schedule_on_time";
	private static final String DETAIL_SCHEDULE_ON_TIME = "schedule_on_time/detail_schedule_on_time";
	
	@RequestMapping(value = "list_schedule_on_time.page", method = RequestMethod.GET)
	public ModelAndView listScheduleOnTimePage(ModelMap model,
			final HttpServletRequest request, Principal principal,
			HttpSession httpSession) throws InvalidParamException,
			InternalServiceException {
		ModelAndView modelView = new ModelAndView(LIST_SCHEDULE_ON_TIME);
		return modelView;
	}
	
	@RequestMapping(value = "add_schedule_on_time.page", method = RequestMethod.GET)
	public ModelAndView addScheduleOnTimePage(ModelMap model,
			final HttpServletRequest request, Principal principal,
			HttpSession httpSession) throws InvalidParamException,
			InternalServiceException {
		ModelAndView modelView = new ModelAndView(ADD_SCHEDULE_ON_TIME);
		
		Map<String, String> params = new HashMap<>();
		String param = null;
		params.put("STATUS", StatusType.ACTIVE.getValue());
		modelView.addObject("list_bus", dataMasterManagement.getAllBusByParam(param));
		modelView.addObject("list_schedule", dataMasterManagement.getAllScheduleBusByParam(params));
		modelView.addObject("list_schedule_on_time", dataMasterManagement.getAllScheduleOnTimeByParam(param));
		modelView.addObject("list_rute_from", dataMasterManagement.getAllRuteFromByParam(params));
		
		return modelView;
	}

	@RequestMapping(value = "update_schedule_on_time.page/{id_schedule_on_time}", method = RequestMethod.GET)
	public ModelAndView updateScheduleOnTimePage(
			@PathVariable Long id_schedule_on_time, ModelMap model,
			final HttpServletRequest request, Principal principal,
			HttpSession httpSession) throws InvalidParamException,
			InternalServiceException {
		ModelAndView modelView = new ModelAndView(UPDATE_SCHEDULE_ON_TIME);
		modelView.addObject("scheduleOnTimeDetails",
				dataMasterManagement.getScheduleOnTimeDetailByID(id_schedule_on_time));
		Map<String, String> params = new HashMap<>();
		String param = null;
		params.put("STATUS", StatusType.ACTIVE.getValue());
		modelView.addObject("list_bus", dataMasterManagement.getAllBusByParam(param));
		modelView.addObject("list_schedule", dataMasterManagement.getAllScheduleBusByParam(params));
		modelView.addObject("list_rute_from", dataMasterManagement.getAllRuteFromByParam(params));
		
		return modelView;
	}

	@RequestMapping(value = "/schedule_on_time/data-table.json", method = RequestMethod.GET)
	@ResponseBody
	public TabelData listScheduleOnTimeDataTable(
			@RequestParam("sEcho") String echo,
			@RequestParam("iDisplayStart") Integer offset,
			@RequestParam("iDisplayLength") Integer limit,
			@RequestParam(value = "sSearch", required = false) String searchByName) {

		TabelData tabelData = new TabelData();
		tabelData.setsEcho(echo);
		// System.out.println("keyString:"+searchByName);
		Page page = dataMasterManagement.findPageScheduleOnTime(offset, limit,
				searchByName);

		tabelData.setsEcho(echo);
		tabelData.setiTotalDisplayRecords(page.getTotalRow());
		tabelData.setiTotalRecords(10);
		tabelData.setAaData(page.getObj());
		return tabelData;
	}

	@Transactional
	@RequestMapping(value = "saveScheduleOnTime.json", method = RequestMethod.POST)
	@ResponseBody
	public JsonResponse actionSaveScheduleOnTime(
			@ModelAttribute("schedule_on_time") ScheduleOnTime scheduleOnTime,
			BindingResult result) throws InvalidParamException,
			InternalServiceException {

		JsonResponse response = new JsonResponse();
		try {
			
			scheduleOnTime = dataMasterManagement.saveScheduleOnTime(scheduleOnTime);
			
			if(CommonUtil.isNotNullOrEmpty(scheduleOnTime.getIdOnTime()))
			{
				response.setMessage(JsonResponse.SUCCESS, "Success.");
			}
			else
			{
				response.setMessage(JsonResponse.VALIDATE_ERROR,
						"Jadwal sudah ada");
			}
		} catch (Exception e) {
			// logger.error(e.getMessage());
			response.setMessage(JsonResponse.VALIDATE_ERROR,
					"Internal Server error. Please contact your administrator");
		}
		return response;
	}

	@Transactional
	@RequestMapping(value = "updateScheduleOnTime.json", method = RequestMethod.POST)
	@ResponseBody
	public JsonResponse actionUpdateScheduleOnTime(
			@ModelAttribute("schedule_on_time") ScheduleOnTime scheduleOnTime,
			BindingResult result) throws InvalidParamException,
			InternalServiceException {

		JsonResponse response = new JsonResponse();
		try {
			ScheduleOnTime scheduleOnTimeOldData = dataMasterManagement
					.getScheduleOnTimeDetailByID(scheduleOnTime.getIdOnTime());

			scheduleOnTime.setDateAdded(scheduleOnTimeOldData.getDateAdded());
			dataMasterManagement.updateScheduleOnTime(scheduleOnTime);
			response.setMessage(JsonResponse.SUCCESS, "Success.");
		} catch (Exception e) {
			// logger.error(e.getMessage());
			response.setMessage(JsonResponse.VALIDATE_ERROR,
					"Internal Server error. Please contact your administrator");
		}

		return response;
	}

	@Transactional
	@RequestMapping(value = "deleteScheduleOnTime.json", method = RequestMethod.POST)
	@ResponseBody
	public JsonResponse actionDeleteScheduleOnTime(
			@RequestParam("id_long") Long id_long)
			throws InvalidParamException, InternalServiceException {

		JsonResponse response = new JsonResponse();
		try {
			dataMasterManagement.deleteScheduleOnTimeByID(id_long);
			response.setMessage(JsonResponse.SUCCESS, "Success.");
		} catch (Exception e) {
			// logger.error(e.getMessage());
			response.setMessage(JsonResponse.VALIDATE_ERROR,
					"Internal Server error. Please contact your administrator");
		}

		return response;
	}
	
	@RequestMapping(value = "detail_schedule_on_time.page/{id_schedule_on_time}", method = RequestMethod.GET)
	public ModelAndView detailScheduleOnTimePage(
			@PathVariable Long id_schedule_on_time, ModelMap model,
			final HttpServletRequest request, Principal principal,
			HttpSession httpSession) throws InvalidParamException,
			InternalServiceException {
		ModelAndView modelView = new ModelAndView(DETAIL_SCHEDULE_ON_TIME);
		modelView.addObject("scheduleOnTimeDetails",
				dataMasterManagement.findPageSitOrder(id_schedule_on_time));
		
		return modelView;
	}
}
