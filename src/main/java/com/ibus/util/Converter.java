package com.ibus.util;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;

import java.util.Iterator;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

public class Converter {
	// We are making use of a single instance to prevent multiple write access
	// to same file.
	private static final Converter INSTANCE = new Converter();

	public static Converter getInstance() {
		return INSTANCE;
	}

	private Converter() {
	}

	public static void xlsx(File input, File output) {
		// TODO Auto-generated method stub
		// For storing data into CSV files
		StringBuffer data = new StringBuffer();

		try {
			FileOutputStream fos = new FileOutputStream(output);
			// Get the workbook object for XLSX file
			XSSFWorkbook wBook = new XSSFWorkbook(new FileInputStream(input));
			// Get first sheet from the workbook
			XSSFSheet sheet = wBook.getSheetAt(0);
			Row row;
			Cell cell;
			// Iterate through each rows from first sheet
			Iterator<Row> rowIterator = sheet.iterator();

			
			while (rowIterator.hasNext()) {
				row = rowIterator.next();

				// For each row, iterate through each columns
				Iterator<Cell> cellIterator = row.cellIterator();
				int count = 0;
				while (cellIterator.hasNext()) {

					cell = cellIterator.next();

					if (cell.getCellType() == Cell.CELL_TYPE_BOOLEAN) {
						boolean valueOfCellBoolean = cell.getBooleanCellValue();
						data.append((count == 0 ? "" : ";")+ valueOfCellBoolean);
					} else if (cell.getCellType() == Cell.CELL_TYPE_NUMERIC) {
						//cell.setCellType(Cell.CELL_TYPE_STRING); 
						double valueOfCellNumeric = cell.getNumericCellValue();
						data.append((count == 0 ? "" : ";")+ valueOfCellNumeric);
						
					} else if (cell.getCellType() == Cell.CELL_TYPE_STRING) {
						String valueOfCellString = cell.getStringCellValue();
						data.append((count == 0 ? "" : ";")+ valueOfCellString);
					} else if (cell.getCellType() == Cell.CELL_TYPE_BLANK) {
						data.append((count == 0 ? "" : ";") + "");
					} else {
						data.append((count == 0 ? "" : ";") + cell);
					}

		
					count++;
				}

				if (count < 19) {
					for (int i = 0; i < (19 - count); i++) {
						data.append(";");
					}
				}

				data.append("\r\n");
			}

			fos.write(data.toString().getBytes());
			fos.close();

		} catch (Exception ioe) {
			ioe.printStackTrace();
		}
	}
	
	
	
	public static StringBuffer xlsx2(File input) {
		// TODO Auto-generated method stub
		// For storing data into CSV files
		StringBuffer data = new StringBuffer();

		try {
		
			// Get the workbook object for XLSX file
			XSSFWorkbook wBook = new XSSFWorkbook(new FileInputStream(input));
			// Get first sheet from the workbook
			XSSFSheet sheet = wBook.getSheetAt(0);
			Row row;
			Cell cell;
			// Iterate through each rows from first sheet
			Iterator<Row> rowIterator = sheet.iterator();

			
			while (rowIterator.hasNext()) {
				row = rowIterator.next();

				// For each row, iterate through each columns
				Iterator<Cell> cellIterator = row.cellIterator();
				int count = 0;
				while (cellIterator.hasNext()) {

					cell = cellIterator.next();

					if (cell.getCellType() == Cell.CELL_TYPE_BOOLEAN) {
						boolean valueOfCellBoolean = cell.getBooleanCellValue();
						data.append((count == 0 ? "" : ";")+ valueOfCellBoolean);
					} else if (cell.getCellType() == Cell.CELL_TYPE_NUMERIC) {
						double valueOfCellNumeric = cell.getNumericCellValue();
						data.append((count == 0 ? "" : ";")+ valueOfCellNumeric);
					} else if (cell.getCellType() == Cell.CELL_TYPE_STRING) {
						String valueOfCellString = cell.getStringCellValue();
						data.append((count == 0 ? "" : ";")+ valueOfCellString);
					} else if (cell.getCellType() == Cell.CELL_TYPE_BLANK) {
						data.append((count == 0 ? "" : ";") + "");
					} else {
						data.append((count == 0 ? "" : ";") + cell);
					}

		
					count++;
				}

				if (count < 19) {
					for (int i = 0; i < (19 - count); i++) {
						data.append(";");
					}
				}

				data.append("\r\n");
			}

		

		} catch (Exception ioe) {
			ioe.printStackTrace();
		}
		
		return data;
	}
	
	
}
