package com.ibus.model;

import java.math.BigDecimal;

public class MyModel {
	
	private String uraian;
	
	private BigDecimal alokasi;
	
	// new field 08-feb-2015
	private String kodeIndikator;
	
	private String kodeOutput;
	
	private String kodeKegiatan;
	
	private String kodeSubKegiatan;
	
	private String kodeJenisBelanja;
	
	private String tipeHirarki;

	public BigDecimal getAlokasi() {
		return alokasi;
	}

	public void setAlokasi(BigDecimal alokasi) {
		this.alokasi = alokasi;
	}

	public String getKodeOutput() {
		return kodeOutput;
	}

	public void setKodeOutput(String kodeOutput) {
		this.kodeOutput = kodeOutput;
	}

	public String getKodeKegiatan() {
		return kodeKegiatan;
	}

	public void setKodeKegiatan(String kodeKegiatan) {
		this.kodeKegiatan = kodeKegiatan;
	}

	public String getKodeSubKegiatan() {
		return kodeSubKegiatan;
	}

	public void setKodeSubKegiatan(String kodeSubKegiatan) {
		this.kodeSubKegiatan = kodeSubKegiatan;
	}

	public String getKodeJenisBelanja() {
		return kodeJenisBelanja;
	}

	public void setKodeJenisBelanja(String kodeJenisBelanja) {
		this.kodeJenisBelanja = kodeJenisBelanja;
	}

	public String getTipeHirarki() {
		return tipeHirarki;
	}

	public void setTipeHirarki(String tipeHirarki) {
		this.tipeHirarki = tipeHirarki;
	}

	public String getUraian() {
		return uraian;
	}

	public void setUraian(String uraian) {
		this.uraian = uraian;
	}

	public String getKodeIndikator() {
		return kodeIndikator;
	}

	public void setKodeIndikator(String kodeIndikator) {
		this.kodeIndikator = kodeIndikator;
	}		
	
	
}