package com.ibus.model;

import java.util.Date;

import org.codehaus.jackson.map.annotate.JsonDeserialize;
import org.codehaus.jackson.map.annotate.JsonSerialize;

import com.ibus.util.formatter.JsonDateTimeSerializerOnlyDate;

import co.id.rokubi.model.Model;
import co.id.rokubi.util.formater.JsonDateDeserializer;
import co.id.rokubi.util.formater.JsonDateTimeSerializer;

public class BeritaPromo implements Model{

	/**
	 * This field was generated by MyBatis Generator. This field corresponds to the database column berita_promo.ID_BERITA_PROMO
	 * @mbggenerated  Tue Aug 23 11:00:06 ICT 2016
	 */
	private Long idBeritaPromo;
	/**
	 * This field was generated by MyBatis Generator. This field corresponds to the database column berita_promo.TITLE
	 * @mbggenerated  Tue Aug 23 11:00:06 ICT 2016
	 */
	private String title;
	/**
	 * This field was generated by MyBatis Generator. This field corresponds to the database column berita_promo.CONTENT_TYPE
	 * @mbggenerated  Tue Aug 23 11:00:06 ICT 2016
	 */
	private String contentType;
	/**
	 * This field was generated by MyBatis Generator. This field corresponds to the database column berita_promo.TIME_START
	 * @mbggenerated  Tue Aug 23 11:00:06 ICT 2016
	 */
	private Date timeStart;
	/**
	 * This field was generated by MyBatis Generator. This field corresponds to the database column berita_promo.TIME_END
	 * @mbggenerated  Tue Aug 23 11:00:06 ICT 2016
	 */
	private Date timeEnd;
	/**
	 * This field was generated by MyBatis Generator. This field corresponds to the database column berita_promo.STATUS
	 * @mbggenerated  Tue Aug 23 11:00:06 ICT 2016
	 */
	private String status;
	/**
	 * This field was generated by MyBatis Generator. This field corresponds to the database column berita_promo.IMAGE_URL
	 * @mbggenerated  Tue Aug 23 11:00:06 ICT 2016
	 */
	private String imageUrl;
	/**
	 * This field was generated by MyBatis Generator. This field corresponds to the database column berita_promo.DATE_ADDED
	 * @mbggenerated  Tue Aug 23 11:00:06 ICT 2016
	 */
	private Date dateAdded;
	/**
	 * This field was generated by MyBatis Generator. This field corresponds to the database column berita_promo.DATE_MODIFIED
	 * @mbggenerated  Tue Aug 23 11:00:06 ICT 2016
	 */
	private Date dateModified;
	/**
	 * This field was generated by MyBatis Generator. This field corresponds to the database column berita_promo.POTONGAN_HARGA
	 * @mbggenerated  Tue Aug 23 11:00:06 ICT 2016
	 */
	private Integer potonganHarga;
	/**
	 * This field was generated by MyBatis Generator. This field corresponds to the database column berita_promo.STATUS_PILIHAN_PROMO
	 * @mbggenerated  Tue Aug 23 11:00:06 ICT 2016
	 */
	private String statusPilihanPromo;
	/**
	 * This field was generated by MyBatis Generator. This field corresponds to the database column berita_promo.CONTENT
	 * @mbggenerated  Tue Aug 23 11:00:06 ICT 2016
	 */
	private String content;


	/**
	 * This method was generated by MyBatis Generator. This method returns the value of the database column berita_promo.ID_BERITA_PROMO
	 * @return  the value of berita_promo.ID_BERITA_PROMO
	 * @mbggenerated  Tue Aug 23 11:00:06 ICT 2016
	 */
	public Long getIdBeritaPromo() {
		return idBeritaPromo;
	}

	/**
	 * This method was generated by MyBatis Generator. This method sets the value of the database column berita_promo.ID_BERITA_PROMO
	 * @param idBeritaPromo  the value for berita_promo.ID_BERITA_PROMO
	 * @mbggenerated  Tue Aug 23 11:00:06 ICT 2016
	 */
	public void setIdBeritaPromo(Long idBeritaPromo) {
		this.idBeritaPromo = idBeritaPromo;
	}

	/**
	 * This method was generated by MyBatis Generator. This method returns the value of the database column berita_promo.TITLE
	 * @return  the value of berita_promo.TITLE
	 * @mbggenerated  Tue Aug 23 11:00:06 ICT 2016
	 */
	public String getTitle() {
		return title;
	}

	/**
	 * This method was generated by MyBatis Generator. This method sets the value of the database column berita_promo.TITLE
	 * @param title  the value for berita_promo.TITLE
	 * @mbggenerated  Tue Aug 23 11:00:06 ICT 2016
	 */
	public void setTitle(String title) {
		this.title = title == null ? null : title.trim();
	}

	/**
	 * This method was generated by MyBatis Generator. This method returns the value of the database column berita_promo.CONTENT_TYPE
	 * @return  the value of berita_promo.CONTENT_TYPE
	 * @mbggenerated  Tue Aug 23 11:00:06 ICT 2016
	 */
	public String getContentType() {
		return contentType;
	}

	/**
	 * This method was generated by MyBatis Generator. This method sets the value of the database column berita_promo.CONTENT_TYPE
	 * @param contentType  the value for berita_promo.CONTENT_TYPE
	 * @mbggenerated  Tue Aug 23 11:00:06 ICT 2016
	 */
	public void setContentType(String contentType) {
		this.contentType = contentType == null ? null : contentType.trim();
	}

	/**
	 * This method was generated by MyBatis Generator. This method returns the value of the database column berita_promo.TIME_START
	 * @return  the value of berita_promo.TIME_START
	 * @mbggenerated  Tue Aug 23 11:00:06 ICT 2016
	 */
	@JsonSerialize(using = JsonDateTimeSerializerOnlyDate.class)
	public Date getTimeStart() {
		return timeStart;
	}

	/**
	 * This method was generated by MyBatis Generator. This method sets the value of the database column berita_promo.TIME_START
	 * @param timeStart  the value for berita_promo.TIME_START
	 * @mbggenerated  Tue Aug 23 11:00:06 ICT 2016
	 */
	@JsonDeserialize(using = JsonDateDeserializer.class)
	public void setTimeStart(Date timeStart) {
		this.timeStart = timeStart;
	}

	/**
	 * This method was generated by MyBatis Generator. This method returns the value of the database column berita_promo.TIME_END
	 * @return  the value of berita_promo.TIME_END
	 * @mbggenerated  Tue Aug 23 11:00:06 ICT 2016
	 */
	@JsonSerialize(using = JsonDateTimeSerializerOnlyDate.class)
	public Date getTimeEnd() {
		return timeEnd;
	}

	/**
	 * This method was generated by MyBatis Generator. This method sets the value of the database column berita_promo.TIME_END
	 * @param timeEnd  the value for berita_promo.TIME_END
	 * @mbggenerated  Tue Aug 23 11:00:06 ICT 2016
	 */
	@JsonDeserialize(using = JsonDateDeserializer.class)
	public void setTimeEnd(Date timeEnd) {
		this.timeEnd = timeEnd;
	}

	/**
	 * This method was generated by MyBatis Generator. This method returns the value of the database column berita_promo.STATUS
	 * @return  the value of berita_promo.STATUS
	 * @mbggenerated  Tue Aug 23 11:00:06 ICT 2016
	 */
	public String getStatus() {
		return status;
	}

	/**
	 * This method was generated by MyBatis Generator. This method sets the value of the database column berita_promo.STATUS
	 * @param status  the value for berita_promo.STATUS
	 * @mbggenerated  Tue Aug 23 11:00:06 ICT 2016
	 */
	public void setStatus(String status) {
		this.status = status == null ? null : status.trim();
	}

	/**
	 * This method was generated by MyBatis Generator. This method returns the value of the database column berita_promo.IMAGE_URL
	 * @return  the value of berita_promo.IMAGE_URL
	 * @mbggenerated  Tue Aug 23 11:00:06 ICT 2016
	 */
	public String getImageUrl() {
		return imageUrl;
	}

	/**
	 * This method was generated by MyBatis Generator. This method sets the value of the database column berita_promo.IMAGE_URL
	 * @param imageUrl  the value for berita_promo.IMAGE_URL
	 * @mbggenerated  Tue Aug 23 11:00:06 ICT 2016
	 */
	public void setImageUrl(String imageUrl) {
		this.imageUrl = imageUrl == null ? null : imageUrl.trim();
	}

	/**
	 * This method was generated by MyBatis Generator. This method returns the value of the database column berita_promo.DATE_ADDED
	 * @return  the value of berita_promo.DATE_ADDED
	 * @mbggenerated  Tue Aug 23 11:00:06 ICT 2016
	 */
	@JsonSerialize(using = JsonDateTimeSerializer.class)
	public Date getDateAdded() {
		return dateAdded;
	}

	/**
	 * This method was generated by MyBatis Generator. This method sets the value of the database column berita_promo.DATE_ADDED
	 * @param dateAdded  the value for berita_promo.DATE_ADDED
	 * @mbggenerated  Tue Aug 23 11:00:06 ICT 2016
	 */
	@JsonSerialize(using = JsonDateTimeSerializer.class)
	public void setDateAdded(Date dateAdded) {
		this.dateAdded = dateAdded;
	}

	/**
	 * This method was generated by MyBatis Generator. This method returns the value of the database column berita_promo.DATE_MODIFIED
	 * @return  the value of berita_promo.DATE_MODIFIED
	 * @mbggenerated  Tue Aug 23 11:00:06 ICT 2016
	 */
	@JsonSerialize(using = JsonDateTimeSerializer.class)
	public Date getDateModified() {
		return dateModified;
	}

	/**
	 * This method was generated by MyBatis Generator. This method sets the value of the database column berita_promo.DATE_MODIFIED
	 * @param dateModified  the value for berita_promo.DATE_MODIFIED
	 * @mbggenerated  Tue Aug 23 11:00:06 ICT 2016
	 */
	@JsonSerialize(using = JsonDateTimeSerializer.class)
	public void setDateModified(Date dateModified) {
		this.dateModified = dateModified;
	}

	/**
	 * This method was generated by MyBatis Generator. This method returns the value of the database column berita_promo.POTONGAN_HARGA
	 * @return  the value of berita_promo.POTONGAN_HARGA
	 * @mbggenerated  Tue Aug 23 11:00:06 ICT 2016
	 */
	public Integer getPotonganHarga() {
		return potonganHarga;
	}

	/**
	 * This method was generated by MyBatis Generator. This method sets the value of the database column berita_promo.POTONGAN_HARGA
	 * @param potonganHarga  the value for berita_promo.POTONGAN_HARGA
	 * @mbggenerated  Tue Aug 23 11:00:06 ICT 2016
	 */
	public void setPotonganHarga(Integer potonganHarga) {
		this.potonganHarga = potonganHarga;
	}

	/**
	 * This method was generated by MyBatis Generator. This method returns the value of the database column berita_promo.STATUS_PILIHAN_PROMO
	 * @return  the value of berita_promo.STATUS_PILIHAN_PROMO
	 * @mbggenerated  Tue Aug 23 11:00:06 ICT 2016
	 */
	public String getStatusPilihanPromo() {
		return statusPilihanPromo;
	}

	/**
	 * This method was generated by MyBatis Generator. This method sets the value of the database column berita_promo.STATUS_PILIHAN_PROMO
	 * @param statusPilihanPromo  the value for berita_promo.STATUS_PILIHAN_PROMO
	 * @mbggenerated  Tue Aug 23 11:00:06 ICT 2016
	 */
	public void setStatusPilihanPromo(String statusPilihanPromo) {
		this.statusPilihanPromo = statusPilihanPromo == null ? null
				: statusPilihanPromo.trim();
	}

	/**
	 * This method was generated by MyBatis Generator. This method returns the value of the database column berita_promo.CONTENT
	 * @return  the value of berita_promo.CONTENT
	 * @mbggenerated  Tue Aug 23 11:00:06 ICT 2016
	 */
	public String getContent() {
		return content;
	}

	/**
	 * This method was generated by MyBatis Generator. This method sets the value of the database column berita_promo.CONTENT
	 * @param content  the value for berita_promo.CONTENT
	 * @mbggenerated  Tue Aug 23 11:00:06 ICT 2016
	 */
	public void setContent(String content) {
		this.content = content == null ? null : content.trim();
	}

	public String getDateTimeStart() {
		return dateTimeStart;
	}

	public void setDateTimeStart(String dateTimeStart) {
		this.dateTimeStart = dateTimeStart;
	}

	public String getDateTimeEnd() {
		return dateTimeEnd;
	}

	public void setDateTimeEnd(String dateTimeEnd) {
		this.dateTimeEnd = dateTimeEnd;
	}


	private String dateTimeEnd;
	private String dateTimeStart;
	
	
	private String pathImageUrl;


	public String getPathImageUrl() {
		return pathImageUrl;
	}

	public void setPathImageUrl(String pathImageUrl) {
		this.pathImageUrl = pathImageUrl;
	}
	
}