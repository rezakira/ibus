package co.id.rokubi.constant;

public enum MessageType {
	
	ERROR("ERR"), WARNING("WRN"), SUCCESS("SCS");
	
	private String value;
	
	private MessageType(String value){
		this.value = value;
	}

	public String getValue() {
		return value;
	}
	
	
}
