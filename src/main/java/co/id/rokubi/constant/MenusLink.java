/**
 * 
 */
package co.id.rokubi.constant;

import java.util.HashMap;
import java.util.Map;

import co.id.rokubi.model.Link;

/**
 * @author Radian
 *
 */
public class MenusLink {
	public static final Map<String, Link> link = new HashMap<String, Link>();

	static {
		// main menu
		link.put("APSET", new Link("", "iconfa-laptop"));
		link.put("HELPS", new Link("", "iconsweets-archive"));

		// sub menu
		link.put("GNSET", new Link("/setting.page", ""));
		link.put("USERS", new Link("/user/users.page", ""));
		link.put("REGUS", new Link("/user/register-user.page", ""));
		link.put("ROLES", new Link("/role/role.page", ""));
		link.put("MENUS", new Link("/app/menus.page", ""));
		link.put("MENUS", new Link("/app/menus.page", ""));
		
		link.put("EVALS", new Link("/evaluasi.page", ""));
		
		

	}

	public static Map<String, Link> getLink() {
		return link;
	}
}
