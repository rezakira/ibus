package co.id.rokubi.constant;

/**
 * this class represent every status of this application; master, document, transactional or non transactional object,
 * volatile or persistent object. it should have one of this status.
 * @author gugun
 *
 */
public enum StatusType {
	ACTIVE("A"), 
	NONACTIVE("D"), 
	PUBLISH("PBL"), 
	UNPUBLISH("NPL"), 
	DELETE("DLT"),
	PENDING("PDG");
	
	private String value;
	
	private StatusType(String value) {
		this.value = value;
	}

	public String getValue() {
		return value;
	}
}
