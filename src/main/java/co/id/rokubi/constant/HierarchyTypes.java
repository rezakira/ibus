package co.id.rokubi.constant;

public enum HierarchyTypes {
	OUTPUT("IDKTR"), 
	KEGIATAN("KGTN"), 
	SUB_KEGIATAN("SBKGT"), 
	JENIS_BELANJA("JNSBL");
	
	private String value;

	private HierarchyTypes(String value) {
		this.value = value;
	}

	public String getValue() {
		return value;
	}
}
