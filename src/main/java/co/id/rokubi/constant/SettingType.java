package co.id.rokubi.constant;

public enum SettingType {
	NAME_OFFICE("NAME"), 
	LANGUAGE("LANGE"), 
	ADDRESS_OFFICE("ADDRS"); 
	

	private String value;

	private SettingType(String value) {
		this.value = value;
	}

	public String getValue() {
		return value;
	}

}
