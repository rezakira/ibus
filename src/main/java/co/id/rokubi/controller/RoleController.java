package co.id.rokubi.controller;

import java.security.Principal;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;








import co.id.rokubi.exception.InternalServiceException;
import co.id.rokubi.exception.InvalidParamException;
import co.id.rokubi.model.JsonResponse;
import co.id.rokubi.model.Page;
import co.id.rokubi.model.Role;
import co.id.rokubi.model.TabelData;
import co.id.rokubi.service.UserManagementService;


@Controller
@RequestMapping("/role")
public class RoleController extends AbstractController {
	
	// Attribute
		private static final String LIST = "common/role/role";
		private static final String DETAIL = "common/role/view-role";

		
		@Autowired
		private UserManagementService roleManagement;
		
		//Methods
		
		
		/**
		 * Methods untuk menampilkan list role
		 * @param model
		 * @param request
		 * @param principal
		 * @param httpSession
		 * @return
		 * @throws InvalidParamException
		 * @throws InternalServiceException
		 */
		@RequestMapping(value = "role.page", method = RequestMethod.GET)
		public ModelAndView RoleListPage(ModelMap model, final HttpServletRequest request,
				Principal principal, HttpSession httpSession) throws InvalidParamException, InternalServiceException {
			
			ModelAndView modelView = new ModelAndView(LIST);
			
			List<Role> role = roleManagement.findActiveRole();
			
			//Get User List from database User
			modelView.addObject("listRole", role);
			
			
			return modelView;
		}
		
		/**
		 * Methods untuk menavigasikan ke halaman detail role dengan 
		 * id role yg sudah dipilih sebelumnya
		 * @param roleid
		 * @return
		 * @throws InvalidParamException
		 * @throws InternalServiceException
		 */
		@RequestMapping(value="detailRole.page/{roleid}", method=RequestMethod.GET)
		public ModelAndView actionEditUserPage (@PathVariable String roleid) throws InvalidParamException, InternalServiceException {
			
			ModelAndView modelView = new ModelAndView(DETAIL);
			
			modelView.addObject("role", roleManagement.findRoleById(roleid));
			
			return modelView;
		}
		
		@SuppressWarnings({ "unchecked", "rawtypes" })
		@Transactional
		@RequestMapping(value = "getRolebyId.json", method = RequestMethod.POST)
		@ResponseBody
		public JsonResponse getRolebyId(@RequestParam("roleId") String roleId) throws InvalidParamException,
				InternalServiceException {
			JsonResponse response = new JsonResponse();
			
			Map mMap = new HashMap();
			
			try {
				mMap.put("role", roleManagement.findRoleById(roleId));
				mMap.put("menu_access", roleManagement.listMenubyRoleId(roleId));
				
				response.setObj(mMap);

				response.setMessage(JsonResponse.SUCCESS, "Success.");
			} catch (Exception e) {
				// logger.error(e.getMessage());
				response.setMessage(JsonResponse.VALIDATE_ERROR,
						"Internal Server error. Please contact your administrator");
			}

			// response.setObj(list(calendar.ge));

			return response;

		}
		
		/**
		 * this method is used to take data menu with pagination (pass offset and limit from client). for using this method, 
		 * the app need to install jquery data table (http://datatables.net/). and this method is used to take data by jquery datatable format.
		 * this method was reviewed by gugun on april 29, 2014
		 * @param echo
		 * @param offset
		 * @param limit
		 * @param searchByName
		 * @param activeStatus
		 * @return
		 */
		@RequestMapping(value = "/data-table.json", method = RequestMethod.GET)
		@ResponseBody
		public TabelData listRoleDataTable(@RequestParam("sEcho") String echo,
				@RequestParam("iDisplayStart") Integer offset,
				@RequestParam("iDisplayLength") Integer limit,
				@RequestParam(value = "sSearch", required = false) String searchByName,
				@RequestParam(value = "sSearch_0", required = false) String activeStatus) {
			
			TabelData tabelData = new TabelData();
			tabelData.setsEcho(echo);
			
			Page page = roleManagement.findPageMenu(offset, limit);
			tabelData.setsEcho(echo);
			tabelData.setiTotalDisplayRecords(page.getTotalRow());
			tabelData.setiTotalRecords(10);
			tabelData.setAaData(page.getObj());
			return tabelData;
		}
		
		/**
		 * this method was reviewed by gugun on april 29, 2014
		 * @param roleid
		 * @return
		 * @throws InvalidParamException
		 * @throws InternalServiceException
		 */
		@RequestMapping(value="/view-role.page/{roleid}", method=RequestMethod.GET)
		public ModelAndView viewRolePage (@PathVariable String roleid) throws InvalidParamException, InternalServiceException {
			ModelAndView modelView = new ModelAndView(DETAIL);
			modelView.addObject("role", roleManagement.findRoleById(roleid));
			modelView.addObject("listMenu", roleManagement.listMenubyRoleId(roleid));
			return modelView;
		}
}
