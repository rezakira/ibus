package co.id.rokubi.controller.bean;


public class AppSetting {
	// Attribute
	private String nameOffice;
	public String getNameOffice() {
		return nameOffice;
	}
	public void setNameOffice(String nameOffice) {
		this.nameOffice = nameOffice;
	}
	private String addressOffice;
	public String getAddressOffice() {
		return addressOffice;
	}
	public void setAddressOffice(String addressOffice) {
		this.addressOffice = addressOffice;
	}
	
	
	private String npwp;
	private String namaDirektur;
	private String noTelp;
	private String jenisUsaha;
	private Integer jmlKaryawan;
	private Integer jmlPk;
	private Integer jmlPm;
	
	public String getNpwp() {
		return npwp;
	}
	public void setNpwp(String npwp) {
		this.npwp = npwp;
	}
	public String getNamaDirektur() {
		return namaDirektur;
	}
	public void setNamaDirektur(String namaDirektur) {
		this.namaDirektur = namaDirektur;
	}
	public String getNoTelp() {
		return noTelp;
	}
	public void setNoTelp(String noTelp) {
		this.noTelp = noTelp;
	}
	public String getJenisUsaha() {
		return jenisUsaha;
	}
	public void setJenisUsaha(String jenisUsaha) {
		this.jenisUsaha = jenisUsaha;
	}
	public Integer getJmlKaryawan() {
		return jmlKaryawan;
	}
	public void setJmlKaryawan(Integer jmlKaryawan) {
		this.jmlKaryawan = jmlKaryawan;
	}
	public Integer getJmlPk() {
		return jmlPk;
	}
	public void setJmlPk(Integer jmlPk) {
		this.jmlPk = jmlPk;
	}
	public Integer getJmlPm() {
		return jmlPm;
	}
	public void setJmlPm(Integer jmlPm) {
		this.jmlPm = jmlPm;
	}
	
}
