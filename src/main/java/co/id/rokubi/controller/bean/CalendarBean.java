/**
 * 
 */
package co.id.rokubi.controller.bean;

import java.util.Date;

import org.codehaus.jackson.map.annotate.JsonSerialize;

import co.id.rokubi.util.formater.JsonDateTimeSerializer;

/**
 * @author Radian
 *
 */
public class CalendarBean {
	
	private String title;
	private Date start;
	private Date end;
	private String description;
	private Boolean allDay;
	private String color;
	private String textColor;
	
	//Methods
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	
	@JsonSerialize(using = JsonDateTimeSerializer.class)
	public Date getStart() {
		return start;
	}
	
	
	public void setStart(Date start) {
		this.start = start;
	}
	
	@JsonSerialize(using = JsonDateTimeSerializer.class)
	public Date getEnd() {
		return end;
	}
	
	public void setEnd(Date end) {
		this.end = end;
	}
	
	
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public Boolean getAllDay() {
		return allDay;
	}
	public void setAllDay(Boolean allDay) {
		this.allDay = allDay;
	}
	public String getColor() {
		return color;
	}
	public void setColor(String color) {
		this.color = color;
	}
	public String getTextColor() {
		return textColor;
	}
	public void setTextColor(String textColor) {
		this.textColor = textColor;
	}
	

}
