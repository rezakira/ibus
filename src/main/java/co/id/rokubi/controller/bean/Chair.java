package co.id.rokubi.controller.bean;

public class Chair {
	private String bis_id_ref;
	private String time_schedule_id;
	private String sit_id;
	private Integer available;
	
	public String getBis_id_ref() {
		return bis_id_ref;
	}
	public void setBis_id_ref(String bis_id_ref) {
		this.bis_id_ref = bis_id_ref;
	}
	public String getTime_schedule_id() {
		return time_schedule_id;
	}
	public void setTime_schedule_id(String time_schedule_id) {
		this.time_schedule_id = time_schedule_id;
	}
	public String getSit_id() {
		return sit_id;
	}
	public void setSit_id(String sit_id) {
		this.sit_id = sit_id;
	}
	public Integer getAvailable() {
		return available;
	}
	public void setAvailable(Integer available) {
		this.available = available;
	}
	
	

}
