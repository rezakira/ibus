package co.id.rokubi.controller.bean;

import org.springframework.web.multipart.MultipartFile;

import co.id.rokubi.model.User;

/**
 * @author Radian
 *
 */
@SuppressWarnings("serial")
public class UserBean extends User {

	private MultipartFile image;

	/**
	 * @return the image
	 */
	public MultipartFile getImage() {
		return image;
	}

	/**
	 * @param image the image to set
	 */
	public void setImage(MultipartFile image) {
		this.image = image;
	}

}



	
