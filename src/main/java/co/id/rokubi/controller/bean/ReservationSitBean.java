package co.id.rokubi.controller.bean;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import org.codehaus.jackson.map.annotate.JsonSerialize;

import co.id.rokubi.util.formater.JsonDateTimeSerializer;

@SuppressWarnings("serial")
public class ReservationSitBean implements Serializable {
	private String booked_id;
	private List<String> list_sit_booked;
	
	public String getBooked_id() {
		return booked_id;
	}
	public void setBooked_id(String booked_id) {
		this.booked_id = booked_id;
	}
	public List<String> getList_sit_booked() {
		return list_sit_booked;
	}
	public void setList_sit_booked(List<String> list_sit_booked) {
		this.list_sit_booked = list_sit_booked;
	}
	
	
	private Date added;
	private Date modifed;
	
	@JsonSerialize(using = JsonDateTimeSerializer.class)
	public Date getAdded() {
		return added;
	}
	public void setAdded(Date added) {
		this.added = added;
	}
	
	@JsonSerialize(using = JsonDateTimeSerializer.class)
	public Date getModifed() {
		return modifed;
	}
	
	public void setModifed(Date modifed) {
		this.modifed = modifed;
	}
	
}
