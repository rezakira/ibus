package co.id.rokubi.controller.bean;

import java.io.Serializable;
import java.util.Date;

import org.codehaus.jackson.map.annotate.JsonSerialize;

import co.id.rokubi.util.formater.JsonDateTimeSerializer;

@SuppressWarnings("serial")
public class ReservationTicketBean implements Serializable {
	private String user_id;
	private String from_destination;
	private String to_destination;
	private String date_reservation;
	private String time_reservation;
	private Integer count_passanger;
	private String booked_id;
	
	
	public String getBooked_id() {
		return booked_id;
	}
	public void setBooked_id(String booked_id) {
		this.booked_id = booked_id;
	}
	public String getUser_id() {
		return user_id;
	}
	public void setUser_id(String user_id) {
		this.user_id = user_id;
	}
	public String getFrom_destination() {
		return from_destination;
	}
	public void setFrom_destination(String from_destination) {
		this.from_destination = from_destination;
	}
	public String getTo_destination() {
		return to_destination;
	}
	public void setTo_destination(String to_destination) {
		this.to_destination = to_destination;
	}
	public String getDate_reservation() {
		return date_reservation;
	}
	public void setDate_reservation(String date_reservation) {
		this.date_reservation = date_reservation;
	}
	public String getTime_reservation() {
		return time_reservation;
	}
	public void setTime_reservation(String time_reservation) {
		this.time_reservation = time_reservation;
	}
	public Integer getCount_passanger() {
		return count_passanger;
	}
	public void setCount_passanger(Integer count_passanger) {
		this.count_passanger = count_passanger;
	}
	
	
	private Date added;
	private Date modifed;
	
	@JsonSerialize(using = JsonDateTimeSerializer.class)
	public Date getAdded() {
		return added;
	}
	public void setAdded(Date added) {
		this.added = added;
	}
	
	@JsonSerialize(using = JsonDateTimeSerializer.class)
	public Date getModifed() {
		return modifed;
	}
	public void setModifed(Date modifed) {
		this.modifed = modifed;
	}


	
	
}
