package co.id.rokubi.controller.bean;

public class Error{
	
	private String messageError;
	
	/**
	 * @return the messageError
	 */
	public String getMessageError() {
		return messageError;
	}

	/**
	 * @param messageError the messageError to set
	 */
	public void setMessageError(String messageError) {
		this.messageError = messageError;
	}

	/**
	 * @return the contentSoal
	 */
	public String getContentSoal() {
		return contentSoal;
	}

	/**
	 * @param contentSoal the contentSoal to set
	 */
	public void setContentSoal(String contentSoal) {
		this.contentSoal = contentSoal;
	}

	private String contentSoal;
	
	public Error()
	{
		
	}
	
	public Error(String messageError,String contentSoal)
	{
		this.messageError = messageError;
		this.contentSoal = contentSoal;
	}
}
