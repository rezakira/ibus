package co.id.rokubi.controller;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;

import co.id.rokubi.exception.InvalidFormException;
import co.id.rokubi.model.FormBean;
import co.id.rokubi.model.Model;
import co.id.rokubi.model.Page;

/**
 * 
 * @author gugun
 *
 */
public abstract class AbstractController {
	
	protected static final String INVALID_ACCESS_PAGE = "invalidaccess";
	
	/**
	 * 
	 * @param username
	 * @return
	 * @throws Exception 
	 */
	protected final boolean hasAccessRight (String taskId) {
		if (taskId == null)
			return false;
			
		SecurityContext context = SecurityContextHolder.getContext();
        if (context == null)
        	return false;
        else {
        	Authentication authentication = context.getAuthentication();
            if (authentication == null)
            	return false;
            for (GrantedAuthority auth : authentication.getAuthorities()) {
                if (taskId.equals(auth.getAuthority())){
                    return true;
                }
            }
            return false;
        }
	}
	
	protected final Map<String, String> getAccessMap () {
		Map<String, String> accessMap = new HashMap<String, String>();
		SecurityContext context = SecurityContextHolder.getContext();
       
        if (context != null) {
        	Authentication authentication = context.getAuthentication();
        	if (authentication != null) {
        		for (GrantedAuthority auth : authentication.getAuthorities()) {
                    accessMap.put(auth.getAuthority(), "active");
                }
        	}
        }
        
        return accessMap;
	}
	
	private static final String TIME_ZONE_HEADER = "timezoneId";
													
	protected final String getTimeZoneId (final HttpServletRequest request) {
		return request.getHeader(TIME_ZONE_HEADER);
	}
	
	private static Logger accessLog = Logger.getLogger(AbstractController.class);

	public void logTrace(Object message) {
		accessLog.trace(message);
	}

	public void logTrace(Object message, Throwable t) {
		accessLog.trace(message, t);
	}
	
	public void logDebug(Object message) {
		accessLog.debug(message);
	}

	public void logDebug(Object message, Throwable t) {
		accessLog.debug(message, t);
	}
	
	public void logInfo(Object message) {
		accessLog.info(message);
	}

	public void logInfo(Object message, Throwable t) {
		accessLog.info(message, t);
	}
	
	public void logWarn(Object message) {
		accessLog.warn(message);
	}
	
	public void logWarn(Object message, Throwable t) {
		accessLog.warn(message, t);
	}

	public void logError (Object message) {
		accessLog.error(message);
	}
	
	public void logError (Object message, Throwable t) {
		accessLog.error(message, t);
	}
	
	public void logFatal (Object message) {
		accessLog.fatal(message);
	}
	
	public void logFatal (Object message, Throwable t) {
		accessLog.fatal(message, t);
	}
	
	public String getCurrentUser () {
		return SecurityContextHolder.getContext().getAuthentication().getName();
	}
	
	public FormBean<? extends Model> view (FormBean<? extends Model> bean) throws InvalidFormException, UnsupportedOperationException {
		throw new UnsupportedOperationException("please implement view(formbean) method");
	}
	
	public Model view (Model model) throws InvalidFormException, UnsupportedOperationException {
		throw new UnsupportedOperationException("please implement view(model) method");
	}
	
	public int insert (Model model) throws InvalidFormException,  UnsupportedOperationException {
		throw new UnsupportedOperationException("please implement insert (model) method");
	}
	
	public int insert (FormBean<? extends Model> bean) throws InvalidFormException,  UnsupportedOperationException  {
		throw new UnsupportedOperationException("please implement insert (frombean) method");
	}
	
	public int update (Model model) throws InvalidFormException,  UnsupportedOperationException {
		throw new UnsupportedOperationException("please implement update (model) method");
	}
	
	public int update (FormBean<? extends Model> bean) throws InvalidFormException,  UnsupportedOperationException  {
		throw new UnsupportedOperationException("please implement update (formbean) method");
	}
	
	public int delete (Model model) throws InvalidFormException,  UnsupportedOperationException {
		throw new UnsupportedOperationException("please implement delete (model) method");
	}
	
	public int delete (FormBean<? extends Model> bean) throws InvalidFormException,  UnsupportedOperationException  {
		throw new UnsupportedOperationException("please implement delete (frombean) method");
	}
	
	public Page listDataPaging (int page, int limit, Map<String, Object> conditions, Map<String, String> sort ) throws InvalidFormException {
		throw new UnsupportedOperationException("please implement listDataPaging method");
	}
	
	public Page listUpdateDataPaging (int page, int limit, Map<String, Object> conditions, Map<String, String> sort ) throws InvalidFormException {
		throw new UnsupportedOperationException("please implement listUpdateDataPaging method");
	}
	
	public Page listDeleteDataPaging (int page, int limit, Map<String, Object> conditions, Map<String, String> sort ) throws InvalidFormException {
		throw new UnsupportedOperationException("please implement listUpdateDataPaging method");
	}
}
