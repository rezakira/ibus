package co.id.rokubi.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;








import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.ibus.model.UserDetails;

import co.id.rokubi.constant.SettingType;
import co.id.rokubi.constant.StatusType;
import co.id.rokubi.controller.bean.AppSetting;
import co.id.rokubi.exception.InternalServiceException;
import co.id.rokubi.exception.InvalidParamException;
import co.id.rokubi.model.JsonResponse;
import co.id.rokubi.model.Menu;
import co.id.rokubi.model.Page;
import co.id.rokubi.model.Setting;
import co.id.rokubi.model.TabelData;
import co.id.rokubi.model.User;
import co.id.rokubi.service.AppManagementService;
import co.id.rokubi.service.UserManagementService;
import co.id.rokubi.util.CommonUtil;

@Controller
@RequestMapping("/app")
public class AppController extends AbstractController {

	private static String pageMenu = "common/tableMenu";
	private static String tabMenu = "common/menu";

	@Autowired
	private AppManagementService appManagementService;
	@Autowired
	private UserManagementService userManagementService;
	@Value("${setting.logo.dir}")
	private String settingLogoDir;
	@Value("${setting.logo.baseurl}")
	private String settingLogoBaseurl;

	/**
	 * Methods untuk menyimpan general setting
	 * 
	 * @param appSetting
	 * @param result
	 * @param request
	 * @return
	 * @throws InternalServiceException 
	 * @throws InvalidParamException 
	 */
	@RequestMapping(value = "/setting/save.json", method = RequestMethod.POST)
	@ResponseBody
	public JsonResponse actionSaveSetting(
			@ModelAttribute("app") AppSetting appSetting, BindingResult result,
			HttpServletRequest request) throws InvalidParamException, InternalServiceException {
		JsonResponse response = new JsonResponse();
		User userDetails = userManagementService.findUserByUsername(getCurrentUser());
		
		if(userDetails.getRoleId().equalsIgnoreCase("MBM"))
		{
			UserDetails userDetailsRef = userDetails.getUserDetails();
			
			
			
			//insert new
			
			if(CommonUtil.isNullOrEmpty(userDetailsRef))
			{
				userDetailsRef = new UserDetails();
				
				userDetailsRef.setUserRefId(getCurrentUser());

				userDetailsRef.setAlamat(appSetting.getAddressOffice());
				userDetailsRef.setJenisUsaha(appSetting.getJenisUsaha());
				userDetailsRef.setJmlKaryawan(appSetting.getJmlKaryawan());
				userDetailsRef.setJmlPkPerbulan(appSetting.getJmlPk());
				userDetailsRef.setJmlPmPerbulan(appSetting.getJmlPm());
				userDetailsRef.setNamaDirektur(appSetting.getNamaDirektur());
				userDetailsRef.setNamaPerusahaan(appSetting.getNameOffice());
				userDetailsRef.setNoTelp(appSetting.getNoTelp());
				userDetailsRef.setNpwp(appSetting.getNpwp());
				
				userManagementService.saveUserDetails(userDetailsRef);
				
			}
			else
			{//updated
				userDetailsRef.setAlamat(appSetting.getAddressOffice());
				userDetailsRef.setJenisUsaha(appSetting.getJenisUsaha());
				userDetailsRef.setJmlKaryawan(appSetting.getJmlKaryawan());
				userDetailsRef.setJmlPkPerbulan(appSetting.getJmlPk());
				userDetailsRef.setJmlPmPerbulan(appSetting.getJmlPm());
				userDetailsRef.setNamaDirektur(appSetting.getNamaDirektur());
				userDetailsRef.setNamaPerusahaan(appSetting.getNameOffice());
				userDetailsRef.setNoTelp(appSetting.getNoTelp());
				userDetailsRef.setNpwp(appSetting.getNpwp());
				
				userManagementService.updateUserDetails(userDetailsRef);
				
			}
			
		}
		else
		{
			List<Setting> setting = new ArrayList<Setting>();
			// Name Office
			Setting attr = new Setting();
			attr.setId(SettingType.NAME_OFFICE.getValue());
			attr.setVal(appSetting.getNameOffice());
			setting.add(attr);
			attr = new Setting();
			// Address Office
			attr.setId(SettingType.ADDRESS_OFFICE.getValue());
			attr.setVal(appSetting.getAddressOffice());
			setting.add(attr);
			attr = new Setting();
	
			appManagementService.saveSettings(setting);
			List<Setting> settings = appManagementService.findAllSettings();
			AppSetting newSetting = new AppSetting();
			
			for (Setting set : settings) {
				String id = set.getId();
				
				if(id.equals("NAME"))
				{
					newSetting.setNameOffice(set.getVal());
				}
				else
				if(id.equals("ADDRS"))
				{
					newSetting.setAddressOffice(set.getVal());
				}
			}
			response.setObj(newSetting);
		}
		
		
		
		response.setMessage(JsonResponse.SUCCESS, "success");
		return response;
	}

	@RequestMapping(value = "menus.page", method = RequestMethod.GET)
	public ModelAndView actionListMenuPage() throws InvalidParamException,
			InternalServiceException {

		ModelAndView modelView = new ModelAndView(pageMenu);

		modelView.addObject("menus", appManagementService.findMenuByParam(""));

		return modelView;
	}

	@RequestMapping(value = "tab-menus.page", method = RequestMethod.GET)
	public ModelAndView actionTabMenuPage() throws InvalidParamException,
			InternalServiceException {

		ModelAndView modelView = new ModelAndView(tabMenu);
		String username = SecurityContextHolder.getContext()
				.getAuthentication().getName();

		modelView.addObject("menus",
				appManagementService.findMenuByUsername(username));

		return modelView;
	}

	@RequestMapping(value = "listActiveMenu.json", method = RequestMethod.GET)
	@ResponseBody
	public JsonResponse getListActiveMenu() throws InvalidParamException,
			InternalServiceException {

		JsonResponse response = new JsonResponse();
		try {

			// List<Menu> menus =
			// appManagementService.findActiveMenuByParam(StatusType.ACTIVE.getValue());
			response.setObj(appManagementService.findActiveMenus());

			response.setMessage(JsonResponse.SUCCESS, "Success.");
		} catch (Exception e) {
			// logger.error(e.getMessage());
			response.setMessage(JsonResponse.VALIDATE_ERROR,
					"Internal Server error. Please contact your administrator");
		}

		return response;
	}

	@Transactional
	@RequestMapping(value = "activedMenu.json", method = RequestMethod.POST)
	@ResponseBody
	public JsonResponse changeActivedMenu(@RequestParam("menuId") String menuId) {

		JsonResponse response = new JsonResponse();
		try {

			Menu menu = appManagementService.findMenuByID(menuId);
			menu.setStatus(StatusType.ACTIVE.getValue());
			appManagementService.saveMenu(menu);

			response.setMessage(JsonResponse.SUCCESS, "Success.");
		} catch (Exception e) {
			// logger.error(e.getMessage());
			response.setMessage(JsonResponse.VALIDATE_ERROR,
					"Internal Server error. Please contact your administrator");
		}

		return response;
	}

	@Transactional
	@RequestMapping(value = "deactivedMenu.json", method = RequestMethod.POST)
	@ResponseBody
	public JsonResponse changeDeactivedMenu(
			@RequestParam("menuId") String menuId) {

		JsonResponse response = new JsonResponse();
		try {
			Menu menu = appManagementService.findMenuByID(menuId);
			menu.setStatus(StatusType.NONACTIVE.getValue());

			appManagementService.saveMenu(menu);

			response.setMessage(JsonResponse.SUCCESS, "Success.");
		} catch (Exception e) {
			// logger.error(e.getMessage());
			response.setMessage(JsonResponse.VALIDATE_ERROR,
					"Internal Server error. Please contact your administrator");
		}

		return response;
	}

	@Transactional
	@RequestMapping(value = "getMenubyUsername.json", method = RequestMethod.POST)
	@ResponseBody
	public JsonResponse getMenubyUsername(
			@RequestParam("username") String username) {

		JsonResponse response = new JsonResponse();
		try {

			response.setObj(appManagementService.findMenuByUsername(username));

			response.setMessage(JsonResponse.SUCCESS, "Success.");
		} catch (Exception e) {
			// logger.error(e.getMessage());
			response.setMessage(JsonResponse.VALIDATE_ERROR,
					"Internal Server error. Please contact your administrator");
		}

		return response;
	}

	@SuppressWarnings({ "unchecked", "rawtypes" })
	@RequestMapping(value = "getUsernameActive.json", method = RequestMethod.GET)
	@ResponseBody
	public JsonResponse getUsernameActive() {
		Map mMap = new HashMap();
		JsonResponse response = new JsonResponse();
		try {

			String username = SecurityContextHolder.getContext()
					.getAuthentication().getName();
			mMap.put("username", username);
			response.setObj(mMap);

			response.setMessage(JsonResponse.SUCCESS, "Success.");
		} catch (Exception e) {
			// logger.error(e.getMessage());
			response.setMessage(JsonResponse.VALIDATE_ERROR,
					"Internal Server error. Please contact your administrator");
		}

		return response;
	}

	/**
	 * this method is used to take data menu with pagination (pass offset and
	 * limit from client). for using this method, the app need to install jquery
	 * data table (http://datatables.net/). and this method is used to take data
	 * by jquery datatable format. this method reviewed by gugun on april 29,
	 * 2014
	 * 
	 * @param echo
	 * @param offset
	 * @param limit
	 * @param searchByName
	 * @param activeStatus
	 * @return
	 */
	@RequestMapping(value = "/menu/data-table.json", method = RequestMethod.GET)
	@ResponseBody
	public TabelData listMenuDataTable(
			@RequestParam("sEcho") String echo,
			@RequestParam("iDisplayStart") Integer offset,
			@RequestParam("iDisplayLength") Integer limit,
			@RequestParam(value = "sSearch", required = false) String searchByName,
			@RequestParam(value = "sSearch_0", required = false) String activeStatus) {

		TabelData tabelData = new TabelData();
		tabelData.setsEcho(echo);

		Page page = appManagementService.findPageMenu(offset, limit);
		tabelData.setsEcho(echo);
		tabelData.setiTotalDisplayRecords(page.getTotalRow());
		tabelData.setiTotalRecords(10);
		tabelData.setAaData(page.getObj());
		return tabelData;
	}

}
