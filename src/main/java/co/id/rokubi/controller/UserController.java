package co.id.rokubi.controller;

import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.security.NoSuchAlgorithmException;
import java.security.Principal;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;

import co.id.rokubi.controller.bean.UserBean;
import co.id.rokubi.exception.InternalServiceException;
import co.id.rokubi.exception.InvalidParamException;

import co.id.rokubi.model.JsonResponse;
import co.id.rokubi.model.Page;
import co.id.rokubi.model.User;
import co.id.rokubi.service.UserManagementService;
import co.id.rokubi.util.CommonUtil;


@Controller
@RequestMapping("/user")
public class UserController extends AbstractController {

	// navigation pages
	private static final String LIST_USER_CHANGE = "common/user/_users-change";
	private static final String LIST = "common/user/users";
	private static final String EDIT = "common/user/edit-user";
	private static final String NEW = "common/user/register-user";
	private final String pageUserLog = "common/user-log";
	private final String pageEditProfile = "common/edit-profile";

	@Autowired
	private UserManagementService userManagementService;

	/**
	 * Methods untuk menavigasikan ke page tambah user
	 * 
	 * @param model
	 * @param request
	 * @param principal
	 * @param httpSession
	 * @return
	 * @throws InvalidParamException
	 * @throws InternalServiceException
	 */
	@RequestMapping(value = "register-user.page", method = RequestMethod.GET)
	public ModelAndView RegisterPage(ModelMap model,
			final HttpServletRequest request, Principal principal,
			HttpSession httpSession) throws InvalidParamException,
			InternalServiceException {

		ModelAndView modelView = new ModelAndView(NEW);

		// Get User List from database User
		modelView.addObject("listrole", userManagementService.findActiveRole());

		return modelView;
	}

	/**
	 * Methods untuk menavigasikan ke page edit user
	 * 
	 * @param username
	 * @return
	 * @throws InvalidParamException
	 * @throws InternalServiceException
	 */
	@RequestMapping(value = "editUser.page/{username}", method = RequestMethod.GET)
	public ModelAndView actionEditUserPage(@PathVariable String username)
			throws InvalidParamException, InternalServiceException {

		ModelAndView modelView = new ModelAndView(EDIT);

		modelView.addObject("user",
				userManagementService.findUserByUsername(username));
		modelView.addObject("listrole", userManagementService.findActiveRole());
		return modelView;
	}

	@RequestMapping(value = "userlog.page", method = RequestMethod.GET)
	public String actionUserLog() {
		return pageUserLog;
	}

	/**
	 * Methods untuk menavigasikan ke page edit profile user yang sedang login
	 * 
	 * @return
	 * @throws InvalidParamException
	 * @throws InternalServiceException
	 */
	@RequestMapping(value = "edit-profile.page", method = RequestMethod.GET)
	public ModelAndView actionEditProfile() throws InvalidParamException,
			InternalServiceException {
		ModelAndView modelView = new ModelAndView(pageEditProfile);

		// Get User from Session
		String username = SecurityContextHolder.getContext()
				.getAuthentication().getName();

		// Get User List from database User
		modelView.addObject("user",
				userManagementService.findUserByUsername(username));
		modelView.addObject("listrole", userManagementService.findActiveRole());
		return modelView;
	}

	/**
	 * Methods untuk menampilkan user list
	 * 
	 * @param model
	 * @param request
	 * @param principal
	 * @param httpSession
	 * @return
	 * @throws InvalidParamException
	 * @throws InternalServiceException
	 */
	@RequestMapping(value = "users.page", method = RequestMethod.GET)
	public ModelAndView userListPage() 
					throws InvalidParamException,
					InternalServiceException {

		ModelAndView modelView = new ModelAndView(LIST);

		// Get User List from database User
		Page page = userManagementService.findUserByParam(1, "%", ""); 
		
		modelView.addObject("listuser", page.getObj());
		modelView.addObject("listrole", userManagementService.findActiveRole());
		modelView.addObject("user_pic_path", userManagementService.getUserPicPath());
		modelView.addObject("page", page.getCurrentPage());
		modelView.addObject("total_page",page.getTotalPage());

		return modelView;
	}
	
	@RequestMapping(value = "/users-change.page", method = RequestMethod.POST)
	public ModelAndView userListPageChange(
			@RequestParam(value="page", required=false) Integer pageParam,
			@RequestParam(value="roleId", required=false) String roleParam, 
			@RequestParam(value="alphabet", required=false) String alphaParam) 
					throws InvalidParamException,
					InternalServiceException {
		
		logDebug("page="+pageParam);
		logDebug("roleId="+roleParam);
		logDebug("alphabet="+alphaParam+",alphabet low="+alphaParam.toLowerCase());
		
		ModelAndView modelView = new ModelAndView(LIST_USER_CHANGE);

		// Get User List from database User
		Page page = userManagementService.findUserByParam(pageParam, roleParam, alphaParam); 
		
		modelView.addObject("listuser", page.getObj());
		modelView.addObject("user_pic_path", userManagementService.getUserPicPath());
		modelView.addObject("page", page.getCurrentPage());
		modelView.addObject("total_page",page.getTotalPage());

		return modelView;
	}

	// Handle Action
	// Handle action new user/register user/edit user

	/**
	 * Methods untuk menyimpan atau me-update user
	 * 
	 * @param user
	 * @param result
	 * @return
	 * @throws InvalidParamException
	 * @throws InternalServiceException
	 */
	@RequestMapping(value = "saveUser.page", method = RequestMethod.POST)
	public String actionSaveUser(@ModelAttribute("user") UserBean user,
			BindingResult result) throws InvalidParamException,
			InternalServiceException {

		// Save User
		User userSave = new User();

		// Check Image User and save it
		MultipartFile multipartfile = user.getImage();
		String filename = "";
		if (multipartfile != null) {

			// save image to disk
			try {
				InputStream inputStream = null;
				OutputStream outputStream = null;
				if (multipartfile.getSize() > 0) {
					inputStream = multipartfile.getInputStream();
					filename = "D:/image/"
							+ multipartfile.getOriginalFilename();
					outputStream = new FileOutputStream(filename);
					int readBytes = 0;
					byte[] buffer = new byte[10000000];
					while ((readBytes = inputStream.read(buffer, 0, 10000000)) != -1) {
						outputStream.write(buffer, 0, readBytes);
					}
					outputStream.close();
					inputStream.close();

					userSave.setImageUrl(multipartfile.getOriginalFilename());
				} else {
					userSave.setImageUrl("empty"); // default image
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		} else {
			userSave.setImageUrl("empty"); // default image
		}

		// not null
	
		userSave.setFirstName(user.getFirstName());
		userSave.setLastName(user.getLastName());
		userSave.setUsername(user.getUsername());
		userSave.setEmail(user.getEmail());
		userSave.setPassword(user.getPassword());
		userSave.setRoleId(user.getRoleId());
		userSave.setUserNo(user.getUserNo());
		
		userSave.setIsAccountNonExpired(true);
		userSave.setIsAccountNonLocked(true);
		userSave.setIsCredentialsNonExpired(true);
		userSave.setIsEnabled(true);
		
		userSave.setCreateDate(new Date());
		userSave.setPublishDate(new Date());
		userSave.setPublisher(getCurrentUser());
		
		userSave.setPassword(null);//default null add by admin
		
		
		userSave.setStatus("A");
		
		userSave.setImageUrl(null);
		
		userManagementService.saveUser(userSave, user.getImage());
		
		//System.out.println("id direktorat ---> " + userSave.getIdDirektorat());

		return pageEditProfile;

	}
	
	
	@RequestMapping(value = "updateUser.page", method = RequestMethod.POST)
	public String actionUpdateUser(@ModelAttribute("user") UserBean user,
			BindingResult result) throws InvalidParamException,
			InternalServiceException {

		// Save User
		User userSave = userManagementService.findUserByUsername(user.getUsername());

		// Check Image User and save it
		MultipartFile multipartfile = user.getImage();
		String filename = "";
		if (multipartfile != null) {

			// save image to disk
			try {
				InputStream inputStream = null;
				OutputStream outputStream = null;
				if (multipartfile.getSize() > 0) {
					inputStream = multipartfile.getInputStream();
					filename = "D:/image/"
							+ multipartfile.getOriginalFilename();
					outputStream = new FileOutputStream(filename);
					int readBytes = 0;
					byte[] buffer = new byte[10000000];
					while ((readBytes = inputStream.read(buffer, 0, 10000000)) != -1) {
						outputStream.write(buffer, 0, readBytes);
					}
					outputStream.close();
					inputStream.close();
					userSave.setImageUrl(multipartfile.getOriginalFilename());
				} else {
					userSave.setImageUrl("empty"); // default image
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		} else {
			userSave.setImageUrl("empty"); // default image
		}

		
		userSave.setFirstName(user.getFirstName());
		userSave.setLastName(user.getLastName());
		userSave.setEmail(user.getEmail());
		userSave.setRoleId(user.getRoleId());
		userSave.setImageUrl(null);
		
		userManagementService.saveUser(userSave, user.getImage());
		
		//System.out.println("id direktorat ---> " + userSave.getIdDirektorat());

		return pageEditProfile;

	}

	// Handle action new user/register user/edit user
	/**
	 * Methods untuk meng update user profile yang sedang aktif
	 * 
	 * @param user
	 * @param result
	 * @return
	 * @throws InvalidParamException
	 * @throws InternalServiceException
	 */
	@RequestMapping(value = "updateProfile.page", method = RequestMethod.POST)
	public ModelAndView actionUpdateProfileUser(
			@ModelAttribute("user") UserBean user, BindingResult result)
			throws InvalidParamException, InternalServiceException {
		ModelAndView modelView = new ModelAndView(pageEditProfile);
		// Save User
		User userSave = user;
		
		//Check Image User and save it
		userManagementService.saveUserProfile(userSave, user.getImage());
		
		User updatedUser = userManagementService.findUserByUsername(SecurityContextHolder.getContext()
				.getAuthentication().getName());
		modelView.addObject("user", updatedUser);
		modelView.addObject("user_pic_url", userManagementService.getUserPicPath());
		modelView.addObject("listrole", userManagementService.findActiveRole());
		return modelView;

	}


	/**
	 * Methods untuk mendapatkan user info dan kembaliannya berupa JSON
	 * 
	 * @return
	 * @throws InvalidParamException
	 * @throws InternalServiceException
	 */
	@RequestMapping(value = "userStatusLogin.json", method = RequestMethod.GET)
	@ResponseBody
	public JsonResponse getUserLogin() throws InvalidParamException,
			InternalServiceException {
		JsonResponse response = new JsonResponse();
		Map<String, Object> jsonObj = new HashMap<String, Object>();

		try {
			String username = SecurityContextHolder.getContext()
					.getAuthentication().getName();
			User userActive = userManagementService
					.findUserByUsername(username);
			UserStatusLogin user = new UserStatusLogin(
					userActive.getUsername(), userActive.getFirstName(),
					userActive.getLastName(), userActive.getEmail(),userActive.getImageUrl());
			jsonObj.put("user", user);
			jsonObj.put("user_pic_path", userManagementService.getUserPicPath());
			response.setObj(jsonObj);

			response.setMessage(JsonResponse.SUCCESS, "Success.");
		} catch (Exception e) {
			logError("response error:", e);
			response.setMessage(JsonResponse.VALIDATE_ERROR,
					"Internal Server error. Please contact your administrator");
		}

		return response;
	}

	/**
	 * Inner Class User Status Login untuk menampung beberapa data user yang di
	 * tampilkan di header
	 * 
	 * @author Radian
	 * 
	 */
	class UserStatusLogin {

		// Attribute
		private String username;
		private String firstName;
		private String lastName;
		private String email;
		private String imageUrl;

		/**
		 * @return the imageUrl
		 */
		public String getImageUrl() {
			return imageUrl;
		}

		/**
		 * @param imageUrl the imageUrl to set
		 */
		public void setImageUrl(String imageUrl) {
			this.imageUrl = imageUrl;
		}

		// contructor
		public UserStatusLogin() {

		}

		public UserStatusLogin(String username, String firstName,
				String lastName, String email, String imageUrl) {
			this.username = username;
			this.firstName = firstName;
			this.lastName = lastName;
			this.email = email;
			this.imageUrl = imageUrl;
			
		}

		// Methods
		public String getUsername() {
			return username;
		}

		public void setUsername(String username) {
			this.username = username;
		}

		public String getFirstName() {
			return firstName;
		}

		public void setFirstName(String firstName) {
			this.firstName = firstName;
		}

		public String getLastName() {
			return lastName;
		}

		public void setLastName(String lastName) {
			this.lastName = lastName;
		}

		public String getEmail() {
			return email;
		}

		public void setEmail(String email) {
			this.email = email;
		}

	}


	
	
	@Transactional
	@RequestMapping(value = "activedUser.json", method = RequestMethod.POST)
	@ResponseBody
	public JsonResponse changeActivedUser(@RequestParam("username") String username) {

		JsonResponse response = new JsonResponse();
		try {

			User user = userManagementService.findUserByUsername(username);
			user.setIsEnabled(true);
			user.setStatus("A");
			
			userManagementService.updateStatusUser(user);

			response.setMessage(JsonResponse.SUCCESS, "Success.");
		} catch (Exception e) {
			// logger.error(e.getMessage());
			response.setMessage(JsonResponse.VALIDATE_ERROR,
					"Internal Server error. Please contact your administrator");
		}

		return response;
	}
	
	@Transactional
	@RequestMapping(value = "deactivedUser.json", method = RequestMethod.POST)
	@ResponseBody
	public JsonResponse changeDeactivedUser(@RequestParam("username") String username) {

		JsonResponse response = new JsonResponse();
		try {
			User user = userManagementService.findUserByUsername(username);
			user.setIsEnabled(false);
			user.setStatus("D");
			
			userManagementService.updateStatusUser(user);
			//user = userManagementService.findUserByUsername(username);
			
			response.setMessage(JsonResponse.SUCCESS, "Success.");
		} catch (Exception e) {
			// logger.error(e.getMessage());
			response.setMessage(JsonResponse.VALIDATE_ERROR,
					"Internal Server error. Please contact your administrator");
		}

		return response;
	}
	
	/**
	 * 
	 * @param username
	 * @return
	 */
	@Transactional
	@RequestMapping(value = "/reset.page", method = RequestMethod.POST)
	public ModelAndView resetPassword(@RequestParam("userid") String username) {
		ModelAndView modelView = new ModelAndView("common/login");
		try {
			userManagementService.resetPassword(username);
		} catch (NoSuchAlgorithmException e) {
			logError("error", e);
		}
		return modelView;
	}
	
	/**
	 * 
	 * @param username
	 * @return
	 */
	@Transactional
	@RequestMapping(value = "/changepass.page", method = RequestMethod.POST)
	public ModelAndView changePassword(@RequestParam("password") String newPassword, @RequestParam("confirmPassword") String confirmPassword) {
		ModelAndView modelView = new ModelAndView(pageEditProfile);
		if (CommonUtil.isNotNullOrEmpty(newPassword) && CommonUtil.isNotNullOrEmpty(confirmPassword) && newPassword.equals(confirmPassword)) {
			userManagementService.changePassword(newPassword);
		} else {
			logInfo("password doesn't match");
		}
		return modelView;
	}
}
