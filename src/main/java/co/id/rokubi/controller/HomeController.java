package co.id.rokubi.controller;

import java.security.Principal;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;






import co.id.rokubi.controller.bean.AppSetting;
import co.id.rokubi.exception.InternalServiceException;
import co.id.rokubi.exception.InvalidParamException;
import co.id.rokubi.model.Menu;
import co.id.rokubi.model.Setting;
import co.id.rokubi.model.User;
import co.id.rokubi.service.AppManagementService;
import co.id.rokubi.service.UserManagementService;
import co.id.rokubi.util.CommonUtil;

@Controller
@RequestMapping("/")
public class HomeController extends AbstractController {

	private final String pageHome = "home";
	private final String pageLogin = "common/login";
	private final String pageForgot = "common/forgot";
	private final String pageGeneralSetting = "general-setting";

	private final String pageMasterSubject = "master-subject";
	private final String pageNewContent = "newContent";

	private final String pageDashboard = "/dashboard/dashboard";
	private final String pageIndikatorChart = "/indikator-chart";
	

	@Autowired
	private AppManagementService appManagementService;

	
	@Autowired
	private UserManagementService userManagementService;	
	
	/**
	 * 
	 * @param model
	 * @param request
	 * @param principal
	 * @return
	 */
	@RequestMapping(method = RequestMethod.GET)
	public ModelAndView index(ModelMap model, final HttpServletRequest request,
			Principal principal, HttpSession httpSession) {
		return home(model, request, principal, httpSession);
	}

	/**
	 * 
	 * @param model
	 * @param request
	 * @param principal
	 * @return
	 */
	@RequestMapping(value = "home.page")
	public ModelAndView home(ModelMap model, final HttpServletRequest request,
			Principal principal, HttpSession httpSession) {
		ModelAndView modelView = new ModelAndView(pageHome);
		try {
			List<Menu> menus = appManagementService.findMenuByUsername(getCurrentUser());
			modelView.addObject("menus", menus);
		} catch (InvalidParamException e) {
			logError("Error Getting Menu", e);
		} catch (InternalServiceException e) {
			logError("Error Getting Menu", e);
		}
		return modelView;
	}

	/**
	 * 
	 * @param model
	 * @param request
	 * @param principal
	 * @return
	 */
	@RequestMapping(value = "login.page")
	public String login(ModelMap model, final HttpServletRequest request,
			Principal principal) {
		return pageLogin;
	}

	@RequestMapping(value = "forgot.page", method = RequestMethod.GET)
	public String forgotPassword(ModelMap model,
			final HttpServletRequest request, Principal principal,
			HttpSession httpSession) {
		return pageForgot;
	}
	private final String pageGeneralSettingMember = "general-setting-member";
	
	@RequestMapping(value = "setting.page", method = RequestMethod.GET)
	public ModelAndView actionGeneralSetting() throws InvalidParamException, InternalServiceException {
		ModelAndView modelView = null;
		
		

		
		AppSetting appSetting = new AppSetting();
		User userDetails = 
				userManagementService.findUserByUsername(getCurrentUser());
		
				
		if(userDetails.getRoleId().equalsIgnoreCase("MBM"))
		{
			modelView = new ModelAndView(pageGeneralSettingMember);
			appSetting.setAddressOffice(CommonUtil.isNotNullOrEmpty(userDetails.getUserDetails())?userDetails.getUserDetails().getAlamat():"");
			appSetting.setNameOffice(CommonUtil.isNotNullOrEmpty(userDetails.getUserDetails())?userDetails.getUserDetails().getNamaPerusahaan():"");
			
			appSetting.setNamaDirektur(CommonUtil.isNotNullOrEmpty(userDetails.getUserDetails())?userDetails.getUserDetails().getNamaDirektur():"");
			appSetting.setNpwp(CommonUtil.isNotNullOrEmpty(userDetails.getUserDetails())?userDetails.getUserDetails().getNpwp():"");
			appSetting.setNoTelp(CommonUtil.isNotNullOrEmpty(userDetails.getUserDetails())?userDetails.getUserDetails().getNoTelp():"");
			appSetting.setJenisUsaha(CommonUtil.isNotNullOrEmpty(userDetails.getUserDetails())?userDetails.getUserDetails().getJenisUsaha():"");
			appSetting.setJmlKaryawan(CommonUtil.isNotNullOrEmpty(userDetails.getUserDetails())?userDetails.getUserDetails().getJmlKaryawan():0);
			appSetting.setJmlPk(CommonUtil.isNotNullOrEmpty(userDetails.getUserDetails())?userDetails.getUserDetails().getJmlPkPerbulan():0);
			appSetting.setJmlPm(CommonUtil.isNotNullOrEmpty(userDetails.getUserDetails())?userDetails.getUserDetails().getJmlPmPerbulan():0);
			
		}
		else
		{
			modelView = new ModelAndView(pageGeneralSetting);
			List<Setting> settings = appManagementService.findAllSettings();
			for (Setting setting : settings) {
				String id = setting.getId();
				if(id.equals("NAME"))
				{
					appSetting.setNameOffice(setting.getVal());
				}
				else
				if(id.equals("ADDRS"))
				{
					appSetting.setAddressOffice(setting.getVal());
				}
			}
		}
		
		modelView.addObject("setting", appSetting);
	
		
		return modelView;
	}

	@RequestMapping(value = "master-subject.page", method = RequestMethod.GET)
	public String actionMasterSubject() {
		return pageMasterSubject;
	}

	@RequestMapping(value = "new-content.page", method = RequestMethod.GET)
	public String actionNewContent() {
		return pageNewContent;
	}
	

	@RequestMapping(value = "dashboard.page", method = RequestMethod.GET)
	public ModelAndView actionDashboardUser() throws InvalidParamException,
			InternalServiceException {
		ModelAndView modelView = new ModelAndView(pageDashboard);
		
	
		
		


		return modelView;
	}
	
	@RequestMapping(value = "indikatorChart.page", method = RequestMethod.GET)
	public ModelAndView actionChartIndicator() throws InvalidParamException,
			InternalServiceException {
		ModelAndView modelView = new ModelAndView(pageIndikatorChart);
		
	
		
		
		return modelView;
	}
	
	
	private final String pageRegistrationFree = "common/register_free";
	@RequestMapping(value = "registration.page", method = RequestMethod.GET)
	public String registrationFreePage(ModelMap model,
			final HttpServletRequest request, Principal principal,
			HttpSession httpSession) {
		return pageRegistrationFree;
	}
	
	
}
