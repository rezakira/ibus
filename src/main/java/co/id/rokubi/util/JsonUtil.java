
package co.id.rokubi.util;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.codehaus.jackson.map.ObjectMapper;

public class JsonUtil {
	@SuppressWarnings("unchecked")
	public static List<Map<String, Object>> jsonList (List<?> list) {
		List<Map<String, Object>> listMap = new ArrayList<Map<String,Object>>();
		ObjectMapper mapper = new ObjectMapper();
		if (list != null){
			for (Object obj : list) {
				listMap.add(mapper.convertValue(obj, Map.class));
			}
		}
		return listMap;
	}
	
	@SuppressWarnings("unchecked")
	public static Map<String, Object> jsonObj (Object obj) {
		ObjectMapper mapper = new ObjectMapper();
		return mapper.convertValue(obj, Map.class);
	}
	
	
	@SuppressWarnings("unchecked")
	public static List<Map<String, Object>> jsonList (List<?> list, List<String> ignoreList) {
		List<Map<String, Object>> listMap = new ArrayList<Map<String,Object>>();
		ObjectMapper mapper = new ObjectMapper();
		if (list != null){ 
			for (Object obj : list) {
				Map<String, Object> map = mapper.convertValue(obj, Map.class);
				for (String keyIgnore : ignoreList) {
					map.remove(keyIgnore);
				}
				listMap.add(map);
			}
		}
		return listMap;
	}
	
	@SuppressWarnings("unchecked")
	public static Map<String, Object> jsonObj (Object obj, List<String> ignoreList) {
		ObjectMapper mapper = new ObjectMapper();
		Map<String, Object> map = mapper.convertValue(obj, Map.class);
		for (String keyIgnore : ignoreList) {
			map.remove(keyIgnore);
		}
		return map;
	}
}
