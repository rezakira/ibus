package co.id.rokubi.util;

import java.awt.Color;
import java.awt.GradientPaint;
import java.io.File;
import java.io.IOException;

import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartUtilities;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.axis.CategoryAxis;
import org.jfree.chart.axis.CategoryLabelPositions;
import org.jfree.chart.axis.NumberAxis;
import org.jfree.chart.labels.ItemLabelAnchor;
import org.jfree.chart.labels.ItemLabelPosition;
import org.jfree.chart.labels.StandardCategoryItemLabelGenerator;
import org.jfree.chart.plot.CategoryPlot;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.chart.renderer.category.BarRenderer;
import org.jfree.data.category.CategoryDataset;
import org.jfree.data.category.DefaultCategoryDataset;
import org.jfree.data.general.DefaultPieDataset;
import org.jfree.ui.TextAnchor;

/**
 * @author Radian
 * 
 */
public class ChartUtil {

	public static void generatePieChart(DefaultPieDataset pieDataSet,
			String titleChart, String pathSave) {

		JFreeChart chart = ChartFactory.createPieChart(titleChart, // Title
				pieDataSet, // Dataset
				true, // Show legend
				true, // Use tooltips
				false // Configure chart to generate URLs?
				);
		try {
			ChartUtilities.saveChartAsJPEG(new File(pathSave), chart, 400, 250);
		} catch (Exception e) {
			System.out.println("Problem occurred creating chart.");
		}
	}
	
	
	public static void generateBarChart(DefaultCategoryDataset dataset,
			String titleChart, String Xdata, String Ydata, String pathSave, int width, int height) {

		JFreeChart chart = 
				ChartFactory.createBarChart(titleChart,
				Xdata, 
				Ydata, 
				dataset, 
				PlotOrientation.VERTICAL,
				false, true, false);
		try {
			
			ChartUtilities.saveChartAsJPEG(new File(pathSave), chart, width, height);
		} catch (Exception e) {
			System.out.println("Problem occurred creating chart.");
		}
	}
	
	@SuppressWarnings("deprecation")
	public static void generateBarChartLakipLaptah (CategoryDataset dataset,
			String titleChart, String Xdata, String Ydata, String pathSave, int width, int height) throws IOException {

        final JFreeChart chart = ChartFactory.createBarChart(
        		//titleChart,       // chart title
        		"",
        		Xdata,               // domain axis label
        		Ydata,                  // range axis label
                dataset,                  // data
                PlotOrientation.VERTICAL, // orientation
                false,                    // include legend
                true,                     // tooltips?
                false                     // URLs?
            );

        chart.setTitle(
        		   new org.jfree.chart.title.TextTitle(titleChart,
        		       new java.awt.Font("SansSerif", java.awt.Font.PLAIN, 10)
        		   )
        		);     
        
        // set the background color for the chart...
        chart.setBackgroundPaint(Color.white);
        //chart.getTitle().setHeight(10);

        // get a reference to the plot for further customisation...
        final CategoryPlot plot = chart.getCategoryPlot();
        plot.setBackgroundPaint(Color.WHITE);
        plot.setDomainGridlinePaint(Color.BLACK);
        plot.setRangeGridlinePaint(Color.BLACK);        
        
        // set the range axis to display integers only...
        final NumberAxis rangeAxis = (NumberAxis) plot.getRangeAxis();
        rangeAxis.setStandardTickUnits(NumberAxis.createIntegerTickUnits());

        // disable bar outlines...
        final BarRenderer renderer = (BarRenderer) plot.getRenderer();
        renderer.setDrawBarOutline(false);
        renderer.setItemMargin(0.10);
        
        // set up gradient paints for series...
        final GradientPaint gp0 = new GradientPaint(
            0.0f, 0.0f, Color.blue, 
            0.0f, 0.0f, Color.lightGray
        );
        final GradientPaint gp1 = new GradientPaint(
            0.0f, 0.0f, Color.green, 
            0.0f, 0.0f, Color.lightGray
        );
        renderer.setSeriesPaint(0, gp0);
        renderer.setSeriesPaint(1, gp1);
        
        
        renderer.setBaseItemLabelGenerator(new StandardCategoryItemLabelGenerator());
        renderer.setBaseItemLabelsVisible(true);
        
        renderer.setItemLabelsVisible(true);
        final ItemLabelPosition p = new ItemLabelPosition(
            ItemLabelAnchor.INSIDE12, TextAnchor.CENTER_RIGHT, 
            TextAnchor.CENTER_RIGHT, -Math.PI / 2.0
        );
        renderer.setPositiveItemLabelPosition(p);

        final ItemLabelPosition p2 = new ItemLabelPosition(
            ItemLabelAnchor.OUTSIDE12, TextAnchor.CENTER_LEFT, 
            TextAnchor.CENTER_LEFT, -Math.PI / 2.0
        );
        renderer.setPositiveItemLabelPositionFallback(p2);
        final CategoryAxis domainAxis = plot.getDomainAxis();
        domainAxis.setCategoryLabelPositions(CategoryLabelPositions.UP_45);
        // OPTIONAL CUSTOMISATION COMPLETED.        
        
		try {			
			ChartUtilities.saveChartAsPNG(new File(pathSave), chart, width, height);
		} catch (Exception e) {
			System.out.println("Problem occurred creating chart.");
		}
	}
	
	@SuppressWarnings("deprecation")
	public static void generateBarChartLaporanKerusakan (CategoryDataset dataset,
			String titleChart, String Xdata, String Ydata, String pathSave, int width, int height) throws IOException {

        final JFreeChart chart = ChartFactory.createBarChart(
        		//titleChart,       // chart title
        		"",
        		Xdata,               // domain axis label
        		Ydata,                  // range axis label
                dataset,                  // data
                PlotOrientation.VERTICAL, // orientation
                true,                    // include legend
                true,                     // tooltips?
                false                     // URLs?
            );
        chart.setTitle(
        		new org.jfree.chart.title.TextTitle 
        			(titleChart, new java.awt.Font("SansSerif", java.awt.Font.BOLD, 16)));     

        // set the background color for the chart...
        chart.setBackgroundPaint(Color.white);

        // get a reference to the plot for further customisation...
        final CategoryPlot plot = chart.getCategoryPlot();
        plot.setBackgroundPaint(Color.WHITE);
        plot.setDomainGridlinePaint(Color.BLACK);
        plot.setRangeGridlinePaint(Color.BLACK);        
        
        // set the range axis to display integers only...
        final NumberAxis rangeAxis = (NumberAxis) plot.getRangeAxis();
        rangeAxis.setStandardTickUnits(NumberAxis.createIntegerTickUnits());

        // disable bar outlines...
        final BarRenderer renderer = (BarRenderer) plot.getRenderer();
        renderer.setDrawBarOutline(false);
        renderer.setItemMargin(0.10);
        
        // set up gradient paints for series...
        final GradientPaint gp0 = new GradientPaint(
        		0.0f, 0.0f, Color.blue, 
        		0.0f, 0.0f, Color.lightGray
        		);
        final GradientPaint gp1 = new GradientPaint(
        		0.0f, 0.0f, Color.green, 
        		0.0f, 0.0f, Color.lightGray
        		);
        final GradientPaint gp2 = new GradientPaint(
        		0.0f, 0.0f, Color.red, 
        		0.0f, 0.0f, Color.lightGray
        		);
        final GradientPaint gp3 = new GradientPaint(
        		0.0f, 0.0f, Color.yellow, 
        		0.0f, 0.0f, Color.lightGray
        		);
        final GradientPaint gp4 = new GradientPaint(
        		0.0f, 0.0f, Color.cyan, 
        		0.0f, 0.0f, Color.lightGray
        		);
        
        renderer.setSeriesPaint(0, gp0);
        renderer.setSeriesPaint(1, gp1);
        renderer.setSeriesPaint(2, gp2);
        renderer.setSeriesPaint(3, gp3);
        renderer.setSeriesPaint(4, gp4);
        
        renderer.setBaseItemLabelGenerator(new StandardCategoryItemLabelGenerator());
        renderer.setBaseItemLabelsVisible(true);
        renderer.setBaseItemLabelFont(new java.awt.Font("SansSerif", java.awt.Font.BOLD, 14));
        
        renderer.setItemLabelsVisible(true);
        final ItemLabelPosition p = new ItemLabelPosition(
            ItemLabelAnchor.INSIDE12, TextAnchor.CENTER_RIGHT);
        renderer.setPositiveItemLabelPosition(p);

        final ItemLabelPosition p2 = new ItemLabelPosition(
            ItemLabelAnchor.OUTSIDE12, TextAnchor.CENTER_LEFT);
        renderer.setPositiveItemLabelPositionFallback(p2);        
        
        final CategoryAxis domainAxis = plot.getDomainAxis();
        domainAxis.setMaximumCategoryLabelLines(4);
        domainAxis.setMaximumCategoryLabelWidthRatio(1); 
        domainAxis.setLabelFont(new java.awt.Font("SansSerif", java.awt.Font.BOLD, 14));
        // OPTIONAL CUSTOMISATION COMPLETED.        
        
		try {			
			ChartUtilities.saveChartAsPNG(new File(pathSave), chart, width, height);
		} catch (Exception e) {
			System.out.println("Problem occurred creating chart.");
		}
	}	
}
