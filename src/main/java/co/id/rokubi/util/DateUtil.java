package co.id.rokubi.util;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;


public class DateUtil {
	public static Date addDays(Date date, int days) {
		Calendar cal = Calendar.getInstance();
		cal.setTime(date);
		cal.add(Calendar.DATE, days); // minus number would decrement the days
		return cal.getTime();
	}

	public static Date addMonth(Date date, int month) {
		Calendar cal = Calendar.getInstance();
		cal.setTime(date);
		cal.add(Calendar.MONTH, month); // minus number would decrement the days
		return cal.getTime();
	}

	public static Date convertStringtoDate(String dateString) {
		Date date = new Date();
		SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
		// String dateInString = "07/06/2013";

		try {
			date = formatter.parse(dateString);
		} catch (java.text.ParseException e) {
			date = new Date();
		}

		return date;
	}

	public static Date convertStringtoDate2(String dateString) {
		Date date = new Date();
		SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
		// String dateInString = "07/06/2013";

		try {
			date = formatter.parse(dateString);
		} catch (java.text.ParseException e) {
			date = new Date();
		}

		return date;
	}

	public static Date convertStringtoDate3(String dateString) {
		Date date = new Date();
		SimpleDateFormat formatter = new SimpleDateFormat("yyyy/MM/dd");
		// String dateInString = "07/06/2013";

		try {
			date = formatter.parse(dateString);
		} catch (java.text.ParseException e) {
			date = new Date();
		}

		return date;
	}

	public static String convertDatetoString(Date date) {
		String result = "";
		SimpleDateFormat formatter = new SimpleDateFormat("yyyy/MM/dd hh:mm:ss a");
		result = formatter.format(date);

		return result;
	}

	public static String convertDatetoString2(Date date) {
		String result = "";
		SimpleDateFormat formatter = new SimpleDateFormat("yyyy/MM/dd");
		result = formatter.format(date);

		return result;
	}

	public static String getMonth(Date date) {
		String result = "";
		SimpleDateFormat formatter = new SimpleDateFormat("MM");
		result = formatter.format(date);

		return result;
	}
	
	public static String getYear(Date date) {
		String result = "";
		SimpleDateFormat formatter = new SimpleDateFormat("yyyy");
		result = formatter.format(date);

		return result;
	}

	public static int totalDayByMonth(int month, int year) {

		if (month == 1 || month == 3 || month == 5 || month == 7 || month == 8
				|| month == 10 || month == 12

		) {

			return 31;
		}
		else if (month == 4 || month == 6 || month == 9 || month == 11 
		) {

			return 30;
		}
		else
		{
			if(year % 4 == 0)
			{
				return 29;
			}
			else
			{
				return 28;
			}
			
		}
	}
	
	
	public static String nameMonth(int month) {

		if(month == 1)
		{
			return "Januari";
		}
		else
		if(month == 2)
		{
			return "Februari";
		}
		else
		if(month == 3)
		{
			return "Maret";
		}
		else
		if(month == 4)
		{
			return "April";
		}
		else
		if(month == 5)
		{
			return "Mei";
		}
		else
		if(month == 6)
		{
			return "Juni";
		}
		else
		if(month == 7)
		{
			return "Juli";
		}
		else
		if(month == 8)
		{
			return "Agustus";
		}
		else
		if(month == 9)
		{
			return "September";
		}
		else
		if(month == 10)
		{
			return "Oktober";
		}
		else
		if(month == 11)
		{
			return "November";
		}
		else
		{
			return "Desember";
		}
	}
}
