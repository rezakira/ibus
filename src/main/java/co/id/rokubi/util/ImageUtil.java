package co.id.rokubi.util;

import java.awt.AlphaComposite;
import java.awt.Graphics2D;
import java.awt.RenderingHints;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Iterator;

import javax.imageio.IIOImage;
import javax.imageio.ImageIO;
import javax.imageio.ImageWriteParam;
import javax.imageio.ImageWriter;
import javax.imageio.stream.ImageOutputStream;

public class ImageUtil {
	public static BufferedImage resizeImageWithHint(BufferedImage originalImage, int width, int height, int type) {

		BufferedImage resizedImage = new BufferedImage(width, height, type);
		Graphics2D g = resizedImage.createGraphics();
		g.drawImage(originalImage, 0, 0, width, height, null);
		g.dispose();
		g.setComposite(AlphaComposite.Src);

		g.setRenderingHint(RenderingHints.KEY_INTERPOLATION,
				RenderingHints.VALUE_INTERPOLATION_BILINEAR);
		g.setRenderingHint(RenderingHints.KEY_RENDERING,
				RenderingHints.VALUE_RENDER_QUALITY);
		g.setRenderingHint(RenderingHints.KEY_ANTIALIASING,
				RenderingHints.VALUE_ANTIALIAS_ON);
		
		return resizedImage;
		
		 
	}
	
	public static String resizeImageWithHint(String dir, String fileName, String ext, int width, int height) throws IOException {
		BufferedImage originalImageBuff = ImageIO.read(new File(dir+fileName+ext));
		int type = originalImageBuff.getType() == 0? BufferedImage.TYPE_INT_ARGB : originalImageBuff.getType();
		BufferedImage resizeImage = resizeImageWithHint(originalImageBuff, width, height, type);
		
		ImageIO.write(resizeImage, ext.substring(1), new File(dir+fileName+"_"+width+"x"+height+ext));
		return fileName+"_"+width+"x"+height+ext;
	}
	
	@SuppressWarnings("resource")
	public static String compressImage(String dir, String fileName, String ext)throws IOException {
		File imageFile = new File(dir+fileName+ext);
		File compressedImageFile = new File(dir+fileName+"_result_compress"+ext);

		InputStream is = new FileInputStream(imageFile);
		OutputStream os = new FileOutputStream(compressedImageFile);

		float quality = 0.5f;

		// create a BufferedImage as the result of decoding the supplied InputStream
		BufferedImage image = ImageIO.read(is);

		// get all image writers for JPG format
		Iterator<ImageWriter> writers = ImageIO.getImageWritersByFormatName("jpg");

		if (!writers.hasNext())
		{
			throw new IllegalStateException("No writers found");
		}
		
		ImageWriter writer = (ImageWriter) writers.next();
		ImageOutputStream ios = ImageIO.createImageOutputStream(os);
		writer.setOutput(ios);

		ImageWriteParam param = writer.getDefaultWriteParam();

		// compress to a given quality
		param.setCompressionMode(ImageWriteParam.MODE_EXPLICIT);
		param.setCompressionQuality(quality);

		// appends a complete image stream containing a single image and
	    //associated stream and image metadata and thumbnails to the output
		writer.write(null, new IIOImage(image, null, null), param);

		// close all streams
		is.close();
		os.close();
		ios.close();
		writer.dispose();
		
		
		return fileName+"_result_compress"+ext;
	}
}
