package co.id.rokubi.util;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class StringUtil {
	public static boolean hasUpperCase(String str) {
		for (char c : str.toCharArray()) {
			if (Character.isUpperCase(c)) {
				return true;
			}
		}
		return false;
	}
	
	public static int indexOfUpper (String str) {
		int idxUpper = 0;
		for (char c : str.toCharArray()) {
			if (Character.isUpperCase(c)) {
				return idxUpper;
			}
			idxUpper++;
		}
		return -1;
	}
	
	public static String fieldToCol (String str) {
		String result = "";
		for (char c : str.toCharArray()) {
			if (Character.isUpperCase(c)) {
				result+=("_"+Character.toLowerCase(c));
			} else {
				result+=c;
			}
		}
		
		return result;
	}
	
	/**
	 * example : [1, 2]
	 * @return
	 */
	public static List<String> stringToListString (String str) {
		str = str.replace("[", "");
		str = str.replace("]", "");
		return Arrays.asList(str.split("\\s*,\\s*"));
	}
	
	public static List<Integer> stringToListInt (String str) {
		List<String> listStr = stringToListString(str);
		List<Integer> listInt = new ArrayList<Integer>();
		
		for (String strObj : listStr) {
			listInt.add(new Integer(strObj));
		}
		
		return listInt;
	}
	
	public static List<Long> stringToListLong (String str) {
		List<String> listStr = stringToListString(str);
		List<Long> listLong = new ArrayList<Long>();
		
		for (String strObj : listStr) {
			listLong.add(new Long(strObj));
		}
		
		return listLong;
	}
	
	private static final String EMAIL_PATTERN = "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";

	public static boolean isEmail (final String email) {
		Pattern  pattern = Pattern.compile(EMAIL_PATTERN);
		Matcher matcher = pattern.matcher(email);
		return matcher.matches();
	}
}
