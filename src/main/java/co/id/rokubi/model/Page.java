package co.id.rokubi.model;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.codehaus.jackson.annotate.JsonIgnore;

public class Page {
	private static final Integer DEFAULT_LIMIT = 10;
	private static final Integer DEFAULT_NAVIGATION_LIMIT = 10;
	private static final String QUERY_PAGING_TEMPLATE = "%s limit %d,%d";

	private List<? extends Model> obj;
	private List<? extends FormBean<? extends Model>> bean;
	private Integer totalRow;
	private Integer currentPage;
	private Integer limit;
	private List<String> sortBy;
	private String message;
	private String status;
	private Integer navigationLimit;
	private Integer totalPage;

	public List<? extends Model> getObj() {
		return obj;
	}

	public void setObj(List<? extends Model> obj) {
		this.obj = obj;
	}

	public List<? extends FormBean<? extends Model>> getBean() {
		return bean;
	}

	public void setBean(List<? extends FormBean<? extends Model>> bean) {
		this.bean = bean;
	}

	public Integer getTotalRow() {
		return totalRow;
	}

	public void setTotalRow(Integer totalRow) {
		this.totalRow = totalRow;
	}

	public Integer getCurrentPage() {
		return currentPage == null || currentPage <= 0 ? new Integer(1)
				: currentPage;
	}
	
	public Integer getNextPage () {
		return getCurrentPage()+1;
	}
	
	public Integer getPreviousPage () {
		return getCurrentPage()-1 < 1 ? 1 : getCurrentPage()-1;
	}

	public void setCurrentPage(Integer currentPage) {
		this.currentPage = currentPage;
	}

	public Integer getLimit() {
		return limit == null || limit <= 0 ? new Integer(DEFAULT_LIMIT) : limit;
	}

	public void setLimit(Integer limit) {
		this.limit = limit;
	}

	public String getSortBy() {
		if (this.sortBy != null && !this.sortBy.isEmpty()) {
			StringBuffer sortByQuery = new StringBuffer();
			for (String string : this.sortBy) {
				sortByQuery.append(string).append(",");
			}
			
			sortByQuery = sortByQuery.replace(sortByQuery.length()-1, sortByQuery.length(), "");
			return sortByQuery.toString();
		} else {
			return "1 DESC";
		}
	}

	public void addSortBy(String sortBy) {
		if (this.sortBy == null){
			this.sortBy = new ArrayList<String>();
		}
		
		this.sortBy.add(sortBy);
	}
	
	public void addSortBy(Entry<String, String> sortBy) {
		if (this.sortBy == null){
			this.sortBy = new ArrayList<String>();
		}
		
		this.sortBy.add(sortBy.getKey() + " " + sortBy.getValue());
	}
	
	public void addSortBy(Map<String, String> sortBy) {
		if (sortBy != null && !sortBy.isEmpty()) {
			for (Entry<String, String> entrySort: sortBy.entrySet()) {
				addSortBy(entrySort);
			}
		}
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public Integer getNavigationLimit() {
		return navigationLimit == null || navigationLimit <= 0 ? new Integer(DEFAULT_NAVIGATION_LIMIT) : navigationLimit;
	}

	public void setNavigationLimit(Integer navigationLimit) {
		this.navigationLimit = navigationLimit;
	}

	@JsonIgnore
	public Integer getOffset() {
		return (getCurrentPage() - 1) * getLimit();
	}

	@JsonIgnore
	public String getQueryPaging() {
		return String.format(QUERY_PAGING_TEMPLATE, getSortBy(), getOffset(),
				getLimit());
	}
	
	public Integer getTotalPage() {
		float limit = getLimit();
		float tmpTotalRow = totalRow;
		this.totalPage = (int) Math.ceil(tmpTotalRow/limit);
		return this.totalPage;
	}
	
	public static Integer calculatePage (Integer offset, Integer limit) {
		return ((int)Math.floor(offset/limit))+1;
	}	
}
