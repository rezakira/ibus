package co.id.rokubi.model;

public interface ICacheable {
	public Object get ();
	public String getId();
}
