package co.id.rokubi.model;

public class JsonResponse {

	public static final int SUCCESS = 1;
	public static final int NO_DATA_PROCESSED = -2;
	public static final int MISMATCH_DATA_TYPE = -3;
	public static final int VALIDATE_ERROR = -4;
	public static final int UNSET_MESSAGE = -99;

	private Object obj;
	private int status;
	private String message;

	public JsonResponse() {
		this.status = UNSET_MESSAGE;
	}

	public JsonResponse(int status, String message) {
		setMessage(status, message);
	}

	public void setMessage(int status, String message) {
		this.status = status;
		this.message = message;
	}

	public Object getObj() {
		return obj;
	}

	public void setObj(Object obj) {
		this.obj = obj;
	}

	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}
}
