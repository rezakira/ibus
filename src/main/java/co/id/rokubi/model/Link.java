package co.id.rokubi.model;

public class Link {
	private String url;
	private String icon;
	
	public Link (String url, String icon) {
		this.url = url;
		this.icon = icon;
	}
	
	public String getUrl() {
		return url;
	}
	public String getIcon() {
		return icon;
	}
}
