package co.id.rokubi.model;

import java.util.List;

public class TabelData {
	private String sEcho;
	private Integer iTotalRecords;
	private Integer iTotalDisplayRecords;
	private List<? extends Model> aaData;
	
	public String getsEcho() {
		return sEcho;
	}

	public void setsEcho(String sEcho) {
		this.sEcho = sEcho;
	}

	public Integer getiTotalRecords() {
		return iTotalRecords;
	}

	public void setiTotalRecords(Integer iTotalRecords) {
		this.iTotalRecords = iTotalRecords;
	}

	public Integer getiTotalDisplayRecords() {
		return iTotalDisplayRecords;
	}

	public void setiTotalDisplayRecords(Integer iTotalDisplayRecords) {
		this.iTotalDisplayRecords = iTotalDisplayRecords;
	}

	public List<? extends Model> getAaData() {
		return aaData;
	}

	public void setAaData(List<? extends Model> aaData) {
		this.aaData = aaData;
	}
	
	

}
