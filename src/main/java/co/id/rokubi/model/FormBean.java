package co.id.rokubi.model;

public interface FormBean<T extends Model> {
	T toModel();
	void fromModel(T model);
	void setModel(T model);
	T getModel();
}
