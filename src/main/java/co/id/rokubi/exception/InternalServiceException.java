package co.id.rokubi.exception;

import co.id.rokubi.constant.MessageType;

/**
 * exception for handling internal error inside service.
 * @author gugun
 *
 */
@SuppressWarnings("serial")
public class InternalServiceException extends AbstractException {

	/**
	 * call {@link AbstractException#AbstractException(String)}
	 * @param message
	 * @param type
	 */
	public InternalServiceException(String message, MessageType type) {
		super(message, type);
	}
	
	/**
	 * call {@link AbstractException#AbstractException(String, MessageType)}
	 * @param message
	 * @param type
	 */
	public InternalServiceException(String message) {
		super(message);
	}
	
}
