package co.id.rokubi.exception;

import co.id.rokubi.model.JsonResponse;

public class InvalidDataException extends Exception {

	private static final long serialVersionUID = -6379460281992851135L;

	private JsonResponse jsonResponse;

	public InvalidDataException() {
	}

	public InvalidDataException(String message) {
		super(message);
	}

	public InvalidDataException(JsonResponse jsonResponse) {
		super(jsonResponse.getMessage());
		this.jsonResponse = jsonResponse;
	}

	public JsonResponse getJsonResponse() {
		return jsonResponse;
	}
}
