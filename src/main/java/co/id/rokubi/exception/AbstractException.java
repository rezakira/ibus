package co.id.rokubi.exception;

import co.id.rokubi.constant.MessageType;
import co.id.rokubi.model.Message;

@SuppressWarnings("serial")
public abstract class AbstractException extends Exception {
	
	private Message messageObj;

	public Message getMessageObj() {
		return messageObj;
	}
	
	/**
	 * consturctor for creating InvalidParamException with the string message and enum {@link MessageType} as type of message.
	 * this constructor is recommended for developers to create instance for non {@link MessageType#ERROR} type of object. 
	 * creating instance for object which has type {@link MessageType#ERROR}, please use {@link AbstractException#AbstractException(String)}, 
	 * because type {@link MessageType#ERROR} is default for that consturctor.
	 * @param message
	 * @param type
	 */
	public AbstractException(String message, MessageType type) {
		this.messageObj = new Message();
		this.messageObj.setMessage(message);
		this.messageObj.setType(type);
	}
	
	/**
	 * constructor for creating InvalidParamException with the string message as a param(parameter).
	 * this constructor will create instance for messageObj and set {@link MessageType#ERROR} as type default value.
	 * @param message
	 */
	public AbstractException(String message) {
		this.messageObj = new Message();
		this.messageObj.setMessage(message);
		this.messageObj.setType(MessageType.ERROR);
	}


}
