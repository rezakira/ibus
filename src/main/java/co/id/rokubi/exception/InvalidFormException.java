package co.id.rokubi.exception;

import co.id.rokubi.model.JsonResponse;

public class InvalidFormException extends Exception {

	private JsonResponse jsonResponse;

	private static final long serialVersionUID = 6050177924980011429L;

	public InvalidFormException() {
	}

	public InvalidFormException(String message) {
		super(message);
	}

	public InvalidFormException(JsonResponse jsonResponse) {
		super(jsonResponse.getMessage());
		this.jsonResponse = jsonResponse;
	}

	public JsonResponse getJsonResponse() {
		return jsonResponse;
	}
}
