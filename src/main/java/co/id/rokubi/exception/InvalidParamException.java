package co.id.rokubi.exception;

import co.id.rokubi.constant.MessageType;

/**
 * this exception is used to help service class to send back validate result to service caller.
 * @author gugun
 */
@SuppressWarnings("serial")
public class InvalidParamException extends AbstractException {
	
	/**
	 * call {@link AbstractException#AbstractException(String)}
	 * @param message
	 * @param type
	 */
	public InvalidParamException(String message, MessageType type) {
		super(message, type);
	}
	
	/**
	 * call {@link AbstractException#AbstractException(String, MessageType)}
	 * @param message
	 * @param type
	 */
	public InvalidParamException(String message) {
		super(message);
	}
}
