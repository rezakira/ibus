package co.id.rokubi.service;

import java.util.List;

import co.id.rokubi.exception.InternalServiceException;
import co.id.rokubi.exception.InvalidParamException;

import co.id.rokubi.model.Menu;
import co.id.rokubi.model.Page;
import co.id.rokubi.model.Role;
import co.id.rokubi.model.Setting;



public interface AppManagementService extends IService {
	List<Menu> findActiveMenus();
	List<Setting> findAllSettings();

	List<Role> findActiveRoles();
	String getMessage(String code) throws InternalServiceException;
	
	// un review method
	void saveSettings (List<Setting> settings);
	
	String getValueOfSetting(String codeValue) throws InternalServiceException;
	

	

	/**
	 * Menu Management
	 */
	
	Menu findMenuByID (String menuId) throws InvalidParamException, InternalServiceException;
	
	Menu findMenuDetailsByID (String menuId) throws InvalidParamException, InternalServiceException;
	
	void saveMenu(Menu menu)  throws InvalidParamException, InternalServiceException;
	
	List<Menu> findMenuByParam(String param) throws InvalidParamException, InternalServiceException;
	
	List<Menu> findActiveMenuByParam(String param) throws InvalidParamException, InternalServiceException;
	
	List<Menu> findMenuByUsername (String username) throws InvalidParamException, InternalServiceException;
	
	Page findPageMenu (Integer offset, Integer limit) ;

	
}
