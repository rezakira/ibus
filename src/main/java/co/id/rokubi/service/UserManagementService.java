package co.id.rokubi.service;

import java.security.NoSuchAlgorithmException;
import java.util.List;

import org.springframework.web.multipart.MultipartFile;






import com.ibus.model.UserDetails;

import co.id.rokubi.exception.InternalServiceException;
import co.id.rokubi.exception.InvalidParamException;
import co.id.rokubi.model.Message;
import co.id.rokubi.model.Page;
import co.id.rokubi.model.Role;
import co.id.rokubi.model.User;

public interface UserManagementService extends IService {
	
	/**
	 * this method is used to find User by username. username is unique key for every User Object. so, 
	 * this method must return max one object.
	 * @param username
	 * unique key for User object
	 * @return
	 * max one User object returned. if the data isn't found, this method will return null.
	 * @throws InvalidParamException
	 * @throws InternalServiceException 
	 */
	User findUserByUsername (String username) throws InvalidParamException, InternalServiceException;
	
	/**
	 * 
	 * @param username
	 * @return
	 * @throws InvalidParamException
	 * @throws InternalServiceException
	 */
	User findUserDetailsByUsername (String username) throws InvalidParamException, InternalServiceException;
	
	/**
	 * 
	 * @param user
	 * @return
	 */
	Message saveUser(User user, MultipartFile file)  throws InvalidParamException, InternalServiceException;
	
	/**
	 * 
	 * @param user
	 * @return
	 */
	Message deleteUser(User user);
	
	/**
	 * 
	 * @param user
	 * @return
	 */
	Message publishUser(User user);
	
	/**
	 * 
	 * @param user
	 * @return
	 */
	Message unpublishUser(User user);
	
	List<Role> findActiveRole();
	
	
	List<Role> findAllRole();
	void saveRole(Role role);
	Role findRoleById(String id);
	void deleteRole(Role role);
	void publishRole(Role role);
	void unpublishRole(Role role);
	
	void findUnreadInbox();
	void findAllInbox();
	
	List<Role> listMenubyRoleId(String roleId) throws InvalidParamException, InternalServiceException;
	
	//tambahan untuk mengambil user list di tabel user di database fine
	public Page findUserByParam(Integer page, String roleId, String alpha) throws InvalidParamException, InternalServiceException;
	
	
	public void updateUserProfile(User user) throws InvalidParamException, InternalServiceException;
	
	public void updateUser(User user) throws InvalidParamException, InternalServiceException;

	
	public List<User> findUserByRoleId(String roleId) throws InvalidParamException, InternalServiceException;
	
	void updateStatusUser(User user);
	
			
		/**
		 * reset password/forgot password. set user password dengan user yang di generate oleh system.
		 * @param uid
		 * @throws NoSuchAlgorithmException
		 */
		public void resetPassword(final String uid) throws NoSuchAlgorithmException;
		
		/**
		 * get base user image directory
		 * @return
		 */
		public String getUserPicPath();
		
		/**
		 * change password
		 * @param newPassword
		 */
		public void changePassword(String newPassword);
		
		/**
		 * save profile
		 * @param user
		 * @param file
		 * @return
		 * @throws InternalServiceException
		 */
		public Message saveUserProfile (User user, MultipartFile file) throws InternalServiceException;

		Page findPageMenu(Integer offset, Integer limit);
		
		
		boolean isUserExist(String username);
		
		UserDetails saveUserDetails(UserDetails userDetails) throws InvalidParamException, InternalServiceException;
		UserDetails updateUserDetails(UserDetails userDetails) throws InvalidParamException, InternalServiceException;
		UserDetails findUserDetailsByUserRefId(String userRefID) throws InvalidParamException, InternalServiceException;
		
		boolean checkValidateUser(String username, String password)
				throws InvalidParamException, InternalServiceException;
}
