package co.id.rokubi.service.impl;

import java.io.File;
import java.io.FileOutputStream;
import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Date;
import java.util.Formatter;
import java.util.List;
import java.util.Random;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.mail.MailSender;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;












import com.ibus.dao.UserDetailsMapper;
import com.ibus.model.UserDetails;

import co.id.rokubi.constant.MessageType;
import co.id.rokubi.constant.StatusType;
import co.id.rokubi.dao.AccessMapper;
import co.id.rokubi.dao.RoleMapper;
import co.id.rokubi.dao.UserMapper;
import co.id.rokubi.exception.InternalServiceException;
import co.id.rokubi.exception.InvalidParamException;
import co.id.rokubi.model.Message;
import co.id.rokubi.model.Page;
import co.id.rokubi.model.Role;
import co.id.rokubi.model.User;
import co.id.rokubi.service.AppManagementService;
import co.id.rokubi.service.UserManagementService;
import co.id.rokubi.util.CommonUtil;
import co.id.rokubi.util.FileUtil;
import co.id.rokubi.util.ImageUtil;
import co.id.rokubi.util.StringUtil;

@Service
public class UserManagementServiceImpl extends AbstractService implements
		UserManagementService {

	@Autowired
	private UserMapper userMapper;
	@Autowired
	private RoleMapper roleMapper;
	@Autowired
	private AccessMapper accessMapper;
	@Autowired
	private AppManagementService appManagementService;

	@Autowired
	private MailSender mailSender;
	@Autowired
	private SimpleMailMessage simpleMailMessage;
	@Value("${user.pic.dir}")
	private String userPicDir;
	@Value("${user.pic.path}")
	private String userPicPath;


	private boolean isRoleExist(String roleId) {
		int count = roleMapper.selectCountByRoleId(roleId);
		if (count > 0) {
			// exist
			return true;
		} else {
			// not exist
			return false;
		}
	}


	@Transactional(readOnly = true)
	@Override
	public User findUserByUsername(String username)
			throws InvalidParamException, InternalServiceException {
		User user = null;

		if (CommonUtil.isNullOrEmpty(username)) {
			throw new InvalidParamException(
					appManagementService
							.getMessage("error.user.username.mandatory"));
		}

		try {
			user = userMapper.selectByPrimaryKey(username);
			user.setUserDetails(userDetailsMapper.selectByPrimaryKey(username));
			
		} catch (Exception ex) {
			logError("error:", ex);
			InternalServiceException internalException = new InternalServiceException(
					appManagementService.getMessage("error.service"));
			internalException.getMessageObj().setDescription(ex.getMessage());
			throw internalException;
		}
		return user;
	}

	@Transactional(readOnly = true)
	@Override
	public User findUserDetailsByUsername(String username) {
		User user = userMapper.selectByPrimaryKey(username);
		user.setAccess(accessMapper.selectByRoleId(user.getRoleId()));
		return user;
	}

	@Autowired
	private UserDetailsMapper userDetailsMapper;
	
	
	/**
	 * save user data to database, set profile picture on server folder.
	 */
	@Transactional(rollbackFor = Exception.class, propagation = Propagation.REQUIRED)
	@Override
	public Message saveUser(User user, MultipartFile file) throws InternalServiceException {
		
		// save picture file
		if (CommonUtil.isNotNullOrEmpty(file)) {
			final String FILE_PIC_NAME = "profile_pic";
			try {
				if (file.getSize() > 0) {
					String dir = userPicDir + File.separatorChar + user.getUsername() + File.separatorChar;
					File newFile = new File(dir);
					if(!newFile.exists()) newFile.mkdirs();
				      
					Pattern p = Pattern.compile("\\.[A-Za-z]*$");
					Matcher m = p.matcher(file.getOriginalFilename());
					String suffix = m.find() ? m.group() : ".jpg";
					String fileDir = dir + FILE_PIC_NAME + suffix; 
					newFile = new File(fileDir);
					if (newFile.exists()) newFile.delete();
					FileUtil.fastChannelCopy(file.getInputStream(), new FileOutputStream(fileDir));
					String fileResultName = ImageUtil.resizeImageWithHint(dir,FILE_PIC_NAME, suffix, 200, 200);
					user.setImageUrl(fileResultName);
				}
			} catch (Exception e) {
				logError("saving user error:", e);
				InternalServiceException ise = new InternalServiceException("Saving Error");
				ise.getMessageObj().setDescription(e.getMessage());
				throw ise;
			}
		}
		
		// save user
		if (!isUserExist(user.getUsername())) {
			//final String nameReceiver = user.getEmail();
			final String passwordReceiver = 
					CommonUtil.isNotNullOrEmpty(user.getPassword())?user.getPassword():getRandomPassword(LEN_PASSWORD);
			
			user.setAuthor("admin");
			user.setPassword(getPasswordSha1(passwordReceiver));
			
			userMapper.insert(user);
			
			
//			UserDetails userDetail = new UserDetails();
//			userDetail.setUserRefId(user.getUsername());
//			userDetail.setNamaPerusahaan(user.getNamaPerusahaan());
//			userDetail.setAlamat(user.getAlamat());
//			userDetail.setNpwp(user.getNpwp());
//			userDetail.setNamaDirektur(user.getNamaDirektur());
//			userDetail.setNoTelp(user.getNoTelepon());
//			userDetail.setJenisUsaha(user.getJenisUsaha());
//			userDetail.setJmlKaryawan(user.getJmlKaryawan());
//			userDetail.setJmlPkPerbulan(user.getJmlPkPerbulan());
//			userDetail.setJmlPmPerbulan(user.getJmlPmPerbulan());
//			
//			if(CommonUtil.isNotNullOrEmpty(user.getNamaPerusahaan()))
//			{
//				userDetailsMapper.insert(userDetail);
//			}
			
			
			//send email to member
//			new Thread() {
//				@Override
//				public void run() {
//					sendMail(nameReceiver, passwordReceiver,nameReceiver,"[i-Bis] Information New Member");
//					logInfo("sent.");
//				}
//			}.start();
			
			
			//send email to admin
//			User userAdmin = userMapper.selectByPrimaryKey("admin");
//			final String emailAdmin = userAdmin.getEmail();
//			final String emailMessage = "You got new member with username : "+ user.getUsername()+",Please check user management for actived user.";
//			new Thread() {
//				@Override
//				public void run() {
//					
//					sendMailwithMessage(emailMessage,emailAdmin,"[i-Bis] Information New Member");
//					logInfo("sent.");
//				}
//			}.start();
			
			
		} else {
			if(user.getImageUrl() == null || user.getImageUrl().equalsIgnoreCase("empty")) 
			{
				User getUser = userMapper.selectByPrimaryKey(user.getUsername());
				user.setImageUrl(getUser.getImageUrl());
			}
			
			user.setEditor(getCurrentUser());
			user.setLastUpdate(new Date());
			userMapper.updateUser(user);
			
		}

		Message message = new Message();
		message.setType(MessageType.SUCCESS);
		message.setMessage("success.");
		return message;
	}

	/**
	 * 
	 */
	@Transactional(rollbackFor = Exception.class, propagation = Propagation.REQUIRED)
	@Override
	public Message deleteUser(User user) {
		userMapper.deleteByPrimaryKey(user.getUsername());

		return null;
	}

	@Transactional(rollbackFor = Exception.class, propagation = Propagation.REQUIRED)
	@Override
	public Message publishUser(User user) {
		String name = SecurityContextHolder.getContext().getAuthentication()
				.getName();
		user.setPublisher(name);
		user.setUnpublishDate(new Date());
		user.setStatus(StatusType.PUBLISH.getValue());
		userMapper.updateByPrimaryKey(user);

		return null;
	}

	@Transactional(rollbackFor = Exception.class, propagation = Propagation.REQUIRED)
	@Override
	public Message unpublishUser(User user) {
		String name = SecurityContextHolder.getContext().getAuthentication()
				.getName();
		user.setUnpublisher(name);
		user.setUnpublishDate(new Date());
		user.setStatus(StatusType.UNPUBLISH.getValue());
		userMapper.updateByPrimaryKey(user);

		return null;
	}

	/**
	 * 
	 */
	@Transactional(rollbackFor = Exception.class, propagation = Propagation.REQUIRED)
	@Override
	public void saveRole(Role role) {
		role.setUpdateTime(new Date());
		role.setStatus(StatusType.ACTIVE.getValue());
		if (!isRoleExist(role.getId())) {
			roleMapper.insert(role);
		} else {
			roleMapper.updateByPrimaryKey(role);
		}
	}

	@Override
	public void findUnreadInbox() {
		

	}

	@Override
	public void findAllInbox() {
		

	}

	/**
	 * 
	 */
	@Transactional(readOnly = true)
	@Override
	public Role findRoleById(String id) {
		
		Role role = roleMapper.selectByPrimaryKey(id);
		return role;
	}

	/**
	 * 
	 */
	@Transactional(readOnly = true)
	@Override
	public List<Role> findActiveRole() {
		
		List<Role> role;
		role = roleMapper.selectByStatus("A");

		return role;
	}

	/**
	 * 
	 */
	@Transactional(readOnly = true)
	@Override
	public List<Role> findAllRole() {
		
		List<Role> role;
		role = roleMapper.selectAll();

		return role;
	}

	@Override
	public void deleteRole(Role role) {
		

	}

	@Override
	public void publishRole(Role role) {
		

	}

	@Override
	public void unpublishRole(Role role) {
		

	}

	@Override
	public Page findUserByParam(Integer page, String roleId, String alpha) throws InvalidParamException, InternalServiceException {
		List<User> listUser;
		Page pageObj = new Page();
		try {
			pageObj.setCurrentPage(page);
			pageObj.setLimit(8);
			listUser = userMapper.selectPageAll(pageObj.getLimit(), pageObj.getOffset(), roleId, alpha+"%", alpha.toLowerCase()+"%");
			
			pageObj.setTotalRow(userMapper.selectCountByParam(roleId, alpha+"%"));
			pageObj.setObj(listUser);
		} catch (Exception ex) {
			logError("error:", ex);
			InternalServiceException internalException = new InternalServiceException(
					appManagementService.getMessage("error.service"));
			internalException.getMessageObj().setDescription(ex.getMessage());
			throw internalException;
		}

		return pageObj;
	}

	


	@Override
	public void updateUserProfile(User user) throws InvalidParamException,
			InternalServiceException {
		
		
		try {
			if (isUserExist(user.getUsername())) {
				userMapper.updateProfileUser(user);
			}
							
		} catch (Exception ex) {
			logError("error:", ex);
			InternalServiceException internalException = new InternalServiceException(
					appManagementService.getMessage("error.service"));
			internalException.getMessageObj().setDescription(ex.getMessage());
			throw internalException;
		}
		
	}

	@Override
	public void updateUser(User user) throws InvalidParamException,
			InternalServiceException {
		
		try {
			if (isUserExist(user.getUsername())) {
				userMapper.updateUser(user);
			}
							
		} catch (Exception ex) {
			logError("error:", ex);
			InternalServiceException internalException = new InternalServiceException(
					appManagementService.getMessage("error.service"));
			internalException.getMessageObj().setDescription(ex.getMessage());
			throw internalException;
		}
	}

	@Override
	public List<User> findUserByRoleId(String roleId)
			throws InvalidParamException, InternalServiceException {
		
		List<User> users = userMapper.selectAllbyRole(roleId,"A");
		return users;
	}

	@Override
	public List<Role> listMenubyRoleId(String roleId)
			throws InvalidParamException, InternalServiceException {
		
		List<Role> listMenu = roleMapper.selectRoleDetails(roleId);
		return listMenu;
	}
	
	private User findUsersByEmail (String email) {
		List<User> listUsers = userMapper.selectByEmail(email);
		
		if (CommonUtil.isNotNullOrEmpty(listUsers)) {
			return listUsers.get(0);
		}
		
		return null;
	}
	
	private final Integer LEN_PASSWORD = 6;
	
	private String getRandomPassword(int length) {
		final String charset = "0123456789";

		Random rand = new Random(System.currentTimeMillis());
		StringBuilder sb = new StringBuilder();
		for (int i = 0; i < length; i++) {
			int pos = rand.nextInt(charset.length());
			sb.append(charset.charAt(pos));
		}
		return sb.toString();
	}
	
	private String getPasswordSha1 (String password) {
		String sha1 = "";
		try {
			MessageDigest crypt = MessageDigest.getInstance("SHA-1");
			crypt.reset();
			crypt.update(password.getBytes("UTF-8"));
			sha1 = byteToHex(crypt.digest());
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}
		return sha1;
	}
	
	private String byteToHex(final byte[] hash) {
		Formatter formatter = new Formatter();
		for (byte b : hash) {
			formatter.format("%02x", b);
		}
		String result = formatter.toString();
		formatter.close();
		return result;
	}
	
	public void sendMail (String username, String password, String email,String title) {
		SimpleMailMessage message = new SimpleMailMessage(simpleMailMessage);
		message.setText(String.format(simpleMailMessage.getText(), username, password));
		message.setTo(email);
		message.setSubject(title);
		
		mailSender.send(message);
	}
	
	public void sendMailwithMessage (String messageEmail, String email,String title) {
		SimpleMailMessage message = new SimpleMailMessage(simpleMailMessage);
		message.setText(messageEmail);
		message.setTo(email);
		message.setSubject(title);
		
		mailSender.send(message);
	}
	
	public void resetPassword(final String uid) throws NoSuchAlgorithmException {
		
		if (CommonUtil.isNotNullOrEmpty(uid)) {
			
			User userTmp = null;
			if (StringUtil.isEmail(uid)) {
				// reset by email
				userTmp = findUsersByEmail(uid);
			} else {
				// reset by username
				try {
					userTmp = findUserByUsername(uid);
				} catch (InvalidParamException e) {
					logError("error", e);
				} catch (InternalServiceException e) {
					logError("error", e);
				}
			}
			
			if (CommonUtil.isNotNullOrEmpty(userTmp)) {
				final String password = getRandomPassword(LEN_PASSWORD);
				final String name = userTmp.getFirstName() + " " + userTmp.getLastName();
				final String email = userTmp.getEmail();
				logInfo("send email to : " + userTmp.getUsername() + "[" +email+"]");
				String newPassSha = getPasswordSha1(password);
				userTmp.setPassword(newPassSha);
				try {
					saveUser(userTmp, null);
				} catch (InternalServiceException e) {
					e.printStackTrace();
				}
				
				new Thread() {
					@Override
					public void run() {
						sendMail(name, password,
								email,"[i-Bus] Reset Password");
						logInfo("sent.");
					}
				}.start();
			}
		}
	}
	@Override
	public void updateStatusUser(User user) {
		
		
		userMapper.updateStatusUser(user);
		
	}
	
	@Override
	public String getUserPicPath() {
		return userPicPath;
	}
	
	public void changePassword(String newPassword) {
		String newPasswordHash = getPasswordSha1(newPassword);
		userMapper.updatePassword(getCurrentUser(), newPasswordHash);
	}
	
	/**
	 * save user data to database, set profile picture on server folder.
	 */
	@Transactional(rollbackFor = Exception.class, propagation = Propagation.REQUIRED)
	@Override
	public Message saveUserProfile (User user, MultipartFile file) throws InternalServiceException {
		
		// save picture file
		if (CommonUtil.isNotNullOrEmpty(file)) {
			final String FILE_PIC_NAME = "profile_pic";
			try {
				if (file.getSize() > 0) {
					String dir = userPicDir + File.separatorChar + user.getUsername() + File.separatorChar;
					File newFile = new File(dir);
					if(!newFile.exists()) newFile.mkdirs();
				      
					Pattern p = Pattern.compile("\\.[A-Za-z]*$");
					Matcher m = p.matcher(file.getOriginalFilename());
					String suffix = m.find() ? m.group() : ".jpg";
					String fileDir = dir + FILE_PIC_NAME + suffix; 
					newFile = new File(fileDir);
					if (newFile.exists()) newFile.delete();
					FileUtil.fastChannelCopy(file.getInputStream(), new FileOutputStream(fileDir));
					String fileResultName = ImageUtil.resizeImageWithHint(dir,FILE_PIC_NAME, suffix, 200, 200);
					
					user.setImageUrl(fileResultName);
				}
			} catch (Exception e) {
				logError("saving user error:", e);
				InternalServiceException ise = new InternalServiceException("Saving Error");
				ise.getMessageObj().setDescription(e.getMessage());
				throw ise;
			}
		}
		
		// save user
		if(user.getImageUrl() == null || user.getImageUrl().equalsIgnoreCase("empty")) {
			User getUser = userMapper.selectByPrimaryKey(user.getUsername());
			user.setImageUrl(getUser.getImageUrl());
		}
		
		user.setEditor(getCurrentUser());
		user.setLastUpdate(new Date());
		
		userMapper.updateProfile(user);
			

		Message message = new Message();
		message.setType(MessageType.SUCCESS);
		message.setMessage("success.");
		return message;
	}

	@Override
	public String getServiceName() {
		
		return null;
	}

	@Override
	public Page findPageMenu(Integer offset, Integer limit) {
		Page page = new Page();
		page.setLimit(limit);
		page.setCurrentPage(Page.calculatePage(offset, limit));
		page.setObj(roleMapper.selectPageAll(limit, offset));
		page.setTotalRow(roleMapper.selectCountAll());
		return page;
	}


	@Override
	public boolean isUserExist(String username) {
		// TODO Auto-generated method stub
		int count = userMapper.selectCountByUsername(username);
		if (count > 0) {
			// exist
			return true;
		} else {
			// not exist
			return false;
		}
	}


	@Override
	public UserDetails updateUserDetails(UserDetails userDetails)
			throws InvalidParamException, InternalServiceException {
		// TODO Auto-generated method stub
		try {
			userDetailsMapper.updateByPrimaryKey(userDetails);
							
		} catch (Exception ex) {
			logError("error:", ex);
			InternalServiceException internalException = new InternalServiceException(
					appManagementService.getMessage("error.service"));
			internalException.getMessageObj().setDescription(ex.getMessage());
			throw internalException;
		}
		return userDetails;
	}


	@Override
	public UserDetails findUserDetailsByUserRefId(String userRefID)
			throws InvalidParamException, InternalServiceException {
		// TODO Auto-generated method stub
		UserDetails userDetails = new UserDetails();
		try {
			userDetails = userDetailsMapper.selectByPrimaryKey(userRefID);
							
		} catch (Exception ex) {
			logError("error:", ex);
			InternalServiceException internalException = new InternalServiceException(
					appManagementService.getMessage("error.service"));
			internalException.getMessageObj().setDescription(ex.getMessage());
			throw internalException;
		}
		return userDetails;
	}


	@Override
	public boolean checkValidateUser(String username, String password)
			throws InvalidParamException, InternalServiceException {
		// TODO Auto-generated method stub
		User user = new User();
		boolean statusValid = false;;

		if (CommonUtil.isNullOrEmpty(username)) {
			throw new InvalidParamException(
					appManagementService
							.getMessage("error.user.username.mandatory"));
		}

		try {

			if (isUserExist(username)) {
				user = userMapper.selectByPrimaryKey(username);
				String roleUser = user.getRoleId();

				// hanya member atau reseller yang dapat login mobile
				if (roleUser.equalsIgnoreCase("MBM") || roleUser.equalsIgnoreCase("ADM")) 
				{
					//cek status
					if (user.getStatus().equals(StatusType.ACTIVE.getValue())) {
						statusValid = true;
					} else {
						statusValid = false;
					}
				}
				
			} else {

			}
			
			if (statusValid == true) {
				String password_ = user.getPassword();
				String password_input = getPasswordSha1(password);
				//match password
				if (password_.equals(password_input)) {
					return true;
				} 
			} 

		} catch (Exception ex) {
			logError("error:", ex);
			InternalServiceException internalException = new InternalServiceException(
					appManagementService.getMessage("error.service"));
			internalException.getMessageObj().setDescription(ex.getMessage());
			throw internalException;
		}

		

		return false;
	}


	@Override
	public UserDetails saveUserDetails(UserDetails userDetails)
			throws InvalidParamException, InternalServiceException {
		// TODO Auto-generated method stub
		try {
			userDetailsMapper.insert(userDetails);
							
		} catch (Exception ex) {
			logError("error:", ex);
			InternalServiceException internalException = new InternalServiceException(
					appManagementService.getMessage("error.service"));
			internalException.getMessageObj().setDescription(ex.getMessage());
			throw internalException;
		}
		return userDetails;
	}

}
