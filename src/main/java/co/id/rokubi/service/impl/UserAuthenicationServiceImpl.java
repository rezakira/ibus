package co.id.rokubi.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.jdbc.JdbcDaoImpl;

import co.id.rokubi.exception.InternalServiceException;
import co.id.rokubi.exception.InvalidParamException;
import co.id.rokubi.model.User;
import co.id.rokubi.service.UserManagementService;
import co.id.rokubi.util.CommonUtil;

/**
 * important: please. don't change this class. this class will be executed from 
 * spring-security.xml (spring security service). 
 * 
 * instance of this class must be done on spring-security.xml. so this class doesn't need @service on the top.
 */
public class UserAuthenicationServiceImpl extends JdbcDaoImpl {
	
	@Autowired
	private UserManagementService userManagementService;
	
	/**
	 * this method will be called from super class (JDBCDaoImpl). the super class need to retrieve user data from database
	 * this project puts all user database access on UserManagementService.java
	 */
	@Override
	protected List<UserDetails> loadUsersByUsername(String username) {
		User tmpUser;
		try {
			tmpUser = userManagementService.findUserByUsername(username);
			if (CommonUtil.isNotNullOrEmpty(tmpUser)) {
				List<UserDetails> listUserDetails = new ArrayList<UserDetails>(); 
				listUserDetails.add(tmpUser);
				setEnableAuthorities(true);
				return listUserDetails;
			}
		} catch (InvalidParamException e) {
			userManagementService.logError("authentication error", e);
		} catch (InternalServiceException e) {
			userManagementService.logError("authentication error", e);
		}
		setEnableAuthorities(false);
		return new ArrayList<UserDetails>();
	}
	
	/**
	 * this method will be called from super class (JDBCDaoImpl) which is part of spring security classes.
	 * this method is used to retrieve user authorities from table access. 
	 * this project puts all user database (including ACCESS table) on UserManagementService.java
	 */
	@SuppressWarnings("unchecked")
	@Override
	protected List<GrantedAuthority> loadUserAuthorities(String username) {
		User user;
		try {
			user = userManagementService.findUserDetailsByUsername(username);
			if (CommonUtil.isNotNullOrEmpty(user)) {
				return (List<GrantedAuthority>) user.getAuthorities();
			} 
		} catch (InvalidParamException e) {
			userManagementService.logError("authentication error", e);
		} catch (InternalServiceException e) {
			userManagementService.logError("authentication error", e);
		}
		return null;
	}
	
}
