package co.id.rokubi.service.impl;

import org.apache.log4j.Logger;
import org.springframework.security.core.context.SecurityContextHolder;

import co.id.rokubi.service.IService;

public abstract class AbstractService implements IService{
	private static Logger serviceLog = Logger.getLogger(AbstractService.class);

	public void logTrace(Object message) {
		serviceLog.trace(message);
	}

	public void logTrace(Object message, Throwable t) {
		serviceLog.trace(message, t);
	}
	
	public void logDebug(Object message) {
		serviceLog.debug(message);
	}

	public void logDebug(Object message, Throwable t) {
		serviceLog.debug(message, t);
	}
	
	public void logInfo(Object message) {
		serviceLog.info(message);
	}

	public void logInfo(Object message, Throwable t) {
		serviceLog.info(message, t);
	}
	
	public void logWarn(Object message) {
		serviceLog.warn(message);
	}
	
	public void logWarn(Object message, Throwable t) {
		serviceLog.warn(message, t);
	}

	public void logError (Object message) {
		serviceLog.error(message);
	}
	
	public void logError (Object message, Throwable t) {
		serviceLog.error(message, t);
	}
	
	public void logFatal (Object message) {
		serviceLog.fatal(message);
	}
	
	public void logFatal (Object message, Throwable t) {
		serviceLog.fatal(message, t);
	}
	
	public String getCurrentUser () {
		return SecurityContextHolder.getContext().getAuthentication().getName();
	}
}
