package co.id.rokubi.service.impl;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.authentication.AuthenticationFailureHandler;


/**
 * important: please. don't change this class. this class will be executed from 
 * spring-security.xml (spring security service). 
 * 
 * instance of this class must be done on spring-security.xml. so this class doesn't need @service on the top.
 */
public class UserAuthenticationFailureServiceImpl extends AbstractService implements AuthenticationFailureHandler {

	@Override
	public void onAuthenticationFailure(HttpServletRequest req,
			HttpServletResponse resp, AuthenticationException auth)
			throws IOException, ServletException {
		logError("login failed", auth);
		req.setAttribute("msg", auth.getMessage());
		req.getRequestDispatcher("login.page").forward(req, resp);
	}

	@Override
	public String getServiceName() {
		return null;
	}

}
