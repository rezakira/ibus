package co.id.rokubi.service.impl;

import java.util.List;
import java.util.Locale;

import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Service;

import com.ibus.cache.AppCache;
import com.ibus.constant.MenusLink;

import co.id.rokubi.constant.SettingType;
import co.id.rokubi.constant.StatusType;
import co.id.rokubi.dao.MenuMapper;
import co.id.rokubi.dao.RoleMapper;
import co.id.rokubi.dao.SettingMapper;
import co.id.rokubi.exception.InternalServiceException;
import co.id.rokubi.exception.InvalidParamException;
import co.id.rokubi.model.Link;
import co.id.rokubi.model.Menu;
import co.id.rokubi.model.Page;
import co.id.rokubi.model.Role;
import co.id.rokubi.model.Setting;
import co.id.rokubi.service.AppManagementService;
import co.id.rokubi.util.CommonUtil;

@Service("appManagementService")
public class AppManagementServiceImpl extends AbstractService implements InitializingBean, AppManagementService {
	
	@Autowired
	private MenuMapper menuMapper;
	@Autowired
	private SettingMapper settingMapper;

	@Autowired
	private RoleMapper roleMapper;
	@Autowired
	private ApplicationContext applicationContext;


	@Override
	public void afterPropertiesSet() throws Exception {
		logInfo("i-Bus APP CACHE LOADER");
		logInfo("=================================================================");
//		logInfo("loading active menu ...");
//		List<Menu> listActiveMenu = findActiveMenus();
//		if (CommonUtil.isNotNullOrEmpty(listActiveMenu)) {
//			for (Menu menu : listActiveMenu) {
//				AppCache.add(menu, CacheType.MENU);
//			}
//		}
//		logInfo("loaded.");
//		
//		logInfo("loading all settings ...");
//		List<Setting> listSetting = findAllSettings();
//		if (CommonUtil.isNotNullOrEmpty(listSetting)) {
//			for (Setting setting : listSetting) {
//				AppCache.add(setting, CacheType.SETTING);
//			}
//		}
//		logInfo("loaded.");
//		
//		logInfo("loading all event ...");
//		List<CalendarEvent> listAllEvents = findAllEvents();
//		if (CommonUtil.isNotNullOrEmpty(listAllEvents)) {
//			for (CalendarEvent event : listAllEvents) {
//				AppCache.add(event, CacheType.CALENDAR);
//			}
//		}
//		logInfo("loaded.");
//		
//		logInfo("loading all role ...");
//		List<Role> listActiveRole = findActiveRoles();
//		if (CommonUtil.isNotNullOrEmpty(listActiveRole)) {
//			for (Role role : listActiveRole) {
//				AppCache.add(role, CacheType.ROLE);
//			}
//		}
//		logInfo("loaded.");
		
		logInfo("=================================================================");
	}
	
	@Override
	public List<Menu> findActiveMenus() {
		return menuMapper.selectByStatus(StatusType.ACTIVE.getValue());
	}

	@Override
	public List<Setting> findAllSettings() {
		return settingMapper.selectAll();
	}

	
	@Override
	public List<Role> findActiveRoles() {
		return roleMapper.selectByStatus(StatusType.ACTIVE.getValue());
	}

	@Override
	public String getMessage(String code)  throws InternalServiceException {
		try {
			Locale loc = Locale.forLanguageTag(AppCache.getSettingMap().get(SettingType.LANGUAGE.getValue()).getVal());
			return applicationContext.getMessage(code, null, loc);
		} catch (Exception ex) {
			logError("SERVICE_ERROR:", ex);
			InternalServiceException internalException = new InternalServiceException("APP DATA ERROR");
			internalException.getMessageObj().setDescription(ex.getMessage());
			throw internalException;
		}
		
	}

	@Override
	public void saveSettings(List<Setting> settings) {
		for (Setting setting : settings) {
			settingMapper.updateByPrimaryKey(setting);
		}
	}


	private boolean isMenuExist(String menuId) {
		int count = menuMapper.selectCountByUsername(menuId);
		if (count > 0) {
			// exist
			return true;
		} else {
			// not exist
			return false;
		}
	}
	
	@Override
	public Menu findMenuByID(String menuId) throws InvalidParamException,
			InternalServiceException {
		
		
		Menu menu = menuMapper.selectByPrimaryKey(menuId);
		
		return menu;
	}

	@Override
	public Menu findMenuDetailsByID(String menuId)
			throws InvalidParamException, InternalServiceException {
		
		

		Menu menu = menuMapper.selectByPrimaryKey(menuId);
		
		return menu;
		
	}

	@Override
	public void saveMenu(Menu menu) throws InvalidParamException,
			InternalServiceException {
		
		if(!isMenuExist(menu.getId()))
		{
			menuMapper.insert(menu);
			
		}
		else
		{
			menuMapper.updateByPrimaryKey(menu);
		}
			
		
	}

	@Override
	public List<Menu> findMenuByParam(String param)
			throws InvalidParamException, InternalServiceException {
		
		
		List<Menu> menus;
		menus = menuMapper.selectAll();
		return menus;
	}

	@Override
	public List<Menu> findActiveMenuByParam(String param)
			throws InvalidParamException, InternalServiceException {
		
		List<Menu> menus;
		menus = menuMapper.selectByStatus(param);
		return menus;
	}


	@Override
	public List<Menu> findMenuByUsername(String username) throws InvalidParamException, InternalServiceException {
		List<Menu> listMenu;
		try {
			listMenu = menuMapper.selectMenubyUsername(username);
			for (Menu menu : listMenu) {
				List<Menu> listSubmenu = menuMapper.selectByParentId(username,menu.getId()); 
				Link link = MenusLink.link.get(menu.getId());
				if (CommonUtil.isNotNullOrEmpty(link)) {
					menu.setLink(link.getUrl());
					menu.setIcon(link.getIcon());
				}
				for (Menu subMenu : listSubmenu) {
					if(CommonUtil.isNotNullOrEmpty(MenusLink.link.get(subMenu.getId()))) {
						Link linkSub = MenusLink.link.get(subMenu.getId());
						if (CommonUtil.isNotNullOrEmpty(linkSub)) {
							subMenu.setLink(linkSub.getUrl());
							subMenu.setIcon(linkSub.getIcon());
						}
					}
					else{
						subMenu.setLink("#");
					}
				}
				
				menu.setSubMenu(listSubmenu);
			}
		}
		catch(Exception ex) {
			logError("SERVICE_ERROR:", ex);
			InternalServiceException internalException = new InternalServiceException("APP DATA ERROR");
			internalException.getMessageObj().setDescription(ex.getMessage());
			throw internalException;
		}
		
		return listMenu;
	}

	@Override
	public String getServiceName() {
		return null;
	}

	@Override
	public Page findPageMenu(Integer offset, Integer limit) {
		Page page = new Page();
		page.setLimit(limit);
		page.setCurrentPage(Page.calculatePage(offset, limit));
		page.setObj(menuMapper.selectPageAll(limit, offset));
		page.setTotalRow(menuMapper.selectCountAll());
		return page;
	}


	@Override
	public String getValueOfSetting(String codeValue)
			throws InternalServiceException {
		// TODO Auto-generated method stub
		List<Setting> settings = findAllSettings();
		

		for (Setting setting : settings) {
			
			String id = setting.getId();
			
			if(id.equals(codeValue))
			{
				return setting.getVal();
				
			}

			
		}
		
		return null;
	}
	
	
}
