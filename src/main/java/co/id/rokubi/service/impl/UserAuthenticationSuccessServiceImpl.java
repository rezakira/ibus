package co.id.rokubi.service.impl;

import java.io.IOException;
import java.util.Date;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.web.authentication.AuthenticationSuccessHandler;

import co.id.rokubi.dao.UserMapper;

/**
 * important: please. don't change this class. this class will be executed from 
 * spring-security.xml (spring security service). 
 * 
 * instance of this class must be done on spring-security.xml. so this class doesn't need @service on the top.
 */
public class UserAuthenticationSuccessServiceImpl extends AbstractService implements AuthenticationSuccessHandler  {
	
	@Autowired
	private UserMapper userMapper;

	@Override
	public void onAuthenticationSuccess(HttpServletRequest req,
			HttpServletResponse resp, Authentication auth) throws IOException,
			ServletException {
		logInfo("success");
		userMapper.updateLastLogin(auth.getName(), new Date());
		resp.sendRedirect("home.page");
	}

	@Override
	public String getServiceName() {
		return null;
	}

}
