package co.id.rokubi.dao;

import co.id.rokubi.model.Menu;

import java.util.List;

import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Result;
import org.apache.ibatis.annotations.Results;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;
import org.apache.ibatis.type.JdbcType;

public interface MenuMapper {
    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table menu
     *
     * @mbggenerated Sun Apr 13 14:40:41 ICT 2014
     */
    @Delete({
        "delete from menu",
        "where ID = #{id,jdbcType=VARCHAR}"
    })
    int deleteByPrimaryKey(String id);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table menu
     *
     * @mbggenerated Sun Apr 13 14:40:41 ICT 2014
     */
    @Insert({
        "insert into menu (ID, TITLE, ",
        "DESCRIPTION, LAST_UPDATE, ",
        "STATUS, PARENT_ID, ",
        "SORT_NO)",
        "values (#{id,jdbcType=VARCHAR}, #{title,jdbcType=VARCHAR}, ",
        "#{description,jdbcType=VARCHAR}, #{lastUpdate,jdbcType=TIMESTAMP}, ",
        "#{status,jdbcType=VARCHAR}, #{parentId,jdbcType=VARCHAR}, ",
        "#{sortNo,jdbcType=INTEGER})"
    })
    int insert(Menu record);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table menu
     *
     * @mbggenerated Sun Apr 13 14:40:41 ICT 2014
     */
    @Select({
        "select",
        "ID, TITLE, DESCRIPTION, LAST_UPDATE, STATUS, PARENT_ID, SORT_NO",
        "from menu",
        "where ID = #{id,jdbcType=VARCHAR}"
    })
    @Results({
        @Result(column="ID", property="id", jdbcType=JdbcType.VARCHAR, id=true),
        @Result(column="TITLE", property="title", jdbcType=JdbcType.VARCHAR),
        @Result(column="DESCRIPTION", property="description", jdbcType=JdbcType.VARCHAR),
        @Result(column="LAST_UPDATE", property="lastUpdate", jdbcType=JdbcType.TIMESTAMP),
        @Result(column="STATUS", property="status", jdbcType=JdbcType.VARCHAR),
        @Result(column="PARENT_ID", property="parentId", jdbcType=JdbcType.VARCHAR),
        @Result(column="SORT_NO", property="sortNo", jdbcType=JdbcType.INTEGER)
    })
    Menu selectByPrimaryKey(String id);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table menu
     *
     * @mbggenerated Sun Apr 13 14:40:41 ICT 2014
     */
    @Select({
        "select",
        "ID, TITLE, DESCRIPTION, LAST_UPDATE, STATUS, PARENT_ID, SORT_NO",
        "from menu"
    })
    @Results({
        @Result(column="ID", property="id", jdbcType=JdbcType.VARCHAR, id=true),
        @Result(column="TITLE", property="title", jdbcType=JdbcType.VARCHAR),
        @Result(column="DESCRIPTION", property="description", jdbcType=JdbcType.VARCHAR),
        @Result(column="LAST_UPDATE", property="lastUpdate", jdbcType=JdbcType.TIMESTAMP),
        @Result(column="STATUS", property="status", jdbcType=JdbcType.VARCHAR),
        @Result(column="PARENT_ID", property="parentId", jdbcType=JdbcType.VARCHAR),
        @Result(column="SORT_NO", property="sortNo", jdbcType=JdbcType.INTEGER)
    })
    List<Menu> selectAll();
    
    @Select({
        "select sub.*, parent.TITLE PARENT_TITLE from menu sub join menu parent ON parent.ID = sub.PARENT_ID order by sub.PARENT_ID, sub.SORT_NO", 
        "limit #{0,jdbcType=VARCHAR}" , "offset #{1,jdbcType=VARCHAR}"
    })
    @Results({
        @Result(column="ID", property="id", jdbcType=JdbcType.VARCHAR, id=true),
        @Result(column="TITLE", property="title", jdbcType=JdbcType.VARCHAR),
        @Result(column="DESCRIPTION", property="description", jdbcType=JdbcType.VARCHAR),
        @Result(column="LAST_UPDATE", property="lastUpdate", jdbcType=JdbcType.TIMESTAMP),
        @Result(column="STATUS", property="status", jdbcType=JdbcType.VARCHAR),
        @Result(column="PARENT_ID", property="parentId", jdbcType=JdbcType.VARCHAR),
        @Result(column="SORT_NO", property="sortNo", jdbcType=JdbcType.INTEGER),
        @Result(column="PARENT_TITLE", property="parentTitle", jdbcType=JdbcType.VARCHAR),
    })
    List<Menu> selectPageAll(Integer limit, Integer offset);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table menu
     *
     * @mbggenerated Sun Apr 13 14:40:41 ICT 2014
     */
    @Update({
        "update menu",
        "set TITLE = #{title,jdbcType=VARCHAR},",
          "DESCRIPTION = #{description,jdbcType=VARCHAR},",
          "LAST_UPDATE = #{lastUpdate,jdbcType=TIMESTAMP},",
          "STATUS = #{status,jdbcType=VARCHAR},",
          "PARENT_ID = #{parentId,jdbcType=VARCHAR},",
          "SORT_NO = #{sortNo,jdbcType=INTEGER}",
        "where ID = #{id,jdbcType=VARCHAR}"
    })
    int updateByPrimaryKey(Menu record);
    

	@Select({ "select",
			"ID, TITLE, DESCRIPTION, LAST_UPDATE, STATUS, PARENT_ID , SORT_NO",
			"from menu", "where STATUS = #{status,jdbcType=VARCHAR}" })
	@Results({
			@Result(column = "ID", property = "id", jdbcType = JdbcType.VARCHAR, id = true),
			@Result(column = "TITLE", property = "title", jdbcType = JdbcType.VARCHAR),
			@Result(column = "DESCRIPTION", property = "description", jdbcType = JdbcType.VARCHAR),
			@Result(column = "LAST_UPDATE", property = "lastUpdate", jdbcType = JdbcType.TIMESTAMP),
			@Result(column = "PARENT_ID", property = "parentId", jdbcType = JdbcType.VARCHAR),
			@Result(column = "SORT_NO", property="sortNo", jdbcType=JdbcType.INTEGER),
			@Result(column = "STATUS", property = "status", jdbcType = JdbcType.VARCHAR) })
	List<Menu> selectByStatus(String status);

	@Select({ "select", "count(1)", "from menu",
			"where ID = #{id, jdbcType=VARCHAR}" })
	@Result(javaType = java.lang.Integer.class)
	Integer selectCountByUsername(String id);
	
	@Select({ "select", "count(1)", "from menu" })
	@Result(javaType = java.lang.Integer.class)
	Integer selectCountAll();

	@Select({
			"select",
			"menu.*",
			"from role, access , user , menu",
			"where access.ROLE_ID=role.ID AND menu.ID = access.MENU_ID AND role.ID = user.ROLE_ID AND user.username = #{username,jdbcType=VARCHAR}",
			"AND menu.PARENT_ID is NULL",
			"ORDER BY menu.SORT_NO ASC"
	})
	@Results({
			@Result(column = "ID", property = "id", jdbcType = JdbcType.VARCHAR, id = true),
			@Result(column = "TITLE", property = "title", jdbcType = JdbcType.VARCHAR),
			@Result(column = "DESCRIPTION", property = "description", jdbcType = JdbcType.VARCHAR),
			@Result(column = "LAST_UPDATE", property = "lastUpdate", jdbcType = JdbcType.TIMESTAMP),
			@Result(column = "PARENT_ID", property = "parentId", jdbcType = JdbcType.VARCHAR),
			@Result(column = "SORT_NO", property="sortNo", jdbcType=JdbcType.INTEGER),
			@Result(column = "STATUS", property = "status", jdbcType = JdbcType.VARCHAR) })
	List<Menu> selectMenubyUsername(String username);

	@Select({ 
		"select",
		"menu.*",
		"from role, access , user , menu",
		"where access.ROLE_ID=role.ID AND menu.ID = access.MENU_ID AND role.ID = user.ROLE_ID AND user.username = #{0,jdbcType=VARCHAR}",
		"AND menu.PARENT_ID = #{1,jdbcType=VARCHAR}",
		"ORDER BY menu.SORT_NO ASC"
	})
	@Results({
			@Result(column = "ID", property = "id", jdbcType = JdbcType.VARCHAR, id = true),
			@Result(column = "TITLE", property = "title", jdbcType = JdbcType.VARCHAR),
			@Result(column = "DESCRIPTION", property = "description", jdbcType = JdbcType.VARCHAR),
			@Result(column = "LAST_UPDATE", property = "lastUpdate", jdbcType = JdbcType.TIMESTAMP),
			@Result(column = "PARENT_ID", property = "parentId", jdbcType = JdbcType.VARCHAR),
			@Result(column = "SORT_NO", property="sortNo", jdbcType=JdbcType.INTEGER),
			@Result(column = "STATUS", property = "status", jdbcType = JdbcType.VARCHAR) })
	List<Menu> selectByParentId(String username,String parentId);
}