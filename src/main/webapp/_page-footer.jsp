<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<div class="footer">
    <div class="footer-left">
        <span><spring:message code="label.copyright"/></span>
    </div>
    <div class="footer-right">
        <span>Visit <a href="#">HappyTax</a></span>
    </div>
</div><!--footer-->