<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

    <style>
        .btn-saving {
            float: left;
        }

        .btn-saving ~ img {
            padding-left: 10px;
            display: inline !important;
        }

        #btn-save {
            width: 100px;
        }

        #btn-save ~ img {
            display: none;
        }
    </style>

    <script>
        $(document)
            .ready(
            function() {
                $('#form_update_bus').validate(
                    {
                        rules : {

                        },
                        messages : {

                        },
                        highlight : function(label) {

                        },
                        success : function(label) {
                            label.text('Ok!').addClass(
                                'valid').closest(
                                '.control-group')
                                .addClass('success');
                        },
                        submitHandler : function(form) {
                            var formData = new FormData(form);



                            $.ajax({
                                url:$(form).attr('action'),
                                type:$(form).attr('method'),
                                data: formData,
                                contentType: false,
                                processData: false
                            }).done(function(data){

                                if(data.status == 1)
                                {
                                    jQuery.jGrowl(data.message, { life: 3000});

                                     $("#main_content").load('<c:url value="/data-master/list_rute_to.page" />'+'/'+'${ruteToDetails.idRuteFromRef}');
                                }
                                else
                                {
                                    jQuery.jGrowl(data.message, { life: 3000});

                                }

                            }).fail(function(){
                                var msg = "Failed";
                                jQuery.jGrowl(msg, { life: 3000});
                            });

                            return false;
                        }
                    });

            });
    </script>
    <!-- BEGIN CONTENT -->


    <!-- BEGIN PAGE BAR -->
    <div class="page-bar">
        <ul class="page-breadcrumb">
            <li>
                <a href="<c:url value="home.page"/>">Data Master</a>
                <i class="fa fa-angle-right"></i>
            </li>
            <li>
                <span>Manajemen Bis</span>
            </li>
        </ul>
        <div class="page-toolbar">
            <div class="btn-group pull-right">
                <a class="btn green" href="#"> <i class="fa fa-angle-left"></i> Back
                </a>

            </div>
        </div>
    </div>
    <!-- END PAGE BAR -->
    <!-- BEGIN PAGE TITLE-->
    <h3 class="page-title"> Edit Bis
    </h3>
    <!-- END PAGE TITLE-->
    <!-- END PAGE HEADER-->
    <div class="row">
        <div class="col-md-12 ">
            <!-- BEGIN SAMPLE FORM PORTLET-->
            <div class="portlet light bordered">
                <div class="portlet-title">
                    <div class="caption font-red-sunglo">
                        <i class="icon-settings font-red-sunglo"></i>
                        <span class="caption-subject bold uppercase"> Form Edit Bis</span>
                    </div>

                </div>
                <div class="portlet-body form">
                    <form id="form_update_bus" class="stdform stdform2" 
					method="post" 
					action="<c:url value="/data-master/updateRuteTo.json" />" enctype="multipart/form-data">
                        <div class="form-body">
						
						
						<input type="text" id="idRuteTo" name="idRuteTo" 
						 value="${ruteToDetails.idRuteTo}"
						 style="display:none"
						 class="form-control">
						 
						 
						<input type="text" id="idRuteFromRef" name="idRuteFromRef" 
						 value="${ruteToDetails.idRuteFromRef}"
						 style="display:none"
						 class="form-control">

                            <div class="form-group form-md-line-input ">
                                <input type="text" id="nameRuteTo" name="nameRuteTo" 
								 value="${ruteToDetails.nameRuteTo}"
								class="form-control">
                                <label for="">Nama Tempat</label>
                                <span class="help-block">Nama Tempat Keberangkatan</span>
                            </div>
                            <div class="form-group form-md-line-input ">
                                <textarea class="form-control" name="descRuteTo" cols="4" rows="3" id="descRuteTo">${ruteToDetails.descRuteTo}</textarea>
                                    <label for="form_control_1">Deskripsi</label>
                                    <span class="help-block">Deskripsi</span>
								
                            </div>
                            <div class="form-group form-md-line-input has-info">

                                    <select name="status" id="status" class="form-control">
                                        <option value="A" <c:if test="${ruteToDetails.status eq 'A' }">selected="selected"</c:if>>Aktif</option>
                                        <option value="D" <c:if test="${ruteToDetails.status eq 'D' }">selected="selected"</c:if>>Non Aktif</option>
                                   
                                    </select>
                                    <label for="form_control_1">Status</label>
                            </div>
                            <div class="form-group form-md-line-input ">
                                <input type="number" id="priceTicketUnit" name="priceTicketUnit" 
								value="${ruteToDetails.priceTicketUnit}"
								class="form-control">
                                <label for="">Harga Tiket</label>
                                <span class="help-block">Harga Tiket</span>
                            </div>

                            </div>
                            <div class="form-actions noborder">
                                <button type="submit" class="btn blue">Submit</button>
                                <button type="button" class="btn default">Cancel</button>
                            </div>
                            </form>
                        </div>
                </div>
                <!-- END SAMPLE FORM PORTLET-->

            </div>
        </div>

