<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

    <style>
        .btn-saving {
            float: left;
        }

        .btn-saving ~ img {
            padding-left: 10px;
            display: inline !important;
        }

        #btn-save {
            width: 100px;
        }

        #btn-save ~ img {
            display: none;
        }
    </style>

    <script>
        $(document)
            .ready(
            function() {
                $('#form_add_schedule_bus').validate(
                    {
                        rules : {

                        },
                        messages : {

                        },
                        highlight : function(label) {

                        },
                        success : function(label) {
                            label.text('Ok!').addClass(
                                'valid').closest(
                                '.control-group')
                                .addClass('success');
                        },
                        submitHandler : function(form) {
                            var formData = new FormData(form);



                            $.ajax({
                                url:$(form).attr('action'),
                                type:$(form).attr('method'),
                                data: formData,
                                contentType: false,
                                processData: false
                            }).done(function(data){

                                if(data.status == 1)
                                {
                                    jQuery.jGrowl(data.message, { life: 3000});
                                    $("#main_content").load('<c:url value="/data-master/list_schedule_bus.page" />');
                                }
                                else
                                {
                                    jQuery.jGrowl(data.message, { life: 3000});

                                }

                            }).fail(function(){
                                var msg = "Failed";
                                jQuery.jGrowl(msg, { life: 3000});
                            });

                            return false;
                        }
                    });

            });
    </script>
    <!-- BEGIN CONTENT -->


    <!-- BEGIN PAGE BAR -->
    <div class="page-bar">
        <ul class="page-breadcrumb">
            <li>
                <a href="<c:url value="home.page"/>">Data Master</a>
                <i class="fa fa-angle-right"></i>
            </li>
            <li>
                <span>Manajemen Schedule Bis</span>
            </li>
        </ul>
        <div class="page-toolbar">
            <div class="btn-group pull-right">
                <a class="btn green" href="#"> <i class="fa fa-angle-left"></i> Back
                </a>

            </div>
        </div>
    </div>
    <!-- END PAGE BAR -->
    <!-- BEGIN PAGE TITLE-->
    <h3 class="page-title"> Add Schedule Bis
    </h3>
    <!-- END PAGE TITLE-->
    <!-- END PAGE HEADER-->
    <div class="row">
        <div class="col-md-12 ">
            <!-- BEGIN SAMPLE FORM PORTLET-->
            <div class="portlet light bordered">
                <div class="portlet-title">
                    <div class="caption font-red-sunglo">
                        <i class="icon-settings font-red-sunglo"></i>
                        <span class="caption-subject bold uppercase"> Form Add Schedule Bis</span>
                    </div>

                </div>
                <div class="portlet-body form">
                    <form id="form_add_schedule_bus" class="stdform stdform2" 
					method="post" action="<c:url value="/data-master/saveScheduleBus.json" />" 
					enctype="multipart/form-data">
                        
						<div class="form-body">
							 <div class="form-group form-md-line-input ">
                                <select name="timeStart" id="timeStart" class="form-control">
								  <option value="07.00">07.00</option>
								  <option value="08.00">08.00</option>
								  <option value="09.00">09.00</option>
								  <option value="10.00">10.00</option>
								  <option value="11.00">11.00</option>
								  <option value="12.00">12.00</option>
								  <option value="13.00">13.00</option>
								  <option value="14.00">14.00</option>
								  <option value="15.00">15.00</option>
								  <option value="16.00">16.00</option>
								  <option value="17.00">17.00</option>
								  <option value="18.00">18.00</option>
								  <option value="19.00">19.00</option>
								</select>
                                <label for="">Waktu Keberangkatan</label>
                                <span class="help-block">Waktu Keberangkatan</span>
                            </div>
                            <div class="form-group form-md-line-input ">
                                <select  name="timeEnd" id="timeEnd" class="form-control">
								  <option value="07.00">07.00</option>
								  <option value="08.00">08.00</option>
								  <option value="09.00">09.00</option>
								  <option value="10.00">10.00</option>
								  <option value="11.00">11.00</option>
								  <option value="12.00">12.00</option>
								  <option value="13.00">13.00</option>
								  <option value="14.00">14.00</option>
								  <option value="15.00">15.00</option>
								  <option value="16.00">16.00</option>
								  <option value="17.00">17.00</option>
								  <option value="18.00">18.00</option>
								  <option value="19.00">19.00</option>
								</select>
                                <label for="">Waktu Sampai</label>
                                <span class="help-block">Waktu Sampai</span>
                            </div>
                            <div class="form-group form-md-line-input has-info">

                                    <select name="status" id="status" class="form-control">
                                        <option value="A" selected>Aktif</option>
                                        <option value="D" >Non Aktif</option>
                                    </select>
                                    <label for="form_control_1">Status</label>
                            </div>
                                

                            </div>
                            <div class="form-actions noborder">
                                <button type="submit" class="btn blue">Submit</button>
                                <button type="button" class="btn default">Cancel</button>
                            </div>
                            </form>
                        </div>
                </div>
                <!-- END SAMPLE FORM PORTLET-->

            </div>
        </div>

