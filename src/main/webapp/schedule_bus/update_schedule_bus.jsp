<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<style>
.btn-saving {
	float: left;
}

.btn-saving ~ img {
	padding-left: 10px;
	display: inline !important;
}

#btn-save {
	width: 100px;
}

#btn-save ~ img {
	display: none;
}
</style>

<script>
        $(document)
            .ready(
            function() {
                $('#form_update_schedule_bus').validate(
                    {
                        rules : {

                        },
                        messages : {

                        },
                        highlight : function(label) {

                        },
                        success : function(label) {
                            label.text('Ok!').addClass(
                                'valid').closest(
                                '.control-group')
                                .addClass('success');
                        },
                        submitHandler : function(form) {
                            var formData = new FormData(form);



                            $.ajax({
                                url:$(form).attr('action'),
                                type:$(form).attr('method'),
                                data: formData,
                                contentType: false,
                                processData: false
                            }).done(function(data){

                                if(data.status == 1)
                                {
                                    jQuery.jGrowl(data.message, { life: 3000});

                                     $("#main_content").load('<c:url value="/data-master/list_schedule_bus.page" />');
                                }
                                else
                                {
                                    jQuery.jGrowl(data.message, { life: 3000});

                                }

                            }).fail(function(){
                                var msg = "Failed";
                                jQuery.jGrowl(msg, { life: 3000});
                            });

                            return false;
                        }
                    });

            });
    </script>
<!-- BEGIN CONTENT -->


<!-- BEGIN PAGE BAR -->
<div class="page-bar">
	<ul class="page-breadcrumb">
		<li><a href="<c:url value="home.page"/>">Data Master</a> <i
			class="fa fa-angle-right"></i></li>
		<li><span>Manajemen Schedule Bis</span></li>
	</ul>
	<div class="page-toolbar">
		<div class="btn-group pull-right">
			<a class="btn green" href="#"> <i class="fa fa-angle-left"></i>
				Back
			</a>

		</div>
	</div>
</div>
<!-- END PAGE BAR -->
<!-- BEGIN PAGE TITLE-->
<h3 class="page-title">Edit Schedule Bis</h3>
<!-- END PAGE TITLE-->
<!-- END PAGE HEADER-->
<div class="row">
	<div class="col-md-12 ">
		<!-- BEGIN SAMPLE FORM PORTLET-->
		<div class="portlet light bordered">
			<div class="portlet-title">
				<div class="caption font-red-sunglo">
					<i class="icon-settings font-red-sunglo"></i> <span
						class="caption-subject bold uppercase"> Form Edit Schedule
						Bis</span>
				</div>

			</div>
			<div class="portlet-body form">
				<form id="form_update_schedule_bus" class="stdform stdform2"
					method="post"
					action="<c:url value="/data-master/updateScheduleBus.json" />"
					enctype="multipart/form-data">
					<div class="form-body">
						<input type="text" value="${scheduleBusDetails.idSchedule}"
							id="idSchedule" name="idSchedule" style="display: none"
							class="form-control">

						<div class="form-group form-md-line-input ">
							<select name="timeStart" id="timeStart" class="form-control">
								<option value="07.00"
									<c:if test="${scheduleBusDetails.timeStart eq '07.00' }">selected="selected"</c:if>>07.00</option>
								<option value="08.00"
									<c:if test="${scheduleBusDetails.timeStart eq '08.00' }">selected="selected"</c:if>>08.00</option>
								<option value="09.00"
									<c:if test="${scheduleBusDetails.timeStart eq '09.00' }">selected="selected"</c:if>>09.00</option>
								<option value="10.00"
									<c:if test="${scheduleBusDetails.timeStart eq '10.00' }">selected="selected"</c:if>>10.00</option>
								<option value="11.00"
									<c:if test="${scheduleBusDetails.timeStart eq '11.00' }">selected="selected"</c:if>>11.00</option>
								<option value="12.00"
									<c:if test="${scheduleBusDetails.timeStart eq '12.00' }">selected="selected"</c:if>>12.00</option>
								<option value="13.00"
									<c:if test="${scheduleBusDetails.timeStart eq '13.00' }">selected="selected"</c:if>>13.00</option>
								<option value="14.00"
									<c:if test="${scheduleBusDetails.timeStart eq '14.00' }">selected="selected"</c:if>>14.00</option>
								<option value="15.00"
									<c:if test="${scheduleBusDetails.timeStart eq '15.00' }">selected="selected"</c:if>>15.00</option>
								<option value="16.00"
									<c:if test="${scheduleBusDetails.timeStart eq '16.00' }">selected="selected"</c:if>>16.00</option>
								<option value="17.00"
									<c:if test="${scheduleBusDetails.timeStart eq '17.00' }">selected="selected"</c:if>>17.00</option>
								<option value="18.00"
									<c:if test="${scheduleBusDetails.timeStart eq '18.00' }">selected="selected"</c:if>>18.00</option>
								<option value="19.00"
									<c:if test="${scheduleBusDetails.timeStart eq '19.00' }">selected="selected"</c:if>>19.00</option>
							</select> <label for="">Waktu Keberangakatan</label> <span
								class="help-block">Waktu Keberangkatan</span>
						</div>
						<div class="form-group form-md-line-input ">
							<select name="timeEnd" id="timeEnd" class="form-control">
								<option value="07.00"
									<c:if test="${scheduleBusDetails.timeEnd eq '07.00' }">selected="selected"</c:if>>07.00</option>
								<option value="08.00"
									<c:if test="${scheduleBusDetails.timeEnd eq '08.00' }">selected="selected"</c:if>>08.00</option>
								<option value="09.00"
									<c:if test="${scheduleBusDetails.timeEnd eq '09.00' }">selected="selected"</c:if>>09.00</option>
								<option value="10.00"
									<c:if test="${scheduleBusDetails.timeEnd eq '10.00' }">selected="selected"</c:if>>10.00</option>
								<option value="11.00"
									<c:if test="${scheduleBusDetails.timeEnd eq '11.00' }">selected="selected"</c:if>>11.00</option>
								<option value="12.00"
									<c:if test="${scheduleBusDetails.timeEnd eq '12.00' }">selected="selected"</c:if>>12.00</option>
								<option value="13.00"
									<c:if test="${scheduleBusDetails.timeEnd eq '13.00' }">selected="selected"</c:if>>13.00</option>
								<option value="14.00"
									<c:if test="${scheduleBusDetails.timeEnd eq '14.00' }">selected="selected"</c:if>>14.00</option>
								<option value="15.00"
									<c:if test="${scheduleBusDetails.timeEnd eq '15.00' }">selected="selected"</c:if>>15.00</option>
								<option value="16.00"
									<c:if test="${scheduleBusDetails.timeEnd eq '16.00' }">selected="selected"</c:if>>16.00</option>
								<option value="17.00"
									<c:if test="${scheduleBusDetails.timeEnd eq '17.00' }">selected="selected"</c:if>>17.00</option>
								<option value="18.00"
									<c:if test="${scheduleBusDetails.timeEnd eq '18.00' }">selected="selected"</c:if>>18.00</option>
								<option value="19.00"
									<c:if test="${scheduleBusDetails.timeEnd eq '19.00' }">selected="selected"</c:if>>19.00</option>
							</select> <label for="">Waktu Sampai</label> <span class="help-block">Waktu
								Sampai</span>
						</div>
						<div class="form-group form-md-line-input has-info">
							<select name="status" id="status" class="form-control">
								<option value="A"
									<c:if test="${schedulBusDetails.status eq 'A' }">selected="selected"</c:if>>Aktif</option>
								<option value="D"
									<c:if test="${schedulBusDetails.status eq 'D' }">selected="selected"</c:if>>Non
									Aktif</option>
							</select> <label for="form_control_1">Status</label>
						</div>


					</div>
					<div class="form-actions noborder">
						<button type="submit" class="btn blue">Submit</button>
						<button type="button" class="btn default">Cancel</button>
					</div>
				</form>
			</div>
		</div>
		<!-- END SAMPLE FORM PORTLET-->

	</div>
</div>

