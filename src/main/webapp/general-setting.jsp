<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

    <style>
        .btn-saving {
            float: left;
        }

        .btn-saving ~ img {
            padding-left: 10px;
            display: inline !important;
        }

        #btn-save {
            width: 100px;
        }

        #btn-save ~ img {
            display: none;
        }
    </style>

    <script>
        $(document).ready(function() {
            $('#setting-form')
                .validate(
                {
                    rules : {

                    },
                    messages : {

                        // 				firstName: "Please enter your first name",
                        // 				lastName: "Please enter your last name",
                        // 				email: {
                        // 					required: "Please enter your e-mail",
                        // 					email: "E-mail is invalid"
                        // 				}
                    },
                    highlight : function(label) {
                        // 				setTimeout($.unblockUI, timeoutLoad);
                    },
                    success : function(label) {
                        label.text('Ok!').addClass(
                            'valid').closest(
                            '.control-group')
                            .addClass('success');
                    },
                    submitHandler : function(form) {
                        //form.submit(); 
                        var formData = new FormData(
                            form);
                        $('#btn-save').addClass(
                            'btn-saving');


                        $.ajax({
                            url:$(form).attr('action'),
                            type:$(form).attr('method'),
                            data: formData,
                            contentType: false,
                            processData: false
                        }).done(function(data){
                            jQuery.jGrowl(data.message, { life: 3000});
                            $('#btn-save').removeClass('btn-saving');
                        }).fail(function(){
                            var msg = "Failed";
                            jQuery.jGrowl(msg, { life: 3000});
                            $('#btn-save').removeClass('btn-saving');
                        });

                        return false;
                    }
                });

        });
    </script>

    <!-- BEGIN PAGE HEADER-->
    <h3 class="page-title"> Aplikasi &amp; Pengaturan

    </h3>
    <div class="page-bar">
        <ul class="page-breadcrumb">
            <li>
                <i class="icon-home"></i>
                <a href="">Home</a>
                <i class="fa fa-angle-right"></i>
            </li>
            <li>
                <span>Pengaturan</span>
            </li>
        </ul>

    </div>
    <!-- END PAGE HEADER-->

    <div class="maincontent">
        <div class="maincontentinner">
            <div class="widgetbox animate2 fadeInRightBig">
                <h4 class="widgettitle">Form Pengaturan</h4>
                <div class="widgetcontent nopadding">
                    <form id="setting-form" class="stdform stdform2" method="post"
                          action="<c:url value="/app/setting/save.json" />"
                          enctype="multipart/form-data">
                        <p>
                            <label>Nama Perusahaan</label> <span class="field"> 

                            <input type="text" name="nameOffice" value="${setting.nameOffice }"
                                   id="nameOffice" class="form-control"  />
                            </span>
                        </p>
                        <p>
                            <label>Alamat Perusahaan</label> <span class="field"> 
                            <textarea rows="10" cols="10" class="form-control" name="addressOffice" id="addressOffice">${setting.addressOffice }</textarea>

                            </span>
                        </p>

                        <p class="stdformbutton">
                            <button class="btn btn-success btn-large" id="btn-save">Simpan</button>
                            <img src="images/loaders/loader6.gif" alt="" />
                        </p>
                    </form>
                </div>
                <!--widgetcontent-->
            </div>

            <%@ include file="/common//mainFooter.jsp"%>

                </div>
            <!--maincontentinner-->
        </div>
        <!--maincontent-->
    </div>
