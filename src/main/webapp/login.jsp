<%@ include file="/_header.jsp"%>

<script>
    jQuery(document).ready(function(){
        jQuery('#login').submit(function(){
            var u = jQuery('#username').val();
            var p = jQuery('#password').val();
            var error = jQuery('#msg').html();
            if(u == '' && p == '') {
                jQuery('.login-alert').fadeIn();
                return false;
            }
        });
    });
</script>

</head>

<body class="loginpage">

<div class="loginpanel">
    <div class="loginpanelinner">
        
        <div class="logo animate0 bounceIn"><img src="<c:url value="/images/logo-kemenkes.png"/>" alt="" /></div>
        <form id="login" action="<c:url value='j_spring_security_check' />" method="post">
            <div class="inputwrapper login-alert">
                <div class="alert alert-error" id="msg">${msg}</div>
            </div>
            <div class="inputwrapper animate1 bounceIn">
                <input type="text" name="j_username" id="username" placeholder="Enter any username" />
            </div>
            <div class="inputwrapper animate2 bounceIn">
                <input type="password" name="j_password" id="password" placeholder="Enter any password" />
            </div>
            <div class="inputwrapper animate3 bounceIn">
                <button name="submit">Sign In</button>
            </div>
            <div class="inputwrapper animate4 bounceIn">
                <label><input type="checkbox" class="remember" name="signin" /> Keep me sign in</label>
            </div>
            
        </form>
        
    </div><!--loginpanelinner-->
</div><!--loginpanel-->

<div class="loginfooter">
   	<p><spring:message code="label.copyright"/></p>
</div>
<%@ include file="/_footer.jsp"%>

