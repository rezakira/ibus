<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

    <style>
        .btn-saving {
            float: left;
        }

        .btn-saving ~ img {
            padding-left: 10px;
            display: inline !important;
        }

        #btn-save {
            width: 100px;
        }

        #btn-save ~ img {
            display: none;
        }
        
    </style>

    <script>
        $(document)
            .ready(
            function() {
                $('#form_update_bus').validate(
                    {
                        rules : {

                        },
                        messages : {

                        },
                        highlight : function(label) {

                        },
                        success : function(label) {
                            label.text("OK!").addClass(
                                'valid').closest(
                                '.control-group')
                                .addClass('success');
                        },
                        submitHandler : function(form) {
                            var formData = new FormData(form);



                            $.ajax({
                                url:$(form).attr('action'),
                                type:$(form).attr('method'),
                                data: formData,
                                contentType: false,
                                processData: false
                            }).done(function(data){

                                if(data.status == 1)
                                {
                                    jQuery.jGrowl(data.message, { life: 3000});
                                    $("#main_content").load('<c:url value="/data-master/list_bus.page" />');
                                }
                                else
                                {
                                    jQuery.jGrowl(data.message, { life: 3000});

                                }

                            }).fail(function(){
                                var msg = "Failed";
                                jQuery.jGrowl(msg, { life: 3000});
                            });

                            return false;
                        }
                    });

            });
    </script>
    <!-- BEGIN CONTENT -->


    <!-- BEGIN PAGE BAR -->
    <div class="page-bar">
        <ul class="page-breadcrumb">
            <li>
                <a href="<c:url value="home.page"/>">Data Master</a>
                <i class="fa fa-angle-right"></i>
            </li>
            <li>
                <span>Manajemen Bis</span>
            </li>
        </ul>
        <div class="page-toolbar">
            <div class="btn-group pull-right">
                <a class="btn green" href="#"> <i class="fa fa-angle-left"></i> Back
                </a>

            </div>
        </div>
    </div>
    <!-- END PAGE BAR -->
    <!-- BEGIN PAGE TITLE-->
    <h3 class="page-title"> Add Bis
    </h3>
    <!-- END PAGE TITLE-->
    <!-- END PAGE HEADER-->
    <div class="row">
        <div class="col-md-12 ">
            <!-- BEGIN SAMPLE FORM PORTLET-->
            <div class="portlet light bordered">
                <div class="portlet-title">
                    <div class="caption font-red-sunglo">
                        <i class="icon-settings font-red-sunglo"></i>
                        <span class="caption-subject bold uppercase"> Form Add Bis</span>
                    </div>

                </div>
                <div class="portlet-body form">
                    <form id="form_update_bus" class="stdform stdform2" 
					method="post" action="<c:url value="/data-master/saveBus.json" />" 
					enctype="multipart/form-data">
                        
						<div class="form-body">

                            <div class="form-group form-md-line-input ">
                                <input type="text" id="nameBus" name="nameBus" class="form-control">
                                <label for="">Nama Bis</label>
                                <span class="help-block">Nama Bis</span>
                            </div>
                            <div class="form-group form-md-line-input ">
                                <textarea class="form-control" name="specsBus" id="specsBus"></textarea>
                                    <label for="form_control_1">Spesifikasi Bis</label>
                                    <span class="help-block">Spesifikasi</span>
								
                            </div>
                            <div class="form-group form-md-line-input ">
                                    <input type="number" id="countOfChair" name="countOfChair" class="form-control">
                                    <label for="">Jumlah Kursi</label>
                                    <span class="help-block">Jumlah Kursi</span>
                            </div>
                            <div class="form-group form-md-line-input has-info">

                                    <select name="status" id="status" class="form-control">
                                        <option value="A" selected>Aktif</option>
                                        <option value="D" >Non Aktif</option>
                                    </select>
                                    <label for="form_control_1">Status</label>
                            </div>
                                

                            </div>
                            <div class="form-actions noborder">
                                <button type="submit" class="btn blue">Submit</button>
                                <button type="button" class="btn default">Cancel</button>
                            </div>
                            </form>
                        </div>
                </div>
                <!-- END SAMPLE FORM PORTLET-->

            </div>
        </div>

