<div class="leftpanel">
        
        <div class="leftmenu">        
            <ul class="nav nav-tabs nav-stacked">
            	<li class="nav-header">Navigation</li>
            	<li class="dropdown active"><a href=""><span class="iconfa-laptop"></span>Application &amp; Setting</a>
                	<ul>
                    	<li><a href="<c:url value="/setting.page"/>">General Setting</a></li>
                        <li><a href="<c:url value="/province.page"/>">Master Province</a></li>
                        <li><a href="<c:url value="/hospital.page"/>">Master Hospital</a></li>
                    </ul>
                </li>
                <li class="dropdown"><a href=""><span class="iconfa-user"></span>User Management</a>
                	<ul>
                    	<li><a href="<c:url value="/users.page"/>">User List</a></li>
                        <li><a href="<c:url value="/user/register.page"/>">Register User</a></li>
                        <li><a href="<c:url value="/user/roles.page"/>">Role</a></li>
                        <li><a href="<c:url value="/user/log.page"/>">User Log</a></li>
                    </ul>
                </li>
                <li class="dropdown"><a href=""><span class="iconfa-th-list"></span>Target &amp; Indicator </a>
                	<ul>
                    	<li><a href="table-static.html">Target</a></li>
                        <li class="dropdown"><a href="table-dynamic.html">Indicator</a></li>
                    </ul>
                </li>
                <li class="dropdown"><a href=""><span class="iconfa-th-list"></span>Data Actual </a>
                	<ul>
                    	<li><a href="<c:url value="/actual-obat-publik.page"/>">Obat Publik</a></li>
                        <li><a href="<c:url value="/actual-yanfar.page"/>">Yanfar</a></li>
                        <li><a href="<c:url value="/actual-alkes.page"/>">Alkes</a></li>
                        <li><a href="<c:url value="/actual-fm.page"/>">FM</a></li>
                        <li><a href="<c:url value="/actual-binfar.page"/>">Binfar</a></li>
                        <li><a href="<c:url value="/actual-evapor.page"/>">Evapor</a></li>
                    </ul>
                </li>
                <li><a href="<c:url value="/chart.page"/>"><span class="iconfa-picture"></span>Report &amp; Chart</a></li>
            </ul>
        </div><!--leftmenu-->
        
    </div><!-- leftpanel -->