<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<style>

.action-menu-table:hover {
	-webkit-transform: scale(1.1);
	-moz-transform:scale(1.1);
	transform: scale(1.1);
}

</style>

<script type="text/javascript">
	$(document).ready(function() {
		// dynamic table
		var oTable = $('#dyntable').dataTable({
			"oLanguage": {
	            "sProcessing": 'Loading Table...'
	        },
			"sPaginationType" : "full_numbers",
			"bLengthChange": false,
	        "bFilter": false,
			"bProcessing": true,
			"bServerSide": true,
			"sAjaxSource": '<c:url value="/app/menu/data-table.json" />',
			"aoColumns": [
			              { "mData" : 'id',
				            	"mRender": function (sourceData, dataType, fullData) {
									if (fullData.status == 'A') {
										return '<img data-status="'+fullData.status+'" data-id="'+fullData.id+'" class="action-menu-table" src="images/icons/active.png" width="32px" alt="" />';
									} else if (fullData.status == 'D') {
										return '<img data-status="'+fullData.status+'" data-id="'+fullData.id+'" class="action-menu-table" src="images/icons/inactive.png" width="32px" alt="" />';
									} 
				                  }
						  },
			              { "mData": "parentTitle" },
						  { "mData": "title" },
						  { "mData": "description" }
			             
			]
		});
		
		$('body').on('click', '.action-menu-table', function () {
			if ($(this).data('status') == 'A') {
				deactive($(this).data('id'));
			} else if ($(this).data('status') == 'D') {
				active($(this).data('id'));
			}
		});
		
		function deactive(id) {
			$.ajax({
				url : '<c:url value="/app/deactivedMenu.json" />',
				type : 'post',
				data : {
					"menuId" : id
				},
				success : function(data) {
					if (data.status == 1) {
						oTable.fnDraw();
					}
				}

			});
		}

		function active(id) {
			$.ajax({
				url : '<c:url value="/app/activedMenu.json" />',
				type : 'post',
				data : {
					"menuId" : id
				},
				success : function(data) {
					if (data.status == 1) {
						oTable.fnDraw();
					}
				}
			});
		}
	});
</script>

<!-- BEGIN PAGE HEADER-->
    <h3 class="page-title"> Menu

    </h3>
    <div class="page-bar">
        <ul class="page-breadcrumb">
            <li>
                <i class="icon-home"></i>
                <a href="">Home</a>
                <i class="fa fa-angle-right"></i>
            </li>
            <li>
                <span>Menu</span>
            </li>
        </ul>

    </div>
    <!-- END PAGE HEADER-->

	<div class="maincontent">
		<div class="maincontentinner">
			<div class="widget animate2 fadeInRightBig">
                <h4 class="widgettitle">Table Master Menu</h4>
                <div class="widgetcontent">
					<table class="table table-striped responsive" id="dyntable">
						<thead>
							<tr>
								<th class="center" style="width: 10%;"></th>
								<th style="width: 40%;">GROUP MENU</th>
								<th class="center" >MENU</th>
								<th class="center" >DESC</th>
							</tr>
						</thead>
					</table>
                </div><!-- widgetcontent-->
            </div><!-- widgetcontent-->
		
			<%@ include file="/common//mainFooter.jsp"%>

		</div>
		<!--maincontentinner-->
	</div>
	<!--maincontent-->
</div>
<!--rightpanel-->