<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<!DOCTYPE html>
<html lang="en">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1.0" />
<meta http-equiv="X-UA-Compatible" content="IE=edge">

<title>HappyTax</title>

<!-- favicon -->
<%-- <link rel="shortcut icon" href="<c:url value="/images/favicon.png"/>"> --%>

<!-- _header css -->
<link rel="stylesheet" href="<c:url value="/css/common.css"/>">
<link rel="stylesheet" href="<c:url value="/css/styleLogin.css"/>">
<link rel="stylesheet" href="<c:url value="/css/style.default.css"/>" />
<link rel="stylesheet" href="<c:url value="/css/responsive-tables.css"/>" />
<link rel="stylesheet" href="<c:url value="/prettify/prettify.css"/>" />

<style>
.circular {
	border-radius: 100px;
	-webkit-border-radius: 100px;
	-moz-border-radius: 100px;
	background: url(http://link-to-your/image.jpg) no-repeat;
}
.bigcircular {
	width:100px;
	height:100px;
	border: 4px solid #3b6c8e;
	border-radius: 200px;
	-webkit-border-radius: 200px;
	-moz-border-radius: 100px;
	background: url(http://link-to-your/image.jpg) no-repeat;
}
</style>
<style>
.timelinelist li .tl-post {
    margin-left: 50px;
    background: none repeat scroll 0% 0% #FFF;
    border: 0px solid #CCC;
}
</style>

<!-- header js -->
<script src="<c:url value="/js/jquery-1.10.2.min.js"/>"></script>
<script src="<c:url value="/js/jquery-migrate-1.2.1.min.js"/>"></script>
<script src="<c:url value="/js/jquery-ui-1.10.3.min.js"/>"></script>
<script src="<c:url value="/js/jquery.uniform.min.js"/>"></script>
<script src="<c:url value="/js/jquery.cookies.js"/>"></script>
<script src="<c:url value="/js/jquery.slimscroll.js"/>"></script>
<script src="<c:url value="/js/jquery.jgrowl.js"/>"></script>
<script src="<c:url value="/js/jquery.dataTables.min.js"/>"></script>
<script src="<c:url value="/js/jquery.alerts.js"/>"></script>
<script src="<c:url value="/js/jquery.validate.min.js"/>"></script>
<script src="<c:url value="/js/jquery.tagsinput.min.js"/>"></script>
<script src="<c:url value="/js/jquery.autogrow-textarea.js"/>"></script>
<script src="<c:url value="/js/chosen.jquery.min.js"/>"></script>
<%-- <script src="<c:url value="/js/flot/jquery.flot.min.js"/>"></script> --%>
<%-- <script src="<c:url value="/js/flot/jquery.flot.resize.min.js"/>"></script> --%>

<script src="<c:url value="/js/bootstrap.min.js"/>"></script>
<script src="<c:url value="/js/bootstrap-fileupload.min.js"/>"></script>
<script src="<c:url value="/js/modernizr.min.js"/>"></script>
<script src="<c:url value="/js/responsive-tables.js"/>"></script>
<%-- <script src="<c:url value="/js/fullcalendar.min.js"/>"></script> --%>
<script src="<c:url value="/prettify/prettify.js"/>"></script>
<script src="<c:url value="/js/elements.js"/>"></script>
<script src="<c:url value="/js/charCount.js"/>"></script>
<script src="<c:url value="/js/jquery.blockUI.js"/>"></script>

<script src="<c:url value="/js/jquery.smartWizard.min.js"/>"></script>

<script src="<c:url value="/js/highcharts.js"/>"></script>
<script src="<c:url value="/js/highcharts-more.js"/>"></script>

<script src="<c:url value="/js/modules/exporting.js"/>"></script>
<script src="<c:url value="/js/jquery.form.min.js"/>"></script>
<script src="<c:url value="/js/custom.js"/>"></script>
<script src="<c:url value="/js/jquery.dataTables.editable.js"/>"></script>
<script src="<c:url value="/js/jquery.jeditable.js"/>"></script>
<script src="<c:url value="/js/jquery.dataTables.columnFilter.js"/>"></script>

<!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!--[if lt IE 9]>
	<script src="<c:url value="/js/html5shiv.js"/>"></script>
	<script src="<c:url value="/js/respond.min.js"/>"></script>
	<![endif]-->
