<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<style>
	.errortitle > span {
		background: #0866c6;
		font-size: 72px;
		margin-left: 0px;
	}
	
	.errortitle {
		text-align: left;
		margin-top: 0px;
		padding: 15px;
	}
</style>

<c:choose>
	<c:when test="${not empty listuser}">
	<div class="peoplelist" >
		<div class="row animate0 fadeInRightBig">
			<c:forEach var="users" items="${listuser}">
				<div class="col-md-6">
					<div class="peoplewrapper">
		       			<div class="thumb">
		       				<div class="btn-group">
		         				<button data-toggle="dropdown" style="width: 80px; margin-bottom: 2px;" class="btn btn-default dropdown-toggle">Action <span class="caret"></span></button>
		   						<ul class="dropdown-menu" style="width: 80px;">
		       						<li><a class="action-edit-users" data-username="${users.username}">Edit</a></li>
		                      		<li>
				                      	<c:choose>
											<c:when test="${users.status == 'A' }">
												<a  onclick="deactive('${users.username}')">Deactive</a>
											</c:when>
											<c:otherwise>
												<a  onclick="active('${users.username}')">Active</a>
											</c:otherwise>
										</c:choose>
		                         	</li>
		                     	</ul>
		                 	</div>
							<c:choose>
								<c:when test="${not empty users.imageUrl}">
									<img src="${user_pic_path}${users.username}/${users.imageUrl}" alt="" />
								</c:when>
								<c:otherwise>
									<img src="<c:url value="/images/default-user.png"/>" alt="" />											
								</c:otherwise>
							</c:choose>
						</div>
						<div class="peopleinfo">
							<h4><a href="">${users.username}</a> 
								<c:choose>
									<c:when test="${users.status eq 'A'}">
										<span class="on"> Active</span>
									</c:when>
									<c:otherwise>
										<span class="off"> Non Active</span>											
									</c:otherwise>
								</c:choose>
							</h4>
							<ul>
								<li><span>Email:</span> ${users.email}</li>
		             			<li><span>Role:</span> ${users.roleName}</li>
		             			<li><span>Created:</span> <fmt:formatDate type="date" value="${users.createDate }" /></li>
		             			<li><span>Last Login:</span> <fmt:formatDate type="date" value="${users.lastLogin }" /></li>
		             			<li><span>Expired Date:</span> <fmt:formatDate type="date" value="${users.expiredDate }" /></li>
		                	</ul>
		            	</div>
		        	</div>
		    	</div>
			</c:forEach>
		</div><!--row-->
		</div><!--peoplelist-->
		<ul class="pagination pagination-sm ul-action">
			<li <c:if test="${page eq 1}">class="disabled"</c:if>><a href="#">Previous</a></li>
			<c:forEach var="i" begin="1" end="${total_page}" step="1" >
				<li <c:if test="${i eq page}">class="active"</c:if> ><a data-val="${i}" href="#">${i}</a></li>
			</c:forEach>
			<li <c:if test="${total_page eq page}">class="disabled"</c:if>><a href="#">Next</a></li>
		</ul>
	</c:when>
	<c:otherwise>
		<div class="errortitle">
	        <span class="animate0 fadeInRightBig">NO DATA</span>
	    </div>
	</c:otherwise>
</c:choose>
