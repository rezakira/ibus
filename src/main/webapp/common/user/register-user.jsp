<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

    <style>
        .btn-saving {
            float: left;
        }

        .btn-saving ~ img {
            padding-left: 10px;
            display: inline !important;
        }

        #btn-save {
            width: 100px;
        }

        #btn-save ~ img {
            display: none;
        }

    </style>

    <script>
        $(document)
            .ready(
            function() {
                $('#form_update_bus').validate(
                    {
                        rules : {

                        },
                        messages : {

                        },
                        highlight : function(label) {

                        },
                        success : function(label) {
                            label.text("OK!").addClass(
                                'valid').closest(
                                '.control-group')
                                .addClass('success');
                        },
                        submitHandler : function(form) {
                            var formData = new FormData(form);



                            $.ajax({
                                url:$(form).attr('action'),
                                type:$(form).attr('method'),
                                data: formData,
                                contentType: false,
                                processData: false
                            }).done(function(data){

                                if(data.status == 1)
                                {
                                    jQuery.jGrowl(data.message, { life: 3000});
                                    $("#main_content").load('<c:url value="/data-master/list_bus.page" />');
                                }
                                else
                                {
                                    jQuery.jGrowl(data.message, { life: 3000});

                                }

                            }).fail(function(){
                                var msg = "Failed";
                                jQuery.jGrowl(msg, { life: 3000});
                            });

                            return false;
                        }
                    });

            });
    </script>
    <!-- BEGIN CONTENT -->


    <!-- BEGIN PAGE BAR -->
    <div class="page-bar">
        <ul class="page-breadcrumb">
            <li>
                <a href="<c:url value="home.page"/>">Home</a>
                <i class="fa fa-angle-right"></i>
            </li>
            <li>
                <span>User</span>
            </li>
        </ul>
        <div class="page-toolbar">
            <div class="btn-group pull-right">
                <a class="btn green" href="#"> <i class="fa fa-angle-left"></i> Back
                </a>

            </div>
        </div>
    </div>
    <!-- END PAGE BAR -->
    <!-- BEGIN PAGE TITLE-->
    <h3 class="page-title"> Tambah User
    </h3>
    <!-- END PAGE TITLE-->
    <!-- END PAGE HEADER-->
    <div class="row">
        <div class="col-md-12 ">
            <!-- BEGIN SAMPLE FORM PORTLET-->
            <div class="portlet light bordered">
                <div class="portlet-title">
                    <div class="caption font-red-sunglo">
                        <i class="icon-settings font-red-sunglo"></i>
                        <span class="caption-subject bold uppercase"> Form Tambah User/Pengguna</span>
                    </div>

                </div>
                <div class="portlet-body form">
                    <form id="form_update_bus" class="stdform stdform2" 
                          method="post" action="<c:url value="" />" 
                          enctype="multipart/form-data">

                        <div class="form-body">

                            <div class="form-group form-md-line-input ">
                                <input type="text" id="" name="" class="form-control">
                                <label for="">First Name</label>
                                <span class="help-block">Nama Depan</span>
                            </div>
                            <div class="form-group form-md-line-input ">
                                <input type="text" id="" name="" class="form-control ">
                                <label for="">Last Name</label>
                                <span class="help-block">Nama Belakang</span>
                            </div>
                            <div class="form-group form-md-line-input ">
                                <input type="text" id="" name="" class="form-control ">
                                <label for="">Username</label>
                                <span class="help-block">Username</span>
                            </div>
                            <div class="form-group form-md-line-input ">
                                <input type="text" id="" name="" class="form-control ">
                                <label for="">Email</label>
                                <span class="help-block">Alamat Email</span>
                            </div>
                            <div class="form-group form-md-line-input has-info">

                                <select name="role" id="role" class="form-control">
                                    <option value="A" selected>Member</option>
                                    <option value="D" >Admin</option>
                                    <option>Operator</option>
                                </select>
                                <label for="form_control_1">Role</label>
                            </div>
                            <div class="form-group">
                                <label for="exampleInputFile1">Foto</label>
                                <input type="file" id="exampleInputFile1">
                                <p class="help-block"> Upload foto </p>
                            </div>
                            


                        </div>
                        <div class="form-actions noborder">
                            <button type="submit" class="btn blue">Submit</button>
                            <button type="button" class="btn default">Cancel</button>
                        </div>
                    </form>
                </div>
            </div>
            <!-- END SAMPLE FORM PORTLET-->

        </div>
    </div>

    <script>
        $(document).ready(function() {
            $('#startDate').datepicker({autoclose:true});
            $('#endDate').datepicker({autoclose:true});
            $('#summernote_1').summernote({height: 300});
        });
    </script>

