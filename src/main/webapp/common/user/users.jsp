<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<style>

.action-menu-table:hover {
	-webkit-transform: scale(1.1);
	-moz-transform:scale(1.1);
	transform: scale(1.1);
}

</style>

<script type="text/javascript">
	$(document).ready(function() {
	
		$('body').off('click', '.action-menu-view-bus');
		$('body').off('click', '.action-menu-edit-bus');
		$('body').off('click', '.action-menu-delete-bus');
		
	
		// dynamic table
		var oTable = $('#dyntable').dataTable({
			"oLanguage": {
	            "sProcessing": 'Loading Table...'
	        },
			"sPaginationType" : "full_numbers",
			"bLengthChange": false,
	        "bFilter": false,
			"bProcessing": true,
			"bServerSide": true,
            "bAutoWidth": false,
            "aoColumns" : [
            { sWidth: '20%' },
            { sWidth: '20%' },
            { sWidth: '20%' },
            { sWidth: '20%' },
            { sWidth: '20%' }
        ],  
			"sAjaxSource": '<c:url value="/data-master/bus/data-table.json" />',
			"aoColumns": [
			              { "mData" : 'idBus',
				            	"mRender": function (sourceData, dataType, fullData) {
									      
								//return	 '<a class="btn btn-icon-only tooltips  green action-menu-view-menucms" href="javascript:;">'
								//		 +'<i class="fa fa-search"></i><span class="tooltiptext">Edit</span></a>'
								return	 '<a class="btn btn-icon-only tooltips  blue action-menu-edit-bus" href="javascript:;" data-id="'+fullData.idBus+'">'
										 +'<i class="fa fa-edit"></i><span class="tooltiptext">Edit</span></a>'
										 +'<a class="btn btn-icon-only tooltips  red action-menu-delete-bus" href="javascript:;" data-id="'+fullData.idBus+'">'
										 +'<i class="fa fa-times"></i><span class="tooltiptext">Delete</span></a>';
									
				                  }
						  },
			              { "mData": "nameBus"},
						  { "mData": "specsBus" },
						  { "mData": "countOfChair" },
						  { "mData": "dateModified"}
			             
			]
		});
		
		//$('body').on('click', '.action-menu-view-bus', function () {
			//alert('view');
		//});
		
		$('body').on('click', '.action-menu-edit-bus', function () {
			$("#main_content").load('<c:url value="/data-master/update_bus.page" />'+'/'+$(this).data('id'));
		});
		
		$('body').on('click', '.action-menu-delete-bus', function () {
			if (!confirm("Apakah anda yakin menghapus data ini ?")){
			      return false;
			}
			else
			{
							$.ajax({
                                url:'<c:url value="/data-master/deleteBus.json" />',
                                type:'post',
                                data: {
									'id_long':$(this).data('id')
								}
                            }).done(function(data){
                                
                            	if(data.status == 1)
                            	{
                            		jQuery.jGrowl(data.message, { life: 3000});
									
									$('#dyntable').dataTable()._fnAjaxUpdate();
								}
                            	else
                            	{
                            		jQuery.jGrowl(data.message, { life: 3000});
                                }
                            	
                            }).fail(function(){
                                var msg = "Failed";
                                jQuery.jGrowl(msg, { life: 3000});
                            });
			}
		});
		
		
	});
</script>


<div class="page-bar">
                        <ul class="page-breadcrumb">
                            <li>
                                <a href="<c:url value="home.page"/>">Home</a>
                                <i class="fa fa-angle-right"></i>
                            </li>
                            <li>
                                <a href="#">Pengguna</a>
                            </li>
                        </ul>
                        
                    </div>
                    <!-- END PAGE BAR -->
                    <!-- BEGIN PAGE TITLE-->
                    <h3 class="page-title"> Pengguna
                    </h3>
                    <!-- END PAGE TITLE-->
                    <!-- END PAGE HEADER-->
                   
                    <div class="row">
                        <div class="col-md-12">
                            <!-- BEGIN EXAMPLE TABLE PORTLET-->
                            <div class="portlet light bordered">
                                <div class="portlet-title">
                                    <div class="caption font-dark">
                                        <i class="icon-settings font-dark"></i>
                                        <span class="caption-subject bold uppercase"> Tabel Pengguna</span>
                                        
                                    </div>
                                 
                                </div>
                                <div class="portlet-body">
                                    <div class="table-toolbar">
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="btn-group">
                                                    <a href="#" data-id="<c:url value="/user/register-user.page"/>" onclick="linkToPage(this);return false;"><button id="sample_editable_1_new" class="btn sbold green">Tambah Pengguna
                                                        <i class="fa fa-plus"></i>
                                                    </button></a>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                              
                                            </div>
                                        </div>
                                    </div>
                                    <table class="table table-striped table-bordered table-hover table-checkable order-column" id="dyntable">
                                        <thead style="width:100%">
                                            <tr>
                                                <th> Action </th>
                                                <th> Username </th>
                                                <th> Nama </th>
                                                <th> Role </th>
                                                <th> Email </th>
                                                <th> Terakhir Update</th>
                                            </tr>
                                        </thead>
                                        
                                    </table>
                                </div>
                            </div>
                            <!-- END EXAMPLE TABLE PORTLET-->
                        </div>
                    </div>