<%@ include file="/common/_header.jsp"%>

<script>
	jQuery(document)
			.ready(
					function() {
						$('#register_form').validate(
											{
											rules : {
												email : {
													required : true
												},
												password : {
													required : true
												}
											},
											messages : {},
											highlight : function(label) {
												// 				setTimeout($.unblockUI, timeoutLoad);
											},
											success : function(label) {
												label.text('Ok!').addClass(
														'valid').closest(
														'.control-group')
														.addClass('success');
											},
											submitHandler : function(form) {
												//form.submit(); 
												var formData = new FormData(form);

														$.ajax(
																{
																	url : $(form).attr('action'),
																	type : $(form).attr('method'),
																	data : formData,
																	contentType : false,
																	processData : false
																})
														.done(
																function(data) {
																	
																	if(data.status == 1)
																	{
																		jQuery.jGrowl("Success Registration Member,Please Check Your Email Inbox",
																				{life : 3000});
																		
																		 setTimeout(function () {
																		       window.location.href = "<c:url value='/' />"; //will redirect to your blog page (an ex: blog.html)
																		 }, 5000); //will call the function after 2 secs.
																		
																	}
																	else
																	{
																		jQuery.jGrowl(data.message,{life : 3000});
																	}
																	

																})
														.fail(
																function() {
																	var msg = "Failed Registration Member, Please Check Your Internet Connection";
																	jQuery.jGrowl(msg,{life : 3000});

																});

												return false;
											}
										});

					});
</script>


<head>
<meta charset="utf-8">
<link href="css/style.css" rel='stylesheet' type='text/css' />
<meta name="viewport" content="width=device-width, initial-scale=1">
<script type="application/x-javascript">
	addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } 
</script>
<style>
div.terms {
	width:450px;
	height:200px;
	border:1px solid #ccc;
	background:#f2f2f2;
	padding:6px;
	overflow:auto;
}
div.terms p ,
div.terms li {font:normal 11px/15px arial;color:#333;}
div.terms h3 {font:bold 14px/19px arial;color:#000;}
div.terms h4 {font:bold 12px/17px arial;color:#000;}
div.terms strong {color:#000;}
</style>
</head>

<body class="login">
	<div id="login">
		<center>
			<div class="main">
				<form id="register_form" 
					action="<c:url value='mobile-api/register-member.json' />"
					method="post">
					<h1>
						<img width="200" src="<c:url value="/images/happytax_logo.png"/>"
							alt="" /><br>
						<label> Register Member </label>
					</h1>
					<div class="inset">
						<p align="left">
							<label id="login_label" for="user" style="align: left;">EMAIL</label>
							<input id="login_input" name="email" type="text"
								placeholder="Email Address" required />

						</p>

						<p align="left">
							<label id="login_label" for="user" style="align: left;">Password</label>
							<input id="login_input" name="password" type="password"
								placeholder="Password" required />

						</p>

						<p align="left">
							<label id="login_label" for="user" style="align: left;">Nama
								Perusahaan</label> <input id="login_input" name="namaPerusahaan"
								type="text" placeholder="Nama Perusahaan" required />

						</p>

						<p align="left">
							<label id="login_label" for="user" style="align: left;">Alamat</label>
							<input id="login_input" name="alamat" type="text"
								placeholder="Alamat" required />

						</p>

						<p align="left">
							<label id="login_label" for="user" style="align: left;">NPWP</label>
							<input id="login_input" name="npwp" type="text"
								placeholder="NPWP" required />
						</p>
						
						<p align="left">
							<label id="login_label" for="user" style="align: left;">Nama Direktur</label>
							<input id="login_input" name="namaDirektur" type="text"
								placeholder="Nama Direktur" required />
						</p>
						
						<p align="left">
							<label id="login_label" for="user" style="align: left;">No Telepon</label>
							<input id="login_input" name="noTelepon" type="text"
								placeholder="No Telepon" required />
						</p>
						
						<p align="left">
							<label id="login_label" for="user" style="align: left;">Jenis Usaha</label>
							<input id="login_input" name="jenisUsaha" type="text"
								placeholder="Jenis Usaha" required />
						</p>
						
						<p align="left">
							<label id="login_label" for="user" style="align: left;">Jumlah Karyawan</label>
							<input id="login_input" name="jmlKaryawan" type="text"
								placeholder="Jumlah Karyawan" required />
						</p>
						
						<p align="left">
							<label id="login_label" for="user" style="align: left;">Jumlah Pajak Keluaran Perbulan</label>
							<input id="login_input" name="jmlPkPerbulan" type="text"
								placeholder="Jumlah Pajak Keluaran Perbulan" required />
						</p>
						
						<p align="left">
							<label id="login_label" for="user" style="align: left;">Jumlah Pajak Masukkan Perbulan</label>
							<input id="login_input" name="jmlPmPerbulan" type="text"
								placeholder="Jumlah Pajak Masukkan Perbulan" required />
						</p>
						
						<div class="terms">
							<h3>Syarat dan Ketentuan</h3>
							<br>
							<h4>Kebijakan Privasi</h4>
							<p align="justify">
								HappyTax mengumpulkan alamat e-mail dari orang yang mengirimkan e-mail. Kami juga
								mengumpulkan informasi tentang apa akses halaman pengguna dan informasi yang
								diberikan kepada kami oleh pengguna melalui survei dan pendaftaran situs. Informasi 
								tersebut mungkin berisi data pribadi tentang Anda termasuk alamat Anda, nomor telepon, 
								nomor kartu kredit dll.
								HappyTax melindungi informasi kartu kredit sesuai dengan standar keamanan. Kami tidak 
								diperbolehkan untuk mengungkapkan informasi pribadi tanpa izin tertulis dari Anda. Namun, 
								informasi tertentu yang dikumpulkan dari Anda dan tentang Anda digunakan dalam konteks 
								menyediakan layanan kami kepada Anda. Informasi yang kami kumpulkan tidak dibagi 
								dengan, dijual atau disewakan kepada orang lain, kecuali dalam keadaan tertentu dan yang 
								penggunaan Layanan tersebut dianggap telah mendapatkan persetujuan yang valid untuk 
								mengungkapkan hal-hal berikut: 
								<br><br>
								1. HappyTax dapat berbagi informasi pribadi dalam rangka untuk menyelidiki, 
								mencegah, atau mengambil tindakan terkait dengan aktivitas ilegal, dugaan 
								penipuan, situasi yang melibatkan kemungkinan ancaman terhadap keselamatan fisik 
								seseorang, pelanggaran istilah HappyTax penggunaan, atau sebagaimana 
								diharuskan oleh hukum.
								<br><br>
								2. HappyTax mempekerjakan perusahaan lain untuk melakukan tugas-tugas atas nama 
								kami dan mungkin perlu untuk berbagi informasi Anda dengan mereka untuk 
								menyediakan produk dan layanan kepada Anda.
								<br><br>
								3. Kami akan mentransfer informasi tentang Anda jika HappyTax dipindahtangankan 
								kepada perusahaan lain. Dalam hal ini, HappyTax akan memberitahu Anda melalui 
								email atau dengan meletakkan pemberitahuan yang jelas di situs web HappyTax 
								sebelum informasi tentang Anda yang sudah ditransfer menjadi tunduk pada 
								kebijakan privasi yang berbeda.
							</p>

							
							<br>
							<h4>Apa yang kami lakukan dengan informasi Anda</h4>
							<p align="justify">
								1. Istilah "Informasi Pribadi" yang digunakan di sini didefinisikan sebagai setiap
								informasi yang mengidentifikasi atau dapat digunakan untuk mengidentifikasi, 
								menghubungi atau mencari orang kepada siapa berkaitan informasi tersebut. 
								Informasi pribadi yang kami kumpulkan akan tunduk pada Kebijakan Privasi ini, 
								sebagai telah diubah dari waktu ke waktu.
								<br><br>
								2. Ketika Anda mendaftar untuk HappyTax kami menanyakan nama, nama perusahaan 
								dan alamat email dan Anda menyatakan kesediaan untuk memberikan informasi 
								tersebut ketika Anda bergabung dengan layanan kami.
								<br><br>
								3. HappyTax menggunakan informasi yang kami kumpulkan untuk keperluan umum 
								berikut: produk dan jasa penyediaan, penagihan, identifikasi dan otentikasi, 
								peningkatan layanan, kontak, dan penelitian.
								<br><br>
								4. Sebagai bagian dari pembelian dan penjualan proses pada HappyTax, Anda akan 
								mendapatkan alamat email dan / atau alamat pengiriman dari pelanggan Anda. Anda 
								setuju bahwa, sehubungan dengan Informasi Pribadi pengguna lain yang Anda 
								dapatkan melalui HappyTax atau melalui komunikasi HappyTax terkait atau transaksi 
								yang difasilitasi HappyTax, HappyTax dengan ini memberikan kepada Anda lisensi 
								untuk menggunakan informasi tersebut hanya untuk komunikasi komersial yang 
								menjadi tanggung jawab Anda tanpa melibatkan HappyTax. HappyTax tidak 
								mentolerir spam. Oleh karena itu, tanpa mengabaikan ketentuan di atas, Anda tidak 
								diperbolehkan untuk menambahkan nama seseorang yang telah membeli item dari 
								Anda, untuk daftar email Anda (email atau surat fisik) tanpa persetujuan mereka.
							</p>
							
							<br>
							<h4>Keamanan</h4>
							<p align="justify">
								Keamanan informasi pribadi Anda adalah sangat penting bagi kami. Bila Anda memasukkan
								informasi sensitif, seperti nomor kartu kredit di formulir pendaftaran kami, kami mengenkripsi 
								transmisi informasi menggunakan teknologi socket layer yang aman (SSL), namun tidak ada 
								yang bersifat 100% terlebih cara Anda menggunakan informasi Anda akan sangat 
								berpengaruh. Sementara kami berusaha untuk menggunakan cara-cara yang dapat diterima 
								secara komersial untuk melindungi informasi pribadi Anda, kami tidak dapat menjamin 
								keamanan mutlak. Jika Anda memiliki pertanyaan tentang keamanan di situs Web kami, 
								Anda dapat email kami di <a href="mailto:support@happysoft.co.id?Subject=[Question]">support@happysoft.co.id</a> 
							
							</p>
							
							<br>
							<h4>Pengungkapan</h4>
							<p align="justify">
								1. HappyTax dapat menggunakan penyedia layanan pihak ketiga untuk menyediakan 
								layanan tertentu kepada Anda dan kami dapat berbagi informasi pribadi dengan 
								penyedia layanan tersebut. Kami memerlukan perusahaan yang dapat berbagi 
								informasi pribadi untuk melindungi data yang secara konsisten dengan kebijakan ini 
								dan untuk membatasi penggunaan informasi pribadi tersebut kepada kinerja 
								pelayanan untuk HappyTax.
								<br><br>
								2. HappyTax dapat mengungkapkan informasi pribadi dalam situasi khusus, seperti 
								untuk mematuhi perintah pengadilan mengharuskan kami untuk melakukannya atau 
								ketika tindakan Anda melanggar Ketentuan Layanan.
								<br><br>
								3. Kami tidak menjual atau memberikan informasi pribadi kepada perusahaan lain untuk 
								memasarkan produk atau jasa mereka sendiri.
							</p>
							
							<br>
							<h4>Cookies</h4>
							<p align="justify">
								Cookie adalah sejumlah kecil data, yang mungkin termasuk pengenal unik anonim. Cookie 
								dikirim ke browser Anda dari situs web dan disimpan pada hard drive komputer Anda. Setiap 
								komputer yang mengakses website kami ditugaskan cookie untuk menyimpan informasi 
								tersebut.
							</p>
							
							<br>
							<h4>Perubahan terhadap Kebijakan Privasi ini</h4>
							<p align="justify">
								Kami berhak untuk mengubah pernyataan privasi ini setiap saat, sehingga Anda disarankan 
								untuk selalu meninjau ulang perubahan sesering mungkin. Jika kami membuat perubahan 
								penting terhadap kebijakan ini, kami akan memberitahu Anda di sini atau dengan cara 
								pemberitahuan di homepage kami sehingga Anda mengetahui informasi apa yang kami 
								kumpulkan dan bagaimana kami menggunakannya.
							</p>
						
						</div>
						<br>
						<p align="left">
						
							<input id="login_input" name="cek_term" type="checkbox" />
							<font color="white">
							Saya Menyetujui Syarat dan Ketentuan diatas</font>
						</p>


					</div>
					<p class="p-container">
						<input id="login_button" type="button" onclick="window.location.href='<c:url value='/' />';" value="Cancel" style="margin: 5px;">
						<input id="login_button" type="submit" value="Register" style="margin: 5px;">
						
					</p>
				</form>
			</div>
		</center>
		<!-- <br><br>    <p>Copyright � 2014 Evolution Technology</p> -->
	</div>
	<!-----//end-copyright---->
</body>


<div class="loginfooter">
	<p>
		<spring:message code="label.copyright" />
	</p>
</div>
<%@ include file="/common/_footer.jsp"%>

