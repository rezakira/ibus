<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ include file="/common/head.jsp"%>

    

    <body class=" login">
        <!-- BEGIN LOGO -->
        <div class="logo">
            <a href="">
                <img src="<c:url value="images/ibus.png"/>" alt="logo" width="300" style="margin:15px;" class="logo-default" /></a>
        </div>
        <!-- END LOGO -->
        <!-- BEGIN LOGIN -->
        <div class="content">
            <!-- BEGIN LOGIN FORM -->
            <form class="login-form" action="<c:url value='j_spring_security_check' />" method="post">
                <h3 class="form-title font-green">Admin Panel</h3>
                <h4 class="form-title font-green">Sign In</h4>
                <div class="alert alert-danger display-hide">
                    <button class="close" data-close="alert"></button>
                    <span> Enter any username and password. </span>
                </div>
                <div class="form-group">
                    <!--ie8, ie9 does not support html5 placeholder, so we just show field title for that-->
                    <label class="control-label visible-ie8 visible-ie9">Username</label>
                    <input class="form-control form-control-solid placeholder-no-fix" type="text" autocomplete="off" placeholder="Username" name="j_username" /> </div>
                <div class="form-group">
                    <label class="control-label visible-ie8 visible-ie9">Password</label>
                    <input class="form-control form-control-solid placeholder-no-fix" type="password" autocomplete="off" placeholder="Password" name="j_password" /> </div>
                <div class="form-actions">
                    <button type="submit" class="btn green uppercase" style="width:100%;">Login</button>
                    
                </div>
            </form>
        </div>
    </body>
    
    
        </html>