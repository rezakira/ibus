<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>


    <!-- BEGIN SIDEBAR -->
    <div class="page-sidebar-wrapper">
        <!-- END SIDEBAR -->
        <!-- DOC: Set data-auto-scroll="false" to disable the sidebar from auto scrolling/focusing -->
        <!-- DOC: Change data-auto-speed="200" to adjust the sub menu slide up/down speed -->
        <div class="page-sidebar navbar-collapse collapse">
            <!-- BEGIN SIDEBAR MENU -->
            <!-- DOC: Apply "page-sidebar-menu-light" class right after "page-sidebar-menu" to enable light sidebar menu style(without borders) -->
            <!-- DOC: Apply "page-sidebar-menu-hover-submenu" class right after "page-sidebar-menu" to enable hoverable(hover vs accordion) sub menu mode -->
            <!-- DOC: Apply "page-sidebar-menu-closed" class right after "page-sidebar-menu" to collapse("page-sidebar-closed" class must be applied to the body element) the sidebar sub menu mode -->
            <!-- DOC: Set data-auto-scroll="false" to disable the sidebar from auto scrolling/focusing -->
            <!-- DOC: Set data-keep-expand="true" to keep the submenues expanded -->
            <!-- DOC: Set data-auto-speed="200" to adjust the sub menu slide up/down speed -->

            <ul class="page-sidebar-menu  page-header-fixed page-sidebar-menu-hover-submenu " 
            		data-keep-expanded="false" data-auto-scroll="true" data-slide-speed="200">
                <c:forEach items="${menus}" var="menu">
                    <li class="nav-item dropdown">
                        <a href="javascript:;" class="nav-link nav-toggle">
                            <i class="icon-folder"></i>
                            <span class="title">${menu.title}</span>
                            <span class="arrow"></span>
                        </a>
                        <ul class="sub-menu">
                            <c:forEach items="${menu.subMenu}" var="subMenu">
                                <li class="nav-item  ">
                                    <a href="<c:url value="${subMenu.link}"/>" class="nav-link ">
                                        <span class="title">${subMenu.title}</span>
                                    </a>
                                </li>
                            </c:forEach>

                        </ul>
                    </li>
                </c:forEach>

            </ul>
            <!-- END SIDEBAR MENU -->
        </div>
        <!-- END SIDEBAR -->
    </div>
    <!-- END SIDEBAR -->