<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
    <style>
        .top-menu{
            margin-top: -46px !important;
        }

        #name{
            color: whitesmoke;
            font-size: large;
            font-style: oblique;
            font-weight: bold;
            padding-top: 20px;
        }

        .page-header.navbar .page-top {
            background: #2b3643;
        }
    </style>
    <script>
        $(document)
            .ready(
            function() {
                jQuery
                    .ajax({
                    url : '<c:url value="/user/userStatusLogin.json"/>',
                    type : 'GET',
                    success : function(data) {
                        jQuery("#dataLogin")
                            .append(data.obj.user.email);
                        if (data.obj.user.imageUrl != null)
                        {
                            document
                                .getElementById("imageProfile").src = data.obj.user_pic_path
                                + data.obj.user.username
                                + '/'
                                + data.obj.user.imageUrl;
                        }
                    },
                    error : function() {
                        if (window.console)
                            console
                                .log('failed load <c:url value="/user/userStatusLogin.json"/>');
                    }
                });




            });
    </script>

    <div class="page-header navbar navbar-fixed-top">
        <!-- BEGIN HEADER INNER -->
        <div class="page-header-inner ">
            <!-- BEGIN LOGO -->
            <div class="page-logo" style="background-color:#2b3643;">
                <a href="">
                    <img src="<c:url value="images/ibus.png"/>" alt="logo" width="100" style="margin:15px;" class="logo-default" /> </a>
                <div class="menu-toggler sidebar-toggler">
                    <!-- DOC: Remove the above "hide" to enable the sidebar toggler button on header -->
                </div>
            </div>
            <!-- END LOGO -->
            <!-- BEGIN RESPONSIVE MENU TOGGLER -->
            <a href="javascript:;" class="menu-toggler responsive-toggler" data-toggle="collapse" data-target=".navbar-collapse"> </a>
            <!-- END RESPONSIVE MENU TOGGLER -->
            <!-- BEGIN PAGE ACTIONS -->
            <!-- DOC: Remove "hide" class to enable the page header actions -->
            <div class="page-actions">

            </div>
            <!-- END PAGE ACTIONS -->
            <!-- BEGIN PAGE TOP -->
            <div class="page-top">

                <div id="name">iBUS Admin Panel</div>
                <!-- BEGIN HEADER SEARCH BOX -->
                <!-- DOC: Apply "search-form-expanded" right after the "search-form" class to have half expanded search box -->

                <!-- END HEADER SEARCH BOX -->
                <!-- BEGIN TOP NAVIGATION MENU -->
                <div class="top-menu">
                    <ul class="nav navbar-nav pull-right">
                        <!-- BEGIN USER LOGIN DROPDOWN -->
                        <!-- DOC: Apply "dropdown-dark" class after below "dropdown-extended" to change the dropdown styte -->
                        <li class="dropdown dropdown-user">
                            <a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">
                                <img alt="" class="img-circle" src="../assets/layouts/layout2/img/avatar3_small.jpg" />
                                <span class="username username-hide-on-mobile"> Nick </span>
                                <i class="fa fa-angle-down"></i>
                            </a>
                            <ul class="dropdown-menu dropdown-menu-default">
                                <li>
                                    <a href="page_user_profile_1.html">
                                        <i class="icon-user"></i> My Profile </a>
                                </li>

                                <li class="divider"> </li>

                                <li>
                                    <a href="#" onclick="logOutApp();">
                                        <i class="icon-key"></i> Log Out </a>
                                </li>
                            </ul>
                        </li>
                        <!-- END USER LOGIN DROPDOWN -->
                        <!-- BEGIN QUICK SIDEBAR TOGGLER -->
                        <!-- DOC: Apply "dropdown-dark" class after below "dropdown-extended" to change the dropdown styte -->

                        <!-- END QUICK SIDEBAR TOGGLER -->
                    </ul>
                </div>
                <!-- END TOP NAVIGATION MENU -->
            </div>
            <!-- END PAGE TOP -->
        </div>
        <!-- END HEADER INNER -->
    </div>
    <!-- END HEADER -->
    <!-- BEGIN HEADER & CONTENT DIVIDER -->
    <div class="clearfix"> </div>
    <!-- END HEADER & CONTENT DIVIDER -->