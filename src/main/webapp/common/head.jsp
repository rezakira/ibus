<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    <%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
        <%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
            <%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>

                <!DOCTYPE html>

                <html lang="en">
                    <!--<![endif]-->
                    <!-- BEGIN HEAD -->

                    <head>
                        <meta charset="utf-8" />
                        <title>i-bus</title>
                        <meta http-equiv="X-UA-Compatible" content="IE=edge">
                        <meta content="width=device-width, initial-scale=1" name="viewport" />
                        <meta content="" name="description" />
                        <meta content="" name="author" />
                        <!-- BEGIN GLOBAL MANDATORY STYLES -->
                        <link href="<c:url value="/assets/global/plugins/font-awesome/css/font-awesome.min.css"/>" rel="stylesheet" type="text/css" />
                        <link href="<c:url value="/assets/global/plugins/simple-line-icons/simple-line-icons.min.css"/>" rel="stylesheet" type="text/css" />
                        <link href="<c:url value="/assets/global/plugins/bootstrap/css/bootstrap.min.css"/>" rel="stylesheet" type="text/css" />
                        <link href="<c:url value="/assets/global/plugins/uniform/css/uniform.default.css"/>" rel="stylesheet" type="text/css" />
                        <link href="<c:url value="/assets/global/plugins/bootstrap-switch/css/bootstrap-switch.min.css"/>" rel="stylesheet" type="text/css" />
                        <!-- END GLOBAL MANDATORY STYLES -->
                        <!-- BEGIN PAGE LEVEL PLUGINS -->
                        <link href="<c:url value="/assets/global/plugins/select2/css/select2.min.css"/>" rel="stylesheet" type="text/css" />
                        <link href="<c:url value="/assets/global/plugins/select2/css/select2-bootstrap.min.css"/>" rel="stylesheet" type="text/css" />
                        <link href="<c:url value="/assets/global/plugins/bootstrap-summernote/summernote.css"/>" rel="stylesheet" type="text/css" />
                        <link href="<c:url value="/assets/global/plugins/datatables/datatables.min.css"/>" rel="stylesheet" type="text/css" />
                        <link href="<c:url value="/assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css"/>" rel="stylesheet" type="text/css" />

                        <!-- END PAGE LEVEL PLUGINS -->
                        <!-- BEGIN THEME GLOBAL STYLES -->
                        <link href="<c:url value="/assets/global/css/components.min.css"/>" rel="stylesheet" id="style_components" type="text/css" />
                        <link href="<c:url value="/assets/global/css/plugins.min.css"/>" rel="stylesheet" type="text/css" />
                        <link href="<c:url value="/assets/global/plugins/jquery.jgrowl.min.css"/>" rel="stylesheet" type="text/css" />
                        <link rel="stylesheet" href="<c:url value="/assets/global/plugins/bootstrap-daterangepicker/daterangepicker.min.css"/>">
                        <link rel="stylesheet" href="<c:url value="/assets/global/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css"/>">
                        <link rel="stylesheet" href="<c:url value="/assets/global/plugins/bootstrap-timepicker/css/bootstrap-timepicker.min.css"/>">
                        <link rel="stylesheet" href="<c:url value="/assets/global/plugins/bootstrap-datetimepicker/css/bootstrap-datetimepicker.min.css"/>">
                        <link rel="stylesheet" href="<c:url value="/assets/global/plugins/clockface/css/clockface.css"/>">
                        <!-- END THEME GLOBAL STYLES -->
                        <!-- BEGIN THEME LAYOUT STYLES -->
                        <link href="<c:url value="/assets/layouts/layout2/css/layout.min.css"/>" rel="stylesheet" type="text/css" />
                        <link href="<c:url value="/assets/layouts/layout2/css/themes/blue.min.css"/>" rel="stylesheet" type="text/css" id="style_color" />
                        <link href="<c:url value="/assets/layouts/layout2/css/custom.min.css"/>" rel="stylesheet" type="text/css" />
                        <link href="<c:url value="/assets/pages/css/login.min.css"/>" rel="stylesheet" type="text/css" />

                        <script src="<c:url value="/assets/global/plugins/jquery.min.js"/>" type="text/javascript"></script>
                        <script src="<c:url value="/assets/global/plugins/bootstrap/js/bootstrap.min.js"/>" type="text/javascript"></script>
                        <script src="<c:url value="/assets/global/plugins/js.cookie.min.js"/>" type="text/javascript"></script>
                        <script src="<c:url value="/assets/global/plugins/bootstrap-hover-dropdown/bootstrap-hover-dropdown.min.js"/>" type="text/javascript"></script>
                        <script src="<c:url value="/assets/global/plugins/jquery-ui/jquery-ui.min.js"/>" type="text/javascript"></script>
                        <script src="<c:url value="/assets/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js"/>" type="text/javascript"></script>
                        <script src="<c:url value="/assets/global/plugins/jquery.blockui.min.js"/>" type="text/javascript"></script>
                        <script src="<c:url value="/assets/global/plugins/uniform/jquery.uniform.min.js"/>" type="text/javascript"></script>
                        <script src="<c:url value="/assets/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js"/>" type="text/javascript"></script>
                        <!-- END CORE PLUGINS -->
                        <!-- BEGIN PAGE LEVEL PLUGINS -->
                        <script src="<c:url value="/assets/global/plugins/moment.min.js"/>" type="text/javascript"></script>
                        <script src="<c:url value="/assets/global/plugins/select2/js/select2.full.min.js"/>" type="text/javascript"></script>
                        <script src="<c:url value="/assets/global/plugins/jquery-validation/js/jquery.validate.min.js"/>" type="text/javascript"></script>
                        <script src="<c:url value="/assets/global/plugins/jquery-validation/js/additional-methods.min.js"/>" type="text/javascript"></script>
                        <script src="<c:url value="/assets/global/plugins/bootstrap-wizard/jquery.bootstrap.wizard.min.js"/>" type="text/javascript"></script>
                        <script src="<c:url value="/assets/global/plugins/datatables/datatables.min.js"/>"  type="text/javascript"></script>
                        <script src="<c:url value="/assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js"/>"  type="text/javascript"></script>
                        <script src="<c:url value="/assets/pages/scripts/table-datatables-managed.min.js"/>"  type="text/javascript"></script>
                        <script src="<c:url value="/assets/global/plugins/bootstrap-summernote/summernote.min.js"/>" type="text/javascript"></script>
                        
                        <script src="<c:url value="/assets/global/plugins/jquery.jgrowl.min.js"/>" type="text/javascript"></script>
                        <script src="<c:url value="/assets/global/plugins/bootstrap-daterangepicker/daterangepicker.min.js"/>"></script>
                        <script src="<c:url value="/assets/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js"/>"></script>
                        <script src="<c:url value="/assets/global/plugins/bootstrap-timepicker/js/bootstrap-timepicker.min.js"/>"></script>
                        <script src="<c:url value="/assets/global/plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker.min.js"/>"></script>
                        <script src="<c:url value="/assets/global/plugins/clockface/js/clockface.js"/>"></script>
                        <!-- END PAGE LEVEL PLUGINS -->
                        <!-- BEGIN THEME GLOBAL SCRIPTS -->
                        <script src="<c:url value="/assets/global/scripts/app.min.js"/>" type="text/javascript"></script>
                        <!-- END THEME GLOBAL SCRIPTS -->
                        <!-- BEGIN PAGE LEVEL SCRIPTS -->
                        <script src="<c:url value="/assets/pages/scripts/login.min.js"/>" type="text/javascript"></script>
                        <script src="<c:url value="/assets/layouts/layout2/scripts/layout.min.js"/>" type="text/javascript"></script>
                        <script src="<c:url value="/assets/layouts/layout2/scripts/demo.min.js"/>" type="text/javascript"></script>
                        <script src="<c:url value="/assets/layouts/global/scripts/quick-sidebar.min.js"/>" type="text/javascript"></script>
						
						
						<script src="<c:url value="/assets/global/plugins/llqrcode.js"/>" type="text/javascript"></script>
						<script src="<c:url value="/assets/global/plugins/webqr.js"/>" type="text/javascript"></script>
                    </head>
                    <style>

                        .tooltips {
                            position: relative;
                            display: inline-block;
                            border-bottom: 1px dotted black;
                        }

                        .tooltips .tooltiptext {
                            visibility: hidden;
                            width: 80px;
                            background-color: black;
                            color: #fff;
                            text-align: center;
                            border-radius: 6px;
                            padding: 5px 0;
                            position: absolute;
                            z-index: 1;
                            bottom: 110%;
                            left: 184%;
                            margin-left: -60px;
                        }
                        }

                        .tooltips .tooltiptext::after {
                            content: "";
                            position: absolute;
                            top: 100%;
                            left: 50%;
                            margin-left: -5px;
                            border-width: 5px;
                            border-style: solid;
                            border-color: black transparent transparent transparent;
                        }

                        .tooltips:hover .tooltiptext {
                            visibility: visible;
                        }
                        .valid{
                            margin-top: 20px;


                        }
                    </style>

                    <!-- END HEAD -->