<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<script>
	jQuery(document).ready(function(){
// 		jQuery('#change-pass').submit(function(){
// 			jQuery.ajax({
// 				url:jQuery(this).attr('action'),
// 				type:jQuery(this).attr('method'),
// 				data: jQuery(this).serialize(),
// 			}).done(function(data){
// 				var msg = "Proses Berhasil. Data telah di simpan";
// 				jQuery.jGrowl(msg, { life: 5000});
// 			}).fail(function(){
// 				var msg = "Failed";
// 				jQuery.jGrowl(msg, { life: 5000});
// 			});
			
// 			return false;
// 		});
		
		$.validator.addMethod('filesize', function(value, element, param) {
		    // param = size (en bytes) 
		    // element = element to validate (<input>)
		    // value = value of the element (file name)
		    return this.optional(element) || (element.files[0].size <= param) 
		});
		
		$('#change-pass').validate({
			rules: {
				password: {
					required: true,
					minlength : 5
				},
				confirmPassword: {
					minlength : 5,
                    equalTo : "#password"
				}
			},
			messages: {
				

			},
			highlight: function(label) {
// 				setTimeout($.unblockUI, timeoutLoad);
		    },
		    success: function(label) {
		    	label
		    		.text('Ok!').addClass('valid')
		    		.closest('.control-group').addClass('success');
		    },
		    submitHandler : function(form) {
		    	 var formData = new FormData(form);

					jQuery.ajax({
						url:jQuery(form).attr('action'),
						type:jQuery(form).attr('method'),
						data: formData,
						//cache: false,
					    contentType: false,
					    processData: false
					}).done(function(data){
						var msg = "Proses Berhasil. Data telah di simpan";
						jQuery.jGrowl(msg, { life: 5000});
					}).fail(function(){
						var msg = "Failed";
						jQuery.jGrowl(msg, { life: 5000});
					});
					
					return false;
		    }
		});
		
		$('#update-profile').validate({
			rules: {
				firstName: {
					required: true,
					maxlength: 15
				},
				lastName: {
					required: true,
					maxlength: 50
				},
				email: {
					required: true,
					email: true,
					maxlength: 100
				},
				image: {
					accept: "png|jpe?g|gif", 
					filesize: 1048576
				}
			},
			messages: {
				image: "File must be JPG, GIF or PNG, less than 1MB"

			},
			highlight: function(label) {
// 				setTimeout($.unblockUI, timeoutLoad);
		    },
		    success: function(label) {
		    	label
		    		.text('Ok!').addClass('valid')
		    		.closest('.control-group').addClass('success');
		    },
		    submitHandler : function(form) {
		    	 var formData = new FormData(form);

					jQuery.ajax({
						url:jQuery(form).attr('action'),
						type:jQuery(form).attr('method'),
						data: formData,
						//cache: false,
					    contentType: false,
					    processData: false
					}).done(function(data){
						var msg = "Proses Berhasil. Data telah di simpan";
						jQuery.jGrowl(msg, { life: 5000});
					}).fail(function(){
						var msg = "Failed";
						jQuery.jGrowl(msg, { life: 5000});
					});
					
					return false;
		    }
		});
		

		setTimeout($.unblockUI, timeoutLoad);
	});
</script>

<div class="rightpanel">
	<ul class="breadcrumbs">
		<li><a href="<c:url value="/home.page"/>"><i
				class="iconfa-home"></i></a> <span class="separator"></span></li>
		<li>Edit Profile</li>
	</ul>

	<div class="pageheader">
		<div class="pageicon animate0 bounceIn">
			<span class="iconfa-edit"></span>
		</div>
		<div class="pagetitle animate1 bounceIn">
			<h1>Edit Profile</h1>
		</div>
	</div>
	<!--pageheader-->

	<div class="maincontent">
		<div class="maincontentinner">
			<div class="row">
				<div class="col-md-4 profile-left">
					<div class="widgetbox">
						<div class="headtitle">
							<h4 class="widgettitle">${user.firstName}'s Password</h4>
						</div>
						<div class="widgetcontent">
							<form id="change-pass" method="post" action="<c:url value="/user/changepass.page" />" >
								<div class="form-group">
		                            <label for="exampleInputEmail1">Password</label>
		                            <input type="password" class="form-control input-default" id="password" name="password" placeholder="Enter New Password">
		                        </div>
		                        <div class="form-group">
		                            <label for="exampleInputPassword1">Confirm Password</label>
		                            <input type="password" class="form-control input-default" id="confirmPassword" name="confirmPassword" placeholder="Confirm New Password">
		                        </div>
								<button type="submit" class="btn btn-primary">Change Password</button>
							</form>
							<!--profilethumb-->
						</div>
					</div>
					<div class="widgetbox">
						<div class="headtitle">
							<h4 class="widgettitle">${user.firstName}'s Information</h4>
						</div>
						<div class="widgetcontent">
							<c:if test="${user.lastLogin != null}">
								<p>
									<label>Last Login</label> 
									<span>
										<fmt:formatDate pattern="E,dd-MMM-yyyy  HH:mm:ss a" value="${user.lastLogin}" />
									</span>
								</p>
							</c:if>
							<c:if test="${user.createDate != null}">
								<p>
									<label>Create Date</label> <span class="field">
									<fmt:formatDate pattern="E,dd-MMM-yyyy  HH:mm:ss a" value="${user.createDate}" />
									
									</span>
								</p>
							</c:if>
							<c:if test="${user.author != null}">
								<p>
									<label>Author</label> <span class="field">${user.author}
									</span>
								</p>
							</c:if>
							<c:if test="${user.lastUpdate != null}">
								<p>
									<label>Last Update</label> <span class="field">
									<fmt:formatDate pattern="E,dd-MMM-yyyy  HH:mm:ss a" value="${user.lastUpdate}" />
									
									</span>
								</p>
							</c:if>
							<c:if test="${user.editor != null}">
								<p>
									<label>Editor</label> <span class="field">${user.editor}
									</span>
								</p>
							</c:if>
							<c:if test="${user.publishDate != null}">
								<p>
									<label>Publish Date</label> <span class="field">
									<fmt:formatDate pattern="E,dd-MMM-yyyy  HH:mm:ss a" value="${user.publishDate}" />
									
									</span>
								</p>
							</c:if>
							<c:if test="${user.publisher != null}">
								<p>
									<label>Publisher</label> <span class="field">${user.publisher}
									</span>
								</p>
							</c:if>
							<c:if test="${user.unpublishDate != null}">
								<p>
									<label>Unpublish Date</label> <span class="field">
									<fmt:formatDate pattern="E,dd-MMM-yyyy  HH:mm:ss a" value="${user.unpublishDate}" />
									
									</span>
								</p>
							</c:if>
							<c:if test="${user.unpublisher != null}">
								<p>
									<label>Unpublisher</label> <span class="field">${user.unpublisher}
									</span>
								</p>
							</c:if>
							<c:if test="${user.unpublishReason != null}">
								<p>
									<label>Unpublish Reason</label> <span class="field">${user.unpublishReason}
									</span>
								</p>
							</c:if>
							<c:if test="${user.expiredDate != null}">
								<p>
									<label>Expired Date</label> <span class="field">
									<fmt:formatDate pattern="E,dd-MMM-yyyy  HH:mm:ss a" value="${user.expiredDate}" />
									
									</span>
								</p>
							</c:if>
							<!--profilethumb-->
						</div>
					</div>
				</div>
				<!--col-md-4-->
				<div class="col-md-8">

					<div class="widgetbox">
						<h4 class="widgettitle">Profile</h4>
						<div class="widgetcontent">
							<form id="update-profile" class="stdform stdform2" method="post"
								enctype="multipart/form-data"
								action="<c:url value="/user/updateProfile.page" />">
								
								<p>
									<label>Username<small>Unique user identifier</small></label> <span
										class="field"> <input type="text" name="username"
										id="username" value="${user.username}" class="form-control" readonly="readonly"/>
									</span>
								</p>
								<p>
									<label>Role<small>User role</small></label> <span
										class="field"><select disabled="disabled" name="roleId" id="role"
										class="uniformselect">
											<option value="">Choose One</option>
											<c:forEach var="role" items="${listrole}">
												<c:choose>
													<c:when test="${user.roleId == role.id}">
														<option value="<c:out value="${role.id}" />"
															selected="selected"><c:out
																value="${role.title}" /></option>
													</c:when>
													<c:otherwise>
														<option value="<c:out value="${role.id}" />"><c:out
																value="${role.title}" /></option>
													</c:otherwise>
												</c:choose>



											</c:forEach>
									</select></span>
								</p>
								
								<c:if test="${user.roleId eq 'STD' }">
									<p>
										<label>Student No</label> <span class="field"> <input
											type="text" class="userno" id="userNo"
											name="userNo" value="${user.userNo}">
										</span>
									</p>
								</c:if>
								<c:if test="${user.roleId eq 'TCH' }">
									<p>
										<label>Employee No</label> <span class="field"> <input
											type="text" class="userno" id="userNo"
											name="userNo" value="${user.userNo}">
										</span>
									</p>
								</c:if>
								
								
								<p>
									<label>First Name<small>Required</small></label> <span
										class="field"> <input type="text" name="firstName"
										id="firstName" value="${user.firstName}"
										class="form-control"  />
									</span>
								</p>
								<p>
									<label>Last Name<small>Required</small></label> <span
										class="field"> <input type="text" name="lastName"
										id="lastName" value="${user.lastName}" class="form-control"
										 />
									</span>
								</p>
								
								<p>
									<label>Email<small>Required</small></label> <span
										class="field"> <input type="text" name="email"
										id="email" value="${user.email}" class="form-control"
										 />
									</span>
								</p>
								<p>
									<label for="exampleInputFile">Profile Picture</label><span
										class="field"> <input type="file" name="image"
										id="image">
									</span>
								</p>
								
								<p></p>
								<br/>
								<button type="submit" class="btn btn-primary">Update Profile</button>
							</form>
						</div>
				</div>
				<!--col-md-8-->
			</div>
			<!--row-->

			<%@ include file="/common//mainFooter.jsp"%>

		</div>
		<!--maincontentinner-->
	</div>
	<!--maincontent-->
</div>
<!--rightpanel-->
</div>