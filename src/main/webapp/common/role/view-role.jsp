<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<script>
	$(document).ready(function(){
		$('#save-btn').click(function(){
			$('#add-subject-form').validate({
				rules: {
					name: "required"
				},
				messages: {
					name: "Please enter your first name"
				},
				highlight: function(label) {
			    },
			    success: function(label) {
			    	label
			    		.text('Ok!').addClass('valid')
			    		.closest('.control-group').addClass('success');
			    },
			    submitHandler : function(form) {
			    	$.ajax({
						url:$('#add-subject-form').attr('action'),
						type:$('#add-subject-form').attr('method'),
						data: $('#add-subject-form').serialize(),
					}).done(function(data){
						var msg = "Success";
						jQuery.jGrowl(msg, { life: 3000});
						$('#content').load('<c:url value="/content/subjects.page"/>', function() {
						});
					}).fail(function(){
						var msg = "Failed";
						jQuery.jGrowl(msg, { life: 3000});
					});
			    }
			});
		});
		
		
	});

</script>

<div class="rightpanel">
	<ul class="breadcrumbs">
		<li><a href="<c:url value="home.page"/>"><i
				class="iconfa-home"></i></a> <span class="separator"></span></li>
		<li>Application &amp; Setting <span class="separator"></span></li>
		<li>Role <span class="separator"></span></li>
		<li>View</li>
	</ul>

	<div class="pageheader">
		<div class="pageicon animate0 fadeInRightBig">
			<span class="iconfa-laptop"></span>
		</div>
		<div class="pagetitle animate1 fadeInRightBig">
			<h5>Application &amp; Setting</h5>
			<h1>Role</h1>
		</div>
	</div>
	<!--pageheader-->

	<div class="maincontent">
		<div class="maincontentinner">
			<div class="widgetbox animate2 fadeInRightBig">
				<h4 class="widgettitle">Role Info</h4>
				<div class="widgetcontent nopadding">
					<form id="add-subject-form" class="stdform stdform2" method="post">
						<p>
							<label>Role <small>title of role</small></label> 
							<span class="field"> 	
								${role.title}
							</span>
						</p>

						<p>
							<label>Description <small>explaination of role</small></label> 
							<span class="field"> 
								${role.description}
							</span>
						</p>
						
						<p>
							<label>Status <small>active/inactive role</small></label> 
							<span class="field"> 
<%-- 								${role.status} --%>
								
			                                	<c:choose>
													<c:when test="${role.status == 'A' }">
														Active
													</c:when>
													<c:otherwise>
														Deactive
													</c:otherwise>
												</c:choose>								
							</span>
						</p>
						
						<p>
							<label>Last Update <small>time role updated</small></label> 
							<span class="field"> 
								<fmt:formatDate type="both" value="${role.updateTime}" />
							</span>
						</p>
						
						<p>
							<label>Menu Access <small>menu can be accessed</small></label> 
							<span class="field"> 
								<c:forEach items="${listMenu}" var="menu"  >
									${menu.nameMenu} <br/>
								</c:forEach>
							</span>
						</p>
					</form>
				</div>
				<!--widgetcontent-->
			</div>

			<%@ include file="/common//mainFooter.jsp"%>

		</div>
		<!--maincontentinner-->
	</div>
	<!--maincontent-->
</div>
