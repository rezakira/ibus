<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<style>

.action-role-table:hover {
	-webkit-transform: scale(1.1);
	-moz-transform:scale(1.1);
	transform: scale(1.1);
}

</style>

<script type="text/javascript">
	$(document).ready(function() {
		var oTable = $('#dyntable').dataTable({
			"oLanguage": {
	            "sProcessing": 'Loading Table...'
	        },
			"sPaginationType" : "full_numbers",
			"bLengthChange": false,
	        "bFilter": false,
			"bProcessing": true,
			"bServerSide": true,
			"sAjaxSource": '<c:url value="/role/data-table.json" />',
			"aoColumns": [
			              { "mData" : 'id',
				            	"mRender": function (sourceData, dataType, fullData) {
				            		return '<img data-id="'+fullData.id+'" class="action-role-table" src="images/icons/view.png" width="32px" alt="" />';
				            	}
				          },
			              { "mData": "title" },
						  { "mData": "description" }
			             
			]
		});
		
		$('body').on('click', '.action-role-table', function () {
			$('#content').load('<c:url value="/role/view-role.page/'+$(this).data('id')+'"/>', function() {
			});
		});

		function info(id) {
			$.ajax({
				url : '<c:url value="/role/data-table.json" />',
				type : 'post',
				data : {
					"roleId" : id
				},
				success : function(data) {
				}
			});
		}
	});
</script>

<!-- BEGIN PAGE HEADER-->
    <h3 class="page-title"> Role
        <small>Peran Pengguna</small>
    </h3>
    <div class="page-bar">
        <ul class="page-breadcrumb">
            <li>
                <i class="icon-home"></i>
                <a href="">Home</a>
                <i class="fa fa-angle-right"></i>
            </li>
            <li>
                <span>Peran Pengguna</span>
            </li>
        </ul>

    </div>
    <!-- END PAGE HEADER-->

	<div class="maincontent">
		<div class="maincontentinner">
			<div class="widget animate2 fadeInRightBig">
				<h4 class="widgettitle">Table Master Role</h4>
				<div class="widgetcontent">
					<table class="table table-striped responsive" id="dyntable">
						<thead>
							<tr>
								<th style="width: 10%"></th>
								<th style="width: 20%">ROLE</th>
								<th>NOTES</th>
							</tr>
						</thead>
					</table>
				</div>
			</div>
			<!--row-->

			<%@ include file="/common//mainFooter.jsp"%>
			<!--footer-->

		</div>
		<!--maincontentinner-->
	</div>
	<!--maincontent-->

</div>
<!--rightpanel-->