<%@ include file="/common/_header.jsp"%>

<script>
    jQuery(document).ready(function(){
    		$('#login_form').validate(
				{
					rules : {
						userid: {
							required: true
						}
					},
					messages : {
					},
					highlight : function(label) {
						// 				setTimeout($.unblockUI, timeoutLoad);
					},
					success : function(label) {
						label.text('Ok!').addClass('valid').closest('.control-group').addClass('success');
					},
					submitHandler : function(form) {
						//form.submit(); 
						var formData = new FormData(form);

						$.ajax({
							url:$(form).attr('action'),
							type:$(form).attr('method'),
							data: formData,
							contentType: false,
						    processData: false
						}).done(function(data){
							jQuery.jGrowl("Success Reset password , Please Check Your Email Inbox", { life: 3000});
							setTimeout(function () {
							       window.location.href = "<c:url value='/' />"; //will redirect to your blog page (an ex: blog.html)
							}, 5000); //will call the function after 2 secs.
							
							 
						}).fail(function(){
							var msg = "Failed Reset Password, Please Check Your Internet Connection";
							jQuery.jGrowl(msg, { life: 3000});
							
						});

						return false;
					}
				});

    });
    
</script>


<head>
		<meta charset="utf-8">
		<link href="css/style.css" rel='stylesheet' type='text/css' />
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>
</head>
 
<body class="login">
	<div id="login">

		<div align="center">
			<div class="main">
				<form id="login_form"
					action="<c:url value='user/reset.page' /> " method="post">
					<h1>
						<img width="200" src="<c:url value="/images/happytax_logo.png"/>" alt="" /><br>
						<br><label> Forgot Password </label>
					</h1>
					<div class="inset">
						<p align="left">
							<label id="login_label" for="user" style="align: left;">USERNAME OR EMAIL</label>
							<input id="login_input" name="userid"  
								type="text" placeholder="Enter Username or email address" />
			
						</p>
					
					</div>

					<p class="p-container">
						<input id="login_button" type="button" onclick="window.location.href='<c:url value='/' />';" value="Cancel" style="margin: 5px;"><input style="margin: 5px;" id="login_button" type="submit" value="Forgot">
					</p>
				</form>
			</div>
		</div>
		<!-- <br><br>    <p>Copyright � 2014 Evolution Technology</p> -->
	</div>
	<!-----//end-copyright---->
</body>


<div class="loginfooter">
   	<p><spring:message code="label.copyright"/></p>
</div>
<%@ include file="/common/_footer.jsp"%>

