<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<script>
	$(document).ready(function(){
		$('.action-message').click(function(){
			$('.action-message').removeClass('selected');
			$(this).addClass('selected');
			$.ajax({
				url : '<c:url value="/user/detailInbox.json"/>',
				type : 'post',
				data : {
					"id" : $(this).val()
				},
				success : function(data) {
					$('#message-detail').text(data.obj.message);
				}
			});
		});
		
		setTimeout($.unblockUI, timeoutLoad);
	});
</script>

<div class="rightpanel">
	<ul class="breadcrumbs ">
		<li><a href="<c:url value="home.page"/>"><i
				class="iconfa-home"></i></a> <span class="separator"></span></li>
		<li>Inbox</li>
	</ul>

	<div class="pageheader">
		<div class="pageicon animate0 bounceIn">
			<span class="iconfa-envelope-alt"></span>
		</div>
		<div class="pagetitle animate1 bounceIn">
			<h5>Message</h5>
			<h1>Inbox</h1>
		</div>
	</div>
	<!--pageheader-->

	<div class="maincontent">
		<div class="maincontentinner">
		   <div class="messagepanel">
<!-- 		   	   <div class="messagehead"> -->
<!--                    <button class="btn btn-success btn-large">Compose Message</button> -->
<!--                </div>messagehead -->
	           <div class="messagemenu">
	               <ul>
<!--                       <li class="back"><a><span class="iconfa-chevron-left"></span> Back</a></li> -->
                      <li class="active"><a href=""><span class="iconfa-inbox"></span> Inbox</a></li>
<!--                       <li><a href=""><span class="iconfa-plane"></span> Sent</a></li> -->
<!--                       <li><a href=""><span class="iconfa-edit"></span> Draft</a></li> -->
<!--                       <li><a href=""><span class="iconfa-trash"></span> Trash</a></li> -->
                  </ul>
	           </div>
	           <div class="messagecontent">
		           <div class="messageleft">
		               <form class="messagesearch">
		                   <input type="text" class="form-control" placeholder="Search message and hit enter..." />
		               </form>
		               <ul class="msglist">
			               <c:forEach var="inbox" items="${listInbox}">
								<li class="action-message" value="${inbox.id}" >
									<div class="thumb">
										<c:choose>
											<c:when test="${inbox.imageUrl}">
												<img src="${user_pic_path}${inbox.username}/${inbox.imageUrl}" alt="" />
											</c:when>
											<c:otherwise>
												<img src="<c:url value="/images/default-user.png"/>" alt="" />
											</c:otherwise>
										</c:choose>
										
									</div>
									<div class="summary">
										<span class="date pull-right"><small><fmt:formatDate type="both" value="${inbox.createDate }" /></small></span>
										<h4>${inbox.username}</h4>
										<p>${fn:substring(inbox.message, 0, 45)}</p>
									</div>
								</li>
							</c:forEach>
		               	</ul>
		           	</div><!--messageleft-->
		            <div class="messageright">
		                <div class="messageview">
		                    <div id="message-detail" class="msgbody">
		                    </div><!--msgbody-->
		                </div><!--messageview-->
		            </div><!--messageright-->
	            </div><!--messagecontent-->
	        </div>
			<%@ include file="/common//mainFooter.jsp"%>
			<!--maincontentinner-->
		</div>
		<!--maincontent-->
	</div>
<!--rightpanel-->
</div>