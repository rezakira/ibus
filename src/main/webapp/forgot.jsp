<%@ include file="/_header.jsp"%>
<title>HappyTax</title>
<link rel="stylesheet" href="<c:url value="/css/forgot.css"/>">
</head>
<body>
  <div id="wrapper">
    <div>
		<h1>Lupa Password</h1>
        <div>
          <div>
            <form  method="POST">
              <div >
                <label>Masukan Username</label>
              </div>
              <div >
                <input type="text" tabindex="1" id="username" name="username" required="required"  />
              </div>
              <div >
                <input name="submit" type="submit" tabindex="2" class="button" value="Reset Password" /> 
              </div>
              <a href="<c:url value='/login.do'/>">Login</a>
            </form>
          </div>
        </div>
    </div>
  </div>
<%@ include file="/_footer.jsp"%>
