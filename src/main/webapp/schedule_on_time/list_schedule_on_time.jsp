<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<style>

.action-menu-table:hover {
	-webkit-transform: scale(1.1);
	-moz-transform:scale(1.1);
	transform: scale(1.1);
}

</style>

<script type="text/javascript">
	$(document).ready(function() {
	
		$('body').off('click', '.action-menu-view-schedule-on-time');
		$('body').off('click', '.action-menu-edit-schedule-on-time');
		$('body').off('click', '.action-menu-delete-schedule-on-time');
		
	
		// dynamic table
		var oTable = $('#dyntable').dataTable({
			"oLanguage": {
	            "sProcessing": 'Loading Table...'
	        },
			"sPaginationType" : "full_numbers",
			"bLengthChange": false,
	        "bFilter": false,
			"bProcessing": true,
			"bServerSide": true,
            "bAutoWidth": true,
            "aoColumns" : [
            { sWidth: '20%' },
            { sWidth: '20%' },
            { sWidth: '20%' },
            { sWidth: '20%' },
            { sWidth: '20%' }
        ],  
			"sAjaxSource": '<c:url value="/data-master/schedule_on_time/data-table.json" />',
			"aoColumns": [
			              { "mData" : 'idOnTime',
				            	"mRender": function (sourceData, dataType, fullData) {
									      
								//return	 '<a class="btn btn-icon-only tooltips  green action-menu-view-menucms" href="javascript:;">'
								//		 +'<i class="fa fa-search"></i><span class="tooltiptext">Edit</span></a>'
								return	 '<a class="btn btn-icon-only tooltips  blue action-menu-edit-schedule-on-time" href="javascript:;" data-id="'+fullData.idOnTime+'">'
										 +'<i class="fa fa-edit"></i><span class="tooltiptext">Edit</span></a>'
										 +'<a class="btn btn-icon-only tooltips  red action-menu-delete-schedule-on-time" href="javascript:;" data-id="'+fullData.idOnTime+'">'
										 +'<i class="fa fa-times"></i><span class="tooltiptext">Delete</span></a>'
										 +'<a class="btn btn-icon-only tooltips  purple action-menu-view-schedule-on-time" href="#" data-id="'+fullData.idOnTime+'">'
										 +'<i class="fa fa-file-text"></i><span class="tooltiptext">Detail</span></a>';
				                  }
						  },
						  { "mData" : 'timeStart' ,
							  "mRender": function (sourceData, dataType, fullData) {
							  return	'<span>' 
							 	 +'Tanggal : </span>'
							  	 +'<span data-id="'+fullData.actualDate+'">' 
							     +''+fullData.actualDate+'</span>'
							     +'<br>'
							     +'Jadwal : </span>'
							 	 +'<br>'
							  	 +'<span data-id="'+fullData.timeStart+'">'
								 +''+fullData.timeStart+'</span>'
								 +'<span data-id="'+fullData.timeEnd+'">'
								 +' - '+fullData.timeEnd+'</span>';
							
		                  		}
						  },
						  { "mData" : 'nameRuteFrom' ,
							  "mRender": function (sourceData, dataType, fullData) {
							  return	 '<span data-id="'+fullData.nameRuteFrom+'">'
								 +''+fullData.nameRuteFrom+'</span>'
								 +'<span data-id="'+fullData.nameRuteto+'">'
								 +' - '+fullData.nameRuteTo+'</span>';
							
		                  		}
						  },
						  { "mData": "nameBus" },
						  { "mData": "nameDriver" },
						  { "mData": "dateModified"},
			             
			]
		});
		
		//$('body').on('click', '.action-menu-view-bus', function () {
			//alert('view');
		//});
		
		$('body').on('click', '.action-menu-view-schedule-on-time', function () {
			$("#main_content").load('<c:url value="/data-master/detail_schedule_on_time.page" />'+'/'+$(this).data('id'));
		});
		
		$('body').on('click', '.action-menu-edit-schedule-on-time', function () {
			$("#main_content").load('<c:url value="/data-master/update_schedule_on_time.page" />'+'/'+$(this).data('id'));
		});
		
		$('body').on('click', '.action-menu-delete-schedule-on-time', function () {
			if (!confirm("Apakah anda yakin menghapus data ini ?")){
			      return false;
			}
			else
			{
							$.ajax({
                                url:'<c:url value="/data-master/deleteScheduleOnTime.json" />',
                                type:'post',
                                data: {
									'id_long':$(this).data('id')
								}
                            }).done(function(data){
                                
                            	if(data.status == 1)
                            	{
                            		jQuery.jGrowl(data.message, { life: 3000});
									
									$('#dyntable').dataTable()._fnAjaxUpdate();
								}
                            	else
                            	{
                            		jQuery.jGrowl(data.message, { life: 3000});
                                }
                            	
                            }).fail(function(){
                                var msg = "Failed";
                                jQuery.jGrowl(msg, { life: 3000});
                            });
			}
		});
		
		
	});
</script>


<div class="page-bar">
                        <ul class="page-breadcrumb">
                            <li>
                                <a href="<c:url value="home.page"/>">Home</a>
                                <i class="fa fa-angle-right"></i>
                            </li>
                            <li>
                                <a href="#">Data Master</a>
                            </li>
                        </ul>
                        
                    </div>
                    <!-- END PAGE BAR -->
                    <!-- BEGIN PAGE TITLE-->
                    <h3 class="page-title"> Manajemen Schedule On Time
                    </h3>
                    <!-- END PAGE TITLE-->
                    <!-- END PAGE HEADER-->
                   
                    <div class="row">
                        <div class="col-md-12">
                            <!-- BEGIN EXAMPLE TABLE PORTLET-->
                            <div class="portlet light bordered">
                                <div class="portlet-title">
                                    <div class="caption font-dark">
                                        <i class="icon-settings font-dark"></i>
                                        <span class="caption-subject bold uppercase"> Tabel Schedule On Time</span>
                                        
                                    </div>
                                 
                                </div>
                                <div class="portlet-body">
                                    <div class="table-toolbar">
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="btn-group">
                                                    <a href="#" data-id="<c:url value="/data-master/add_schedule_on_time.page"/>" onclick="linkToPage(this);return false;"><button id="sample_editable_1_new" class="btn sbold green">Tambah Schedule On Time
                                                        <i class="fa fa-plus"></i>
                                                    </button></a>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                              
                                            </div>
                                        </div>
                                    </div>
                                    <table class="table table-striped table-bordered table-hover table-checkable order-column" id="dyntable">
                                        <thead style="width:100%">
                                            <tr>
                                                <th width="17%"> Action </th>
                                                <th> Waktu </th>
                                                <th> Rute </th>
                                                <th> Bis </th>
                                                <th> Nama Driver</th>
                                                <th> Tanggal Update </th>
                                            </tr>
                                        </thead>
                                        
                                    </table>
                                </div>
                            </div>
                            <!-- END EXAMPLE TABLE PORTLET-->
                        </div>
                    </div>