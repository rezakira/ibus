<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

    <style>
        .btn-saving {
            float: left;
        }

        .btn-saving ~ img {
            padding-left: 10px;
            display: inline !important;
        }

        #btn-save {
            width: 100px;
        }

        #btn-save ~ img {
            display: none;
        }
    </style>

    <script>
    function getListRuteTo()
	{
			$.get("<c:url value="/data-master/get-information-to-destination.json" />"+"?id_from="+$('#ruteFromIdRef').val(), function( data ) {
				
				
				$('#ruteToIdRef').find('option').remove().end();
				
				for(var counter=0;counter<data.obj.length;counter++)
				{
					$('#ruteToIdRef').append($("<option></option>")
					.attr("value",data.obj[counter].id)
					.text(data.obj[counter].value)); 
					
					if(counter == 0)$('#ruteToIdRef').val(data.obj[counter].id);
				}
				getRuteToDetail();
				
			});
			
	}
	
	function getRuteToDetail()
	{
			
			$.get("<c:url value="/data-master/get-destination-detail.json" />"+"?id="+$('#ruteToIdRef').val(), function( data ) {
				priceUnitDestination = data.obj.priceTicketUnit;
				$('#harga_unit_ref').text(priceUnitDestination);
				changePrice();
			});
	}
	
		
		
        $(document)
            .ready(
            function() {
            	getListRuteTo();
            	$("#actualDate").datepicker({ dateFormat: 'yy-mm-dd' , startDate: '0d'});
                $('#form_add_schedule_on_time').validate(
                    {
                        rules : {

                        },
                        messages : {

                        },
                        highlight : function(label) {

                        },
                        success : function(label) {
                            label.text('Ok!').addClass(
                                'valid').closest(
                                '.control-group')
                                .addClass('success');
                        },
                        submitHandler : function(form) {
                            var formData = new FormData(form);



                            $.ajax({
                                url:$(form).attr('action'),
                                type:$(form).attr('method'),
                                data: formData,
                                contentType: false,
                                processData: false
                            }).done(function(data){

                                if(data.status == 1)
                                {
                                    jQuery.jGrowl(data.message, { life: 3000});
                                    $("#main_content").load('<c:url value="/data-master/list_schedule_on_time.page" />');
                                }
                                else
                                {
                                    jQuery.jGrowl(data.message, { life: 3000});

                                }

                            }).fail(function(){
                                var msg = "Failed";
                                jQuery.jGrowl(msg, { life: 3000});
                            });

                            return false;
                        }
                    });

            });
    </script>
 
    <!-- BEGIN CONTENT -->

	
    <!-- BEGIN PAGE BAR -->
    <div class="page-bar">
        <ul class="page-breadcrumb">
            <li>
                <a href="<c:url value="home.page"/>">Data Master</a>
                <i class="fa fa-angle-right"></i>
            </li>
            <li>
                <span>Manajemen Schedule On Time</span>
            </li>
        </ul>
        <div class="page-toolbar">
            <div class="btn-group pull-right">
                <a class="btn green" href="#"> <i class="fa fa-angle-left"></i> Back
                </a>

            </div>
        </div>
    </div>
    <!-- END PAGE BAR -->
    <!-- BEGIN PAGE TITLE-->
    <h3 class="page-title"> Add Schedule On Time
    </h3>
    <!-- END PAGE TITLE-->
    <!-- END PAGE HEADER-->
    <div class="row">
        <div class="col-md-12 ">
            <!-- BEGIN SAMPLE FORM PORTLET-->
            <div class="portlet light bordered">
                <div class="portlet-title">
                    <div class="caption font-red-sunglo">
                        <i class="icon-settings font-red-sunglo"></i>
                        <span class="caption-subject bold uppercase"> Form Add Schedule On Time</span>
                    </div>

                </div>
                <div class="portlet-body form">
                    <form id="form_add_schedule_on_time" class="stdform stdform2" 
					method="post" action="<c:url value="/data-master/saveScheduleOnTime.json" />" 
					enctype="multipart/form-data">
                        
						<div class="form-body">

							<div class="form-group form-md-line-input ">
								<select name="idSchedule" id="idSchedule" class="form-control">
									<c:forEach var="schedule" items="${list_schedule}" varStatus="scheduleStatus">
										<option value="${schedule.idSchedule}" <c:if test="${scheduleStatus.index eq 0 }">selected="selected"</c:if>>${schedule.timeStart}-${schedule.timeEnd}</option>
									</c:forEach>
								</select> 
								<label for="">Waktu</label> <span
									class="help-block">Waktu</span>
							</div>
							
							<div class="form-group form-md-line-input ">
							<select name="idBus" id="idbus" class="form-control">
								<c:forEach items="${list_bus}" var="bus" varStatus="busStatus">
									<option value="${bus.idBus}" <c:if test="${busStatus.index eq 0 }">selected="selected"</c:if>>${bus.nameBus}</option>
								</c:forEach>
							</select> <label for="">Bis</label> <span
								class="help-block">Bis</span>
							</div>
                            
                         	<div class="form-group form-md-line-input ">
							<select name="ruteFromIdRef" id="ruteFromIdRef" class="form-control " onchange="getListRuteTo()">
                                <c:forEach var="ruteFrom" items="${list_rute_from}" varStatus="ruteFromStatus">
									<option value="${ruteFrom.idRuteFrom}" <c:if test="${ruteFromStatus.index eq 0 }">selected="selected"</c:if>>${ruteFrom.nameRuteFrom}</option>
								</c:forEach>
                             </select>
							<label for="">Tempat Keberangkatan</label> <span
								class="help-block">Tempat Keberangkatan</span>
							</div>
							
							<div class="form-group form-md-line-input ">
							<select name="ruteToIdRef" id="ruteToIdRef" class="form-control" onchange="getRuteToDetail()">
								
							</select> <label for="">Tempat Tujuan</label> <span
								class="help-block">Tempat Tujuan</span>
							</div>
                            
                            <div class="form-group form-md-line-input ">
                                <input type="text" id="nameDriver" name="nameDriver" class="form-control">
                                <label for="">Nama Driver</label>
                                <span class="help-block">Nama Driver</span>
                            </div>
                            
                            <div class="form-group form-md-line-input has-info">

                                    <select name="status" id="status" class="form-control">
                                        <option value="B" selected>Belum Berangkat</option>
                                        <option value="D" >Dijalan</option>
                                        <option value="S" >Sudah Sampai</option>
                                    </select>
                                    <label for="form_control_1">Status</label>
                            </div>
                            
                            <div class="form-group form-md-line-input ">
                                <textarea class="form-control" name="note" id="note"></textarea>
                                    <label for="form_control_1">Note</label>
                                    <span class="help-block">Note</span>
								
                            </div>
                            
                            <div class="form-group form-md-line-input ">
                                <input type="text" id="actualDate" name="actualDate"
                                                   class="form-control  date-picker ">
                                <label for="">Tanggal</label>
                                <span class="help-block">Tanggal</span>
                            </div>
                                
                            </div>
                            <div class="form-actions noborder">
                                <button type="submit" class="btn blue">Submit</button>
                                <button type="button" class="btn default">Cancel</button>
                            </div>
                            </form>
                        </div>
                </div>
                <!-- END SAMPLE FORM PORTLET-->

            </div>
        </div>
