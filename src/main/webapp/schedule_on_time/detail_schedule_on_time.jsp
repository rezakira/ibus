<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<style>

.action-menu-table:hover {
	-webkit-transform: scale(1.1);
	-moz-transform:scale(1.1);
	transform: scale(1.1);
}

</style>
<script>
function fillTable()
{
	
}	

</script>
<script type="text/javascript">
	$(document).ready(function() {
		
		<c:forEach var="scheduleOnTimeDetails" items="${scheduleOnTimeDetails}" varStatus="scheduleOnTimeDetailsStatus">
			console.log(${scheduleOnTimeDetails.noKursi});
			$("#body_data_sit_order").append('<tr>'+
			'<td>${scheduleOnTimeDetails.noKursi}</td>'+
			'<td>${scheduleOnTimeDetails.statusString}</td>'+
			'<td>${scheduleOnTimeDetails.dateStringLastUpdate}</td>'+
			'</tr>');
		</c:forEach>
		
		
		$('body').off('click', '.action-menu-view-schedule-on-time');
		$('body').off('click', '.action-menu-edit-schedule-on-time');
		$('body').off('click', '.action-menu-delete-schedule-on-time');
		
		
		
		//$('body').on('click', '.action-menu-view-bus', function () {
			//alert('view');
		//});
		
		$('body').on('click', '.action-menu-view-schedule-on-time', function () {
			$("#main_content").load('<c:url value="/data-master/detail_schedule_on_time.page" />'+'/'+$(this).data('id'));
		});
		
		$('body').on('click', '.action-menu-edit-schedule-on-time', function () {
			$("#main_content").load('<c:url value="/data-master/update_schedule_on_time.page" />'+'/'+$(this).data('id'));
		});
		
		$('body').on('click', '.action-menu-delete-schedule-on-time', function () {
			if (!confirm("Apakah anda yakin menghapus data ini ?")){
			      return false;
			}
			else
			{
							$.ajax({
                                url:'<c:url value="/data-master/deleteScheduleOnTime.json" />',
                                type:'post',
                                data: {
									'id_long':$(this).data('id')
								}
                            }).done(function(data){
                                
                            	if(data.status == 1)
                            	{
                            		jQuery.jGrowl(data.message, { life: 3000});
									
									$('#dyntable').dataTable()._fnAjaxUpdate();
								}
                            	else
                            	{
                            		jQuery.jGrowl(data.message, { life: 3000});
                                }
                            	
                            }).fail(function(){
                                var msg = "Failed";
                                jQuery.jGrowl(msg, { life: 3000});
                            });
			}
		});
		
		
	});
</script>


<div class="page-bar">
                        <ul class="page-breadcrumb">
                            <li>
                                <a href="<c:url value="home.page"/>">Home</a>
                                <i class="fa fa-angle-right"></i>
                            </li>
                            <li>
                                <a href="#">Data Master</a>
                            </li>
                        </ul>
                        
                    </div>
                    <!-- END PAGE BAR -->
                    <!-- BEGIN PAGE TITLE-->
                    <h3 class="page-title"> Detail Schedule On Time
                    </h3>
                    <!-- END PAGE TITLE-->
                    <!-- END PAGE HEADER-->
                   
                    <div class="row">
                        <div class="col-md-12">
                            <!-- BEGIN EXAMPLE TABLE PORTLET-->
                            <div class="portlet light bordered">
                                <div class="portlet-title">
                                    <div class="caption font-dark">
                                        <i class="icon-settings font-dark"></i>
                                        <span class="caption-subject bold uppercase"> Tabel Kursi Schedule On Time</span>
                                        
                                    </div>
                                 
                                </div>
                                <div class="portlet-body">
                                    <table class="table table-striped table-bordered table-hover table-checkable order-column" id="dyntable">
                                        <thead style="width:100%">
                                            <tr>
                                                <th> No. Kursi </th>
                                                <th> Status </th>
                                                <th> Tanggal Update </th>
                                            </tr>
                                        </thead>
										<tbody id="body_data_sit_order">
											<!--
											<tr>
												<td>TST 1</td>
												<td>TST 1</td>
												<td>TST 1</td>
												<td>TST 1</td>
											</tr>-->
										</tbody>
                                    </table>
                                </div>
                            </div>
                            <!-- END EXAMPLE TABLE PORTLET-->
                        </div>
                    </div>
