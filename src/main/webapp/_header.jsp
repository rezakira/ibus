<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta name="viewport" content="width=device-width, initial-scale=1.0" />
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	
	<title>HappyTax</title>
	
    <!-- favicon -->
<%--     <link rel="shortcut icon" href="<c:url value="/images/favicon.png"/>"> --%>
    
    <!-- _header css -->
    <link rel="stylesheet" href="<c:url value="/css/common.css"/>">
    <link rel="stylesheet" href="<c:url value="/css/style.default.css"/>"/>
	
	<!-- header js -->
	<script src="<c:url value="/js/jquery-1.10.2.min.js"/>"></script>
	<script src="<c:url value="/js/jquery-migrate-1.2.1.min.js"/>"></script>
	<script src="<c:url value="/js/jquery-ui-1.10.3.min.js"/>"></script>
	<script src="<c:url value="/js/bootstrap.min.js"/>"></script>
	<script src="<c:url value="/js/modernizr.min.js"/>"></script>
	<script src="<c:url value="/js/jquery.cookies.js"/>"></script>
	<script src="<c:url value="/js/custom.js"/>"></script>
<%-- 	<script src="<c:url value="/js/cookie.js"/>"></script>             --%>
	<!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
	<!--[if lt IE 9]>
	<script src="<c:url value="/js/html5shiv.js"/>"></script>
	<script src="<c:url value="/js/respond.min.js"/>"></script>
	<![endif]-->
