<%@ include file="/_header.jsp"%>

<script src="<c:url value="/js/jquery.uniform.min.js"/>"></script>
<script src="<c:url value="/js/flot/jquery.flot.min.js"/>"></script>
<script src="<c:url value="/js/flot/jquery.flot.resize.min.js"/>"></script>
<script src="<c:url value="/js/responsive-tables.js"/>"></script>
<script src="<c:url value="/js/jquery.slimscroll.js"/>"></script>

<link rel="stylesheet" href="css/style.default.css" />
<link rel="stylesheet" href="css/responsive-tables.css">

<script src="prettify/prettify.js"></script>
<script src="js/responsive-tables.js"></script>
<script src="js/jquery.dataTables.min.js"></script>

<script type="text/javascript">
    jQuery(document).ready(function(){
        // dynamic table
        jQuery('#dyntable').dataTable({
            "sPaginationType": "full_numbers",
            "aaSortingFixed": [[0,'asc']],
            "fnDrawCallback": function(oSettings) {
                jQuery.uniform.update();
            }
        });
        
        jQuery('#dyntable2').dataTable( {
            "bScrollInfinite": true,
            "bScrollCollapse": true,
            "sScrollY": "300px"
        });
        
    });
</script>

</head>

<body>

<div id="mainwrapper" class="mainwrapper">
    
    <%@ include file="/_page-header.jsp"%>
    
    <%@ include file="/_left-panel.jsp"%>
    
    <div class="rightpanel">
        
        <ul class="breadcrumbs">
            <li><a href="dashboard.html"><i class="iconfa-user"></i></a> <span class="separator"></span></li>
            <li>Application &amp; Setting <span class="separator"></span></li>
            <li>Province</li>
        </ul>
        
        <div class="pageheader">
            <div class="pageicon"><span class="iconfa-user"></span></div>
            <div class="pagetitle">
                <h1>Master Province</h1>
            </div>
        </div><!--pageheader-->
        
        <div class="maincontent">
            <div class="maincontentinner">
            	<div class="row">
                    <div id="dashboard-left" class="col-md-8">
                        <div class="widgetbox">
		                	<h4 class="widgettitle">Master Data Province</h4>
			                <table class="table table-bordered table-infinite" id="dyntable2">
			                    <colgroup>
			                        <col class="con0"/>
			                        <col class="con1"/>
			                        <col class="con0"/>
			                    </colgroup>
			                    <thead>
			                        <tr>
			                            <th class="head0">Code</th>
			                            <th class="head1">Name</th>
			                            <th class="head0">Number of Hospital</th>
			                        </tr>
			                    </thead>
			                    <tbody>
			                        <tr>
			                            <td>001</td>
			                            <td>Nangroe Aceh Darusalam</td>
			                            <td>10</td>
			                        </tr>
			                        <tr>
			                            <td>003</td>
			                            <td>Sumatra Utara</td>
			                            <td>10</td>
			                        </tr>
			                        <tr>
			                            <td>004</td>
			                            <td>Kalimantan Barat</td>
			                            <td>10</td>
			                        </tr>
			                        <tr>
			                            <td>005</td>
			                            <td>Sulawesi Selatan</td>
			                            <td>10</td>
			                        </tr>
			                        <tr>
			                            <td>006</td>
			                            <td>DKI Jakarta</td>
			                            <td>10</td>
			                        </tr>
			                        <tr>
			                            <td>007</td>
			                            <td>Jawa Barat</td>
			                            <td>10</td>
			                        </tr>
			                        <tr>
			                            <td>008</td>
			                            <td>Banten</td>
			                            <td>10</td>
			                        </tr>
			                        <tr>
			                            <td>009</td>
			                            <td>DI Yogyakarta</td>
			                            <td>10</td>
			                        </tr>
			                        <tr>
			                            <td>010</td>
			                            <td>Bali</td>
			                            <td>10</td>
			                        </tr>
			                    </tbody>
			                </table>
			            </div>
                        <br />
                    </div><!--col-md-8-->
                    
                    <div id="dashboard-right" class="col-md-4">
                        
                        <h5 class="subtitle">Announcements</h5>
                        
                        <div class="divider15"></div>
                        
                        <div class="alert alert-block">
                              <button data-dismiss="alert" class="close" type="button">&times;</button>
                              <h4>Warning!</h4>
                              <p style="margin: 8px 0">Best check yo self, you're not looking too good. Nulla vitae elit libero, a pharetra augue. Praesent commodo cursus magna.</p>
                        </div><!--alert-->
                        
                        <br />
                        
                        <h5 class="subtitle">Summaries</h5>
                            
                        <div class="divider15"></div>
                        
                        <div class="widgetbox">                        
                        <div class="headtitle">
                            <div class="btn-group">
                                <button data-toggle="dropdown" class="btn dropdown-toggle">Action <span class="caret"></span></button>
                                <ul class="dropdown-menu">
                                  <li><a href="#">Action</a></li>
                                  <li><a href="#">Another action</a></li>
                                  <li><a href="#">Something else here</a></li>
                                  <li class="divider"></li>
                                  <li><a href="#">Separated link</a></li>
                                </ul>
                            </div>
                            <h4 class="widgettitle">Widget Box</h4>
                        </div>
                        <div class="widgetcontent">
                            Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.
                        </div><!--widgetcontent-->
                        </div><!--widgetbox-->
                        
                        <br />
                                                
                    </div><!-- col-md-4 -->
                </div><!--row-->
                
                <div class="footer">
                    <div class="footer-left">
                        <span><spring:message code="label.copyright"/></span>
                    </div>
                </div><!--footer-->
                
            </div><!--maincontentinner-->
        </div><!--maincontent-->
        
    </div><!--rightpanel-->
    
</div><!--mainwrapper-->
<script type="text/javascript">
    jQuery(document).ready(function() {
        
      // simple chart
		var flash = [[0, 11], [1, 9], [2,12], [3, 8], [4, 7], [5, 3], [6, 1]];
		var html5 = [[0, 5], [1, 4], [2,4], [3, 1], [4, 9], [5, 10], [6, 13]];
      var css3 = [[0, 6], [1, 1], [2,9], [3, 12], [4, 10], [5, 12], [6, 11]];
			
		function showTooltip(x, y, contents) {
			jQuery('<div id="tooltip" class="tooltipflot">' + contents + '</div>').css( {
				position: 'absolute',
				display: 'none',
				top: y + 5,
				left: x + 5
			}).appendTo("body").fadeIn(200);
		}
	
			
		var plot = jQuery.plot(jQuery("#chartplace"),
			   [ { data: flash, label: "Flash(x)", color: "#6fad04"},
              { data: html5, label: "HTML5(x)", color: "#06c"},
              { data: css3, label: "CSS3", color: "#666"} ], {
				   series: {
					   lines: { show: true, fill: true, fillColor: { colors: [ { opacity: 0.05 }, { opacity: 0.15 } ] } },
					   points: { show: true }
				   },
				   legend: { position: 'nw'},
				   grid: { hoverable: true, clickable: true, borderColor: '#666', borderWidth: 2, labelMargin: 10 },
				   yaxis: { min: 0, max: 15 }
				 });
		
		var previousPoint = null;
		jQuery("#chartplace").bind("plothover", function (event, pos, item) {
			jQuery("#x").text(pos.x.toFixed(2));
			jQuery("#y").text(pos.y.toFixed(2));
			
			if(item) {
				if (previousPoint != item.dataIndex) {
					previousPoint = item.dataIndex;
						
					jQuery("#tooltip").remove();
					var x = item.datapoint[0].toFixed(2),
					y = item.datapoint[1].toFixed(2);
						
					showTooltip(item.pageX, item.pageY,
									item.series.label + " of " + x + " = " + y);
				}
			
			} else {
			   jQuery("#tooltip").remove();
			   previousPoint = null;            
			}
		
		});
		
		jQuery("#chartplace").bind("plotclick", function (event, pos, item) {
			if (item) {
				jQuery("#clickdata").text("You clicked point " + item.dataIndex + " in " + item.series.label + ".");
				plot.highlight(item.series, item.datapoint);
			}
		});
    
        
        //datepicker
        jQuery('#datepicker').datepicker();
        
        // tabbed widget
        jQuery('.tabbedwidget').tabs();
        
        
    
    });
</script>

<%@ include file="/_footer.jsp"%>
