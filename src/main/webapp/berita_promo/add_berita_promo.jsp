<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>

    <style>
        .btn-saving {
            float: left;
        }

        .btn-saving ~ img {
            padding-left: 10px;
            display: inline !important;
        }

        #btn-save {
            width: 100px;
        }

        #btn-save ~ img {
            display: none;
        }

    </style>

    <script>
        $(document)
            .ready(
            function() {
                $('#form_add_berita_promo').validate(
                    {
                        rules : {
                        	
                        },
                        messages : {

                        },
                        highlight : function(label) {

                        },
                        success : function(label) {
                            label.text("OK!").addClass(
                                'valid').closest(
                                '.control-group')
                                .addClass('success');
                        },
                        submitHandler : function(form) {
                            var formData = new FormData(form);



                            $.ajax({
                                url:$(form).attr('action'),
                                type:$(form).attr('method'),
                                data: formData,
                                contentType: false,
                                processData: false
                            }).done(function(data){
								
                                if(data.status == 1)
                                {
                                    jQuery.jGrowl(data.message, { life: 3000});
                                    $("#main_content").load('<c:url value="/berita_promo/list_berita_promo.page" />');
                                }
                                else
                                {
                                    jQuery.jGrowl(data.message, { life: 3000});

                                }

                            }).fail(function(){
                                var msg = "Failed";
                                jQuery.jGrowl(msg, { life: 3000});
                            });

                            return false;
                        }
                    });

            });
    </script>
    <!-- BEGIN CONTENT -->


    <!-- BEGIN PAGE BAR -->
<div class="page-bar">
	<ul class="page-breadcrumb">
		<li><a href="<c:url value="home.page"/>">Home</a> <i
			class="fa fa-angle-right"></i></li>
		<li><span>Berita &amp; Promo</span></li>
	</ul>
	<div class="page-toolbar">
		<div class="btn-group pull-right">
			<a class="btn green" href="#"> <i class="fa fa-angle-left"></i>
				Back
			</a>

		</div>
	</div>
</div>
<!-- END PAGE BAR -->
<!-- BEGIN PAGE TITLE-->
<h3 class="page-title">Tambah Artikel</h3>
<!-- END PAGE TITLE-->
<!-- END PAGE HEADER-->
<div class="row">
	<div class="col-md-12 ">
		<!-- BEGIN SAMPLE FORM PORTLET-->
		<div class="portlet light bordered">
			<div class="portlet-title">
				<div class="caption font-red-sunglo">
					<i class="icon-settings font-red-sunglo"></i> <span
						class="caption-subject bold uppercase"> Form Tambah Artikel</span>
				</div>
			</div>
			<div class="portlet-body form">
				<form id="form_add_berita_promo" class="stdform stdform2"
					method="post"
					action="<c:url value="/berita_promo/saveBeritaPromo.json" />"
					enctype="multipart/form-data">

					<div class="form-body">
						<div class="form-group form-md-line-input ">
							<input type="text" id="title" name="title" class="form-control">
							<label for="">Judul/Title</label> <span class="help-block">Judul
								Artikel</span>
						</div>
						<div class="form-group form-md-line-input has-info">

							<select name="contentType" id="contentType" class="form-control" onchange="hidePotonganHarga()">
								<option value="0" selected>Berita</option>
								<option value="1">Promo</option>
							</select> <label for="form_control_1">Kategori</label>
						</div>
						<div class="form-group form-md-line-input ">
							<input type="text" id="dateTimeStart" name="dateTimeStart"
								class="form-control date-picker"><label for="">Tanggal
								Mulai</label> <span class="help-block">Tanggal Mulai Artikel</span>
						</div>
						<div class="form-group form-md-line-input ">
							<input type="text" id="dateTimeEnd" name="dateTimeEnd"
								class="form-control date-picker"> <label for="">Tanggal
								Berakhir</label> <span class="help-block">Tanggal Berakhir
								Artikel</span>
						</div>
						<div class="form-group form-md-line-input ">
							<label for="">Konten Artikel</label>
							<textarea name="content" id="content"></textarea>
						</div>
						<div class="form-group form-md-line-input ">
							<input type="hidden" id="potonganHarga" name="potonganHarga"
								class="form-control"> <label for="" id="labelPotonganHarga">Potongan
								Harga</label> <span class="help-block">Potongan Harga</span>
						</div>
						<div class="form-group form-md-line-input has-info">
							<select name="status" id="status" class="form-control">
								<option value="0" selected>Draft</option>
								<option value="1">Publish</option>
							</select> <label for="form_control_1">Status</label>
						</div>
						<div class="form-group">
							<label for="exampleInputFile1">Gambar</label> <input type="file"
								id="image" name="image">
							<p class="help-block">Upload gambar</p>
						</div>

						<div class="form-actions noborder">
							<button type="submit" class="btn blue">Submit</button>
							<button type="button" class="btn default">Cancel</button>
						</div>
					</div>
				</form>
			</div>
		</div>
		<!-- END SAMPLE FORM PORTLET-->

	</div>
</div>

<script>
		function hidePotonganHarga() 
		{
			if($('#contentType').val() == '1')
			{
				document.getElementById('potonganHarga').type = 'text';
				document.getElementById('labelPotonganHarga').style.display = 'inline';
			}
			else if($('#contentType').val() == '0')
			{
				document.getElementById('potonganHarga').type = 'hidden';
				document.getElementById('labelPotonganHarga').style.display = 'none';
			}
		}
		
		function convertDate()
		{	var today = new Date();
			var timeStart = $('#dateTimeStart').val() || '';
			console.log('masuk fungsi');
			console.log('timeStart:'+timeStart);
			
			
			if(timeStart != '')
			{
				console.log('masuk kondisi');
				var startDate = $('#dateTimeStart').val().trim().replace(/\W/g,',');
				var convertStartDate = new Date(startDate);
				var millisecondsStartDate = convertStartDate.getTime();
				var millisecondsToday = today.getTime();
				var milliseconds = millisecondsStartDate - millisecondsToday;
				var seconds = milliseconds / 1000;
				var days = Math.floor(seconds / 86400) + 2;
				var plus = '+';
				var d = 'd';
				var stringdays = days.toString();
				var combineplus = plus.concat(stringdays);
				var combine = combineplus.concat(d);
				
				console.log("timeEnd String:"+combine);
				$('#dateTimeEnd').datepicker({startDate : combine , autoclose:true});
			}
		}
		
		
        $(document).ready(function() {
			hidePotonganHarga();			
            //$('#timeStart').datepicker({format : 'yyyy-mm-dd',autoclose:true, startDate: '0d'});
            //$('#timeEnd').datepicker({format : 'yyyy-mm-dd',autoclose:true, startDate: '0d'});
			
			$('#dateTimeStart')
			.datepicker({
				format: 'yyyy-mm-dd',
				startDate: '0d',
				autoclose:true
			}).on('changeDate', function(e) {
            // Revalidate the date field
				var today = new Date();
				var timeStart = $('#dateTimeStart').val() || '';
				console.log('masuk fungsi');
				console.log('timeStart:'+timeStart);
				
				
				if(timeStart != '')
				{
					console.log('masuk kondisi');
					var startDate = $('#dateTimeStart').val().trim().replace(/\W/g,',');
					var convertStartDate = new Date(startDate);
					var millisecondsStartDate = convertStartDate.getTime();
					var millisecondsToday = today.getTime();
					var milliseconds = millisecondsStartDate - millisecondsToday;
					var seconds = milliseconds / 1000;
					var days = Math.floor(seconds / 86400) + 2;
					var plus = '+';
					var d = 'd';
					var stringdays = days.toString();
					var combineplus = plus.concat(stringdays);
					var combine = combineplus.concat(d);
					
					console.log("timeEnd String:"+combine);
					
					$('#dateTimeEnd').datepicker('setStartDate', combine);
					//$('#timeEnd').datepicker({startDate : combine , autoclose:true});
				}
			});

			$('#dateTimeEnd')
			.datepicker({
				format: 'yyyy-mm-dd',
				startDate: '+1d',
				autoclose:true
			});
			
	
			
            $('#content').summernote({height: 300});
        });
		
</script>

