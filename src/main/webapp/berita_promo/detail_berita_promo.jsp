<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>


    <style>
        .btn-saving {
            float: left;
        }

        .btn-saving ~ img {
            padding-left: 10px;
            display: inline !important;
        }

        #btn-save {
            width: 100px;
        }

        #btn-save ~ img {
            display: none;
        }
		
		#colAction{
			width: 200px;
		}

    </style>

    <script>
    	 var oTable = $('#dyntable').dataTable({
                "oLanguage": {
                    "sProcessing": 'Loading Table...'
                },
                "sPaginationType" : "full_numbers",
                "bLengthChange": false,
                "bFilter": false,
                "bProcessing": true,
                "bServerSide": true,
                "bAutoWidth": true,
				"autoWidth": true,
                "aoColumns" : [
                    { sWidth: '20%' },
                    { sWidth: '20%' },
                    { sWidth: '20%' },
                    { sWidth: '20%' },
                    { sWidth: '20%' }
                ],  
                "sAjaxSource": '<c:url value="/berita_promo/promo_berlaku/data-table.json" />',
                "aoColumns": [
                    { "mData" : 'idPromoBerlaku',
                     "mRender": function (sourceData, dataType, fullData) {
	                         return	 '<a class="btn btn-icon-only tooltips  red action-menu-delete-promo-berlaku" href="javascript:;" data-id="'+fullData.idPromoBerlaku+'">'
	                             +'<i class="fa fa-times"></i><span class="tooltiptext">Delete</span></a>'						 
							
                    
                     }
                    },
                    { "mData" : 'nameRuteFrom'},
					{ "mData" : 'nameRuteTo'},

                ]
            });
			
	function getListRuteTo()
	{
			$.get("<c:url value="/data-master/get-information-to-destination.json" />"+"?id_from="+$('#idRuteAwal').val(), function( data ) {
				
				
				$('#idRuteTujuan').find('option').remove().end();
				
				for(var counter=0;counter<data.obj.length;counter++)
				{
					$('#idRuteTujuan').append($("<option></option>")
					.attr("value",data.obj[counter].id)
					.text(data.obj[counter].value)); 
					
					if(counter == 0)$('#idRuteTujuan').val(data.obj[counter].id);
				}
				getRuteToDetail();
				
			});
			
	}
	
	function getRuteToDetail()
	{
			
			$.get("<c:url value="/data-master/get-destination-detail.json" />"+"?id="+$('#idRuteTujuan').val(), function( data ) {
				priceUnitDestination = data.obj.priceTicketUnit;
				$('#harga_unit_ref').text(priceUnitDestination);
				changePrice();
			});
	}

        $(document)
            .ready(
            function() {
			
				getListRuteTo();
				$('body').off('click', '.action-menu-delete-promo-berlaku');
                $('#form_update_berita_promo').validate(
                    {
                        rules : {

                        },
                        messages : {

                        },
                        highlight : function(label) {

                        },
                        success : function(label) {
                            label.text("OK!").addClass(
                                'valid').closest(
                                '.control-group')
                                .addClass('success');
                        },
                        submitHandler : function(form) {
                            var formData = new FormData(form);



                            $.ajax({
                                url:$(form).attr('action'),
                                type:$(form).attr('method'),
                                data: formData,
                                contentType: false,
                                processData: false
                            }).done(function(data){

                                if(data.status == 1)
                                {
                                    jQuery.jGrowl(data.message, { life: 3000});
                                    $("#main_content").load('<c:url value="/berita_promo/list_berita_promo.page" />');
                                }
                                else
                                {
                                    jQuery.jGrowl(data.message, { life: 3000});

                                }

                            }).fail(function(){
                                var msg = "Failed";
                                jQuery.jGrowl(msg, { life: 3000});
                            });

                            return false;
                        }
                    });
					
					$('#form_add_promo_berlaku').validate(
                    {
                        rules : {

                        },
                        messages : {

                        },
                        highlight : function(label) {

                        },
                        success : function(label) {
                            label.text("OK!").addClass(
                                'valid').closest(
                                '.control-group')
                                .addClass('success');
                        },
                        submitHandler : function(form) {
                            var formData = new FormData(form);



                            $.ajax({
                                url:$(form).attr('action'),
                                type:$(form).attr('method'),
                                data: formData,
                                contentType: false,
                                processData: false
                            }).done(function(data){

                                if(data.status == 1)
                                {
									console.log("Masuk data.status == 1");
                                    jQuery.jGrowl(data.message, { life: 3000});
									$("#main_content").load('<c:url value="/berita_promo/detail_berita_promo.page/${beritaPromoDetails.idBeritaPromo}" />');
									$("div").remove(".modal-backdrop.fade.in");
								}
                                else
                                {
                                    jQuery.jGrowl(data.message, { life: 3000});

                                }

                            }).fail(function(){
                                var msg = "Failed";
                                jQuery.jGrowl(msg, { life: 3000});
                            });

                            return false;
                        }
                    });
					
					$('body').on('click', '.action-menu-delete-promo-berlaku', function () {
					if (!confirm("Apakah anda yakin menghapus data ini ?")){
						return false;
					}
					else
					{
						$.ajax({
							url:'<c:url value="/berita_promo/deletePromoBerlaku.json" />',
							type:'post',
							data: {
								'id_long':$(this).data('id')
							}
						}).done(function(data){
							
							if(data.status == 1)
							{
								jQuery.jGrowl(data.message, { life: 3000});
								
								$('#dyntable').dataTable()._fnAjaxUpdate();
							}
							else
							{
								jQuery.jGrowl(data.message, { life: 3000});
							}
							
						}).fail(function(){
							var msg = "Failed";
							jQuery.jGrowl(msg, { life: 3000});
						});
					}
			});

            });
    </script>
    <!-- BEGIN CONTENT -->


    <!-- BEGIN PAGE BAR -->
    <div class="page-bar">
        <ul class="page-breadcrumb">
            <li>
                <a href="<c:url value="home.page"/>">Home</a>
                <i class="fa fa-angle-right"></i>
            </li>
            <li>
                <span>Berita &amp; Promo</span>
            </li>
        </ul>
        <div class="page-toolbar">
            <div class="btn-group pull-right">
                <a class="btn green" href="#"> <i class="fa fa-angle-left"></i> Back
                </a>

            </div>
        </div>
    </div>
    <!-- END PAGE BAR -->
    <!-- BEGIN PAGE TITLE-->
    <h3 class="page-title"> Detail Promo
    </h3>
	
    <!-- END PAGE TITLE-->
    <!-- END PAGE HEADER-->
    <div class="row">
        <div class="col-md-12 ">
            <!-- BEGIN SAMPLE FORM PORTLET-->
            <div class="portlet light bordered">
                <div class="portlet-title">
                    <div class="caption font-red-sunglo">
                        <i class="icon-settings font-red-sunglo"></i>
                        <span class="caption-subject bold uppercase"> Detail Promo </span>
                    </div>

                </div>
                <div class="portlet-body form">
                    <form id="form_update_berita_promo" class="stdform stdform2" 
                          method="post" action="<c:url value="/berita_promo/updateBeritaPromo.json" />" 
                          enctype="multipart/form-data">
                        <div class="form-body">
							<input type="text" value="${beritaPromoDetails.idBeritaPromo}"
							id="idBeritaPromo" name="idBeritaPromo" style="display: none"
							class="form-control">
							
                            <div class="form-group form-md-line-input ">
                                <input type="text" style="display: none" id="title" name="title" class="form-control" value="${beritaPromoDetails.title}">
                                <span>${beritaPromoDetails.title}</span>
								<label for="">Nama Promo</label>
                                <span class="help-block">Nama Promo</span>
                            </div>
							
							<div class="form-group form-md-line-input ">
                                <label for="">Isi Promo</label>
								${beritaPromoDetails.content}
								<input type="text" name="content" style="display: none" id="content" value="${beritaPromoDetails.content}">
                            </div>
                            
                            <div class="form-group form-md-line-input ">
                                <input type="text" style="display: none" id="dateTimeStart" name="dateTimeStart" 
											value=" <fmt:formatDate pattern="YYYY-MM-dd" value="${beritaPromoDetails.timeStart}" />"
                                                   class="form-control  date-picker ">
								<span id="timeStart"></span>
                                <label for="">Periode Awal Promo</label>
                                <span class="help-block">Periode Awal Promo</span>
                            </div>
					
                            <div class="form-group form-md-line-input ">
                               <input type="text" style="display: none" id="dateTimeEnd" name="dateTimeEnd" 
											value=" <fmt:formatDate pattern="YYYY-MM-dd" value="${beritaPromoDetails.timeEnd}" />"
                                                   class="form-control  date-picker ">
                                <span id="timeEnd"></span>
								<label for="">Periode Akhir Promo</label>
                                <span class="help-block">Periode Akhir Promo</span>
                            </div>
                             
							<div class="form-group form-md-line-input ">
                                <input type="text" style="display: none" id="potonganHarga" name="potonganHarga" class="form-control" value="${beritaPromoDetails.potonganHarga}">
                                <span>${beritaPromoDetails.potonganHarga}</span>
								<label for="" id="labelPotonganHarga">Potongan Harga</label>
                                <span class="help-block" id="labelPotonganHarga">Potongan Harga</span>
                            </div>
							<div class="form-group form-md-line-input">
								<label for="exampleInputFile1">Gambar</label> 
								<p></p>
								<img src="${beritaPromoDetails.pathImageUrl}" onerror="this.src='<c:url value="/images/ibus.png" />'" width="30%">	
							</div>
							
                            <div class="form-group form-md-line-input has-info">
                                <select name="statusPilihanPromo" id="statusPilihanPromo" class="form-control" onchange="hideTable()">
                                    <option value="0" <c:if test="${beritaPromoDetails.statusPilihanPromo eq '0' }">selected="selected"</c:if>>Semua Jurusan</option>
                                    <option value="1" <c:if test="${beritaPromoDetails.statusPilihanPromo eq '1' }">selected="selected"</c:if>>Pilihan Jurusan</option>
                                </select>
                                <label for="form_control_1">Promo ini berlaku</label>
                            </div>
                            
                            <div class="form-group form-md-line-input has-info">
                                <div id="table_ref_">
									<label id="labelPilihanJurusan" for="form_control_1">Pilihan Jurusan</label>
									<p> </p>
									<button id="buttonPilihanJurusan" type="button" class="btn blue" data-toggle="modal" data-target="#myModal">Tambah Pilihan Jurusan</button>
									<table class="table table-striped table-bordered table-hover table-checkable order-column" id="dyntable">
										<thead style="width:100%">
											<tr>
												<th id="colAction"> Action </th>
												<th> Rute Awal </th>
												<th> Rute Tujuan </th>							
											</tr>
										</thead>
									</table>
								</div>
                            </div>
                          </div>
                        <div class="form-actions noborder">
                            <button type="submit" class="btn blue">Submit</button>
                            <button type="button" class="btn default">Cancel</button>
                        </div>
						<div class="form-group form-md-line-input ">
                               <input type="text" style="display: none" id="contentType" name="contentType" value="${beritaPromoDetails.contentType}">
						</div>
						<div class="form-group form-md-line-input ">
						   <input type="text" style="display: none" id="status" name="status" value="${beritaPromoDetails.status}">
						</div>
						<div class="form-group">
							<input style="display: none" type="file"
								id="image" name="image">				
						</div>
                    </form>
                    
							
							<!-- Modal -->
						<div class="modal fade" id="myModal">
								<div class="modal-dialog">
								
								  <!-- Modal content-->
								  <div class="modal-content">
									<div class="modal-header">
									  <button type="button" class="close" data-dismiss="modal">&times;</button>
									  <h4 class="modal-title">Tambah Rute</h4>
									</div>
									<div class="modal-body">
										<!-- BEGIN SAMPLE FORM PORTLET-->
										<div class="portlet light bordered">
											<div class="portlet-title">
												<div class="caption font-red-sunglo">
													<i class="icon-settings font-red-sunglo"></i>
													<span class="caption-subject bold uppercase"> Form Tambah Rute</span>
												</div>

											</div>
											<div class="portlet-body form">
												<form id="form_add_promo_berlaku" class="stdform stdform2" 
												method="post" action="<c:url value="/berita_promo/savePromoBerlaku.json" />" 
												enctype="multipart/form-data">
													
													<div class="form-body">
													<input type="text" 
														value="${beritaPromoDetails.idBeritaPromo}"
														id="idBeritaPromo" name="idBeritaPromo" style="display:none" 
														class="form-control">
														
													<input type="text" 
														value="" style="display:none" id="statusPilihanPromoUpdate" name="statusPilihanPromoUpdate" 
														class="form-control">
														
														<div class="form-group form-md-line-input has-info">

																<select name="idRuteAwal" id="idRuteAwal" class="form-control" onchange="getListRuteTo()">
																	<c:forEach var="ruteFrom" items="${list_rute_from}" varStatus="ruteFromStatus">
																		<option value="${ruteFrom.idRuteFrom}" <c:if test="${ruteFromStatus.index eq 0 }">selected="selected"</c:if>>${ruteFrom.nameRuteFrom}</option>
																	</c:forEach>
																</select>
																<label for="form_control_1">Rute Awal</label>
														</div>
														
														<div class="form-group form-md-line-input has-info">

																<select name="idRuteTujuan" id="idRuteTujuan" class="form-control">
																	
																</select>
																<label for="form_control_1">Rute Tujuan</label>
														</div>
															

													</div>
													
													<div class="form-actions noborder">
														<button type="submit" class="btn blue">Submit</button>
													</div>
													
												</form>
											</div>
										</div>
										
											<!-- END SAMPLE FORM PORTLET-->
									<div class="modal-footer">
										<div class="form-actions noborder">
											<button type="button" data-dismiss="modal" class="btn default">Cancel</button>
										</div>
									</div>
									</div>
								  
								</div>
							  </div>
                            
                        </div>
                </div>
            </div>
            <!-- END SAMPLE FORM PORTLET-->

        </div>
    </div>

    <script>

		function hideTable()
		{
			if($('#statusPilihanPromo').val() == '0')
			{
				$('#table_ref_').hide();
			}
			else if($('#statusPilihanPromo').val() == '1')
			{
				
				$('#table_ref_').show();
				$('#statusPilihanPromoUpdate').val('1');
			}
		}
		
		function displayStatusPilihanPromo()
		{
			if(statusPilihanPromo == "1"){
				$('#statusPilihanPromo').append( '<option value="1" selected>Pilihan Jurusan</option>' );
				console.log($('#statusPilihanPromo').val());
			}
			else if(statusPilihanPromo == "0"){
				$('#statusPilihanPromo').append( '<option value="0" selected>Semua Jurusan</option>' );
				console.log($('#statusPilihanPromo').val());
			}
		}
		
        $(document).ready(function() {
			var startDate = new Date($('#dateTimeStart').val());
			var startYear = startDate.getFullYear();
			var startMonth = (1 + startDate.getMonth()).toString();
			startMonth = startMonth.length > 1 ? startMonth : '0' + startMonth;
			var startDay = startDate.getDate().toString();
			startDay = startDay.length > 1 ? startDay : '0' + startDay;
			var dateStart = startYear + '-' + startMonth + '-' + startDay;
			
			var endDate = new Date($('#dateTimeEnd').val());
			var endYear = endDate.getFullYear();
			var endMonth = (1 + endDate.getMonth()).toString();
			endMonth = endMonth.length > 1 ? endMonth : '0' + endMonth;
			var endDay = endDate.getDate().toString();
			endDay = endDay.length > 1 ? endDay : '0' + endDay;
			var dateEnd = endYear + '-' + endMonth + '-' + endDay;
			
			document.getElementById("timeStart").innerHTML = dateStart;
			document.getElementById("timeEnd").innerHTML = dateEnd;
			
			displayStatusPilihanPromo();
			hideTable();			
            //$('#timeStart').datepicker({format : 'yyyy-mm-dd',autoclose:true, startDate: '0d'});
            //$('#timeEnd').datepicker({format : 'yyyy-mm-dd',autoclose:true, startDate: '0d'});
			
			$('#dateTimeStart')
			.datepicker({
				format: 'yyyy-mm-dd',
				startDate: '0d',
				autoclose:true
			}).on('changeDate', function(e) {
            // Revalidate the date field
				var today = new Date();
				var timeStart = $('#dateTimeStart').val() || '';
				console.log('masuk fungsi');
				console.log('timeStart:'+timeStart);
				
				
				if(timeStart != '')
				{
					console.log('masuk kondisi');
					var startDate = $('#dateTimeStart').val().trim().replace(/\W/g,',');
					var convertStartDate = new Date(startDate);
					var millisecondsStartDate = convertStartDate.getTime();
					var millisecondsToday = today.getTime();
					var milliseconds = millisecondsStartDate - millisecondsToday;
					var seconds = milliseconds / 1000;
					var days = Math.floor(seconds / 86400) + 2;
					var plus = '+';
					var d = 'd';
					var stringdays = days.toString();
					var combineplus = plus.concat(stringdays);
					var combine = combineplus.concat(d);
					
					console.log("timeEnd String:"+combine);
					
					$('#dateTimeEnd').datepicker('setStartDate', combine);
					//$('#timeEnd').datepicker({startDate : combine , autoclose:true});
				}
			});

			$('#dateTimeEnd')
			.datepicker({
				format: 'yyyy-mm-dd',
				startDate: '+1d',
				autoclose:true
			});

        });
		
		
    </script>


