<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>


    <style>
        .btn-saving {
            float: left;
        }

        .btn-saving ~ img {
            padding-left: 10px;
            display: inline !important;
        }

        #btn-save {
            width: 100px;
        }

        #btn-save ~ img {
            display: none;
        }

    </style>

    <script>
    	
        $(document)
            .ready(
            function() {
                $('#form_update_berita_promo').validate(
                    {
                        rules : {

                        },
                        messages : {

                        },
                        highlight : function(label) {

                        },
                        success : function(label) {
                            label.text("OK!").addClass(
                                'valid').closest(
                                '.control-group')
                                .addClass('success');
                        },
                        submitHandler : function(form) {
                            var formData = new FormData(form);



                            $.ajax({
                                url:$(form).attr('action'),
                                type:$(form).attr('method'),
                                data: formData,
                                contentType: false,
                                processData: false
                            }).done(function(data){

                                if(data.status == 1)
                                {
                                    jQuery.jGrowl(data.message, { life: 3000});
                                    $("#main_content").load('<c:url value="/berita_promo/list_berita_promo.page" />');
                                }
                                else
                                {
                                    jQuery.jGrowl(data.message, { life: 3000});

                                }

                            }).fail(function(){
                                var msg = "Failed";
                                jQuery.jGrowl(msg, { life: 3000});
                            });

                            return false;
                        }
                    });

            });
    </script>
    <!-- BEGIN CONTENT -->


    <!-- BEGIN PAGE BAR -->
    <div class="page-bar">
        <ul class="page-breadcrumb">
            <li>
                <a href="<c:url value="home.page"/>">Home</a>
                <i class="fa fa-angle-right"></i>
            </li>
            <li>
                <span>Berita &amp; Promo</span>
            </li>
        </ul>
        <div class="page-toolbar">
            <div class="btn-group pull-right">
                <a class="btn green" href="#"> <i class="fa fa-angle-left"></i> Back
                </a>

            </div>
        </div>
    </div>
    <!-- END PAGE BAR -->
    <!-- BEGIN PAGE TITLE-->
    <h3 class="page-title"> Update Artikel
    </h3>
	
    <!-- END PAGE TITLE-->
    <!-- END PAGE HEADER-->
    <div class="row">
        <div class="col-md-12 ">
            <!-- BEGIN SAMPLE FORM PORTLET-->
            <div class="portlet light bordered">
                <div class="portlet-title">
                    <div class="caption font-red-sunglo">
                        <i class="icon-settings font-red-sunglo"></i>
                        <span class="caption-subject bold uppercase"> Form Update Artikel</span>
                    </div>

                </div>
                <div class="portlet-body form">
                    <form id="form_update_berita_promo" class="stdform stdform2" 
                          method="post" action="<c:url value="/berita_promo/updateBeritaPromo.json" />" 
                          enctype="multipart/form-data">
                        <div class="form-body">
							<input type="text" value="${beritaPromoDetails.idBeritaPromo}"
							id="idBeritaPromo" name="idBeritaPromo" style="display: none"
							class="form-control">
							
                            <div class="form-group form-md-line-input ">
                                <input type="text" id="title" name="title" class="form-control" value="${beritaPromoDetails.title}">
                                <label for="">Judul/Title</label>
                                <span class="help-block">Judul Artikel</span>
                            </div>
                            
                            <div class="form-group form-md-line-input has-info">
                                <select name="contentType" id="contentType" class="form-control" onchange="hidePotonganHarga()">
                                    <option value="${beritaPromoDetails.contentType}" id="contentTypeOption" selected="selected"></option>
                                </select>
                                <label for="form_control_1">Kategori</label>
                            </div>
                            
                            <div class="form-group form-md-line-input ">
                                <input style="display: none" type="text" id="dateTimeStart" name="dateTimeStart" 
											value=" <fmt:formatDate pattern="YYYY-MM-dd" value="${beritaPromoDetails.timeStart}" />"
                                                   class="form-control  date-picker ">
								<span id="timeStart"></span>
                                <label for="">Tanggal Mulai</label>
                                <span class="help-block">Tanggal Mulai Artikel</span>
                            </div>
                            <div class="form-group form-md-line-input ">
                               <input type="text" style="display: none" id="dateTimeEnd" name="dateTimeEnd" 
											value=" <fmt:formatDate pattern="YYYY-MM-dd" value="${beritaPromoDetails.timeEnd}" />"
                                                   class="form-control  date-picker ">
                                <span id="timeEnd"></span>
								<label for="">Tanggal Berakhir</label>
                                <span class="help-block">Tanggal Berakhir Artikel</span>
                            </div>
                             <div class="form-group form-md-line-input ">
                                <label for="">Konten Artikel</label>
                                <textarea name="content" id="content">${beritaPromoDetails.content}</textarea>
                            </div>
							<div class="form-group form-md-line-input ">
                                <input type="text" id="potonganHarga" name="potonganHarga" class="form-control" value="${beritaPromoDetails.potonganHarga}">
                                <label for="" id="labelPotonganHarga">Potongan Harga</label>
                                <span class="help-block" id="labelPotonganHarga">Potongan Harga</span>
                            </div>
                            <div class="form-group form-md-line-input has-info">
                                <select name="status" id="status" class="form-control">
                                    <option value="0" <c:if test="${beritaPromoDetails.status eq '0' }">selected="selected"</c:if>>Draft</option>
                                    <option value="1" <c:if test="${beritaPromoDetails.status eq '1' }">selected="selected"</c:if>>Publish</option>
                                </select>
                                <label for="form_control_1">Status</label>
                            </div>
                            <div class="form-group form-md-line-input has-info" id="status_ref">
                                <select name="statusPilihanPromo" id="statusPilihanPromo" class="form-control">
                                    <option value="${beritaPromoDetails.statusPilihanPromo}" id="statusPilihanPromoOption" selected="selected"></option>
                                </select>
                                <label for="form_control_1">Status Pilihan Promo</label>
                            </div>
							<img src="${beritaPromoDetails.pathImageUrl}" onerror="this.src='<c:url value="/images/ibus.png" />'" width="30%">
                            <p> </p>
							<br></br>
							<input type="hidden" id="imageOld" name="image" value="${beritaPromoDetails.imageUrl}">
                            
							<div class="form-group">
                                <label for="exampleInputFile1">Upload Gambar Baru</label>
                                <input type="file" id="imageNew" name="image">
                                <p class="help-block"> Upload gambar baru </p>
                            </div>
                        </div>
                        <div class="form-actions noborder">
                            <button type="submit" class="btn blue">Submit</button>
                            <button type="button" class="btn default">Cancel</button>
                        </div>
                    </form>
                </div>
            </div>
            <!-- END SAMPLE FORM PORTLET-->

        </div>
    </div>

    <script>
		function hidePotonganHarga() 
		{
			if($('#contentType').val() == '1')
			{
				document.getElementById('potonganHarga').type = 'text';
				document.getElementById('labelPotonganHarga').style.display = 'inline';
			}
			else if($('#contentType').val() == '0')
			{
				document.getElementById('potonganHarga').type = 'hidden';
				document.getElementById('labelPotonganHarga').style.display = 'none';
			}
		}
		
		if($('#contentType').val() == '1')
		{
			document.getElementById("contentTypeOption").innerHTML = "Promo";
		}
		else if($('#contentType').val() == '0')
		{
			document.getElementById("contentTypeOption").innerHTML = "Berita";
		}
		
		if($('#statusPilihanPromo').val() == '1' && $('#contentType').val() == '1')
		{
			
			document.getElementById("statusPilihanPromoOption").innerHTML = "Pilihan Jurusan";
		}
		else if($('#statusPilihanPromo').val() == '0' && $('#contentType').val() == '1')
		{
			
			document.getElementById("statusPilihanPromoOption").innerHTML = "Semua Jurusan";
		}
		else if($('#contentType').val() == '0')
		{
			$('#status_ref').hide();
		}
		
        $(document).ready(function() {
			var startDate = new Date($('#dateTimeStart').val());
			var startYear = startDate.getFullYear();
			var startMonth = (1 + startDate.getMonth()).toString();
			startMonth = startMonth.length > 1 ? startMonth : '0' + startMonth;
			var startDay = startDate.getDate().toString();
			startDay = startDay.length > 1 ? startDay : '0' + startDay;
			var dateStart = startYear + '-' + startMonth + '-' + startDay;
			
			var endDate = new Date($('#dateTimeEnd').val());
			var endYear = endDate.getFullYear();
			var endMonth = (1 + endDate.getMonth()).toString();
			endMonth = endMonth.length > 1 ? endMonth : '0' + endMonth;
			var endDay = endDate.getDate().toString();
			endDay = endDay.length > 1 ? endDay : '0' + endDay;
			var dateEnd = endYear + '-' + endMonth + '-' + endDay;
			
			document.getElementById("timeStart").innerHTML = dateStart;
			document.getElementById("timeEnd").innerHTML = dateEnd;
			
			hidePotonganHarga();			
            //$('#timeStart').datepicker({format : 'yyyy-mm-dd',autoclose:true, startDate: '0d'});
            //$('#timeEnd').datepicker({format : 'yyyy-mm-dd',autoclose:true, startDate: '0d'});
			
			$('#dateTimeStart')
			.datepicker({
				format: 'yyyy-mm-dd',
				startDate: '0d',
				autoclose:true
			}).on('changeDate', function(e) {
            // Revalidate the date field
				var today = new Date();
				var timeStart = $('#dateTimeStart').val() || '';
				console.log('masuk fungsi');
				console.log('timeStart:'+timeStart);
				
				
				if(timeStart != '')
				{
					console.log('masuk kondisi');
					var startDate = $('#dateTimeStart').val().trim().replace(/\W/g,',');
					var convertStartDate = new Date(startDate);
					var millisecondsStartDate = convertStartDate.getTime();
					var millisecondsToday = today.getTime();
					var milliseconds = millisecondsStartDate - millisecondsToday;
					var seconds = milliseconds / 1000;
					var days = Math.floor(seconds / 86400) + 2;
					var plus = '+';
					var d = 'd';
					var stringdays = days.toString();
					var combineplus = plus.concat(stringdays);
					var combine = combineplus.concat(d);
					
					console.log("timeEnd String:"+combine);
					
					$('#dateTimeEnd').datepicker('setStartDate', combine);
					//$('#timeEnd').datepicker({startDate : combine , autoclose:true});
				}
			});

			$('#dateTimeEnd')
			.datepicker({
				format: 'yyyy-mm-dd',
				startDate: '+1d',
				autoclose:true
			});
	
			
            $('#content').summernote({height: 300});
        });
		
		
    </script>


