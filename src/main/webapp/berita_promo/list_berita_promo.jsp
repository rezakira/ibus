<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

    <style>

        .action-menu-table:hover {
            -webkit-transform: scale(1.1);
            -moz-transform:scale(1.1);
            transform: scale(1.1);
        }

    </style>

    <script type="text/javascript">
        $(document).ready(function() {

            $('body').off('click', '.action-menu-view-berita_promo');
            $('body').off('click', '.action-menu-edit-berita_promo');
            $('body').off('click', '.action-menu-delete-berita_promo');


            // dynamic table
            var oTable = $('#dyntable').dataTable({
                "oLanguage": {
                    "sProcessing": 'Loading Table...'
                },
                "sPaginationType" : "full_numbers",
                "bLengthChange": false,
                "bFilter": false,
                "bProcessing": true,
                "bServerSide": true,
                "aoautoWidth": true,
                "aoColumns" : [
                    { sWidth: '20%' },
                    { sWidth: '20%' },
                    { sWidth: '20%' },
                    { sWidth: '20%' },
                    { sWidth: '20%' },
					{ sWidth: '20%' }
                ],  
                "sAjaxSource": '<c:url value="/berita_promo/berita_promo/data-table.json" />',
                "aoColumns": [
                    { "mData" : 'idBeritaPromo',
                     "mRender": function (sourceData, dataType, fullData) {
                    	 if(fullData.contentType == '1'){
	                         return	 '<a class="btn btn-icon-only tooltips  blue action-menu-edit-berita_promo" href="javascript:;" data-id="'+fullData.idBeritaPromo+'">'
	                             +'<i class="fa fa-edit"></i><span class="tooltiptext">Edit</span></a>'
	                             +'<a class="btn btn-icon-only tooltips  red action-menu-delete-berita_promo" href="javascript:;" data-id="'+fullData.idBeritaPromo+'">'
	                             +'<i class="fa fa-times"></i><span class="tooltiptext">Delete</span></a>'						 
								 +'<a class="btn btn-icon-only tooltips purple action-menu-view-berita_promo" href="#" data-id="'+fullData.idBeritaPromo+'">'
								 +'<i class="fa fa-file-text"></i><span class="tooltiptext">Detail</span></a>';
                    	 }
                    	 else if(fullData.contentType == '0')
						 {
							 return	 '<a class="btn btn-icon-only tooltips  blue action-menu-edit-berita_promo" href="javascript:;" data-id="'+fullData.idBeritaPromo+'">'
	                             +'<i class="fa fa-edit"></i><span class="tooltiptext">Edit</span></a>'
	                             +'<a class="btn btn-icon-only tooltips  red action-menu-delete-berita_promo" href="javascript:;" data-id="'+fullData.idBeritaPromo+'">'
	                             +'<i class="fa fa-times"></i><span class="tooltiptext">Delete</span></a>';
						 }

                     }
                    },
                    { "mData": "title"},
					{ "mData": "timeStart" },
					{ "mData": "timeEnd" },
					{ "mData" : "status" ,
						"mRender": function (sourceData, dataType, fullData) {
							if(fullData.status == '0'){
								return	 '<span data-id="'+fullData.status+'">'
								+'Draft</span>'
							}
							else if(fullData.status == '1'){
								return	 '<span data-id="'+fullData.status+'">'
								+'Publish</span>'
							}
						}
					},
					{ "mData" : "contentType" ,
						"mRender": function (sourceData, dataType, fullData) {
							if(fullData.contentType == '0'){
								  return	 '<span data-id="'+fullData.contentType+'">'
								 +'Berita</span>'
							}
							else if(fullData.contentType == '1'){
								return	 '<span data-id="'+fullData.contentType+'">'
								+'Promo</span>'
							}
						}
					},
					{ "mData": "dateModified" }
                ]
            });

            $('body').on('click', '.action-menu-view-berita_promo', function () {
				$("#main_content").load('<c:url value="/berita_promo/detail_berita_promo.page"/>'+'/'+$(this).data('id'));
            });

            $('body').on('click', '.action-menu-edit-berita_promo', function () {
                $("#main_content").load('<c:url value="/berita_promo/update_berita_promo.page" />'+'/'+$(this).data('id'));
            });

            $('body').on('click', '.action-menu-delete-berita_promo', function () {
                if (!confirm("Apakah anda yakin menghapus data ini ?")){
                    return false;
                }
                else
                {
                    $.ajax({
                        url:'<c:url value="/berita_promo/deleteBeritaPromo.json" />',
                        type:'post',
                        data: {
                            'id_long':$(this).data('id')
                        }
                    }).done(function(data){

                        if(data.status == 1)
                        {
                            jQuery.jGrowl(data.message, { life: 3000});

                            $('#dyntable').dataTable()._fnAjaxUpdate();
                        }
                        else
                        {
                            jQuery.jGrowl(data.message, { life: 3000});
                        }

                    }).fail(function(){
                        var msg = "Failed";
                        jQuery.jGrowl(msg, { life: 3000});
                    });
                }
            });


        });
    </script>


    <div class="page-bar">
        <ul class="page-breadcrumb">
            <li>
                <a href="<c:url value="home.page"/>">Home</a>
                <i class="fa fa-angle-right"></i>
            </li>
            <li>
                <a href="#">Berita &amp; Promo</a>
            </li>
        </ul>

    </div>
    <!-- END PAGE BAR -->
    <!-- BEGIN PAGE TITLE-->
    <h3 class="page-title"> Manajemen Berita &amp; Promo
    </h3>
    <!-- END PAGE TITLE-->
    <!-- END PAGE HEADER-->

    <div class="row">
        <div class="col-md-12">
            <!-- BEGIN EXAMPLE TABLE PORTLET-->
            <div class="portlet light bordered">
                <div class="portlet-title">
                    <div class="caption font-dark">
                        <i class="icon-settings font-dark"></i>
                        <span class="caption-subject bold uppercase"> Tabel Artikel </span>

                    </div>

                </div>
                <div class="portlet-body">
                    <div class="table-toolbar">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="btn-group">
                                    <a href="#" data-id="<c:url value="/berita_promo/add_berita_promo.page"/>" onclick="linkToPage(this);return false;"><button id="sample_editable_1_new" class="btn sbold green">Tambah Artikel
                                        <i class="fa fa-plus"></i>
                                        </button></a>
                                </div>
                            </div>
                            <div class="col-md-6">

                            </div>
                        </div>
                    </div>
                    <table class="table table-striped table-bordered table-hover table-checkable order-column" id="dyntable">
                        <thead style="width:100%">
                            <tr>
								<th width="15%"> Action </th>
								<th> Judul </th>
								<th> Tanggal Mulai </th>
								<th> Tanggal berakhir </th>
								<th> Status </th>
								<th> Kategori </th>
								<th> Tanggal Update </th>						
							</tr>
                        </thead>

                    </table>
                </div>
            </div>
            <!-- END EXAMPLE TABLE PORTLET-->
        </div>
    </div>