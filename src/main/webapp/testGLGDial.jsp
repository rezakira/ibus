<html>
<head>
<title>AJAX Mobile Dashboard: GLG Graphics Server Dashboard Demo</title>

<script type="text/javascript">


var update_interval = 3000;
var graph_size_string = "width=500&height=312";
var dial_size_string = "width=150&height=150";
var small_size = false;
var last_x, last_y;
var tooltip_active = false;
var tooltip_object = null;
var dialog_object = null;
var tooltip_timeout_id = 0;
var tooltip_interval = 500;    // msec
var interval_id = 0;
var idle_timeout_id = 0;

var ie  = document.all
var ns6 = document.getElementById&&!document.all

function OnLoad()
{
   tooltip_object = document.getElementById( "tooltip_div" );
   dialog_object = document.getElementById( "dialog_div" );

   ChangeUpdateInterval();    // Start updates with requested interval.
}

function StopUpdates()
{
   if( interval_id )
   {
      clearInterval( interval_id );
      interval_id = 0;
   }

   if( idle_timeout_id )
   {
      clearTimeout( idle_timeout_id );
      idle_timeout_id = 0;
   }

   document.getElementById( "stop_updates" ).disabled = true;   // disable
   document.getElementById( "start_updates" ).disabled = false; // enable
}

function StartUpdates()
{
   StopUpdates();

   UpdateImages( false );   // Show current state

   interval_id = setInterval( "UpdateImages( false )", update_interval );

   document.getElementById( "stop_updates" ).disabled = false;   // enable
   document.getElementById( "start_updates" ).disabled = true;   // disable

   // Stop updates after 10 minutes of inactivity.
   idle_timeout_id = setTimeout( "LongIdling()", 600000 );
}

function ChangeUpdateInterval()
{
   if( document.getElementById( "3sec" ).selected )
      update_interval = 3000;
   else if( document.getElementById( "5sec" ).selected )
      update_interval = 5000;
   else if( document.getElementById( "10sec" ).selected )
      update_interval = 10000;
   else if( document.getElementById( "1min" ).selected )
      update_interval = 60000;
   else
      alert( "Invalid update interval!" );

   StartUpdates();
}

function ChangeSize()
{
    if( small_size )     // Set larger size
    {
       graph_size_string = "width=500&height=312";
       dial_size_string  = "width=150&height=150";
       small_size = false;
    }
    else                 // Set smaller size
    {
       graph_size_string = "width=400&height=252";
       dial_size_string  = "width=120&height=120";
       small_size = true;
    }

    UpdateImages( true );   // Redraw the first graph

    StartUpdates();         // Redraw the rest of images
}

function UpdateImages( update_first_graph_only )
{
   // Append current time to the URL prevent caching of the image.
   var date = new Date();
   var time_string = "&time=" + date.getTime()

   //if( update_first_graph_only )
     //UpdateGraph1( time_string );
   //else
   //{
     UpdateDial( "dial1", 0, "Server+1", time_string );
     UpdateDial( "dial2", 1, "Server+2", time_string );
     UpdateDial( "dial3", 3, "Server+3", time_string );
     UpdateDial( "dial4", 2, "Total", time_string );

     //UpdateGraph2( time_string );
   //}
}

function UpdateDial( dial_id, dial_type, label, time_string )
{
   // Form the URL string.
   var url = "dial?" + dial_size_string + "&dial_type=" + dial_type + 
                 "&label=" + label + time_string;

   // Reload dial image with new data.
   document.getElementById( dial_id ).src = url;         
}

function UpdateGraph1( time_string )
{
   // Form the URL string.
   var url = "glg_graph_selection?" + graph_size_string + time_string;

   // Reload image with new data.
   document.getElementById( "packed_bar_graph" ).src = url; 
}

function UpdateGraph2( time_string )
{
   // Form the URL string.
   var url = "glg_real_time_graph?" + graph_size_string + "&graph_type=0" + 
                time_string;

   // Reload image with new data.
   document.getElementById( "real_time_graph" ).src = url;         
}

// Process mouse click events to find selected object
function OnClick( event )
{
   EraseTooltip();
   EraseDataDialog();

   // Append current time to the URL to prevent caching.
   var date = new Date();
   var time_string = "&time=" + date.getTime()

   // Coordinates of the mouse click
   var position_string = 
         "&x=" + GetEventX( event ) + "&y=" + GetEventY( event );

   var url = 
       "glg_graph_selection?action=ProcessSelection&sel_type=MouseClick" + 
       "&" + graph_size_string +     // Process selection for the curr. size!
       position_string + time_string;

   GetData( url, ProcessClickData );
}

function ProcessClickData( xml_http )
{
   if( xml_http.readyState != 4 )
      return;

   if( xml_http.status == 200 )
      DisplayDataDialog( xml_http );
   else
      alert( "Can't process selection, returned status: " + xml_http.status );
}

function DisplayDataDialog( xml_http )
{
   if( xml_http.responseText != "None" )
   {
      document.getElementById( "dialog_data" ).innerHTML = 
          xml_http.responseText;
      PositionObject( dialog_object );
      dialog_object.style.display = "";
   }
   else
      EraseDataDialog();
}

function EraseDataDialog()
{
   dialog_object.style.display = "none";
}

function OnMove( event )
{
   if( tooltip_object == null )
     return;     // No tooltip object found in HTML!

   tooltip_active = false;  // Mouse moved: discard any pending tooltip.

   EraseTooltip();          // Erase any displayed tooltips.

   if( event == null ) // Mouse moved out: don't start tooltip timer.
      return;

   // Store tooltip coordinates and start tooltip timeout.
   last_x = GetEventX( event );
   last_y = GetEventY( event );

   StartTooltipTimeout();
}

function StartTooltipTimeout()
{
   tooltip_timeout_id = setTimeout( "QueryTooltip()", tooltip_interval );   
}

function StopTooltipTimeout()
{
   if( tooltip_timeout_id )
   {
      clearInterval( tooltip_timeout_id );
      tooltip_timeout_id = 0;
   }
}

// Query if there is an object with a tooltip at the mouse location.
// Return tooltip string or null.
  //
function QueryTooltip()
{
   tooltip_active = true;

   // Append current time to the URL to prevent caching of the request url.
   var date = new Date();
   var time_string = "&time=" + date.getTime();

   // Tooltip coordinates: last pos.
   var position_string = "&x=" + last_x + "&y=" + last_y;

   // Find tooltip at the requested location.
   var url = 
       "glg_graph_selection?action=ProcessSelection&sel_type=Tooltip" + 
       "&" + graph_size_string +      // Process selection for the curr. size!
       position_string + time_string;

   GetData( url, ProcessTooltipData );
}

// Popup tooltip (if the mouse hasn't moved yet).
function ProcessTooltipData( xml_http )
{
   if( xml_http.readyState != 4 )
      return;

   if( xml_http.status == 200 )   
     PopupTooltip( xml_http.responseText );
   else
     alert( "Can't process selection, returned status: " + xml_http.status );
}

function PopupTooltip( tooltip_string )
{
   if( !tooltip_active )   // Mouse moved: don't popup.
     return;

   if( tooltip_string == "None" )
      return;   // No tooltip found in the drawing at requested location.

   PositionObject( tooltip_object, last_x, last_y );

   tooltip_object.style.display = "";
   document.getElementById( "tooltip" ).innerHTML = tooltip_string;
}

function EraseTooltip()
{
   StopTooltipTimeout();
   tooltip_object.style.display = "none";
}

function PositionObject( object )
{
   var graph = document.getElementById( "packed_bar_graph" );
   var x = last_x + GetOffsetX( graph ) + 10;
   var y = last_y + GetOffsetY( graph ) + 10;

   if( ns6 )
   {
      object.style.left = x;
      object.style.top = y;
   }
   else
   {
      object.style.pixelLeft = x;
      object.style.pixelTop = y;
   }
}

// Request data to update an active dialog for the selected object.
function GetData( url, callback )
{
   var xml_http = GetXmlHttp();
   if( xml_http == null )
     return false;

   xml_http.open( "GET", url, true );

   // JavaScript has closures, so we can do it! 
   // See http://javascript.crockford.com/private.html
   //
   xml_http.onreadystatechange = function (){ callback( xml_http ) };

   xml_http.send( null );
   return true;
}

// Used to query object selection on mouse click
function GetXmlHttp()
{
   var xml_http = null;

   if( window.XMLHttpRequest )   // Mozilla and new IE
   {
      xml_http = new XMLHttpRequest();
   }
   else if( window.ActiveXObject )  // Older IE
   {
      var i;
      var version_strings = 
        new Array( "MSXML2.XMLHttp.7.0",
                   "MSXML2.XMLHttp.6.0",
                   "MSXML2.XMLHttp.5.0",
                   "MSXML2.XMLHttp.4.0",
                   "MSXML2.XMLHttp.3.0",
                   "MSXML2.XMLHttp",
                   "Microsoft.XMLHttp" );

      for( i=0; i < version_strings.length; i++ )
      {
         try
         {
            xml_http = new ActiveXObject( version_strings[i] );
            if( xml_http )
            {
               //alert( "Using " + version_strings[i] );
               break;
            }
         }
         catch( err )
         {
            //alert( version_strings[i] + " not supported." );
         }
      }
   }

   if( xml_http == null )
      alert( "Your browser does not support XMLHttpRequest, please upgrade." );

   return xml_http;
}

function GetEventX( event )
{
   var graph = document.getElementById( "packed_bar_graph" );

   return event.offsetX ? event.offsetX : event.pageX - GetOffsetX( graph );
}

function GetEventY( event )
{
   var graph = document.getElementById( "packed_bar_graph" );

   return event.offsetY ? event.offsetY : event.pageY - GetOffsetY( graph );
}

function GetOffsetX( element )
{
   var offset_x = 0;

   for( var curr = element; curr; curr = curr.offsetParent )
      offset_x += curr.offsetLeft;

   return offset_x;
}

function GetOffsetY( element )
{
   var offset_y = 0;

   for( var curr = element; curr; curr = curr.offsetParent )
      offset_y += curr.offsetTop;

   return offset_y;
}

// Stop updates after 10 minutes of inactivity to free bandwidth.
function LongIdling()
{
   idle_timeout_id = 0;   /* reset stored id */
   StopUpdates();
   alert( "Stopped updates after 10 minutes of inactivity. Click OK to resume." );
   StartUpdates();
}

</script>

  <link rel="icon" href="http://www.genlogic.com/favicon.ico" type="image/x-icon">
  <link rel="shortcut icon" href="http://www.genlogic.com/favicon.ico" type="image/x-icon"> 
</head>

<body bgcolor="white" onload="OnLoad()" style="font-family:Arial;">

<p style="font-size:80%; font-family:arial;">
<a href="http://www.genlogic.com">Home</a> | 
<a href="http://www.genlogic.com/ajax_demos.html">AJAX Demos</a> | 
<a href="http://www.genlogic.com/demos.html">Java Demos</a> | 
<a href="http://www.genlogic.com/graphics_server.html">AJAX Graphics Server</a> |
<a href="http://www.genlogic.com/index.html#Products">Products</a>
</p>

<h2> GLG Graphics Server: AJAX Dashboard Demo </h2>

<hr style="width: 100%; height: 2px;">

<input id="start_updates" type="button" value="Start Updates" 
       onclick="StartUpdates()" />
<input id="stop_updates" type="button" value="Stop Updates" 
       onclick="StopUpdates()" />
<input type="button" value="Change Dashboard Size" onclick="ChangeSize()" />
Update Interval: 
<select size="1" name="interval_menu" onchange="ChangeUpdateInterval()">
      <option id="3sec"  value="3"  selected>3 seconds</OPTION>
      <option id="5sec"  value="5"          >5 seconds</OPTION>
      <option id="10sec" value="10"         >10 seconds</OPTION>
      <option id="1min"  value="60"         >1 minute</OPTION>
</select>

<hr style="width: 100%; height: 2px;">
Click on the top graph to <b>display selected values</b> or move the mouse over it for <b>tooltips</b>.
<hr style="width: 100%; height: 2px;">

<table id="main_table"
 style="text-align: left; width: 100%;" border="1" cellpadding="0"
 cellspacing="0">
  <tbody>
    <tr>
      <td style="vertical-align: top; height: 100px; width: 100px">
        <img id="dial1" alt="Loading Image..." title="Server 1 Load"
         src="dial?width=150&height=150&dial_type=0&label=Server+1"
         style="border: 1px solid ; border-color: grey;" hspace="3" vspace="3"
        >
      </td>

      <td id="graph_td" style="vertical-align: top;" rowspan="4">
        <table id="graph_table" 
         style="text-align: left; width: 100%;" border="0" 
         cellpadding="0" cellspacing="0">
        <tbody>
<!--           <tr> -->
<!--             <td style="vertical-align: top; height: 200px;"> -->
<!--               <img id= "packed_bar_graph" alt="" -->
<!--                src="glg_graph_selection?width=500&height=312&title=Used+Bandwidth" -->
<!--                onclick="OnClick(event)"  -->
<!--                onmousemove="OnMove(event)" onmouseout="OnMove(null)" -->
<!--                style="border: 1px solid ; border-color: grey;"  -->
<!--                hspace="3" vspace="3" -->
<!--               > -->
<!--             </td> -->
          </tr>
<!--           <tr> -->
<!--             <td style="vertical-align: top; height: 200px;"> -->
<!--               <img id="real_time_graph" alt="Loading Image..." title="Server Load" -->
<!--                src="glg_real_time_graph?width=500&height=312&graph_type=0&title=Server+Load" -->
<!--                style="border: 1px solid ; border-color: grey;"  -->
<!--                hspace="3" vspace="3" -->
<!--               > -->
<!--             </td> -->
<!--           </tr> -->
        </tbody>
        </table>
      </td>
    </tr>
    <tr>
      <td style="vertical-align: top; height: 100px; width: 100px;">
        <img id="dial2" alt="Loading Image..." title="Server 2 Load"
         src="dial?width=150&height=150&dial_type=1&label=Server+2"
         style="border: 1px solid ; border-color: grey;" hspace="3" vspace="3"
        >
        </td>
    </tr>
    <tr>
      <td style="vertical-align: top; height: 100px; width: 100px;">
        <img id="dial3" alt="Loading Image..." title="Server 3 Load"
         src="dial?width=150&height=150&dial_type=3&label=Server+3"
         style="border: 1px solid ; border-color: grey;" hspace="3" vspace="3"
        >
        </td>
    </tr>
    <tr>
      <td style="vertical-align: top; height: 100px; width: 100px;">
        <img id="dial4" alt="Loading Image..." title="Total Load"
         src="dial?width=150&height=150&dial_type=2&label=Total"
         style="border: 1px solid ; border-color: grey;" hspace="3" vspace="3"
        >
        </td>
    </tr>
  </tbody>
</table>


<div id="dialog_div" onclick="EraseDataDialog()"
   style="position:absolute; display:none; top:200px; left:200px; 
   z-index:10000;">
<table border=0 cellspacing=1 cellpadding=2 style="background-color:black;">
   <tr><td border=1 style="background-color:white;" >
   <span id="dialog_data"> No Data </span>
   <hr style="width: 100%; height: 2px;">
   <input type="button" value="Close" onclick="EraseDataDialog()" />
   </td></tr>
</table>
</div>

<div id="tooltip_div"
   style="position:absolute; display:none; top:0px; left:0px; z-index:10000;">
<table border=0 cellspacing=1 cellpadding=2 style="background-color:black;">
   <tr><td id="tooltip" border=1 
       style="background-color:#ffffcc;" >
   Tooltip
   </td></tr>
</table>
</div>

</body>
</html>
