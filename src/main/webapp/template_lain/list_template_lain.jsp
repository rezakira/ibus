<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<style>
.action-role-table:hover {
	-webkit-transform: scale(1.1);
	-moz-transform: scale(1.1);
	transform: scale(1.1);
}
</style>

<script type="text/javascript">
	$(document).ready(function() {
						$('body').off('click', '.action-edit');
						$('body').off('click', '.action-export');
						

						var oTable = $('#dyntable')
								.dataTable(
										{
											"oLanguage" : {
												"sProcessing" : 'Loading Table...'
											},
											"dom":' <"search"f><"top"l>rt<"bottom"ip><"clear">',
											"aoColumnDefs": [],
									        "sPaginationType" : "full_numbers",
									        "bLengthChange" : false,
									        "bProcessing" : true,
									        "bServerSide" : true,
									        "bUseRendered" : true,
									        "bSortable": false, 
											"sAjaxSource" : '<c:url value="/faktur_lain/ref-history-template-lain/data-table.json" />',
											"aoColumns" : [

													{
														"mData" : "idUseFaktur"
													},
													{
														"mData" : "category",
														"mRender" : function(
																sourceData,
																dataType,
																fullData) {
															
															
															if(fullData.category == 'DL')
															{
															  	return 'Dokumen Lain';
															}
															else
															if(fullData.category == 'BJ')
															{
															  	return 'Kode Barang dan Jasa';
															}
															else
															if(fullData.category == 'LW')
															{
															  	return 'Lawan Transaksi';
															}
															else
															if(fullData.category == 'RDL')
															{
															  	return 'Retur Dokumen Lain';
															}
															else
															if(fullData.category == 'RPK')
															{
															  	return 'Retur Pajak Keluaran';
															}
															else
															if(fullData.category == 'RPM')
															{
															  	return 'Retur Pajak Masukkan';
															}
															else
															{
															  	return fullData.category; 	
															}
														},
													},
													{
														"mData" : "submittedBy",
													},
													{
														"mData" : "dateModified"
													},
													{
														"mData" : "idUseFaktur",
														"mRender" : function(
																sourceData,
																dataType,
																fullData) {
															
															var result = ''; 
															
															//result+='<a class="action-edit btn btn-primary" data-id="'+fullData.idUseFaktur+'"> <i class="iconsweets-create iconsweets-white"></i> Lihat Detail </a><br><br>';
															
															
															result+='<a class="action-export-template-lain btn btn-success" data-cat="'+fullData.category+'" data-id="'+fullData.idUseFaktur+'"> <i class="iconsweets-outgoing iconsweets-white"></i> Export ke CSV </a><br><br>';
															
															
															
															return result;
														}
													}

											]
										});

					

// 								$('body').on('click','.action-edit',function() {
// 									$('#content').load('<c:url value="/faktur/list-pajak-keluaran-detail.page/'
// 													+ $(this).data('id')+ '"/>', function() {
// 									});

// 									return false;
// 								});
								
								$('body').on('click','.action-export-template-lain',function() {
									
									var category_ = $(this).data('cat');
									var url = '';
									
									if(category_ == 'DL')
									{
										url = '<c:url value="/faktur_lain/downloadDokumenLain/'+ $(this).data('id')+ '"/>';
											
									}
									
									if(category_ == 'BJ')
									{
										url = '<c:url value="/faktur_lain/downloadKodeBarangJasa/'+ $(this).data('id')+ '"/>';
											
									}
									
									if(category_ == 'LW')
									{
										url = '<c:url value="/faktur_lain/downloadLawanTransaksi/'+ $(this).data('id')+ '"/>';
											
									}
									
									if(category_ == 'RDL')
									{
										url = '<c:url value="/faktur_lain/downloadReturDokumenLain/'+ $(this).data('id')+ '"/>';
											
									}
									
									if(category_ == 'RPK')
									{
										url = '<c:url value="/faktur_lain/downloadReturPajakKeluaran/'+ $(this).data('id')+ '"/>';
											
									}
									
									if(category_ == 'RPM')
									{
										url = '<c:url value="/faktur_lain/downloadReturPajakMasukkan/'+ $(this).data('id')+ '"/>';
											
									}
									
									window.open(url);
									return false;
								});
								
// 								$('body').on('click','.action-export-template',function() {
									
// 									var url = '<c:url value="/faktur/downloadExportFileTemplatePK/'+ $(this).data('id')+ '"/>';
			
// 									window.open(url);
// 									return false;
// 								});


					});

	$("a#addNew").click(function() {
		$("#content").load($(this).attr('href'));
		return false;
	});
	
</script>

<div class="rightpanel">
	<ul class="breadcrumbs">
		<li><a href="<c:url value="home.page"/>"><i
				class="iconfa-home"></i></a> <span class="separator"></span></li>
		<li>Export Template Lain <span class="separator"></span></li>
		<li>Daftar History Template Lain</li>
	</ul>

	<div class="pageheader">
		<div class="pageicon animate0 fadeInRightBig">
			<span class="iconfa-laptop"></span>
		</div>
		<div class="pagetitle animate1 fadeInRightBig">
			<h5>Export Template Lain</h5>
			<h1>Daftar History Template Lain</h1>
		</div>
	</div>
	<!--pageheader-->

	<div class="maincontent">
		<div class="maincontentinner">
			<div class="widget animate2 fadeInRightBig">
			<h4 class="widgettitle">Daftar History Template Lain</h4>
				<div class="widgetcontent">
					<table class="table table-striped responsive" id="dyntable">
						<thead>
							<tr>
								<th style="width: 30%">ID History</th>
								<th style="width: 30%">Kategori</th>
								<th style="width: 15%">Submitted By</th>
								<th style="width: 30%">Date Modified</th>
								
								<th></th>
							</tr>
						</thead>
					</table>
				</div>
			</div>
			<!--row-->

			<%@ include file="/common//mainFooter.jsp"%>
			<!--footer-->

		</div>
		<!--maincontentinner-->
	</div>
	<!--maincontent-->

</div>
<!--rightpanel-->