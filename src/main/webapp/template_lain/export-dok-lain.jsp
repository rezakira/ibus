<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<style>
.btn-saving {
	float: left;
}

.btn-saving ~ img {
	padding-left: 10px;
	display: inline !important;
}

#btn-save {
	width: 100px;
}

#btn-save ~ img {
	display: none;
}
</style>

<script>
	$(document)
			.ready(
					function() {
						$('#import-pajak-keluaran').validate(
										{
											rules : {
												
											},
											messages : {
			
											},
											highlight : function(label) {
								
											},
											success : function(label) {
												label.text('Ok!').addClass(
														'valid').closest(
														'.control-group')
														.addClass('success');
											},
											submitHandler : function(form) {
												//form.submit(); 
												var formData = new FormData(form);
												$('#btn-save').addClass('btn-saving');
												
												
												$.ajax({
													url:$(form).attr('action'),
													type:$(form).attr('method'),
													data: formData,
													contentType: false,
												    processData: false
												}).done(function(data){
													jQuery.jGrowl(data.message, { life: 3000});
													$('#btn-save').removeClass('btn-saving');
													
													if(data.status == 1)
													{
														$('#content').load('<c:url value="/faktur_lain/list-template-lain.page"/>', function() {
														});
													}
													
												}).fail(function(){
													var msg = "Failed";
													jQuery.jGrowl(msg, { life: 3000});
													$('#btn-save').removeClass('btn-saving');
												});

												return false;
											}
										});

					});
</script>

<div class="rightpanel">
	<ul class="breadcrumbs">
		<li><a href="<c:url value="home.page"/>"> <i
				class="iconfa-home"></i></a> <span class="separator"></span></li>
		<li>Export Template Lain <span class="separator"></span></li>
		<li>Import Dokumen Lain</li>
	</ul>

	<div class="pageheader">
		<div class="pageicon animate0 fadeInRightBig">
			<span class="iconfa-cogs"></span>
		</div>
		<div class="pagetitle animate1  fadeInRightBig">
			<h5>Export Template Lain</h5>
			<h1>Import Dokumen Lain</h1>
		</div>
	</div>
	<!--pageheader-->

	<div class="maincontent">
		<div class="maincontentinner">
			<div class="widgetbox animate2 fadeInRightBig">
				 <a class="btn btn-warning" id="linkTemplatePK"
					href="<c:url value="/faktur_lain/downloadTemplateDokumenLain"/>"><font color="white">Download Template Dokumen Lain</font></a>	
				<br></br> 
				
				<h4 class="widgettitle">Form Import Dokumen Lain</h4>
				<div class="widgetcontent nopadding">
					<form id="import-pajak-keluaran" class="stdform stdform2" method="post"
						action="<c:url value="/faktur_lain/uploadDokumenLain.json" />"
						enctype="multipart/form-data">
						<p>
							<label for="exampleInputFile">File Import Dokumen Lain</label>
							<span class="field"> 
								<input type="file" name="file_template" id="file_template">
							</span>
						</p>

						<p class="stdformbutton">
							<button class="btn btn-success btn-large" id="btn-save">Import</button>
							<img src="images/loaders/loader6.gif" alt="" />
						</p>
					</form>
				</div>
				<!--widgetcontent-->
			</div>

			<%@ include file="/common//mainFooter.jsp"%>

		</div>
		<!--maincontentinner-->
	</div>
	<!--maincontent-->
</div>
