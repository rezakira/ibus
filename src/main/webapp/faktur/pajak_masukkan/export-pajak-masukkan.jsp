<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<style>
.btn-saving {
	float: left;
}

.btn-saving ~ img {
	padding-left: 10px;
	display: inline !important;
}

#btn-save {
	width: 100px;
}

#btn-save ~ img {
	display: none;
}
</style>

<script>
	$(document)
			.ready(
					function() {
						$('#setting-form')
								.validate(
										{
											rules : {
												
											},
											messages : {
												
											// 				firstName: "Please enter your first name",
											// 				lastName: "Please enter your last name",
											// 				email: {
											// 					required: "Please enter your e-mail",
											// 					email: "E-mail is invalid"
											// 				}
											},
											highlight : function(label) {
												// 				setTimeout($.unblockUI, timeoutLoad);
											},
											success : function(label) {
												label.text('Ok!').addClass(
														'valid').closest(
														'.control-group')
														.addClass('success');
											},
											submitHandler : function(form) {
												//form.submit(); 
												var formData = new FormData(
														form);
												$('#btn-save').addClass(
														'btn-saving');
												
												
												$.ajax({
													url:$(form).attr('action'),
													type:$(form).attr('method'),
													data: formData,
													contentType: false,
												    processData: false
												}).done(function(data){
													jQuery.jGrowl(data.message, { life: 3000});
													$('#btn-save').removeClass('btn-saving');
													
													if(data.status == 1)
													{
														$('#content').load('<c:url value="/faktur/list-pajak-masukkan-detail.page/'
																+ data.obj + '"/>', function() {
														});
													}
												}).fail(function(){
													var msg = "Failed";
													jQuery.jGrowl(msg, { life: 3000});
													$('#btn-save').removeClass('btn-saving');
												});

												return false;
											}
										});

					});
</script>

<div class="rightpanel">
	<ul class="breadcrumbs">
		<li><a href="<c:url value="home.page"/>"> <i
				class="iconfa-home"></i></a> <span class="separator"></span></li>
		<li>Export Pajak Masukkan <span class="separator"></span></li>
		<li>Import Template Pajak Masukkan</li>
	</ul>

	<div class="pageheader">
		<div class="pageicon animate0 fadeInRightBig">
			<span class="iconfa-cogs"></span>
		</div>
		<div class="pagetitle animate1  fadeInRightBig">
			<h5>Export Pajak Masukkan</h5>
			<h1>Import Template Pajak Masukkan</h1>
		</div>
	</div>
	<!--pageheader-->

	<div class="maincontent">
		<div class="maincontentinner">
			<div class="widgetbox animate2 fadeInRightBig">
				 <a class="btn btn-warning" id="linkTemplatePM"
					href="<c:url value="/faktur/downloadTemplatePM"/>"><font color="white">Download Template Pajak Masukkan</font></a>	
					<br><br>
				<h4 class="widgettitle">Import Template Pajak Masukkan</h4>
				<div class="widgetcontent nopadding">
					<form id="setting-form" class="stdform stdform2" method="post"
						action="<c:url value="/faktur/uploadPM.json" />"
						enctype="multipart/form-data">
						<p>
							<label for="exampleInputFile">File Import Pajak Masukkan</label>
							<span class="field"> 
								<input type="file" name="file_template" id="file_template">
							</span>
						</p>
					<p class="stdformbutton">
							<button class="btn btn-success btn-large" id="btn-save">Import</button>
							<img src="images/loaders/loader6.gif" alt="" />
						</p>
						<a href="https://play.google.com/store/apps/details?id=rokubi.co.id.happytax" target="_blank">
					<img align="left" style="margin: 5px;" width="150" src="<c:url value="/images/google-play.png"/>" alt="" />
					<div align="left" style="margin-top: 5px;"><font color="black"  size="4">Download Aplikasi Happytax <br> Barcode Scanner</font></div>
					</a>
			
					</form>
				</div>
				<!--widgetcontent-->
			</div>

			<%@ include file="/common//mainFooter.jsp"%>

		</div>
		<!--maincontentinner-->
	</div>
	<!--maincontent-->
</div>
