<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<style>
.action-role-table:hover {
	-webkit-transform: scale(1.1);
	-moz-transform: scale(1.1);
	transform: scale(1.1);
}
</style>

<script type="text/javascript">
	var idUseFaktur = "${idUseFaktur}";
	var url_detail = '<c:url value="/faktur/ref-pajak-masukkan-detail/data-table.json" />' +'/'+idUseFaktur;
	
	$(document).ready(function() {
						$('body').off('click', '.action-edit-pm');
						$('body').off('click', '.action-change-status-pm');
						$('body').off('click', '.action-view-pm');
						
						

						var oTable = $('#dyntable')
								.dataTable(
										{
											"oLanguage" : {
												"sProcessing" : 'Loading Table...'
											},
											"sPaginationType" : "full_numbers",
											"bLengthChange" : false,
											"bFilter" : false,
											"bProcessing" : true,
											"bServerSide" : true,
											"sAjaxSource" : url_detail,
											"aoColumns" : [

													{
														"mData" : "noFaktur"
													},
													{
														"mData" : "npwp",
													},
													{
														"mData" : "tglFaktur"
													},
													{
														"mData" : "nama"
													},
													{
														"mData" : "status",
														"mRender" : function(
																sourceData,
																dataType,
																fullData) {
															
														
															if(fullData.status == 'A')
															{
																return	'<span class="label label-success">Active</span>';
															}
															else
															{
																return 	'<span class="label label-danger">Hide</span>';
															}	
															
														}
													},
													{
														"mData" : "noFaktur",
														"mRender" : function(
																sourceData,
																dataType,
																fullData) {
															
															var result = '';
															result += '<a class="action-view-pm btn btn-primary" data-nofaktur="'+fullData.idReference+'"><i class="iconsweets-pdf iconsweets-white"></i> Lihat Detail</a><br><br>';
															result += '<a class="action-edit-pm btn btn-warning" data-id="'+fullData.id+'"><i class="iconsweets-create iconsweets-white"></i> Ubah</a><br><br>';
															
															
															
															if(fullData.status == 'A')
															{
																result += '<a class="action-change-status-pm btn btn-danger" data-id="'+fullData.id+'" data-status="'+fullData.status+'" data-used="'+fullData.idUseFaktur+'"><i class="iconsweets-trashcan iconsweets-white"></i> Hide</a>';
															}
															else
															{
																result += '<a class="action-change-status-pm btn btn-success" data-id="'+fullData.id+'" data-status="'+fullData.status+'" data-used="'+fullData.idUseFaktur+'"><i class="iconsweets-flag2 iconsweets-white"></i> Active</a>';
															}	
															
															
															return result;
															
														}
													}

											]
										});

								$('body').on('click','.action-edit-pm',function() {
									$('#content').load('<c:url value="/faktur/update-pajak-masukkan-detail.page/'
											+ $(this).data('id')+ '"/>', function() {
									});

									return false;
								});
						
						

								$('body').on('click','.action-view-pm',function() {
									$('#content').load('<c:url value="/faktur/faktur-masukkan-detail.page/'
													+ $(this).data('nofaktur')+ '"/>', function() {
											});

									return false;
								});
								
								
								$('body').on('click','.action-change-status-pm',function() {
										
									var idUse =$(this).data('used');
									$.ajax(
											{
												url : '<c:url value="/faktur/updateStatusPM.json" />',
												type : 'post',
												data : {
													idPajakMasukkan: $(this).data('id'),
													status:$(this).data('status')=='A'?'D':'A'
												}
											})
										.done(function(data) {

												var msg = data.message;

												if (data.status == 1) {
													jQuery.jGrowl(msg,{life : 3000});
													
													$('#content').load('<c:url value="/faktur/list-pajak-masukkan-detail.page/'
															+idUse+ '"/>', function() {
													});
													
													
												} else {
													jQuery.jGrowl(msg,{life : 3000});
												}

											})
										.fail(function() {

												var msg = "Failed";
												jQuery.jGrowl(msg,{life : 3000});

										});

									return false;
								});


					});

	$("a#addNew").click(function() {
		$("#content").load($(this).attr('href'));
		return false;
	});
	
	$("a#export").click(function() {
		window.open($(this).attr('href'));
		return false;
	});
	

	$("a#export_template").click(function() {
		window.open($(this).attr('href'));
		return false;
	});
	
	
	
</script>

<div class="rightpanel">
	<ul class="breadcrumbs">
		<li><a href="<c:url value="home.page"/>"><i
				class="iconfa-home"></i></a> <span class="separator"></span></li>
		<li>Export Pajak Masukkan <span class="separator"></span></li>
		<li>Daftar No Faktur</li>
	</ul>

	<div class="pageheader">
		<div class="pageicon animate0 fadeInRightBig">
			<span class="iconfa-laptop"></span>
		</div>
		<div class="pagetitle animate1 fadeInRightBig">
			<h5>Export Pajak Masukkan</h5>
			<h1>Daftar No Faktur</h1>
		</div>
	</div>
	<!--pageheader-->

	<div class="maincontent">
		<div class="maincontentinner">
			<div class="widget animate2 fadeInRightBig">
		
			
				 <a class="btn btn-warning" id="addNew"
					href="<c:url value="/faktur/list-pajak-masukkan.page"/>">
					<font color="white">Kembali ke Daftar History Pajak Masukkan</font></a>
					
					<a class="btn btn-danger" id="export"
					href="<c:url value="/faktur/downloadFilePM"/>/${idUseFaktur}">
					<font color="white">Export ke CSV</font></a>
					
					<a class="btn btn-success" id="export_template"
					href="<c:url value="/faktur/downloadExportFileTemplatePM"/>/${idUseFaktur}">
					<font color="white">Export Data ke Template</font></a>
					
					<br></br> 
					
				<h4 class="widgettitle">Daftar History Pajak Masukkan Detail</h4>
				<div class="widgetcontent">
					<table class="table table-striped responsive" id="dyntable">
						<thead>
							<tr>
								<th style="width: 20%">No Faktur</th>
								<th style="width: 15%">NPWP</th>
								<th style="width: 15%">Tanggal Faktur</th>
								<th style="width: 25%">Nama</th>
								<th style="width: 5%">Status</th> 
								<th></th>
							</tr>
						</thead>
					</table>
				</div>
			</div>
			<!--row-->

			<%@ include file="/common//mainFooter.jsp"%>
			<!--footer-->

		</div>
		<!--maincontentinner-->
	</div>
	<!--maincontent-->

</div>
<!--rightpanel-->