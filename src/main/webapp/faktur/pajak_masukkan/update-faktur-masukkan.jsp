<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<style>
.action-role-table:hover {
	-webkit-transform: scale(1.1);
	-moz-transform: scale(1.1);
	transform: scale(1.1);
}
</style>

<script type="text/javascript">
$(document).ready(function() {
					$('#form-update-pm').validate(
					{
						rules : {
							
						},
						messages : {
			
						},
						highlight : function(label) {
							// 				setTimeout($.unblockUI, timeoutLoad);
						},
						success : function(label) {
							label.text('Ok!').addClass(
									'valid').closest(
									'.control-group')
									.addClass('success');
						},
						submitHandler : function(form) {
							var formData = new FormData(
									form);
							$('#btn-save').addClass('btn-saving');
							
							
							$.ajax({
								url:$(form).attr('action'),
								type:$(form).attr('method'),
								data: formData,
								contentType: false,
							    processData: false
							}).done(function(data){
								jQuery.jGrowl(data.message, { life: 3000});
								$('#btn-save').removeClass('btn-saving');
								
								if(data.status == 1)
								{
									$("#content").load('<c:url value="/faktur/list-pajak-masukkan-detail.page"/>/${fakturMasukkanDetail.idUseFaktur}');
								}
								
							}).fail(function(){
								var msg = "Failed";
								jQuery.jGrowl(msg, { life: 3000});
								$('#btn-save').removeClass('btn-saving');
							});

							return false;
						}
					});

});


$("a#addNew").click(function() {
		$("#content").load($(this).attr('href'));
		return false;
});
	
</script>

<div class="rightpanel">
	<ul class="breadcrumbs">
		<li><a href="<c:url value="home.page"/>"><i
				class="iconfa-home"></i></a> <span class="separator"></span></li>
		<li>Export Pajak Masukkan <span class="separator"></span></li>
		<li>Update Faktur Masukkan</li>
	</ul>

	<div class="pageheader">
		<div class="pageicon animate0 fadeInRightBig">
			<span class="iconfa-laptop"></span>
		</div>
		<div class="pagetitle animate1 fadeInRightBig">
			<h5>Export Pajak Masukkan</h5>
			<h1>Update Faktur Masukkan</h1>
		</div>
	</div>
	<!--pageheader-->

	<div class="maincontent">
		<div class="maincontentinner">
			<div class="widgetbox animate2 fadeInRightBig">
				<a class="btn btn-warning" id="addNew"
					href="<c:url value="/faktur/list-pajak-masukkan-detail.page"/>/${fakturMasukkanDetail.idUseFaktur}">
					<font color="white">Kembali ke Daftar No Faktur</font></a><br></br> 
					
				<h4 class="widgettitle">Informasi Faktur Keluaran</h4>
				<div class="widgetcontent nopadding">
					<form id="form-update-pm" class="stdform stdform2" 
						method="post"
						action="<c:url value="/faktur/updatePMDetail.json" />"
						enctype="multipart/form-data">
						
						<input type="text" 
						name="id" 
						value="${fakturMasukkanDetail.id }"
						id="id" style="display: none;"  />
						
						<input type="text" 
						name="idUseFaktur" 
						value="${fakturMasukkanDetail.idUseFaktur }"
						id="idUseFaktur" style="display: none;"  />
						
						<input type="text" 
						name="status" 
						value="${fakturMasukkanDetail.status }"
						id="status" style="display: none;"  />
						
				
						<p>
							<label for="exampleInputFile">Kode Jenis Transaksi</label>
							<span class="field"> 
								<input type="text" name="kdJenisTransaksi" value="${fakturMasukkanDetail.kdJenisTransaksi }"
								id="kdJenisTransaksi" class="form-control"  />
							</span>
						</p>
						<p>
							<label for="exampleInputFile">FG Pengganti</label>
							<span class="field"> 
							
								<input type="text" name="fgPengganti" value="${fakturMasukkanDetail.fgPengganti }"
								id="fgPengganti" class="form-control"  />
								
							</span>
						</p>
						<p>
							<label for="exampleInputFile">No Faktur</label>
							<span class="field"> 
							
								<input type="text" name="noFaktur" value="${fakturMasukkanDetail.noFaktur }"
									id="noFaktur" class="form-control"  />
							</span>
						</p>
						<p>
							<label for="exampleInputFile">Masa Pajak</label>
							<span class="field"> 
							<input type="text" name="masaPajak" value="${fakturMasukkanDetail.masaPajak }"
									id="masaPajak" class="form-control"  />
							</span>
						</p>
						<p>
							<label for="exampleInputFile">Tahun Pajak</label>
							<span class="field"> 
							<input type="text" name="tahunPajak" value="${fakturMasukkanDetail.tahunPajak }"
									id="tahunPajak" class="form-control"  />
									
							</span>
						</p>
						<p>
							<label for="exampleInputFile">Tanggal Faktur</label>
							<span class="field"> 
							<input type="text" name="tglFaktur" value="${fakturMasukkanDetail.tglFaktur }"
									id="tglFaktur" class="form-control"  />
							</span>
						</p>
						<p>
							<label for="exampleInputFile">NPWP</label>
							<span class="field"> 
								<input type="text" name="npwp" value="${fakturMasukkanDetail.npwp }"
									id="npwp" class="form-control"  />
									
							</span>
						</p>
						<p>
							<label for="exampleInputFile">Nama</label>
							<span class="field"> 
							<input type="text" name="nama" value="${fakturMasukkanDetail.nama }"
									id="nama" class="form-control"  />
									
									
							</span>
						</p>
						<p>
							<label for="exampleInputFile">Alamat</label>
							<span class="field"> 
							<input type="text" name="alamat" value="${fakturMasukkanDetail.alamat }"
									id="alamat" class="form-control"  />
							
							
							
							</span>
						</p>
						<p>
							<label for="exampleInputFile">Jumlah DPP</label>
							<span class="field"> 
							
							<input type="text" name="jumlahDpp" value="${fakturMasukkanDetail.jumlahDpp}"
									id="jumlahDpp" class="form-control"  />
									
							 
							
								
							</span>
						</p>
						<p>
							<label for="exampleInputFile">Jumlah PPN</label>
							<span class="field"> 
							
								<input type="text" name="jumlahPpn" value="${fakturMasukkanDetail.jumlahPpn}"
									id="jumlahPpn" class="form-control"  />
									
								
							</span>
						</p>
						<p>
							<label for="exampleInputFile">Jumlah PPNBM</label>
							<span class="field"> 
							
							<input type="text" name="jumlahPpnbm" value="${fakturMasukkanDetail.jumlahPpnbm}"
									id="jumlahPpnbm" class="form-control"  />

							</span>
						</p>
						<p>
							<label for="exampleInputFile">Is Creditable</label>
							<span class="field"> 
							
							<input type="text" name="isCreditable" value="${fakturMasukkanDetail.isCreditable }"
									id="isCreditable" class="form-control"  />
							</span>
						</p>
						
						<p class="stdformbutton">
							<button class="btn btn-success btn-large" id="btn-save">Simpan</button>
							
						</p>

						
					</form>
				</div>
				<!--widgetcontent-->
			</div>

			<!--row-->

			<%@ include file="/common//mainFooter.jsp"%>
			<!--footer-->

		</div>
		<!--maincontentinner-->
	</div>
	<!--maincontent-->

</div>
<!--rightpanel-->