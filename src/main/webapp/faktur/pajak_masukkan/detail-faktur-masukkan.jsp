<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<style>
.action-role-table:hover {
	-webkit-transform: scale(1.1);
	-moz-transform: scale(1.1);
	transform: scale(1.1);
}
</style>

<script type="text/javascript">
	

	$("a#addNew").click(function() {
		$("#content").load($(this).attr('href'));
		return false;
	});
	
</script>

<div class="rightpanel">
	<ul class="breadcrumbs">
		<li><a href="<c:url value="home.page"/>"><i
				class="iconfa-home"></i></a> <span class="separator"></span></li>
		<li>Export Pajak Masukkan <span class="separator"></span></li>
		<li>Detail Faktur Masukkan</li>
	</ul>

	<div class="pageheader">
		<div class="pageicon animate0 fadeInRightBig">
			<span class="iconfa-laptop"></span>
		</div>
		<div class="pagetitle animate1 fadeInRightBig">
			<h5>Export Pajak Masukkan</h5>
			<h1>Detail Faktur Masukkan</h1>
		</div>
	</div>
	<!--pageheader-->

	<div class="maincontent">
		<div class="maincontentinner">
			<div class="widgetbox animate2 fadeInRightBig">
				<a class="btn btn-warning" id="addNew"
					href="<c:url value="/faktur/list-pajak-masukkan-detail.page"/>/${fakturMasukkanDetail.idUseFaktur}">
					<font color="white">Kembali ke Daftar No Faktur</font></a><br></br> 
					
				<h4 class="widgettitle">Informasi Faktur Keluaran</h4>
				<div class="widgetcontent nopadding">
					<form class="stdform stdform2">
						<p>
							<label for="exampleInputFile">Kode Jenis Transaksi</label>
							<span class="field"> 
								${fakturMasukkanDetail.kdJenisTransaksi}
							</span>
						</p>
						<p>
							<label for="exampleInputFile">FG Pengganti</label>
							<span class="field"> 
								${fakturMasukkanDetail.fgPengganti}
							</span>
						</p>
						<p>
							<label for="exampleInputFile">No Faktur</label>
							<span class="field"> 
								${fakturMasukkanDetail.noFaktur}
							</span>
						</p>
						<p>
							<label for="exampleInputFile">Masa Pajak</label>
							<span class="field"> 
								${fakturMasukkanDetail.masaPajak}
							</span>
						</p>
						<p>
							<label for="exampleInputFile">Tahun Pajak</label>
							<span class="field"> 
								${fakturMasukkanDetail.tahunPajak}
							</span>
						</p>
						<p>
							<label for="exampleInputFile">Tanggal Faktur</label>
							<span class="field"> 
								${fakturMasukkanDetail.tglFaktur}
							</span>
						</p>
						<p>
							<label for="exampleInputFile">NPWP</label>
							<span class="field"> 
								${fakturMasukkanDetail.npwp}
							</span>
						</p>
						<p>
							<label for="exampleInputFile">Nama</label>
							<span class="field"> 
								${fakturMasukkanDetail.nama}
							</span>
						</p>
						<p>
							<label for="exampleInputFile">Alamat</label>
							<span class="field"> 
								${fakturMasukkanDetail.alamat}
							</span>
						</p>
						<p>
							<label for="exampleInputFile">Jumlah DPP</label>
							<span class="field"> 
							 <fmt:formatNumber type="number" 
            						maxFractionDigits="3" value="${fakturMasukkanDetail.jumlahDpp}" />
							
								
							</span>
						</p>
						<p>
							<label for="exampleInputFile">Jumlah PPN</label>
							<span class="field"> 
							 <fmt:formatNumber type="number" 
            						maxFractionDigits="3" value="${fakturMasukkanDetail.jumlahPpn}" />
								
							</span>
						</p>
						<p>
							<label for="exampleInputFile">Jumlah PPNBM</label>
							<span class="field"> 
							 <fmt:formatNumber type="number" 
            						maxFractionDigits="3" value="${fakturMasukkanDetail.jumlahPpnbm}" />
								
							</span>
						</p>
						<p>
							<label for="exampleInputFile">Is Creditable</label>
							<span class="field"> 
								${fakturMasukkanDetail.isCreditable}
							</span>
						</p>
						

						
					</form>
				</div>
				<!--widgetcontent-->
			</div>

			<!--row-->

			<%@ include file="/common//mainFooter.jsp"%>
			<!--footer-->

		</div>
		<!--maincontentinner-->
	</div>
	<!--maincontent-->

</div>
<!--rightpanel-->