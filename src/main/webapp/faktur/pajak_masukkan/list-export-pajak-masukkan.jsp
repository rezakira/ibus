<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<style>
.action-role-table:hover {
	-webkit-transform: scale(1.1);
	-moz-transform: scale(1.1);
	transform: scale(1.1);
}
</style>

<script type="text/javascript">
	$(document).ready(function() {
						$('body').off('click', '.action-edit');
						$('body').off('click', '.action-export');
						

						var oTable = $('#dyntable')
								.dataTable(
										{
											"oLanguage" : {
												"sProcessing" : 'Loading Table...'
											},
											"dom":' <"search"f><"top"l>rt<"bottom"ip><"clear">',
											"aoColumnDefs": [],
									        "sPaginationType" : "full_numbers",
									        "bLengthChange" : false,
									        "bProcessing" : true,
									        "bServerSide" : true,
									        "bUseRendered" : true,
									        "bSortable": false, 
											"sAjaxSource" : '<c:url value="/faktur/ref-pajak-masukkan/data-table.json" />',
											"aoColumns" : [

													{
														"mData" : "idUseFaktur"
													},
													{
														"mData" : "submittedBy",
													},
													{
														"mData" : "dateModified"
													},
													{
														"mData" : "idUseFaktur",
														"mRender" : function(
																sourceData,
																dataType,
																fullData) {
															
															return '<a class="action-edit btn btn-primary" data-id="'+fullData.idUseFaktur+'"> <i class="iconsweets-create iconsweets-white"></i> Lihat Detail </a><br><br>'+
															'<a class="action-export btn btn-success" data-id="'+fullData.idUseFaktur+'"> <i class="iconsweets-outgoing iconsweets-white"></i> Export ke CSV</a><br><br>'+
															'<a class="action-export-template btn btn-danger" data-id="'+fullData.idUseFaktur+'"> <i class="iconsweets-outgoing iconsweets-white"></i> Export Data ke Template XLSX</a>';
														}
													}

											]
										});

					

								$('body').on('click','.action-edit',function() {
									$('#content').load('<c:url value="/faktur/list-pajak-masukkan-detail.page/'
													+ $(this).data('id')+ '"/>', function() {
											});

									return false;
								});
								
								$('body').on('click','.action-export',function() {
									
									var url = '<c:url value="/faktur/downloadFilePM/'+ $(this).data('id')+ '"/>';
			
									window.open(url);
									return false;
								});
								
								$('body').on('click','.action-export-template',function() {
									
									var url = '<c:url value="/faktur/downloadExportFileTemplatePM/'+ $(this).data('id')+ '"/>';
			
									window.open(url);
									return false;
								});


					});

	$("a#addNew").click(function() {
		$("#content").load($(this).attr('href'));
		return false;
	});
	
</script>

<div class="rightpanel">
	<ul class="breadcrumbs">
		<li><a href="<c:url value="home.page"/>"><i
				class="iconfa-home"></i></a> <span class="separator"></span></li>
		<li>Export Pajak Masukkan <span class="separator"></span></li>
		<li>Daftar History Pajak Masukkan</li>
	</ul>

	<div class="pageheader">
		<div class="pageicon animate0 fadeInRightBig">
			<span class="iconfa-laptop"></span>
		</div>
		<div class="pagetitle animate1 fadeInRightBig">
			<h5>Export Pajak Masukkan</h5>
			<h1>Daftar History Pajak Masukkan</h1>
		</div>
	</div>
	<!--pageheader-->

	<div class="maincontent">
		<div class="maincontentinner">
			<div class="widget animate2 fadeInRightBig">
				 <a class="btn btn-success" id="addNew"
					href="<c:url value="/faktur/export-pajak-masukkan.page"/>"><font color="white">Import Pajak Masukkan</font></a>
				 <a class="btn btn-warning" id="linkTemplatePM"
					href="<c:url value="/faktur/downloadTemplatePM"/>"><font color="white">Download Template Pajak Masukkan</font></a>	
				<br></br> 
				<h4 class="widgettitle">Daftar History Pajak Masukkan</h4>
				<div class="widgetcontent">
					<table class="table table-striped responsive" id="dyntable">
						<thead>
							<tr>
								<th style="width: 30%">ID History Pajak Masukkan</th>
								<th style="width: 15%">Submitted By</th>
								<th style="width: 30%">Date Modified</th>
								
<!-- 								<th style="width: 5%">STATUS</th> -->
								<th></th>
							</tr>
						</thead>
					</table>
				</div>
			</div>
			<!--row-->

			<%@ include file="/common//mainFooter.jsp"%>
			<!--footer-->

		</div>
		<!--maincontentinner-->
	</div>
	<!--maincontent-->

</div>
<!--rightpanel-->