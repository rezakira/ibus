<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<style>
.action-role-table:hover {
	-webkit-transform: scale(1.1);
	-moz-transform: scale(1.1);
	transform: scale(1.1);
}
</style>

<script type="text/javascript">
	var noFaktur = "${fakturKeluaranDetail.idReference}";
	var url_detail = '<c:url value="/faktur/faktur-keluaran-detail/data-table.json" />' +'/'+noFaktur;
	
	$(document).ready(function() {
		$('#form-update-pk').validate(
				{
					rules : {
						
					},
					messages : {
		
					},
					highlight : function(label) {
						// 				setTimeout($.unblockUI, timeoutLoad);
					},
					success : function(label) {
						label.text('Ok!').addClass(
								'valid').closest(
								'.control-group')
								.addClass('success');
					},
					submitHandler : function(form) {
						var formData = new FormData(
								form);
						$('#btn-save').addClass('btn-saving');
						
						
						$.ajax({
							url:$(form).attr('action'),
							type:$(form).attr('method'),
							data: formData,
							contentType: false,
						    processData: false
						}).done(function(data){
							jQuery.jGrowl(data.message, { life: 3000});
							$('#btn-save').removeClass('btn-saving');
							
							if(data.status == 1)
							{
								$("#content").load('<c:url value="/faktur/update-pajak-keluaran-detail.page"/>/${fakturKeluaranDetail.id}');
							}
							
						}).fail(function(){
							var msg = "Failed";
							jQuery.jGrowl(msg, { life: 3000});
							$('#btn-save').removeClass('btn-saving');
						});

						return false;
					}
				});

		
					


					});

	$("a#addNew").click(function() {
		$("#content").load($(this).attr('href'));
		return false;
	});
	
</script>

<div class="rightpanel">
	<ul class="breadcrumbs">
		<li><a href="<c:url value="home.page"/>"><i
				class="iconfa-home"></i></a> <span class="separator"></span></li>
		<li>Export Pajak Keluaran <span class="separator"></span></li>
		<li>Update Faktur Keluaran</li>
	</ul>

	<div class="pageheader">
		<div class="pageicon animate0 fadeInRightBig">
			<span class="iconfa-laptop"></span>
		</div>
		<div class="pagetitle animate1 fadeInRightBig">
			<h5>Export Pajak Keluaran</h5>
			<h1>Update Faktur Keluaran</h1>
		</div>
	</div>
	<!--pageheader-->
	
	

	<div class="maincontent">
		<div class="maincontentinner">
			<div class="widgetbox animate2 fadeInRightBig">
				<a class="btn btn-warning" id="addNew"
					href="<c:url value="/faktur/update-pajak-keluaran-detail.page"/>/${fakturKeluaranDetail.id}">
					<font color="white">Kembali</font></a><br></br> 
					
				<h4 class="widgettitle">Informasi Faktur Keluaran</h4>
				<div class="widgetcontent nopadding">
					<form 
						id="form-update-pk"
						class="stdform stdform2"
						method="post"
						action="<c:url value="/faktur/updatePKObjekDetail.json" />"
						enctype="multipart/form-data">
						
						<input type="text" name="id" value="${fakturKeluaranDetail.id }"
								id="id" style="display: none;"  />
								
								
						<p>
							<label for="exampleInputFile">Kode Objek</label>
							<span class="field"> 
							
								<input type="text" name="kodeObjek" value="${fakturKeluaranDetail.kodeObjek }"
								id="kodeObjek" class="form-control"  />
								
							</span>
						</p>
						<p>
							<label for="exampleInputFile">Nama Objek</label>
							<span class="field"> 
							
								<input type="text" name="namaObjek" value="${fakturKeluaranDetail.namaObjek }"
								id="namaObjek" class="form-control"  />
							
							</span>
						</p>
						<p>
							<label for="exampleInputFile">Harga Satuan</label>
							<span class="field"> 
								<input type="text" name="hargaSatuan" value="${fakturKeluaranDetail.hargaSatuan }"
								id="hargaSatuan" class="form-control" />
							</span>
						</p>
						<p>
							<label for="exampleInputFile">Jumlah Barang</label>
							<span class="field"> 
							<input type="text" name="jumlahBarang" value="${fakturKeluaranDetail.jumlahBarang }"
								id="jumlahBarang" class="form-control"  />
							</span>
						</p>
						<p>
							<label for="exampleInputFile">Harga Total</label>
							<span class="field"> 
							<input type="text" name="hargaTotal" value="${fakturKeluaranDetail.hargaTotal }"
								id="hargaTotal" class="form-control"  />
								
							</span>
						</p>
						<p>
							<label for="exampleInputFile">Diskon</label>
							<span class="field"> 
							<input type="text" name="diskon" value="${fakturKeluaranDetail.diskon }"
								id="diskon" class="form-control"  />
								
							</span>
						</p>
						<p>
							<label for="exampleInputFile">DPP</label>
							<span class="field"> 
							
							<input type="text" name="dpp" value="${fakturKeluaranDetail.dpp }"
								id="dpp" class="form-control"  />
								
							</span>
						</p>
						<p>
							<label for="exampleInputFile">PPN</label>
							<span class="field"> 
							<input type="text" name="ppn" value="${fakturKeluaranDetail.ppn }"
								id="ppn" class="form-control"  />
								
							</span>
						</p>
						<p>
							<label for="exampleInputFile">Tarif PPNBM</label>
							<span class="field"> 
							<input type="text" name="tarifPpnbm" value="${fakturKeluaranDetail.tarifPpnbm }"
								id="tarifPpnbm" class="form-control"  />
								
							</span>
						</p>
						
						<p>
							<label for="exampleInputFile">PPNBM</label>
							<span class="field"> 
							<input type="text" name="ppnbm" value="${fakturKeluaranDetail.ppnbm }"
								id="ppnbm" class="form-control"  />
								
							</span>
						</p>
					
						
						<p class="stdformbutton">
							<button class="btn btn-success btn-large" id="btn-save">Simpan</button>
							
						</p>
						

						
					</form>
				</div>
				<!--widgetcontent-->
			</div>
		

			<%@ include file="/common//mainFooter.jsp"%>
			<!--footer-->

		</div>
		<!--maincontentinner-->
	</div>
	<!--maincontent-->

</div>
<!--rightpanel-->