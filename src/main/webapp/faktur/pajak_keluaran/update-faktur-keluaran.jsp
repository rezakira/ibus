<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<style>
.action-role-table:hover {
	-webkit-transform: scale(1.1);
	-moz-transform: scale(1.1);
	transform: scale(1.1);
}
</style>

<script type="text/javascript">
	var noFaktur = "${fakturKeluaranDetail.idReference}";
	var url_detail = '<c:url value="/faktur/faktur-keluaran-detail/data-table.json" />' +'/'+noFaktur;
	
	$(document).ready(function() {
		$('#form-update-pk').validate(
				{
					rules : {
						
					},
					messages : {
		
					},
					highlight : function(label) {
						// 				setTimeout($.unblockUI, timeoutLoad);
					},
					success : function(label) {
						label.text('Ok!').addClass(
								'valid').closest(
								'.control-group')
								.addClass('success');
					},
					submitHandler : function(form) {
						var formData = new FormData(
								form);
						$('#btn-save').addClass('btn-saving');
						
						
						$.ajax({
							url:$(form).attr('action'),
							type:$(form).attr('method'),
							data: formData,
							contentType: false,
						    processData: false
						}).done(function(data){
							jQuery.jGrowl(data.message, { life: 3000});
							$('#btn-save').removeClass('btn-saving');
							
							if(data.status == 1)
							{
								$("#content").load('<c:url value="/faktur/list-pajak-keluaran-detail.page"/>/${fakturKeluaranDetail.idUseFaktur}');
							}
							
						}).fail(function(){
							var msg = "Failed";
							jQuery.jGrowl(msg, { life: 3000});
							$('#btn-save').removeClass('btn-saving');
						});

						return false;
					}
				});

		
						$('body').off('click', '.action-edit-pk-objek');
						$('body').off('click', '.action-change-status-pk-objek');
						
						var oTable = $('#dyntable')
								.dataTable(
										{
											"oLanguage" : {
												"sProcessing" : 'Loading Table...'
											},
											"sPaginationType" : "full_numbers",
											"bLengthChange" : false,
											"bFilter" : false,
											"bProcessing" : true,
											"bServerSide" : true,
											"sAjaxSource" : url_detail,
											"aoColumns" : [

													{
														"mData" : "kodeObjek"
													},
													{
														"mData" : "namaObjek",
													},
													{
														"mData" : "hargaSatuan"
													},
													{
														"mData" : "jumlahBarang"
													},
													{
														"mData" : "hargaTotal"
													},
													{
														"mData" : "diskon"
													},
													{
														"mData" : "dpp"
													},
													{
														"mData" : "ppn"
													},
													{
														"mData" : "tarifPpnbm"
													},
													{
														"mData" : "ppnbm"
													},
													{
														"mData" : "status",
														"mRender" : function(
																sourceData,
																dataType,
																fullData) {
															
														
															if(fullData.status == 'A')
															{
																return	'<span class="label label-success">Active</span>';
															}
															else
															{
																return 	'<span class="label label-danger">Hide</span>';
															}	
															
														}
													},
													{
														"mData" : "kodeObjek",
														"mRender" : function(
																sourceData,
																dataType,
																fullData) {
															
															var result = '';
															result += '<a title="Ubah" class="action-edit-pk-objek btn btn-warning" data-id="'+fullData.id+'"><i class="iconsweets-create iconsweets-white"></i></a><br><br>';
															
															
															if(fullData.status == 'A')
															{
																result += '<a title="Hide" class="action-change-status-pk-objek btn btn-danger" data-id="'+fullData.id+'" data-status="'+fullData.status+'" data-used="'+fullData.idUseFaktur+'"><i class="iconsweets-trashcan iconsweets-white"></i></a>';
															}
															else
															{
																result += '<a title="Active" class="action-change-status-pk-objek btn btn-success" data-id="'+fullData.id+'" data-status="'+fullData.status+'" data-used="'+fullData.idUseFaktur+'"><i class="iconsweets-flag2 iconsweets-white"></i></a>';
															}	
															
															
															return result;
															
														}
													}

											]
										});

					

								$('body').on('click','.action-edit-pk-objek',function() {
									
									$('#content').load('<c:url value="/faktur/update-pajak-keluaran-objek.page/'
											+ $(this).data('id')+ '"/>', function() {
									});
								

									return false;
								});
								
								$('body').on('click','.action-change-status-pk-objek',function() {
								
										$.ajax(
											{
												url : '<c:url value="/faktur/updateStatusPK.json" />',
												type : 'post',
												data : {
													id: $(this).data('id'),
													status:$(this).data('status')=='A'?'D':'A'
												}
											})
										.done(function(data) {

												var msg = data.message;

												if (data.status == 1) {
													jQuery.jGrowl(msg,{life : 3000});
													
													$("#content").
														load('<c:url value="/faktur/update-pajak-keluaran-detail.page"/>/${fakturKeluaranDetail.id}');
													
													
												} else {
													jQuery.jGrowl(msg,{life : 3000});
												}

											})
										.fail(function() {

												var msg = "Failed";
												jQuery.jGrowl(msg,{life : 3000});

										});

									return false;
								});


					});

	$("a#addNew").click(function() {
		$("#content").load($(this).attr('href'));
		return false;
	});
	
</script>

<div class="rightpanel">
	<ul class="breadcrumbs">
		<li><a href="<c:url value="home.page"/>"><i
				class="iconfa-home"></i></a> <span class="separator"></span></li>
		<li>Export Pajak Keluaran <span class="separator"></span></li>
		<li>Update Faktur Keluaran</li>
	</ul>

	<div class="pageheader">
		<div class="pageicon animate0 fadeInRightBig">
			<span class="iconfa-laptop"></span>
		</div>
		<div class="pagetitle animate1 fadeInRightBig">
			<h5>Export Pajak Keluaran</h5>
			<h1>Update Faktur Keluaran</h1>
		</div>
	</div>
	<!--pageheader-->

	<div class="maincontent">
		<div class="maincontentinner">
			<div class="widgetbox animate2 fadeInRightBig">
				<a class="btn btn-warning" id="addNew"
					href="<c:url value="/faktur/list-pajak-keluaran-detail.page"/>/${fakturKeluaranDetail.idUseFaktur}">
					<font color="white">Kembali ke Daftar No Faktur</font></a><br></br> 
					
				<h4 class="widgettitle">Informasi Faktur Keluaran</h4>
				<div class="widgetcontent nopadding">
					<form 
						id="form-update-pk"
						class="stdform stdform2"
						method="post"
						action="<c:url value="/faktur/updatePKDetailByNoFaktur.json" />"
						enctype="multipart/form-data">
						
						<input type="text" name="idUseFaktur" value="${fakturKeluaranDetail.idUseFaktur }"
								id="idUseFaktur" style="display: none;"  />
								
								
						<p>
							<label for="exampleInputFile">Kode Jenis Transaksi</label>
							<span class="field"> 
							
								<input type="text" name="kdJenisTransaksi" value="${fakturKeluaranDetail.kdJenisTransaksi }"
								id="kdJenisTransaksi" class="form-control"  />
								
							</span>
						</p>
						<p>
							<label for="exampleInputFile">FG Pengganti</label>
							<span class="field"> 
							
								<input type="text" name="fgPengganti" value="${fakturKeluaranDetail.fgPengganti }"
								id="fgPengganti" class="form-control"  />
							
							</span>
						</p>
						<p>
							<label for="exampleInputFile">No Faktur</label>
							<span class="field"> 
								<input type="text" name="noFaktur" value="${fakturKeluaranDetail.noFaktur }"
								id="noFaktur" class="form-control" readonly="readonly" />
							</span>
						</p>
						<p>
							<label for="exampleInputFile">Masa Pajak</label>
							<span class="field"> 
							<input type="text" name="masaPajak" value="${fakturKeluaranDetail.masaPajak }"
								id="masaPajak" class="form-control"  />
							</span>
						</p>
						<p>
							<label for="exampleInputFile">Tahun Pajak</label>
							<span class="field"> 
							<input type="text" name="tahunPajak" value="${fakturKeluaranDetail.tahunPajak }"
								id="tahunPajak" class="form-control"  />
								
							</span>
						</p>
						<p>
							<label for="exampleInputFile">Tanggal Faktur</label>
							<span class="field"> 
							<input type="text" name="tglFaktur" value="${fakturKeluaranDetail.tglFaktur }"
								id="tglFaktur" class="form-control"  />
								
							</span>
						</p>
						<p>
							<label for="exampleInputFile">NPWP</label>
							<span class="field"> 
							
							<input type="text" name="npwp" value="${fakturKeluaranDetail.npwp }"
								id="npwp" class="form-control"  />
								
							</span>
						</p>
						<p>
							<label for="exampleInputFile">Nama</label>
							<span class="field"> 
							<input type="text" name="nama" value="${fakturKeluaranDetail.nama }"
								id="nama" class="form-control"  />
								
							</span>
						</p>
						<p>
							<label for="exampleInputFile">Alamat</label>
							<span class="field"> 
							<input type="text" name="alamat" value="${fakturKeluaranDetail.alamat }"
								id="alamat" class="form-control"  />
								
							</span>
						</p>
						<p>
							<label for="exampleInputFile">Jumlah DPP</label>
							<span class="field"> 
							<input type="text" name="jumlahDpp" value="${fakturKeluaranDetail.jumlahDpp }"
								id="jumlahDpp" class="form-control"  />
								
								
<%-- 							 <fmt:formatNumber type="number"  --%>
<%--              						maxFractionDigits="3" value="${fakturKeluaranDetail.jumlahDpp}" />  --%>
							
								
							</span>
						</p>
						<p>
							<label for="exampleInputFile">Jumlah PPN</label>
							<span class="field"> 
							
							<input type="text" name="jumlahPpn" value="${fakturKeluaranDetail.jumlahPpn }"
								id="jumlahPpn" class="form-control"  />
								
<%-- 							 <fmt:formatNumber type="number"  --%>
<%--             						maxFractionDigits="3" value="${fakturKeluaranDetail.jumlahPpn}" />  --%>
								
							</span>
						</p>
						<p>
							<label for="exampleInputFile">Jumlah PPNBM</label>
							<span class="field"> 
							
							<input type="text" name="jumlahPpnbm" value="${fakturKeluaranDetail.jumlahPpnbm }"
								id="jumlahPpnbm" class="form-control"  />
								
<%-- 							 <fmt:formatNumber type="number"  --%>
<%--              						maxFractionDigits="3" value="${fakturKeluaranDetail.jumlahPpnbm}" />  --%>
								
							</span>
						</p>
						<p>
							<label for="exampleInputFile">ID Keterangan Tambahan</label>
							<span class="field"> 
							<input type="text" name="idKeteranganTambahan" value="${fakturKeluaranDetail.idKeteranganTambahan }"
								id="idKeteranganTambahan" class="form-control"  />
								
							</span>
						</p>
						<p>
							<label for="exampleInputFile">FG Uang Muka</label>
							<span class="field"> 
							
							<input type="text" name="fgUangMuka" value="${fakturKeluaranDetail.fgUangMuka }"
								id="fgUangMuka" class="form-control"  />
								
								
<%-- 							<fmt:formatNumber type="number"  --%>
<%--             						maxFractionDigits="3" value="${fakturKeluaranDetail.fgUangMuka}" /> --%>
								
							</span>
						</p>
						<p>
							<label for="exampleInputFile">Uang Muka DPP</label>
							<span class="field"> 
							<input type="text" name="uangMukaDpp" value="${fakturKeluaranDetail.uangMukaDpp }"
								id="uangMukaDpp" class="form-control"  />
								
								
<%-- 							<fmt:formatNumber type="number"  --%>
<%--             						maxFractionDigits="3" value="${fakturKeluaranDetail.uangMukaDpp}" /> --%>
								
							</span>
						</p>
						<p>
							<label for="exampleInputFile">Uang Muka PPN</label>
							<span class="field"> 
								<input type="text" name="uangMukaPpn" value="${fakturKeluaranDetail.uangMukaPpn }"
								id="uangMukaPpn" class="form-control"  />
								
								
<%-- 							<fmt:formatNumber type="number"  --%>
<%--             						maxFractionDigits="3" value="${fakturKeluaranDetail.uangMukaPpn}" /> --%>
								
							</span>
						</p>
						<p>
							<label for="exampleInputFile">Uang Muka PPNBM</label>
							<span class="field"> 
							
								<input type="text" name="uangMukaPpnbm" value="${fakturKeluaranDetail.uangMukaPpnbm }"
								id="uangMukaPpnbm" class="form-control"  />
								
								
<%-- 							<fmt:formatNumber type="number"  --%>
<%--             						maxFractionDigits="3" value="${fakturKeluaranDetail.uangMukaPpnbm}" /> --%>
								
							</span>
						</p>
						<p>
							<label for="exampleInputFile">Referensi</label>
							<span class="field"> 
							<input type="text" name="referensi" value="${fakturKeluaranDetail.referensi }"
								id="referensi" class="form-control"  />
								
							</span>
						</p>
						
						<p class="stdformbutton">
							<button class="btn btn-success btn-large" id="btn-save">Simpan</button>
							
						</p>
						

						
					</form>
				</div>
				<!--widgetcontent-->
			</div>
			<div class="widget animate2 fadeInRightBig">
				
					 
				<h4 class="widgettitle">Daftar Objek</h4>
				<div class="widgetcontent">
					<table class="table table-striped responsive" id="dyntable">
						<thead>
							<tr>
								<th style="width: 10%">Kode</th>
								<th style="width: 10%">Nama</th>
								<th style="width: 10%">Harga Satuan</th>
								<th style="width: 10%">Jumlah Barang</th>
								<th style="width: 10%">Harga Total</th>
								<th style="width: 10%">Diskon</th>
								<th style="width: 10%">DPP</th>
								<th style="width: 10%">PPN</th>
								<th style="width: 10%">Tarif PPNBM</th>
								<th style="width: 10%">PPNBM</th>
								<th style="width: 5%">Status</th>
								<th></th>
							</tr>
						</thead>
					</table>
				</div>
			</div>
			<!--row-->

			<%@ include file="/common//mainFooter.jsp"%>
			<!--footer-->

		</div>
		<!--maincontentinner-->
	</div>
	<!--maincontent-->

</div>
<!--rightpanel-->