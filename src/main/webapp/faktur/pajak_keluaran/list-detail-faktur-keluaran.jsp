<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<style>
.action-role-table:hover {
	-webkit-transform: scale(1.1);
	-moz-transform: scale(1.1);
	transform: scale(1.1);
}
</style>

<script type="text/javascript">
	var noFaktur = "${fakturKeluaranDetail.idReference}";
	var url_detail = '<c:url value="/faktur/faktur-keluaran-detail/data-table.json" />' +'/'+noFaktur;
	
	$(document).ready(function() {
						$('body').off('click', '.action-edit');
						

						var oTable = $('#dyntable')
								.dataTable(
										{
											"oLanguage" : {
												"sProcessing" : 'Loading Table...'
											},
											"sPaginationType" : "full_numbers",
											"bLengthChange" : false,
											"bFilter" : false,
											"bProcessing" : true,
											"bServerSide" : true,
											"sAjaxSource" : url_detail,
											"aoColumns" : [

													{
														"mData" : "kodeObjek"
													},
													{
														"mData" : "namaObjek",
													},
													{
														"mData" : "hargaSatuan"
													},
													{
														"mData" : "jumlahBarang"
													},
													{
														"mData" : "hargaTotal"
													},
													{
														"mData" : "diskon"
													},
													{
														"mData" : "dpp"
													},
													{
														"mData" : "ppn"
													},
													{
														"mData" : "tarifPpnbm"
													},
													{
														"mData" : "ppnbm"
													},
													{
														"mData" : "kodeObjek",
														"mRender" : function(
																sourceData,
																dataType,
																fullData) {
															
															return '';
															
														}
													}

											]
										});

					

								$('body').on('click','.action-edit',function() {
								

									return false;
								});


					});

	$("a#addNew").click(function() {
		$("#content").load($(this).attr('href'));
		return false;
	});
	
</script>

<div class="rightpanel">
	<ul class="breadcrumbs">
		<li><a href="<c:url value="home.page"/>"><i
				class="iconfa-home"></i></a> <span class="separator"></span></li>
		<li>Export Pajak Keluaran <span class="separator"></span></li>
		<li>Detail Faktur Keluaran</li>
	</ul>

	<div class="pageheader">
		<div class="pageicon animate0 fadeInRightBig">
			<span class="iconfa-laptop"></span>
		</div>
		<div class="pagetitle animate1 fadeInRightBig">
			<h5>Export Pajak Keluaran</h5>
			<h1>Detail Faktur Keluaran</h1>
		</div>
	</div>
	<!--pageheader-->

	<div class="maincontent">
		<div class="maincontentinner">
			<div class="widgetbox animate2 fadeInRightBig">
				<a class="btn btn-warning" id="addNew"
					href="<c:url value="/faktur/list-pajak-keluaran-detail.page"/>/${fakturKeluaranDetail.idUseFaktur}">
					<font color="white">Kembali ke Daftar No Faktur</font></a><br></br> 
					
				<h4 class="widgettitle">Informasi Faktur Keluaran</h4>
				<div class="widgetcontent nopadding">
					<form class="stdform stdform2">
						<p>
							<label for="exampleInputFile">Kode Jenis Transaksi</label>
							<span class="field"> 
								${fakturKeluaranDetail.kdJenisTransaksi}
							</span>
						</p>
						<p>
							<label for="exampleInputFile">FG Pengganti</label>
							<span class="field"> 
								${fakturKeluaranDetail.fgPengganti}
							</span>
						</p>
						<p>
							<label for="exampleInputFile">No Faktur</label>
							<span class="field"> 
								${fakturKeluaranDetail.noFaktur}
							</span>
						</p>
						<p>
							<label for="exampleInputFile">Masa Pajak</label>
							<span class="field"> 
								${fakturKeluaranDetail.masaPajak}
							</span>
						</p>
						<p>
							<label for="exampleInputFile">Tahun Pajak</label>
							<span class="field"> 
								${fakturKeluaranDetail.tahunPajak}
							</span>
						</p>
						<p>
							<label for="exampleInputFile">Tanggal Faktur</label>
							<span class="field"> 
								${fakturKeluaranDetail.tglFaktur}
							</span>
						</p>
						<p>
							<label for="exampleInputFile">NPWP</label>
							<span class="field"> 
								${fakturKeluaranDetail.npwp}
							</span>
						</p>
						<p>
							<label for="exampleInputFile">Nama</label>
							<span class="field"> 
								${fakturKeluaranDetail.nama}
							</span>
						</p>
						<p>
							<label for="exampleInputFile">Alamat</label>
							<span class="field"> 
								${fakturKeluaranDetail.alamat}
							</span>
						</p>
						<p>
							<label for="exampleInputFile">Jumlah DPP</label>
							<span class="field"> 
							 <fmt:formatNumber type="number" 
            						maxFractionDigits="3" value="${fakturKeluaranDetail.jumlahDpp}" />
							
								
							</span>
						</p>
						<p>
							<label for="exampleInputFile">Jumlah PPN</label>
							<span class="field"> 
							 <fmt:formatNumber type="number" 
            						maxFractionDigits="3" value="${fakturKeluaranDetail.jumlahPpn}" />
								
							</span>
						</p>
						<p>
							<label for="exampleInputFile">Jumlah PPNBM</label>
							<span class="field"> 
							 <fmt:formatNumber type="number" 
            						maxFractionDigits="3" value="${fakturKeluaranDetail.jumlahPpnbm}" />
								
							</span>
						</p>
						<p>
							<label for="exampleInputFile">ID Keterangan Tambahan</label>
							<span class="field"> 
								${fakturKeluaranDetail.idKeteranganTambahan}
							</span>
						</p>
						<p>
							<label for="exampleInputFile">FG Uang Muka</label>
							<span class="field"> 
							<fmt:formatNumber type="number" 
            						maxFractionDigits="3" value="${fakturKeluaranDetail.fgUangMuka}" />
								
							</span>
						</p>
						<p>
							<label for="exampleInputFile">Uang Muka DPP</label>
							<span class="field"> 
							<fmt:formatNumber type="number" 
            						maxFractionDigits="3" value="${fakturKeluaranDetail.uangMukaDpp}" />
								
							</span>
						</p>
						<p>
							<label for="exampleInputFile">Uang Muka PPN</label>
							<span class="field"> 
							<fmt:formatNumber type="number" 
            						maxFractionDigits="3" value="${fakturKeluaranDetail.uangMukaPpn}" />
								
							</span>
						</p>
						<p>
							<label for="exampleInputFile">Uang Muka PPNBM</label>
							<span class="field"> 
							<fmt:formatNumber type="number" 
            						maxFractionDigits="3" value="${fakturKeluaranDetail.uangMukaPpnbm}" />
								
							</span>
						</p>
						<p>
							<label for="exampleInputFile">Referensi</label>
							<span class="field"> 
								${fakturKeluaranDetail.referensi}
							</span>
						</p>

						
					</form>
				</div>
				<!--widgetcontent-->
			</div>
			<div class="widget animate2 fadeInRightBig">
				
					 
				<h4 class="widgettitle">Daftar Objek</h4>
				<div class="widgetcontent">
					<table class="table table-striped responsive" id="dyntable">
						<thead>
							<tr>
								<th style="width: 10%">Kode</th>
								<th style="width: 10%">Nama</th>
								<th style="width: 10%">Harga Satuan</th>
								<th style="width: 10%">Jumlah Barang</th>
								<th style="width: 10%">Harga Total</th>
								<th style="width: 10%">Diskon</th>
								<th style="width: 10%">DPP</th>
								<th style="width: 10%">PPN</th>
								<th style="width: 10%">Tarif PPNBM</th>
								<th style="width: 10%">PPNBM</th>
								
								
<!-- 								<th style="width: 5%">STATUS</th> -->
								<th></th>
							</tr>
						</thead>
					</table>
				</div>
			</div>
			<!--row-->

			<%@ include file="/common//mainFooter.jsp"%>
			<!--footer-->

		</div>
		<!--maincontentinner-->
	</div>
	<!--maincontent-->

</div>
<!--rightpanel-->