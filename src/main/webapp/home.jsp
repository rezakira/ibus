<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
    <%@ include file="/common/head.jsp"%>

        <style>
            .menu-loading {
                padding-left: 30px;
                height: 13px;
            }

        </style>

        <script>
            // loading
            var timeoutLoad = 600;


            $(document).ready(function() {
                $('#main_content').ajaxStart($.blockUI).ajaxStop(function(){
                    setTimeout($.unblockUI, timeoutLoad);
                });
                
                $('.menu-loading').hide();

                $('.dropdown > ul > li > a').click(function(){
                  
                    var comp = $(this);
                    $(comp).children('img').show();
                    $("#main_content").load($(this).attr('href'), function(response, status, xhr) {
                        if (response.indexOf("forgot.page") >= 0) {
                            window.location.replace('<c:url value="/login.page" />');
                        } else {
                            setTimeout(function(){
                                $(comp).children('img').hide();
                            }, 600); 
                        }
                    });
                    return false;
                });

                $('.dropdown > a').click(function(){
                    $('.dropdown').removeClass('active');
                    $(this).parent().addClass('active');
                });

                $('#edit-profile').click(function(){
                    // 			$.blockUI();
                    $("#main_content").load($(this).attr('href'), function() {
                        // 				setTimeout($.unblockUI, timeoutLoad);
                    });
                    return false;
                });


                $("#main_content").load('<c:url value="/dashboard.page" />');
                
                
                
            });
			
			function linkToPage(obj)
            {
				$("#main_content").load(obj.getAttribute('data-id'));
            }
            
            function logOutApp()
            {
				location.replace("<c:url value="/j_spring_security_logout" />");
            }
        </script>
        </head>



                <body class="page-header-fixed page-sidebar-closed-hide-logo page-container-bg-solid">
                    <%@ include file="/common/topheader.jsp"%>
                        <!-- BEGIN CONTAINER -->
                        <div class="page-container">
                            <%@ include file="/common/menu.jsp"%>
                                <div class="page-content-wrapper">
                                    <!-- BEGIN CONTENT BODY -->
                                    <div class="page-content" id="main_content">
                                       
                                    </div>
                                </div>
                                </div>
                            <%@ include file="/common/mainFooter.jsp"%>
                                </body>
                            </html>