<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

    <style>
        .btn-saving {
            float: left;
        }

        .btn-saving ~ img {
            padding-left: 10px;
            display: inline !important;
        }

        #btn-save {
            width: 100px;
        }

        #btn-save ~ img {
            display: none;
        }
        .linkTab{
            background: white;
        }
        .disabled{
            border: none;
            background: none !important;
            -moz-appearance: none;
        }
    </style>

    <script>
	var priceUnitDestination = 0;
	var totalPrice = 0;
	var initial = true;
	
		function changePrice()
		{
		
			totalPrice = (priceUnitDestination-(priceUnitDestination*$('#discount').val()/100))*($('#totalPerson').val());
			$('#total_harga_ref').text(totalPrice);
		}
	
		function getListRuteTo()
		{
					
				$.get("<c:url value="/data-master/get-information-to-destination.json" />"+"?id_from="+$('#idRuteFrom').val(), function( data ) {
					
					$('#idRuteTo').find('option').remove().end();
					
					for(var counter=0;counter<data.obj.length;counter++)
					{
						$('#idRuteTo').append($("<option></option>")
						.attr("value",data.obj[counter].id)
						.text(data.obj[counter].value)); 
						
						if(!initial)
						{
							if(counter == 0)$('#idRuteTo').val(data.obj[counter].id);
						}
						else
						{
							
							$('#idRuteTo').val('${orderDetails.idRuteTo}');
							initial = false;
						}
					}
					getRuteToDetail();
					
				});
				
		}
		
		function getRuteToDetail()
		{
				
				$.get("<c:url value="/data-master/get-destination-detail.json" />"+"?id="+$('#idRuteTo').val(), function( data ) {
					priceUnitDestination = data.obj.priceTicketUnit;
					$('#harga_unit_ref').text(priceUnitDestination);
					changePrice();
				});
		}
		
		
        $(document)
            .ready(
            function() {
			
				getListRuteTo();
				
                $('#form_update_bus').validate(
                    {
                        rules : {

                        },
                        messages : {

                        },
                        highlight : function(label) {

                        },
                        success : function(label) {
                            label.text('Ok!').addClass(
                                'valid').closest(
                                '.control-group')
                                .addClass('success');
                        },
                        submitHandler : function(form) {
                            var formData = new FormData(form);



                            $.ajax({
                                url:$(form).attr('action'),
                                type:$(form).attr('method'),
                                data: formData,
                                contentType: false,
                                processData: false
                            }).done(function(data){

                                if(data.status == 1)
                                {
                                    jQuery.jGrowl(data.message, { life: 3000});
                                    $("#main_content").load('<c:url value="/transaction/list_order.page" />');
                                }
                                else
                                {
                                    jQuery.jGrowl(data.message, { life: 3000});

                                }

                            }).fail(function(){
                                var msg = "Failed";
                                jQuery.jGrowl(msg, { life: 3000});
                            });

                            return false;
                        }
                    });

            });
    </script>
    <!-- BEGIN CONTENT -->


    <!-- BEGIN PAGE BAR -->
    <div class="page-bar">
        <ul class="page-breadcrumb">
            <li>
                <a href="<c:url value="home.page"/>">Manajemen Transaksi</a>
                <i class="fa fa-angle-right"></i>
            </li>
            <li>
                <span>Order</span>
            </li>
        </ul>
        <div class="page-toolbar">
            <div class="btn-group pull-right">
                <a class="btn green" href="#"> <i class="fa fa-angle-left"></i> Back
                </a>
            </div>
        </div>
    </div>
    <!-- END PAGE BAR -->
    <!-- BEGIN PAGE TITLE-->
    <h3 class="page-title"> Tambah Order
    </h3>
    <!-- END PAGE TITLE-->
    <!-- END PAGE HEADER-->
    <div class="row">
        <div class="col-md-12">
            <form id="commentForm" method="get" action="" class="form-horizontal">

                <div id="rootwizard">
                    <ul>
                        <li class="linkTab"><a href="#tab1" data-toggle="tab">Form Order</a></li>
                        <li class="linkTab"><a href="#tab2" data-toggle="tab">Tabel Kursi</a></li>
                    </ul>
                    <div class="tab-content">

                        <div class="tab-pane" id="tab1">
                            <div class="portlet light bordered">
                                <div class="portlet-title">
                                    <div class="caption font-dark">
                                        <i class="icon-settings font-dark"></i>
                                        <span class="caption-subject bold uppercase"> Form Order</span>

                                    </div>

                                </div>
                                <div class="portlet-body">
									<input id="idOrder" name="idOrder" value="${orderDetails.idOrder}" style="display:none">
									
                                    <div class="form-group">
                                        <label class="control-label col-md-2" for="name">Nama User</label>
                                        <div class="controls col-md-4">
                                            <select disabled name="userId" id="userId" style="width:100%;" class="form-control disabled select2">
												<c:forEach var="member" items="${list_member}" varStatus="memberStatus">
													 <option value="${member.username}" <c:if test="${member.username eq orderDetails.userId }">selected="selected"</c:if>>${member.username}-${member.firstName} ${member.lastName}</option>
												</c:forEach>
                                               
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-md-2" for="name">Jam</label>
                                        <div class="controls col-md-4">
                                            <select disabled name="scheduleId" id="scheduleId" class="form-control disabled ">
                                               <c:forEach var="schedule" items="${list_schedule}" varStatus="scheduleStatus">
													 <option value="${schedule.idSchedule}" <c:if test="${schedule.idSchedule eq orderDetails.scheduleId }">selected="selected"</c:if>>${schedule.timeStart}-${schedule.timeEnd}</option>
												</c:forEach>
                                            </select>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="control-label col-md-2" for="name">Tanggal</label>
                                        <div class="controls col-md-4">
                                            <input disabled type="text" id="orderDate" name="orderDate" 
											value=" <fmt:formatDate pattern="MM/dd/yyyy" value="${orderDetails.orderDate}" />"
                                                   class="form-control disabled  date-picker ">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-md-2" for="name">Tempat Keberangkatan</label>
                                        <div class="controls col-md-4">
                                            <select disabled name="idRuteFrom" id="idRuteFrom" class="form-control disabled " onchange="getListRuteTo()">
                                                <c:forEach var="ruteFrom" items="${list_rute_from}" varStatus="ruteFromStatus">
													 <option value="${ruteFrom.idRuteFrom}" <c:if test="${ruteFrom.idRuteFrom eq orderDetails.idRuteFrom }">selected="selected"</c:if>>${ruteFrom.nameRuteFrom}</option>
												</c:forEach>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-md-2" for="name">Tujuan</label>
                                        <div class="controls col-md-4">
                                            <select disabled name="idRuteTo" id="idRuteTo" class="form-control disabled " onchange="getRuteToDetail()">
                                                
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-md-2" for="name">Harga Tiket</label>
                                        <div class="controls col-md-4">
                                            <div class="form-control-static">Rp <span id="harga_unit_ref">20.000,</span>-</div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-md-2" for="name">Diskon</label>
                                        <div class="controls col-md-4">
                                            <input disabled type="text" onkeyup="changePrice()" id="discount" name="discount" value="${orderDetails.discount}"
                                                   class="form-control disabled">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-md-2" for="name">Jumlah Penumpang</label>
                                        <div class="controls col-md-4">
                                            <input disabled type="number" onkeyup="changePrice()" id="totalPerson" name="totalPerson" value="${orderDetails.totalPerson}"
                                                   class="form-control disabled ">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-md-2" for="name">Total Harga</label>
                                        <div class="controls col-md-4">
                                            <div class="form-control-static">Rp <span id="total_harga_ref">20.000,</span>-</div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-md-2" for="name">Status</label>
                                        <div class="controls col-md-4">
                                            <select name="status" id="status" class="form-control ">
                                                <option value="1" <c:if test="${orderDetails.status eq '1' }">selected="selected"</c:if>>Booking</option>
                                                <option value="2" <c:if test="${orderDetails.status eq '2' }">selected="selected"</c:if>>Sudah Dibayar</option>
                                                <option value="3" <c:if test="${orderDetails.status eq '3' }">selected="selected"</c:if>>Dibatalkan</option>
                                                <option value="4" <c:if test="${orderDetails.status eq '4' }">selected="selected"</c:if>>Ditolak</option>
												<option value="5" <c:if test="${orderDetails.status eq '5' }">selected="selected"</c:if>>Selesai</option>
                                            </select>
                                        </div>
                                    </div>
									<div class="form-group">
                                        <label class="control-label col-md-2" for="name">QR Code</label>
                                        <div class="controls col-md-4">
                                            <img src="http://localhost:8080/img_ibus/image_QR_Code/${eTicketDetails.path}" onerror="this.src='http://localhost:8080/i-bus/images/ibus.png'" width="50%">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="tab-pane" id="tab2">
                            

                            <div class="row">
                                <div class="col-md-12">
                                    <!-- BEGIN EXAMPLE TABLE PORTLET-->
                                    <div class="portlet light bordered">
                                        <div class="portlet-title">
                                            <div class="caption font-dark">
                                                <i class="icon-settings font-dark"></i> <span
                                                                                              class="caption-subject bold uppercase"> Tabel Kursi</span>
                                            </div>

                                        </div>
                                        <div class="actions">
                                            
                                        </div>
                                        <div class="portlet-body">


                                            <table
                                                   class="table table-striped table-bordered table-hover table-checkable order-column"
                                                   id="dyntable">
                                                <thead>
                                                    <tr>
                                                        <th>No Kursi</th>
                                                        <th>Status</th>
                                                        <th>Tanggal Update</th>
                                                    </tr>
                                                </thead>
                                                
                                            </table>
                                        </div>
                                    </div>
                                    <!-- END EXAMPLE TABLE PORTLET-->
                                </div>
                            </div>
                            <div class="btn-group">
                                
                            </div>
                        </div>
                        <ul class="pager wizard">
                            <li class="previous"><a href="javascript:;">Previous</a></li>
                            <li class="next"><a href="javascript:;">Next</a></li>
                            <li class="next finish" style="display: none;"><a href="javascript:;">Finish</a></li>
                        </ul>
                    </div>
                </div>
            </form>


        </div>
    </div>
    <script>
        $(document).ready(function() {
			if(${orderDetails.status} == "2")
			{
				$("#status option[value='1']").remove();
				$("#status option[value='4']").remove();
			}
			else if(${orderDetails.status} == "5")
			{
				$("#status option[value='1']").remove();
				$("#status option[value='2']").remove();
				$("#status option[value='3']").remove();
				$("#status option[value='4']").remove();
			}
			
            $("#userId").select2();
            $('#orderDate').datepicker({autoclose:true});
			// dynamic table
		var oTable = $('#dyntable').dataTable({
			"oLanguage": {
	            "sProcessing": 'Loading Table...'
	        },
			"sPaginationType" : "full_numbers",
			"bLengthChange": false,
	        "bFilter": false,
			"bProcessing": true,
			"bServerSide": true,
            "bAutoWidth": true,
            "aoColumns" : [
            { sWidth: '20%' },
            { sWidth: '20%' },
            { sWidth: '20%' },
            { sWidth: '20%' },
            { sWidth: '20%' }
        ],  
			"sAjaxSource": '<c:url value="/transaction/order/data-table-sit.json/${orderDetails.idOrder}" />',
			"aoColumns": [
			              
						  { "mData": "noKursi" },
						  { "mData" : "status" ,
							"mRender": function (sourceData, dataType, fullData) {
								if(fullData.status == '1'){
									return	 '<span data-id="'+fullData.status+'">'
									+'BOOKING</span>'
								}
								else if(fullData.status == '2'){
									return	 '<span data-id="'+fullData.status+'">'
									+'DIBAYAR</span>'
								}
								else if(fullData.status == '3'){
									return	 '<span data-id="'+fullData.status+'">'
									+'DIBATALKAN</span>'
								}
								else if(fullData.status == '4'){
									return	 '<span data-id="'+fullData.status+'">'
									+'DITOLAK</span>'
								}
							}
						  },
						  { "mData": "dateModified"},
			             
			]
		});
		
            var oTable = $('#dyntables').dataTable({
                "oLanguage": {
                    "sProcessing": 'Loading Table...'
                },
                "sPaginationType" : "full_numbers",
                "bLengthChange": false,
                "bFilter": false,
                "bProcessing": true,
                "bServerSide": true,
                "bAutoWidth": false,
                "aoColumns" : [
                    { sWidth: '25%' },
                    { sWidth: '25%' },
                    { sWidth: '25%' },
                    { sWidth: '25%' },
                ],  
                "sAjaxSource": '<c:url value="/data-master/bus/data-table.json" />',
                "aoColumns": [
                    { "mData" : 'idBus',
                     "mRender": function (sourceData, dataType, fullData) {

                         //return	 '<a class="btn btn-icon-only tooltips  green action-menu-view-menucms" href="javascript:;">'
                         //		 +'<i class="fa fa-search"></i><span class="tooltiptext">Edit</span></a>'
                         return	 '<a class="btn btn-icon-only tooltips  blue action-menu-edit-bus" href="javascript:;" data-id="'+fullData.idBus+'">'
                             +'<i class="fa fa-edit"></i><span class="tooltiptext">Edit</span></a>'
                             +'<a class="btn btn-icon-only tooltips  red action-menu-delete-bus" href="javascript:;" data-id="'+fullData.idBus+'">'
                             +'<i class="fa fa-times"></i><span class="tooltiptext">Delete</span></a>';

                     }
                    },
                    { "mData": "nameBus"},
                    { "mData": "specsBus" },
                    { "mData": "dateModified"}

                ]
            });
        });

        $('#rootwizard .finish').click(function() {
           $("#main_content").load('<c:url value="/transaction/list_order.page" />');
        });
        $('#rootwizard').bootstrapWizard({
            'onTabShow': function(tab, navigation, index) {
                var $total = navigation.find('li').length;
                var $current = index+1;
                var $percent = ($current/$total) * 100;
                $('#rootwizard').find('.bar').css({width:$percent+'%'});

                // If it's the last tab then hide the last button and show the finish instead
                if($current >= $total) {
                    $('#rootwizard').find('.pager .next').hide();
                    $('#rootwizard').find('.pager .finish').show();
                    $('#rootwizard').find('.pager .finish').removeClass('disabled');
                } else {
                    $('#rootwizard').find('.pager .next').show();
                    $('#rootwizard').find('.pager .finish').hide();
                }

            },
            'tabClass': 'nav nav-pills',
            'onNext': function(tab, navigation, index) {
                //var $valid = $("#commentForm").valid();
                //if(!$valid) {
                //$validator.focusInvalid();
                //return false;
                //}
				
                var isNext = false;
                if(index == 1)
                {
                    var formData = new FormData();
					formData.append('idOrder', $('#idOrder').val());
                    formData.append('userId', $('#userId').val());
                    formData.append('scheduleId', $('#scheduleId').val());
                    formData.append('idRuteFrom', $('#idRuteFrom').val());
                    formData.append('idRuteTo', $('#idRuteTo').val());
					formData.append('priceTicketUnit', priceUnitDestination);
                    formData.append('discount', $('#discount').val());
                    formData.append('totalPerson', $('#totalPerson').val());
                    formData.append('totalPrice', totalPrice);
					formData.append('status', $('#status').val());
					formData.append('orderDate', $('#orderDate').val());
					
					var url_update = '';
					if($('#idOrder').val() == '0')
					{	
						url_update = '<c:url value="/transaction/saveOrder.json" />';
					}
					else
					{
						url_update = '<c:url value="/transaction/updateOrder.json" />';
					}

                    $.ajax({
                        url:url_update,
                        type:'post',
                        data: formData,
                        contentType: false,
                        processData: false,
                        async: false
                    }).done(function(data){

                        if(data.status == 1)
                        {
                            jQuery.jGrowl(data.message, { life: 3000});
							
							$('#idOrder').val(data.obj.idOrder);
                            isNext=true;
                        }
                        else
                        {
                            jQuery.jGrowl(data.message, { life: 3000});
                        }

                    }).fail(function(){
                        var msg = "Failed";
                        jQuery.jGrowl(msg, { life: 3000});
                    });
                }


                return isNext;
            }
        });	

    </script>

