<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

    <style>
        .btn-saving {
            float: left;
        }

        .btn-saving ~ img {
            padding-left: 10px;
            display: inline !important;
        }

        #btn-save {
            width: 100px;
        }

        #btn-save ~ img {
            display: none;
        }
        .linkTab{
            background: white;
            display: none !important;
        }
    </style>

    <script>
	var priceUnitDestination = 0;
	var totalPrice = 0;
	var totalBookedSit = 0;
	var selectedSit = 0;
	
		function changePrice()
		{
		
			totalPrice = (priceUnitDestination-(priceUnitDestination*$('#discount').val()/100))*($('#totalPerson').val());
			$('#total_harga_ref').text(totalPrice);
		}
	
		function getListRuteTo()
		{
				$.get("<c:url value="/data-master/get-information-to-destination.json" />"+"?id_from="+$('#idRuteFrom').val(), function( data ) {
					
					
					$('#idRuteTo').find('option').remove().end();
					
					for(var counter=0;counter<data.obj.length;counter++)
					{
						$('#idRuteTo').append($("<option></option>")
						.attr("value",data.obj[counter].id)
						.text(data.obj[counter].value)); 
						
						if(counter == 0)$('#idRuteTo').val(data.obj[counter].id);
					}
					getRuteToDetail();
					
				});
				
		}
		
		function getRuteToDetail()
		{
				
				$.get("<c:url value="/data-master/get-destination-detail.json" />"+"?id="+$('#idRuteTo').val(), function( data ) {
					priceUnitDestination = data.obj.priceTicketUnit;
					$('#harga_unit_ref').text(priceUnitDestination);
					changePrice();
				});
		}
		
		
        $(document)
            .ready(
            function() {
			
				getListRuteTo();
				
                $('#form_update_bus').validate(
                    {
                        rules : {

                        },
                        messages : {

                        },
                        highlight : function(label) {

                        },
                        success : function(label) {
                            label.text('Ok!').addClass(
                                'valid').closest(
                                '.control-group')
                                .addClass('success');
                        },
                        submitHandler : function(form) {
                            var formData = new FormData(form);



                            $.ajax({
                                url:$(form).attr('action'),
                                type:$(form).attr('method'),
                                data: formData,
                                contentType: false,
                                processData: false
                            }).done(function(data){

                                if(data.status == 1)
                                {
                                    jQuery.jGrowl(data.message, { life: 3000});
                                    $("#main_content").load('<c:url value="/transaction/list_order.page" />');
                                }
                                else
                                {
                                    jQuery.jGrowl(data.message, { life: 3000});

                                }

                            }).fail(function(){
                                var msg = "Failed";
                                jQuery.jGrowl(msg, { life: 3000});
                            });

                            return false;
                        }
                    });

            });
    </script>
    <!-- BEGIN CONTENT -->


    <!-- BEGIN PAGE BAR -->
    <div class="page-bar">
        <ul class="page-breadcrumb">
            <li>
                <a href="<c:url value="home.page"/>">Manajemen Transaksi</a>
                <i class="fa fa-angle-right"></i>
            </li>
            <li>
                <span>Order</span>
            </li>
        </ul>
        <div class="page-toolbar">
            <div class="btn-group pull-right">
                <a class="btn green" href="#"> <i class="fa fa-angle-left"></i> Back
                </a>
            </div>
        </div>
    </div>
    <!-- END PAGE BAR -->
    <!-- BEGIN PAGE TITLE-->
    <h3 class="page-title"> Tambah Order
    </h3>
    <!-- END PAGE TITLE-->
    <!-- END PAGE HEADER-->
    <div class="row">
        <div class="col-md-12">
            <form id="commentForm" method="get" action="" class="form-horizontal">

                <div id="rootwizard">
                    <ul>
                        <li class="linkTab"><a href="#tab1" data-toggle="tab">Form Order</a></li>
                        <li class="linkTab"><a href="#tab2" data-toggle="tab">Tabel Kursi</a></li>
                    </ul>
                    <div class="tab-content">

                        <div class="tab-pane" id="tab1">
                            <div class="portlet light bordered">
                                <div class="portlet-title">
                                    <div class="caption font-dark">
                                        <i class="icon-settings font-dark"></i>
                                        <span class="caption-subject bold uppercase"> Step 1 - Form Order</span>

                                    </div>

                                </div>
                                <div class="portlet-body">
									<input id="idOrder" name="idOrder" value="0" style="display:none">
									
                                    <div class="form-group">
                                        <label class="control-label col-md-2" for="name">Nama User</label>
                                        <div class="controls col-md-4">
                                            <select name="userId" id="userId" style="width:100%;" class="form-control select2">
												<c:forEach var="member" items="${list_member}" varStatus="memberStatus">
													 <option value="${member.username}" <c:if test="${memberStatus.index eq 0 }">selected="selected"</c:if>>${member.username}-${member.firstName} ${member.lastName}</option>
												</c:forEach>
                                               
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-md-2" for="name">Jam</label>
                                        <div class="controls col-md-4">
                                            <select name="scheduleId" id="scheduleId" class="form-control ">
                                               <c:forEach var="schedule" items="${list_schedule}" varStatus="scheduleStatus">
													 <option value="${schedule.idSchedule}" <c:if test="${scheduleStatus.index eq 0 }">selected="selected"</c:if>>${schedule.timeStart}-${schedule.timeEnd}</option>
												</c:forEach>
                                            </select>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="control-label col-md-2" for="name">Tanggal</label>
                                        <div class="controls col-md-4">
                                            <input type="text" id="orderDate" name="orderDate"
                                                   class="form-control  date-picker ">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-md-2" for="name">Tempat Keberangkatan</label>
                                        <div class="controls col-md-4">
                                            <select name="idRuteFrom" id="idRuteFrom" class="form-control " onchange="getListRuteTo()">
                                                <c:forEach var="ruteFrom" items="${list_rute_from}" varStatus="ruteFromStatus">
													 <option value="${ruteFrom.idRuteFrom}" <c:if test="${ruteFromStatus.index eq 0 }">selected="selected"</c:if>>${ruteFrom.nameRuteFrom}</option>
												</c:forEach>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-md-2" for="name">Tujuan</label>
                                        <div class="controls col-md-4">
                                            <select name="idRuteTo" id="idRuteTo" class="form-control " onchange="getRuteToDetail()">
                                                
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-md-2" for="name">Harga Tiket</label>
                                        <div class="controls col-md-4">
                                            <div class="form-control-static">Rp <span id="harga_unit_ref">20.000,</span>-</div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-md-2" for="name">Diskon</label>
                                        <div class="controls col-md-4">
                                            <input type="text" onkeyup="changePrice()" id="discount" name="discount" value="0"
                                                   class="form-control">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-md-2" for="name">Jumlah Penumpang</label>
                                        <div class="controls col-md-4">
                                            <input type="number" onkeyup="changePrice()" id="totalPerson" name="totalPerson" value="1"
                                                   class="form-control ">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-md-2" for="name">Total Harga</label>
                                        <div class="controls col-md-4">
                                            <div class="form-control-static">Rp <span id="total_harga_ref">20.000,</span>-</div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-md-2" for="name">Status</label>
                                        <div class="controls col-md-4">
                                            <select name="status" id="status" class="form-control ">
                                                <option value="1" selected>Booking</option>
                                                <option value="2">Sudah Dibayar</option>
												<option value="5">Selesai</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="tab-pane" id="tab2">
                            

                            <div class="row">
                                <div class="col-md-12">
                                    <!-- BEGIN EXAMPLE TABLE PORTLET-->
                                    <div class="portlet light bordered">
                                        <div class="portlet-title">
                                            <div class="caption font-dark">
                                                <i class="icon-settings font-dark"></i> <span
                                                                                              class="caption-subject bold uppercase">Step 2 - Tabel Kursi</span>
                                            </div>

                                        </div>
                                        <div class="actions">
                                            
                                        </div>
                                        <div class="portlet-body">
                                            Jumlah Kursi Yang Dipesan Tinggal <span id="sisa_kursi_dipesan">0</span><br></br>


                                            <table
                                                   class="table table-striped table-bordered table-hover table-checkable order-column"
                                                   id="dyntable">
                                                <thead>
                                                    <tr>
                                                        <th>Action</th>
                                                        <th>No Kursi</th>
                                                        <th>Status</th>
                                                        <th>Tanggal Update</th>
                                                    </tr>
                                                </thead>
                                                <tbody id="body_data_sit_order">
													<!--
                                                    <tr>
                                                        <td>TST 1</td>
                                                        <td>TST 1</td>
                                                        <td>TST 1</td>
                                                        <td>TST 1</td>
                                                    </tr>-->
                                                   
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                    <!-- END EXAMPLE TABLE PORTLET-->
                                </div>
                            </div>
                            <div class="btn-group">
                                
                            </div>
                        </div>
                        <ul class="pager wizard">
                            <li class="previous"><a href="javascript:;">Previous</a></li>
                            <li class="next"><a href="javascript:;">Next</a></li>
                            <li class="next finish" style="display: none;"><a href="javascript:;">Finish</a></li>
                        </ul>
                    </div>
                </div>
            </form>


        </div>
    </div>
    <script>
        $(document).ready(function() {
            $("#userId").select2();
            $('#orderDate').datepicker({
                autoclose:true,
                startDate: '0d'
            });
			
           
        });
		
		function checkedChoiceSit(obj)
		{
		
			
			
				if(document.getElementById(obj.getAttribute('id')).checked) 
				{
					if(selectedSit < 1)
					{
						alert('Jumlah kursi yanga dapat dipesan sudah habis');
						document.getElementById(obj.getAttribute('id')).checked = false;
						
					}
					else
					{
						selectedSit-=1;
						$('#sisa_kursi_dipesan').html('<strong>'+selectedSit+'</strong>');
						
						var formData = new FormData();
						formData.append('idSitOrder', obj.getAttribute('id'));
						formData.append('noKursi', obj.getAttribute('data-nokursi'));
						formData.append('idOrderRef', $('#idOrder').val());
						formData.append('scheduleOtRefId', obj.getAttribute('data-scheduleOTId'));
						
						
						$.ajax({
							url:'<c:url value="/transaction/saveSitOrder.json" />',
							type:'post',
							data: formData,
							contentType: false,
							processData: false,
							async: false
						}).done(function(data){

							if(data.status == 1)
							{
								jQuery.jGrowl(data.message, { life: 3000});
							
							}
							else
							{
								jQuery.jGrowl(data.message, { life: 3000});
							}

						}).fail(function(){
							var msg = "Failed";
							jQuery.jGrowl(msg, { life: 3000});
						});
					}		
				} 			
				else {
						selectedSit+=1;
						$('#sisa_kursi_dipesan').html('<strong>'+selectedSit+'</strong>');
						
						
						$.ajax({
							url:'<c:url value="/transaction/removeSitOrder.json" />',
							type:'post',
							data: {
								"id_sit_order":obj.getAttribute('id')
							},
						}).done(function(data){

							if(data.status == 1)
							{
								jQuery.jGrowl(data.message, { life: 3000});
							
							}
							else
							{
								jQuery.jGrowl(data.message, { life: 3000});
							}

						}).fail(function(){
							var msg = "Failed";
							jQuery.jGrowl(msg, { life: 3000});
						});
							
				}
			
			
			
		
		}

        $('#rootwizard .finish').click(function() {
             $("#main_content").load('<c:url value="/transaction/list_order.page" />');
        });
        $('#rootwizard').bootstrapWizard({
            'onTabShow': function(tab, navigation, index) {
                var $total = navigation.find('li').length;
                var $current = index+1;
                var $percent = ($current/$total) * 100;
                $('#rootwizard').find('.bar').css({width:$percent+'%'});

                // If it's the last tab then hide the last button and show the finish instead
                if($current >= $total) {
                    $('#rootwizard').find('.pager .next').hide();
                    $('#rootwizard').find('.pager .finish').show();
                    $('#rootwizard').find('.pager .finish').removeClass('disabled');
                } else {
                    $('#rootwizard').find('.pager .next').show();
                    $('#rootwizard').find('.pager .finish').hide();
                }

            },
            'tabClass': 'nav nav-pills',
            'onNext': function(tab, navigation, index) {
   
				
                var isNext = false;
                if(index == 1)
                {
                    var formData = new FormData();
					formData.append('idOrder', $('#idOrder').val());
                    formData.append('userId', $('#userId').val());
                    formData.append('scheduleId', $('#scheduleId').val());
                    formData.append('idRuteFrom', $('#idRuteFrom').val());
                    formData.append('idRuteTo', $('#idRuteTo').val());
					formData.append('priceTicketUnit', priceUnitDestination);
                    formData.append('discount', $('#discount').val());
                    formData.append('totalPerson', $('#totalPerson').val());
                    formData.append('totalPrice', totalPrice);
					formData.append('status', $('#status').val());
					formData.append('orderDate', $('#orderDate').val());
					
					var url_update = '';
					if($('#idOrder').val() == '0')
					{	
						url_update = '<c:url value="/transaction/saveOrder.json" />';
					}
					else
					{
						url_update = '<c:url value="/transaction/updateOrder.json" />';
					}

                    $.ajax({
                        url:url_update,
                        type:'post',
                        data: formData,
                        contentType: false,
                        processData: false,
                        async: false
                    }).done(function(data){

                        if(data.status == 1)
                        {
                            jQuery.jGrowl(data.message, { life: 3000});
							
							$('#idOrder').val(data.obj.idOrder);
							totalBookedSit = $('#totalPerson').val();
							selectedSit = totalBookedSit*1;
							
							$('#sisa_kursi_dipesan').html('<strong>'+selectedSit+'</strong>');
							
							$("#body_data_sit_order").empty();
							
							for(var i=0;i<data.obj.listSit.length;i++)
							{
			
								$("#body_data_sit_order").append('<tr>'+
								'<td>'+(data.obj.listSit[i].status=="3"?'<input type="checkbox" data-scheduleOTId="'+data.obj.listSit[i].scheduleOT+'" data-nokursi="'+data.obj.listSit[i].noKursi+'" onchange="checkedChoiceSit(this);return false;" id="'+data.obj.listSit[i].idSitOrder+'"/>':'')+'</td>'+
								'<td>'+data.obj.listSit[i].noKursi+'</td>'+
								'<td>'+data.obj.listSit[i].statusString+'</td>'+
								'<td>'+data.obj.listSit[i].dateStringLastUpdate+'</td>'+
								'</tr>');
							}
							
                            isNext=true;
                        }
                        else
                        {
                            jQuery.jGrowl(data.message, { life: 3000});
                        }

                    }).fail(function(){
                        var msg = "Failed";
                        jQuery.jGrowl(msg, { life: 3000});
                    });
                }


                return isNext;
            }
        });	

    </script>

