<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>


    <style>
        .btn-saving {
            float: left;
        }

        .btn-saving ~ img {
            padding-left: 10px;
            display: inline !important;
        }

        #btn-save {
            width: 100px;
        }

        #btn-save ~ img {
            display: none;
        }
		
		#colAction{
			width: 200px;
		}

    </style>
    <!-- BEGIN CONTENT -->
    <script>
        $(document)
            .ready(
            function() {
			
                $('#form_proceed_order').validate(
                    {
                        rules : {
                        	
                        },
                        messages : {

                        },
                        highlight : function(label) {

                        },
                        success : function(label) {
                            label.text("OK!").addClass(
                                'valid').closest(
                                '.control-group')
                                .addClass('success');
                        },
                        submitHandler : function(form) {
                            var formData = new FormData(form);



                            $.ajax({
                                url:$(form).attr('action'),
                                type:$(form).attr('method'),
                                data: formData,
                                contentType: false,
                                processData: false
                            }).done(function(data){
								
                                if(data.status == 1)
                                {
                                    jQuery.jGrowl(data.message, { life: 3000});
                                    $("#main_content").load('<c:url value="/transaction/list_order.page" />');
                                }
                                else
                                {
                                    jQuery.jGrowl(data.message, { life: 3000});

                                }

                            }).fail(function(){
                                var msg = "Failed";
                                jQuery.jGrowl(msg, { life: 3000});
                            });

                            return false;
                        }
                    });

            });
    </script>

    <!-- BEGIN PAGE BAR -->
    <div class="page-bar">
        <ul class="page-breadcrumb">
            <li>
                <a href="<c:url value="home.page"/>">Home</a>
                <i class="fa fa-angle-right"></i>
            </li>
            <li>
                <span>Detail &amp; Order</span>
            </li>
        </ul>
        <div class="page-toolbar">
            <div class="btn-group pull-right">
                <a class="btn green" href="#"> <i class="fa fa-angle-left"></i> Back
                </a>

            </div>
        </div>
    </div>
    <!-- END PAGE BAR -->
    <!-- BEGIN PAGE TITLE-->
    <h3 class="page-title"> Detail Order
    </h3>
	
    <!-- END PAGE TITLE-->
    <!-- END PAGE HEADER-->
    <div class="row">
        <div class="col-md-12 ">
            <!-- BEGIN SAMPLE FORM PORTLET-->
            <div class="portlet light bordered">
                <div class="portlet-title">
                    <div class="caption font-red-sunglo">
                        <i class="icon-settings font-red-sunglo"></i>
                        <span class="caption-subject bold uppercase"> Detail Order </span>
                    </div>

                </div>
                <div class="portlet-body form">
                    <form id="form_proceed_order" class="stdform stdform2" 
                          method="post" action="<c:url value="/transaction/proceed_order.json" />" 
                          enctype="multipart/form-data">
                        <div class="form-body">
							<input type="text" value="${orderDetails.idOrder}"
							id="idOrder" name="idOrder" style="display: none"
							class="form-control">
							<input type="text" value="${orderDetails.userId}"
							id="userId" name="userId" style="display: none"
							class="form-control">
							<input type="text" value="${orderDetails.scheduleId}"
							id="scheduleId" name="scheduleId" style="display: none"
							class="form-control">
							<input type="text" value="${orderDetails.idRuteFrom}"
							id="idRuteFrom" name="idRuteFrom" style="display: none"
							class="form-control">
							<input type="text" value="${orderDetails.idRuteTo}"
							id="idRuteTo" name="idRuteTo" style="display: none"
							class="form-control">
							<input type="text" value="${orderDetails.priceTicketUnit}"
							id="priceTicketUnit" name="priceTiketUnit" style="display: none"
							class="form-control">
							<input type="text" value="${orderDetails.discount}"
							id="discount" name="discount" style="display: none"
							class="form-control">
							<input type="text" value="${orderDetails.totalPerson}"
							id="totalPerson" name="totalPerson" style="display: none"
							class="form-control">
							<input type="text" value="${orderDetails.totalPrice}"
							id="totalPrice" name="totalPrice" style="display: none"
							class="form-control">
							<input type="text" value="${orderDetails.status}"
							id="status" name="status" style="display: none"
							class="form-control">
							<input type="text" value="${orderDetails.qrcode}"
							id="qrcode" name="qrcode" style="display: none"
							class="form-control">
							<input type="text" value="${orderDetails.orderDate}"
							id="orderDate" name="orderDate" style="display: none"
							class="form-control">
							<div class="form-group form-md-line-input ">
								<div class="col-md-2">
									<label for="">ID Order</label>
									<span class="help-block">ID Order</span>
								</div>
								<div class="col-md-2">
									<span>${orderDetails.idOrder}</span>
								</div>
                            </div>
							
                            <div class="form-group form-md-line-input ">
								<div class="col-md-2">
									<label for="">User ID</label>
									<span class="help-block">User ID</span>
								</div>
								<div class="col-md-2">
									<span>${orderDetails.userId}</span>
								</div>
                            </div>
							
							<div class="form-group form-md-line-input ">
								<div class="col-md-2">
									<label for="">Schedule</label>
									<span class="help-block">Schedule</span>
                                </div>
								<div class="col-md-2">
									<span>${scheduleDetails.timeStart} - ${scheduleDetails.timeEnd}</span>
								</div>
                            </div>
							
                            <div class="form-group form-md-line-input ">
								<div class="col-md-2">
									<label for="">Rute</label>
									<span class="help-block">Rute</span>
                                </div>
								<div class="col-md-2">
									<span>${ruteFromDetails.nameRuteFrom} - ${ruteToDetails.nameRuteTo}</span>
								</div>
                            </div>
					
                            <div class="form-group form-md-line-input ">
								<div class="col-md-2">
									<label for="">Harga Tiket Unit</label>
									<span class="help-block">Harga Tiket Unit</span>
                                </div>
								<div class="col-md-2">
									<span>${orderDetails.priceTicketUnit}</span>
								</div>
                            </div>
							
							<div class="form-group form-md-line-input ">
								<div class="col-md-2">
									<label for="">Discount</label>
									<span class="help-block">Discount</span>
                                </div>
								<div class="col-md-2">
									<span>${orderDetails.discount}</span>
								</div>
                            </div>
							
							<div class="form-group form-md-line-input ">
								<div class="col-md-2">
									<label for="">Jumlah Penumpang</label>
									<span class="help-block">Jumlah Penumpang</span>
                                </div>
								<div class="col-md-2">
									<span>${orderDetails.totalPerson}</span>
								</div>
                            </div>
                             
							<div class="form-group form-md-line-input ">
								<div class="col-md-2">
									<label for="">Total Harga Tiket</label>
									<span class="help-block">Total Harga Tiket</span>
                                </div>
								<div class="col-md-2">
									<span>${orderDetails.totalPrice}</span>
								</div>
							</div>
							
							<div class="form-group form-md-line-input ">
								<div class="col-md-2">
									<label for="">Status Terakhir</label>
									<span class="help-block">Status Terakhir</span>
                                </div>
								<div class="col-md-2">
									<span id="labelStatus"></span>
								</div>
                            </div>
							
							<div class="form-group form-md-line-input ">
								<div class="col-md-2">
									<label for="">Tanggal Order</label>
									<span class="help-block">Tanggal Order</span>
								</div>
								<div class="col-md-2">
									<span id="labelOrderDate">${orderDateDetails}</span>
								</div>
                            </div>
							<div class="form-actions noborder">
								<button id="buttonSubmit" type="submit" class="btn blue">Submit</button>
							</div>
                    </form>
                </div>
            </div>
            <!-- END SAMPLE FORM PORTLET-->

        </div>
    </div>
	
	<script>
		$(document).ready(function() {
			if(${orderDetails.status} == "1")
			{
				document.getElementById("labelStatus").innerHTML = "Booking";
			}
			else if(${orderDetails.status} == "2")
			{
				document.getElementById("labelStatus").innerHTML = "Dibayar";
			}
			else if(${orderDetails.status} == "3")
			{
				document.getElementById("labelStatus").innerHTML = "Dibatalkan";
			}
			else if(${orderDetails.status} == "4")
			{
				document.getElementById("labelStatus").innerHTML = "Ditolak";
			}
			else if(${orderDetails.status} == "5")
			{
				document.getElementById("labelStatus").innerHTML = "Selesai";
				$( "#buttonSubmit" ).remove();
			}
		});
	</script>