<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

    <style>
        .btn-saving {
            float: left;
        }

        .btn-saving ~ img {
            padding-left: 10px;
            display: inline !important;
        }

        #btn-save {
            width: 100px;
        }

        #btn-save ~ img {
            display: none;
        }
		
		img{
			border:0;
		}
		#v{
			width:320px;
			height:240px;
		}
		#qr-canvas{
			display:none;
		}
		#qrfile{
			width:320px;
			height:240px;
		}
		#mp1{
			text-align:center;
			font-size:35px;
		}
		#imghelp{
			position:relative;
			left:0px;
			top:-160px;
			z-index:100;
			font:18px arial,sans-serif;
			background:#f0f0f0;
			margin-left:35px;
			margin-right:35px;
			padding-top:10px;
			padding-bottom:10px;
			border-radius:20px;
		}
		.selector{
			margin:0;
			padding:0;
			cursor:pointer;
			margin-bottom:-5px;
		}
		#outdiv
		{
			margin-left:30%;
			width:320px;
			height:240px;
			border: solid;
			border-width: 3px 3px 3px 3px;
		}
		#result{
			margin-left:12%;
			border: solid;
			border-width: 1px 1px 1px 1px;
			padding:20px;
			width:70%;
		}
		#down{
			margin-left:42%;
		}
		
		#submit{
			margin-left:40%;
		}
        
    </style>
	
	<script>
		$(document)
            .ready(
            function() {
                $('#form_check_qr_code').validate(
                    {
                        rules : {
                        	
                        },
                        messages : {

                        },
                        highlight : function(label) {

                        },
                        success : function(label) {
                            label.text("OK!").addClass(
                                'valid').closest(
                                '.control-group')
                                .addClass('success');
                        },
                        submitHandler : function(form) {
                            var formData = new FormData(form);



                            $.ajax({
                                url:$(form).attr('action'),
                                type:$(form).attr('method'),
                                data: formData,
                                contentType: false,
                                processData: false
                            }).done(function(data){
								
                                if(data.status == 1)
                                {
                                    jQuery.jGrowl(data.message, { life: 3000});
									var x = $('#id').val();
									var str1 = $('#id').val();
									var str2 = '<c:url value="/transaction/order/detail_order.page/" />';
									var res = str2.concat(str1);
                                    $("#main_content").load(res);
									console.log(x);
                                }
                                else
                                {
                                    jQuery.jGrowl(data.message, { life: 3000});

                                }

                            }).fail(function(){
                                var msg = "Failed";
                                jQuery.jGrowl(msg, { life: 3000});
                            });

                            return false;
                        }
                    });

            });
	</script>

    <!-- BEGIN CONTENT -->


    <!-- BEGIN PAGE BAR -->
    <div class="page-bar">
        <ul class="page-breadcrumb">
            <li>
                <a href="<c:url value="home.page"/>">Cek QR Code</a>
                <i class="fa fa-angle-right"></i>
            </li>
            <li>
                <span>Cek QR Code</span>
            </li>
        </ul>
        <div class="page-toolbar">
            <div class="btn-group pull-right">
                <a class="btn green" href="#"> <i class="fa fa-angle-left"></i> Back
                </a>

            </div>
        </div>
    </div>
    <!-- END PAGE BAR -->
    <!-- BEGIN PAGE TITLE-->
    <h3 class="page-title"> Cek QR Code
    </h3>
    <!-- END PAGE TITLE-->
    <!-- END PAGE HEADER-->
    <div class="row">
        <div class="col-md-12 ">
            <!-- BEGIN SAMPLE FORM PORTLET-->
            <div class="portlet light bordered">
                <div class="portlet-title">
                    <div class="caption font-red-sunglo">
                        <i class="icon-settings font-red-sunglo"></i>
                        <span class="caption-subject bold uppercase"> Cek QR Code </span>
                    </div>

                </div>
                <div class="portlet-body form">
                    <form id="form_check_qr_code" class="stdform stdform2" 
					method="POST" action="<c:url value="/transaction/checkqr.json" />" enctype="multipart/form-data">
                        
						<div class="form-body">
							<div id="mainbody">
								<img class="selector" id="webcamimg" src="<c:url value="/images/vid.png"/>" onclick="setwebcam()" align="left" />
								<img class="selector" id="qrimg" src="<c:url value="/images/cam.png"/>" onclick="setimg()" align="right"/>
								<div id="outdiv">
								</div>
								<img id="down" src="<c:url value="/images/down.png"/>"/>
								<div id="result" onchange="fillInput()"></div>
								<input type="text" id="id" name="id" style="display:none"></input>
								<canvas id="qr-canvas" width="800" height="600"></canvas>
								<script type="text/javascript">load();</script>
								<div id="submit">
								<p> </p>
								<br></br>
									<button type="submit" class="btn blue">Check Order</button>
								</div>
								</form>
							</div>
                        </div>
                </div>
                <!-- END SAMPLE FORM PORTLET-->

            </div>
        </div>
		<script>
			 function fillInput()
			 {
				 var x = document.getElementById("result").innerHTML;
				 $('#id').val(x);
				 document.forms["form_check_qr_code"].submit();
				 //document.getElementById("form_check_qr_code").submit();
			 }
		</script>

