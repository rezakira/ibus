<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

    <style>

        .action-menu-table:hover {
            -webkit-transform: scale(1.1);
            -moz-transform:scale(1.1);
            transform: scale(1.1);
        }

    </style>

    <script type="text/javascript">
        $(document).ready(function() {

            $('body').off('click', '.action-menu-view-order');
            $('body').off('click', '.action-menu-edit-order');
            $('body').off('click', '.action-menu-delete-order');


            // dynamic table
            var oTable = $('#dyntable').dataTable({
                "oLanguage": {
                    "sProcessing": 'Loading Table...'
                },
                "sPaginationType" : "full_numbers",
                "bLengthChange": false,
                "bFilter": false,
                "bProcessing": true,
                "bServerSide": true,
                "bAutoWidth": false,
                "aoColumns" : [
                    { sWidth: '20%' },
                    { sWidth: '20%' },
                    { sWidth: '20%' },
                    { sWidth: '20%' },
                    { sWidth: '20%' }
                ],  
                "sAjaxSource": '<c:url value="/transaction/order/data-table.json" />',
                "aoColumns": [
                    { "mData" : 'idOrder',
                     "mRender": function (sourceData, dataType, fullData) {

                         //return	 '<a class="btn btn-icon-only tooltips  green action-menu-view-menucms" href="javascript:;">'
                         //		 +'<i class="fa fa-search"></i><span class="tooltiptext">Edit</span></a>'
                         return	 '<a class="btn btn-icon-only tooltips  blue action-menu-edit-order" href="javascript:;" data-id="'+fullData.idOrder+'">'
                             +'<i class="fa fa-edit"></i><span class="tooltiptext">Edit</span></a>';
                             //+'<a class="btn btn-icon-only tooltips  red action-menu-delete-order" href="javascript:;" data-id="'+fullData.idOrder+'">'
                             //+'<i class="fa fa-times"></i><span class="tooltiptext">Delete</span></a>';

                     }
                    },
                    { "mData": "idOrder"},
                    { "mData": "labelStatus" },
                    { "mData": "userId",
                     "mRender": function (sourceData, dataType, fullData) {

                         return	 'No.Handphone:'+fullData.userRef.username+'<br>'+
								 'Nama Lengkap:'+fullData.userRef.firstName+' '+fullData.userRef.lastName+'<br>'+
								 'E-mail:'+fullData.userRef.email;
                     } },
                    { "mData": "scheduleId",
                     "mRender": function (sourceData, dataType, fullData) {

                         return	 'Tanggal Booking:'+fullData.orderDate+'<br>'+
								 'Rute:'+fullData.ruteFrom.nameRuteFrom+'-'+fullData.ruteTo.nameRuteTo;
                     } },
                    { "mData": "totalPrice"},
                    { "mData": "dateModified"}

                ]
            });

            //$('body').on('click', '.action-menu-view-order', function () {
            //alert('view');
            //});

            $('body').on('click', '.action-menu-edit-order', function () {
                $("#main_content").load('<c:url value="/transaction/update_order.page" />'+'/'+$(this).data('id'));
            });

           


        });
    </script>


    <div class="page-bar">
        <ul class="page-breadcrumb">
            <li>
                <a href="<c:url value="home.page"/>">Home</a>
                <i class="fa fa-angle-right"></i>
            </li>
            <li>
                <a href="#">Manajemen Transaksi</a>
            </li>
        </ul>

    </div>
    <!-- END PAGE BAR -->
    <!-- BEGIN PAGE TITLE-->
    <h3 class="page-title"> Manajemen Transaksi
    </h3>
    <!-- END PAGE TITLE-->
    <!-- END PAGE HEADER-->

    <div class="row">
        <div class="col-md-12">
            <!-- BEGIN EXAMPLE TABLE PORTLET-->
            <div class="portlet light bordered">
                <div class="portlet-title">
                    <div class="caption font-dark">
                        <i class="icon-settings font-dark"></i>
                        <span class="caption-subject bold uppercase"> Tabel Order</span>

                    </div>

                </div>
                <div class="portlet-body">
                    <div class="table-toolbar">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="btn-group">
                                    <a href="#" data-id="<c:url value="/transaction/add_order.page"/>" onclick="linkToPage(this);return false;"><button id="sample_editable_1_new" class="btn sbold green">Tambah Order
                                        <i class="fa fa-plus"></i>
                                        </button></a>
										<a href="#" data-id="<c:url value="/transaction/check_qr_code.page"/>" onclick="linkToPage(this);return false;"><button id="sample_editable_1_new" class="btn sbold green">Cek QR Code
                                        
                                        </button></a>
                                </div>
                            </div>
                            <div class="col-md-6">

                            </div>
                        </div>
                    </div>
                    <table class="table table-striped table-bordered table-hover table-checkable order-column" id="dyntable">
                        <thead style="width:100%">
                            <tr>
                                <th> Action </th>
                                <th> ID order </th>
                                <th> Status </th>
                                <th> User ID </th>
                                <td> Jadwal &amp; Rute</td>
                                <th> Harga Tiket</th>
                                <th> Tanggal Update </th>
                            </tr>
                        </thead>

                    </table>
                </div>
            </div>
            <!-- END EXAMPLE TABLE PORTLET-->
        </div>
    </div>