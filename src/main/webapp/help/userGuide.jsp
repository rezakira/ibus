<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<style>
.action-role-table:hover {
	-webkit-transform: scale(1.1);
	-moz-transform: scale(1.1);
	transform: scale(1.1);
}

h3{
    margin-left: -10px;
}
.ereportform-tab>p {
	background: white;
	border: none;
}

.ereportform-tab>p>label {
	font-size: 12px;
	padding-top: 5px;
	padding-left: 8px;
	width: 0px;
}

.ereportform-tab>p>span {
	margin-left: 120px !important;
	padding: 2px !important;
	border: none !important;
}
</style>

<script type="text/javascript">
$(function(){
  $('#sidemenu a').on('click', function(e){
    e.preventDefault();

    if($(this).hasClass('open')) {
      // do nothing because the link is already open
    } else {
      var oldcontent = $('#sidemenu a.open').attr('href');
      var newcontent = $(this).attr('href');
      
      $(oldcontent).fadeOut('fast', function(){
        $(newcontent).fadeIn().removeClass('hidden');
        $(oldcontent).addClass('hidden');
      });
      
     
      $('#sidemenu a').removeClass('open');
      $(this).addClass('open');
    }
  });
});
</script>

<body>

	<div id="mainwrapper" class="mainwrapper">

		<div class="rightpanel">
			<ul class="breadcrumbs">
				<li><a href="<c:url value="home.page"/>"><i
						class="iconfa-home"></i></a> <span class="separator"></span></li>
				<li>BANTUAN<span class="separator"></span></li>
				<li>Bantuan Pengguna</li>
			</ul>

			<div class="pageheader">
				<div class="pageicon animate0 fadeInRightBig">
					<span class="iconfa-laptop"></span>
				</div>
				<div class="pagetitle animate1 fadeInRightBig">
					<h5>BANTUAN</h5>
					<h1>Bantuan Pengguna</h1>
				</div>
			</div>

			<div class="maincontent">
				<div class="maincontentinner">

            
            <div class="row">
                
                <div class="col-md-9">
                    
                    
            <div class="topicpanel">
                <div id="1" class="topic-content">
                    <h5><strong>Login</strong></h5>
                    <img src="<c:url value="/images/help/er1 (1).jpg"/>" /><br>
                    <strong>Gambar 1 Form Login e-report</strong><br>
                    <p>Masukan USERNAME dan PASSWORD, kemudian klik Login. </p>
                    <p>Selanjutnya aplikasi akan meload halaman beranda(homepage) seperti gambar di bawah ini.</p>
                    <br>
                    <img width="700" src="<c:url value="/images/help/er1 (2).jpg"/>" alt="" />
                    <p><strong>Gambar 2 Homepage e-report</strong></p>
                    <p></p>
                </div>                      
                <div id="2" class="topic-content hidden">
                    <h5><strong>Bar Chart per Triwulan</strong></h5>
                    <p>Pada saat user berhasil login, aplikasi akan menampilkan homepage yang berisikan bar chart dari indikator capaian. Bar chart tersebut meload data laporan per triwulan indikator capaian untuk current year period. Bar chart tersebut dijelaskan sebagai berikut :</p>
                    <img src="<c:url value="/images/help/er1 (3).jpg"/>" /><br>
                    <strong>Gambar 3 Triwulan Bar Chart 1</strong><br>
                    <p>Perhatikan indikator triwulan yang diberi border berwarna merah. Bandingan dengan warna bar chart yang tertampil. Indikator triwulan yang menyala menandakan data yang diload di bar chart. Selain chart pertriwulan, juga diload chart target pertriwulan yang bersangkitan. Gambar berikutnya menunjukan <strong>jika hanya satu triwulan saja yang dipilih.</strong></p>
                    <img src="<c:url value="/images/help/er1 (4).jpg"/>" />
                    <p><strong>Gambar 4 Triwulan Bar Chart 2</strong></p>
                    <p>Warna yang ditampilkan di bar chart sesuai dengan warna indikator triwulan yang dipilih.</p>
                    <p>Berikutnya adalah gambar bar chart per triwulan dari indikator capaian lain. Pemilihan periode triwulan dilakukan dengan klik di bagian triwulan yang bersangkutan.</p>
                    <img src="<c:url value="/images/help/er1 (5).jpg"/>" />
                    <p><strong>Gambar 5 Triwulan Bar Chart 3</strong></p>
                    <img src="<c:url value="/images/help/er1 (6).jpg"/>" />
                    <p><strong>Gambar 6 Triwulan Bar Chart 4</strong></p>
                    <img src="<c:url value="/images/help/er1 (7).jpg"/>" />
                    <p><strong>Gambar 7 Triwulan Bar Chart 5</strong></p>
                    <img src="<c:url value="/images/help/er1 (8).jpg"/>" />
                    <p><strong>Gambar 8 Triwulan Bar Chart 6</strong></p>

                    <p></p>
                </div>
                <div id="31" class="topic-content hidden">
                    <h5><strong>Data Indikator</strong></h5>
                    <p>User mengakses menu Indikator & Capaian -> Data Indikator. Menu ini berisi data-data indikator capaian yang digunakan. </p>
                    <img width="700" src="<c:url value="/images/help/er1 (9).jpg"/>" />
                    <p><strong>Gambar 9 Tabel Master Indikator</strong></p>
                    <p>Pada tampilan di atas, terdapat menu Edit dan Target (diberi border merah). Menu Edit digunakan untuk mengedit data detail indikator. Sedangkan Target untuk set target capaian per periode tertentu. Berikut adalah tampilan pada saat mengakses menu Edit Indikator :</p>
                    <img width="700" src="<c:url value="/images/help/er1 (10).jpg"/>" />
                    <p><strong>Gambar 10 Edit Indikator</strong></p>
                    <p>Edit Nama Indikator atau Penjelasan pada field yang disediakan. Setelah selesai, klik Save.</p>
                    <p>Selanjutnya adalah Set Target Indikator Capaian. Caranya klik Target. Kemudian aplikasi akan meload formulir Indikator Detail seperti pada gambar di bawah ini : </p>
                    <img width="700" src="<c:url value="/images/help/er1 (11).jpg"/>" />
                    <p><strong>Gambar 11 Target Indikator Detail</strong></p>
                    <p>Isi data indikator detail mulai dari Tahun, Target Triwulan I sampai Triwulan IV. Lalu klik Save.</p>
                    <p></p>
                </div>
                <div id="32" class="topic-content hidden">
                    <h5><strong>Capaian Dit. Bina Obat Publik dan Perbekkes</strong></h5>
                    <p>User mengakses menu Indikator & Capaian -> Capaian Dit. Bina Obat Publik dan Perbekkes. Menu ini terdiri dari 6 submenu yang dijelaskan sebagai berikut :</p><br>
                    <p><strong>Ketersediaan Obat dan Vaksin</strong></p>
                    <img width="700" src="<c:url value="/images/help/er1 (12).jpg"/>" />
                    <p><strong>Gambar 12 Ketersediaan Obat dan Vaksin</strong></p>
                    <p>Perhatikan field-field yang diberi border merah. Field-field tersebut dapat diisi atau diupdate. Untuk field tahun dan triwulan, pilih data yang sesuai. Setelah selesai, klik Simpan.</p><br>
                    <p><strong>Penggunaan Obat Generik di Fasilitas Layanan Kesehatan</strong></p>
                    <img width="700" src="<c:url value="/images/help/er1 (13).jpg"/>" />
                    <p><strong>Gambar 13 Penggunaan Obat Generik</strong></p>
                    <p>Sama seperti submenu sebelumnya, di submenu ini juga perhatikan field-dield yang diberi border merah. Pilih tahun dan triwulannya, isi persentase capaiannya, kemudian klik Simpan.</p><br>
                    <p><strong>Persentase Instalasi Farmasi Kab/Kota yang Sesuai Standar</strong></p>
                    <img width="700" src="<c:url value="/images/help/er1 (14).jpg"/>" />
                    <p><strong>Gambar 14 Persentase Instalasi Farmasi</strong></p><br>
                    <p><strong>Upload Data Dukung Capaian Indikator</strong></p>
                    <img width="700" src="<c:url value="/images/help/er1 (15).jpg"/>" />
                    <p><strong>Gambar 15 Upload Data Dukung Capaian Indikator</strong></p>
                    <p>Form di atas adalah untuk mengupload file oblik pendukung capaian indikator. Pilih tahun dan triwulan capaian indikator. Pastikan file oblik sudah dipilih melalui button choose file. Data sudah dipilih ketika tulisan No file chosen sudah berganti menjadi nama file file oblik yang akan diupload. Setelah selesai, klik Simpan.</p><br>
                    <p><strong>Download Data Dukung Capaian Indikator</strong></p>
                    <img width="700" src="<c:url value="/images/help/er1 (16).jpg"/>" />
                    <p><strong>Gambar 16 Download Data Dukung Capaian Indikator</strong></p>
                    <p>Sebelum mendownload, pastikan pilih dulu kriteria tahun dan triwulan dari file oblik yang akan didownload. Setelah itu pilih Download OBLIK File. Aplikasi akan secara otomatis mendownload file.</p><br>
                    <p><strong>Daftar Capaian Dit. Bina Obat Publik dan Perbekkes</strong></p>
                    <p>Untuk melihat daftar capaian dari Dit. Bina Obat Publik dan Perbekkes, pilih submenu Daftar Capaian Dit. Bina Obat Publik dan Perbekkes seperti gambar di bawah ini :</p>
                    <img width="700" src="<c:url value="/images/help/er1 (17).jpg"/>" />
                    <p><strong>Gambar 17  Daftar Capaian Dit. Bina Obat Publik dan Perbekkes 1</strong></p>
                    <p>Selanjutnya, aplikasi akan meload halaman daftar capaian seperti gambar berikutnya. </p>
                    <img width="700" src="<c:url value="/images/help/er1 (18).jpg"/>" />
                    <p><strong>Gambar 18  Daftar Capaian Dit. Bina Obat Publik dan Perbekkes 2</strong></p>
                    <p></p>
                </div><!--topic-content-->
                <div id="33" class="topic-content hidden">
                    <h5><strong>Capaian Dit. Bina Produksi dan Distribusi Alat Kesehatan</strong></h5>
                    <p>User mengakses menu Indikator & Capaian -> Capaian Dit. Bina Produksi dan Distribusi Alat Kesehatan. Menu ini terdiri dari 4 submenu yang dijelaskan sebagai berikut :</p>
                    <p><strong>Persentase Sarana Produksi Alkes dan PKRT yang Memenuhi Cara Produksi yang Baik</strong></p>
                    <img width="700" src="<c:url value="/images/help/er1 (19).jpg"/>" />
                    <p><strong>Gambar 19 Persentase Sarana Produksi Alkes dan PKRT</strong></p><br>
                    <p><strong>Persentase Sarana Distribusi Alkes dan PKRT yang Memenuhi Persyaratan Distribusi</strong></p>
                    <img width="700" src="<c:url value="/images/help/er1 (20).jpg"/>" />
                    <p><strong>Gambar 20 Persentase Sarana Distribusi Alkes dan PKRT</strong></p><br>
                    <p><strong>Persentase Produk Alkes yang Beredar Memenuhi Persyaratan Keamanan, Mutu, Manfaat</strong></p>
                    <img width="700" src="<c:url value="/images/help/er1 (21).jpg"/>" />
                    <p><strong>Gambar 21 Persentase Produk Alkes yang Beredar</strong></p>
                    <p>Pengisian seluruh submenu di atas, yaitu dengan mengisi field-field yang diberi border merah. Setelah terisi, klik Simpan.</p>
                    <br><p><strong>Daftar Capaian Dit. Bina Produksi dan Distribusi Alat Kesehatan </strong></p>
                    <p>Untuk melihat daftar capaian dari Dit. Bina Obat Publik dan Perbekkes, pilih submenu Daftar Capaian Dit. Bina Obat Publik dan Perbekkes seperti gambar di bawah ini :</p>
                    <img width="700" src="<c:url value="/images/help/er1 (22).jpg"/>" />
                    <p><strong>Gambar 22 Daftar Capaian Dit. Bina Produksi dan Distribusi Alat Kesehatan 1</strong></p>
                    <p>Selanjutnya, aplikasi akan meload halaman daftar capaian seperti gambar berikutnya. </p>
                    <img width="700" src="<c:url value="/images/help/er1 (23).jpg"/>" />
                    <p><strong>Gambar 23 Daftar Capaian Dit. Bina Produksi dan Distribusi Alat Kesehatan 2</strong></p>
                    
                    <p></p>
                </div><!--topic-content-->
                <div id="34" class="topic-content hidden">
                    <h5><strong>Capaian Dit. Bina Pelayanan Kefarmasian</strong></h5>
                    <p>User mengakses menu Indikator & Capaian -> Capaian Dit. Bina Pelayanan Kefarmasian. Menu ini terdiri dari 4 submenu yang dijelaskan sebagai berikut.</p><br>
                    <p><strong>Persentase Instalasi RS Pemerintah yang Melaksanakan Pelayanan Kefarmasian Sesuai Standar</strong></p>
                    <img width="700" src="<c:url value="/images/help/er1 (24).jpg"/>" />
                    <p><strong>Gambar 24 Persentase Instalasi Farmasi Rumah Sakit</strong></p>
                    <br><p><strong>Persentase Puskesmas Perawatan yang Melaksanakan Pelayanan Kefarmasian Sesuai Standar</strong></p>
                    <img width="700" src="<c:url value="/images/help/er1 (25).jpg"/>" />
                    <p><strong>Gambar 25 Persentase Puskesmas Perawatan yang Melaksanakan Pelayanan Kefarmasian</strong></p><br>
                    <p><strong>Persentase Penggunaan Obat Rasional di Sarana Pelayanan Kesehatan Dasar Pemerintah</strong></p>
                    <img width="700" src="<c:url value="/images/help/er1 (26).jpg"/>" />
                    <p><strong>Gambar 26 Persentase Penggunaan Obat Rasional</strong></p>
                    <p>Untuk ketiga submenu diatas, kembali lihat field-field yang diberi border merah. Pilih tahun dan triwulan dari indikator  yang akan diisikan. Isi detail indikatornya, kemudian klik Simpan.</p>
                    <br><p><strong>Daftar Capaian Dit. Bina Pelayanan Kefarmasian</strong></p>
                    <p>Untuk melihat daftar capaian dari Dit. Bina Pelayanan Kefarmasian, pilih submenu Daftar Capaian Dit. Bina Pelananan Kefarmasian seperti gambar di bawah ini :</p>
                    <img width="700" src="<c:url value="/images/help/er1 (27).jpg"/>" />
                    <p><strong>Gambar 27 Daftar Capaian Dit. Bina Pelayanan Kefarmasian 1</strong></p>
                    <p>Selanjutnya, aplikasi akan meload halaman daftar capaian seperti gambar berikutnya.</p>
                    <img width="700" src="<c:url value="/images/help/er1 (28).jpg"/>" />
                    <p><strong>Gambar 28  Daftar Capaian Dit. Bina Pelayanan Kefarmasian 2</strong></p>
                    <p></p>
                </div><!--topic-content-->
                <div id="35" class="topic-content hidden">
                    <h5><strong>Capaian Dit. Bina Produksi dan Distribusi Kefarmasian</strong></h5>
                    <p>User mengakses menu Indikator & Capaian  Capaian Dit. Bina Produksi dan Distribusi Kefarmasian. Menu ini terdiri dari 3 submenu yang dijelaskan sebagai berikut.</p>
                    <br><p><strong>Jumlah Bahan Baku Obat dan Obat Tradisional Produksi Dalam Negeri</strong></p>
                    <img width="700" src="<c:url value="/images/help/er1 (29).jpg"/>" />
                    <p><strong>Gambar 29 Jumlah Bahan Baku Obat dan Obat Tradisional</strong></p>
                    <br><p><strong>Jumlah Standar Produk Kefarmasian yang Disusun Dalam Rangka Pembinaan Produksi dan Distribusi</strong></p>
                    <img width="700" src="<c:url value="/images/help/er1 (30).jpg"/>" />
                    <p><strong>Gambar 30 Jumlah Standar Produk Kefarmasian</strong></p>
                    <br><p><strong>Daftar Capaian Dit. Bina Produksi dan Distribusi Kefarmasian</strong></p>
                    <p>Untuk melihat daftar capaian dari Dit. Bina Pelayanan Kefarmasian, pilih submenu Daftar Capaian Dit. Bina Pelananan Kefarmasian seperti gambar di bawah ini :</p>
                    <img width="700" src="<c:url value="/images/help/er1 (31).jpg"/>" />
                    <p><strong>Gambar 31 Daftar Capaian Dit. Bina Produksi dan Distribusi Kefarmasian 1</strong></p>
                    <p>Selanjutnya, aplikasi akan meload halaman daftar capaian seperti gambar berikutnya.</p>
                    <img width="700" src="<c:url value="/images/help/er1 (32).jpg"/>" />
                    <p><strong>Gambar 32 Daftar Capaian Dit. Bina Produksi dan Distribusi Kefarmasian 2</strong></p>
                    <p></p>
                </div><!--topic-content-->
                <div id="36" class="topic-content hidden">
                    <h5><strong>Capaian Sekretariat Ditjen</strong></h5>
                    <p>User mengakses menu Indikator & Capaian -> Capaian Sekretariat Ditjen. Menu ini terdiri dari 3 submenu yang dijelaskan sebagai berikut.</p>
                    <br><p><strong>Persentase Dokumen Anggaran yang Diselesaikan</strong></p>
                    <img width="700" src="<c:url value="/images/help/er1 (33).jpg"/>" />
                    <p><strong>Gambar 33 Persentase Dokumen Aggaran</strong></p>
                    <br><p><strong>Persentase Dukungan Manajemen dan Pelaksanaan Program Kefarmasian di Daerah Dalam Rangka Dekonsentrasi</strong></p>
                    <img width="700" src="<c:url value="/images/help/er1 (34).jpg"/>" />
                    <p><strong>Gambar 34 Persentase Dukungan Manajemen</strong></p>
                    <br><p><strong>Daftar Capaian Sekretariat Ditjen</strong></p>
                    <p>Untuk melihat daftar capaian dari Sekretariat Ditjen, pilih submenu Daftar Capaian Sekretariat Ditjen seperti gambar di bawah ini :</p>
                    <img width="700" src="<c:url value="/images/help/er1 (35).jpg"/>" />
                    <p><strong>Gambar 35 Daftar Capaian Sekretariat Ditjen 1</strong></p>
                    <p>Selanjutnya, aplikasi akan meload halaman daftar capaian seperti gambar berikutnya.</p>
                    <img width="700" src="<c:url value="/images/help/er1 (36).jpg"/>" />
                    <p><strong>Gambar 36 Daftar Capaian Sekretariat Ditjen 2</strong></p>
                    <p></p>
                </div><!--topic-content-->
                <div id="41" class="topic-content hidden">
                    <h5><strong>Komponen RPK</strong></h5>
                    <p>RPK terdiri dari berbagai komponen disesuaikan dengan kebutuhan yang sudah didefinisikan oleh user sendiri. Cara untuk menambahkan data RPK adalah dengan melalukan upload file RPK ke aplikasi. Perhatikan gambar berikutnya. Untuk melakukan upload file pastikan pilih tahun dan file yang sesuai. Untuk memilih file yang akan diupload klik button Choose File. Kemudian klik Upload.</p>
                    <img width="700" src="<c:url value="/images/help/er1 (37).jpg"/>" />
                    <p><strong>Gambar 37 Tambah Data Komponen RPK</strong></p>
                    <p></p>
                </div>
                <div id="42" class="topic-content hidden">
                    <h5><strong>Dafter Komponen RPK</strong></h5>
                    <p>Untuk melihat Daftar Komponen RPK, user tinggal memilih menu Daftar Komponen RPK dan aplikasi akan meload halaman Tabel Daftar RPK seperti gambar di bawah ini :</p>
                    <img width="700" src="<c:url value="/images/help/er1 (38).jpg"/>" />
                    <p><strong>Gambar 38 Daftar Komponen RPK</strong></p>
                    <p>Pada halamanDaftar Komponen RPK, user dapat mendownload RPK, edit komponen RPK atau menghapus data RPK. Pada saat ingin mendownload RPK, pilih button Download RPK. Dan untuk melakukan Edit Komponen RPK, klik icon Edit. Begitupun untuk delete komponen RPK, klik icon Delete(perhatikan setiap button/icon yang diberi border berwarna merah pada gambar sebelumnya).</p>
                    <p>Untuk Edit Komponen RPK, aplikasi akan meload form edit komponen RPK seperti pada gambar di bawah ini.
</p>
                    <img width="700" src="<c:url value="/images/help/er1 (39).jpg"/>" />
                    <p><strong>Gambar 39 Edit Komponen RPK 1</strong></p>
                    <img width="700" src="<c:url value="/images/help/er1 (40).jpg"/>" />
                    <p><strong>Gambar 40 Edit Komponen RPK 2</strong></p>
                    <p>Isi seluruh data detail komponen RPK kemudian klik Simpan.</p>
                    <p>Selanjutnya adalah untuk menghapus data komponen RPK, klik icon delete. Aplikasi akan meload form confirmation penghapusan data seperti gambar di bawah ini.
</p>
                    <img width="700" src="<c:url value="/images/help/er1 (41).jpg"/>" />
                    <p><strong>Gambar 41 Notifikasi Hapus Data RPK RPD</strong></p>
                    <p>Pilih Hapus untuk menghapus data komponen RPK atau Batal untuk membatalkan pilihan.</p>
                    <p></p>
                </div><!--topic-content-->
                <div id="43" class="topic-content hidden">
                    <h5><strong>Pemantauan RPK RPD</strong></h5>
                    <p>Pada menu ini, user memilih tahun pemantauan RPK, kemudian download file RPK RPD yang akan dicek seperti pada gambar di bawah ini :</p>
                    <img width="700" src="<c:url value="/images/help/er1 (42).jpg"/>" />
                    <p><strong>Gambar 42 Pemantauan RPK</strong></p>
                    <p></p>
                </div><!--topic-content-->
                <div id="44" class="topic-content hidden">
                    <h5><strong>Tambah Data RPK</strong></h5>
                    <p>Untuk tambah data RPK, pada halaman daftar komponen RPK, di sebelah kanan bawah, terdapat tiga field kosong terdiri dari Kode, Komponen dan Indikator. Isi setiap field komponen RPK tersebut, kemudian tekan enter. Data komponen RPK akan bertambah.</p>
                    <img width="700" src="<c:url value="/images/help/er1 (43).jpg"/>" />
                    <p><strong>Gambar 43 Tambah Data RPK</strong></p>
                    <p></p>
                </div><!--topic-content-->
                <div id="51" class="topic-content hidden">
                    <h5><strong>Laporan Tahunan (LAPTAH)</strong></h5>
                    <br><p><strong>Download LAPTAH </strong></p>
                    <p>Untuk mendownload LAPTAH, pilih submenu Download LAPTAH. Kemudian klik button Download LAPTAH File seperti pada gambar di bawah ini :</p>
                    <img width="700" src="<c:url value="/images/help/er1 (44).jpg"/>" />
                    <p><strong>Gambar 44 Download LAPTAH</strong></p>
                    <br><p><strong>Template LAPTAH</strong></p>
                    <p>LAPTAH memiliki format template tersendiri sesuai dengan yang dibuat oleh user. Untuk itu, user dapat mengedit template LAPTAH dengan mengakses submenu Download/Upload Template LAPTAH. Perhatikan gambar di bawah ini.</p>
                    <img width="700" src="<c:url value="/images/help/er1 (45).jpg"/>" />
                    <p><strong>Gambar 45 Download/Upload Template LAPTAH</strong></p>
                    <p>Border no 1 menunjukan button untuk mendownload template LAPTAH. Dan border no 2 menunjukan button untuk memilih file template LAPTAH yang akan digunakan untuk diupload ke aplikasi, setelah itu klik button Upload.</p>
                    <p></p>
                    
                </div><!--topic-content-->
                <div id="52" class="topic-content hidden">
                    <h5><strong>Laporan Akuntabilitas Instansi Pemerintah(LAKIP)</strong></h5>
                    <p><strong>Download LAKIP</strong></p>
                    <p>Untuk mendownload LAKIP, pilih submenu Download LAKIP. Kemudian klik button Download LAKIP File seperti pada gambar di bawah ini :</p>
                    <img width="700" src="<c:url value="/images/help/er1 (46).jpg"/>" />
                    <p><strong>Gambar 46 Download LAKIP</strong></p>
                    <br><p><strong>Template LAKIP</strong></p>
                    <p>LAKIP memiliki format template tersendiri sesuai dengan yang dibuat oleh user. Untuk itu, user dapat mengedit template LAKIP dengan mengakses submenu Download/Upload Template LAKIP. Perhatikan gambar di bawah ini.</p>
                    <img width="700" src="<c:url value="/images/help/er1 (47).jpg"/>" />
                    <p><strong>Gambar 47 Download/Upload Template LAKIP</strong></p>
                    <p>Border no 1 menunjukan button untuk mendownload template LAKIP. Dan border no 2 menunjukan button untuk memilih file template LAKIP yang akan digunakan untuk diupload ke aplikasi, setelah itu klik button Upload.</p>
                    <p></p>
                </div><!--topic-content-->
                <div id="53" class="topic-content hidden">
                    <h5><strong>Chart Indikator Capaian</strong></h5>
                    <p>Dalam aplikasi e-report, ditampilkan laporan yang dikelola oleh aplikasi dalam bentuk bar chart. Untuk mengakses barchart ini, user pilih menu Laporan -> Chart Indikator Capaian. Berikut ini adalah penjelasannya.</p>
                    <br><p><strong>Dit. Bina Pelayanan Kefarmasian</strong></p>
                    <img width="700" src="<c:url value="/images/help/er1 (48).jpg"/>" />
                    <p><strong>Gambar 48 Bar Chart Dit. Bina Pelayanan Kefarmasian</strong></p>
                    <p>Perhatikan indikator tahun yang diberi border warna merah. Pada gambar di atas, hanya indikator tahun 2014 yang menyala. Ini menandakan bahwa chart yang ditampilkan merupakan gambaran chart indikator capaian hanya pada tahun tersebut. Bandingkan dengan gambar di bawah ini :</p>
                    <img width="700" src="<c:url value="/images/help/er1 (49).jpg"/>" />
                    <p><strong>Gambar 49 Bar Chart - Period Selection</strong></p>
                    <p>Lihat indikator tahun yang menyala berikut warna penandanya. Biru untu 2014 dan hitam untuk 2013. Lihat barchartnya, ditampilkan bar chart  tambahan berwarna hitam sesuai indikator tahun yang dipilih. Demikian dengan tahun-tahun lainnya. Gambar berikutnya akan memperlihatkan bar chart jika semua indikator tahun dipilih.</p>
                    <img width="700" src="<c:url value="/images/help/er1 (50).jpg"/>" />
                    <p><strong>Gambar 50 Bar Chart - All Period Selection</strong></p>
                    <br><p><strong>Dit. Bina Obat Publik dan Perbekkes</strong></p>
                    <img width="700" src="<c:url value="/images/help/er1 (51).jpg"/>" />
                    <p><strong>Gambar 51 Bar Chart Dit. Bina Publik dan Perbekkes</strong></p>
                    <br><p><strong>Sekretariat Ditjen Binfar & Alkes</strong></p>
                    <img width="700" src="<c:url value="/images/help/er1 (52).jpg"/>" />
                    <p><strong>Gambar 52 BarChart Sekretariat Ditjen Binfar & Alkes</strong></p>
                    <br><p><strong>Dit. Bina Produksi dan Distribusi Alat Kesehatan</strong></p>
                    <img width="700" src="<c:url value="/images/help/er1 (53).jpg"/>" />
                    <p><strong>Gambar 53 Bar Chart Dit. Bina Produksi dan Distribusi Alkes</strong></p>
                    <br><p><strong>Dit. Bina Produksi dan Distribusi Kefarmasian</strong></p>
                    <img width="700" src="<c:url value="/images/help/er1 (54).jpg"/>" />
                    <p><strong>Gambar 54 Bar Chart Bina Produksi dan Distribusi Kefarmasian</strong></p>
                    <p>Perhatikan kembali bagian yang dibatasi oleh border merah pada setiap gambar laporan bar chart di atas. Bagian tersebut menunjukan indikator tahun yang dapat dipilih user. Aplikasi akan meload bar chart sesuai dengan indikator tahun yang dipilih.</p>
                    <p></p>
                </div><!--topic-content-->
                <div id="6" class="topic-content hidden">
                    <h5><strong>Edit Profile User</strong></h5><br>
                    <p>Untuk mengedit data profile, user pilih menu Edit Profile seperti pada gambar di bawah ini.</p>
                    <img width="700" src="<c:url value="/images/help/er1 (55).jpg"/>" />
                    <p><strong>Gambar 55 Select Edit Profile</strong></p>
                    <p>Selanjutnya, aplikasi akan meload form edit profile user. Isi data yang ingin di edit.</p>
                    <img width="700" src="<c:url value="/images/help/er1 (56).jpg"/>" />
                    <p><strong>Gambar 56 Form Edit Profile 1</strong></p>
                    <img width="700" src="<c:url value="/images/help/er1 (57).jpg"/>" />
                    <p><strong>Gambar 57 Form Edit Profile 2</strong></p>
                    <p>User dapat mengganti password untuk akses aplikasi pada tabel System’s Password. Dan untuk mengupdate data profile, dilakukan di tabel Profile.</p>
                    <p></p>
                </div><!--topic-content-->
                <div id="71" class="topic-content hidden">
                    <h5><strong>Master Menu</strong></h5>
                    <br><p>User memilih menu Aplikasi & Pengaturan -> Menu. Aplikasi akan meload page Master Menu seperti pada gambar berikut :</p>
                    <img width="700" src="<c:url value="/images/help/er1 (58).jpg"/>" />
                    <p><strong>Gambar 58 Master Menu</strong></p>
                    
                </div><!--topic-content-->
                <div id="72" class="topic-content hidden">
                    <h5><strong>Tambah atau Hapus Menu</strong></h5>
                    <img width="700" src="<c:url value="/images/help/er1 (59).jpg"/>" />
                    <p><strong>Gambar 59 Tambah/Hapus Menu</strong></p>
                    <p>Tidak adanya tanda ceklist pada icon menunjukan bahwa menu tersebut sudah dihapus dari struktur (border merah no 1). Begitupun sebaliknya (border merah no 2).</p>
                    <p></p>
                </div><!--topic-content-->
                <div id="73" class="topic-content hidden">
                    <h5><strong>Peran Pengguna (Role List)</strong></h5>
                    <p>Untuk pengelolaan data peran pengguna, pilih menu Aplikasi & Pengaturan -> Peran Pengguna (border merah no 1). Aplikasi akan meload halaman Role seperti gambar di bawah ini.</p>
                    <img width="700" src="<c:url value="/images/help/er1 (60).jpg"/>" />
                    <p><strong>Gambar 60 Role User</strong></p>
                    <p>Untuk melihat detail dari role, klik icon role (border merah no 2). Aplikasi akan meload halaman view data role detail seperti gambar di bawah ini.</p>
                    <img width="700" src="<c:url value="/images/help/er1 (61).jpg"/>" />
                    <p><strong>Gambar 61 Detail Role User</strong></p>
                    <p></p>
                </div><!--topic-content-->
                <div id="74" class="topic-content hidden">
                    <h5><strong>Pengguna (User)</strong></h5>
                    <br><p>Untuk pengelolaan data pengguna, pilih menu Aplikasi & Pengaturan -> Pengguna (border merah no 1). Aplikasi akan meload halaman User seperti gambar di bawah ini.</p>
                    <img width="700" src="<c:url value="/images/help/er1 (62).jpg"/>" />
                    <p><strong>Gambar 62 User</strong></p>
                    <p>Keterangan :</p>
                    <p>2 : button untuk tambah data user</p>
                    <p>3 : navigasi untuk melihat kategori user pengguna</p>
                    <p>4 : navigasi untuk melihat daftar user berdasarkan abjad</p>
                    <br><p><strong>Tambah Data User</strong></p>
                    <p>Setelah klik button New User(border merah no 2), aplikasi akan meload form User Info seperti gambar di bawah ini :</p>
                    <img width="700" src="<c:url value="/images/help/er1 (63).jpg"/>" />
                    <p><strong>Gambar 63 Create User Info</strong></p>
                    <p>Perhatikan setiap field yang diberi border warna merah, isi setiap field tersebut. Adapun untuk jenis role dan direktorat pilihannya disajikan pada gambar di bawah ini. Setelah selesai mengisi data, klik Save.</p>
                    <img width="700" src="<c:url value="/images/help/er1 (64).jpg"/>" />
                    <p><strong>Gambar 64 Pilihan Role dan Direktori</strong></p>
                    <br><p><strong>User Action</strong></p>
                    <p>Untuk melakukan edit data user atau deative status, pilin menu ation seperti yang ditunjukan pada gambar berikut :</p>
                    <img width="700" src="<c:url value="/images/help/er1 (65).jpg"/>" />
                    <p><strong>Gambar 65 Select User Action</strong></p>
                    <p></p>
                </div>
                <div id="75" class="topic-content hidden">
                    <h5><strong>Direktorat</strong></h5>
                    <br><p>Untuk pengelolaan data pengguna, pilih menu Aplikasi & Pengaturan  Direktorat (border merah no 1). Aplikasi akan meload halaman Direktorat seperti gambar di bawah ini.</p>
                    <img width="700" src="<c:url value="/images/help/er1 (66).jpg"/>" />
                    <p><strong>Gambar 66 List Direktorat</strong></p>
                    <p>Untuk mengedit data direktorat, pilih button Edit yang diberi tanda border merah nomor 2. Aplikasi akan meload page edit deraktori sebagaimana ditunjukan oleh gambar berikutnya.</p>
                    <img width="700" src="<c:url value="/images/help/er1 (67).jpg"/>" />
                    <p><strong>67 Edit Direktorat</strong></p>
                    <p></p>
                </div>
                <div id="81" class="topic-content hidden">
                    <h5><strong>Bantuan Pengguna</strong></h5>
                    <br><p>Untuk bantuan pengguna, pilih menu Bantuan -> Bantuan Pengguna (border merah no 1). Dan untuk melihat penjelasan topik bantuan sesuai dengan kategori, pilih salah satu kategorinya (contoh border merah nomor 2 untuk menu kategori Login).</p>
                    <img width="700" src="<c:url value="/images/help/er1 (68).jpg"/>" />
                    <p><strong>Gambar 68 Bantuan Pengguna</strong></p>
                    <p></p>
                </div><!--topic-content-->
                <div id="82" class="topic-content hidden">
                    <h5><strong>Email Blast</strong></h5>
                    <br><p>Untuk meminta bantuan via email, pilih menu Bantuan -> Email Blast(border merah no 1). Aplikasi akan meload halaman Email Blast seperti gambar di bawah ini. Kemudian klik button Kirim untuk mulai mengcompose dan mengirim email.</p>
                    <img width="700" src="<c:url value="/images/help/er1 (69).jpg"/>" />
                    <p><strong>Gambar 69 Email Blast</strong></p>
                    <p></p>
                </div><!--topic-content-->
                <div id="83" class="topic-content hidden">
                    <h5><strong>Laporan Kerusakan</strong></h5>
                    <br><p>Untuk melaporkan kerusakan pada aplikasi, akses menu Bantuan  Laporan Kerusakan. Aplikasi akan meload halaman Laporan Kerusakan seperti pada gambar di bawah ini.</p>
                    <img width="700" src="<c:url value="/images/help/er1 (70).jpg"/>" />
                    <p><strong>Gambar 70 Laporan Kerusakan</strong></p>
                    <p>Keterangan :</p>
                    <p>2		: text area untuk mendeskripsikan kerusakan yang terjadi</p>
                    <p>3 	: button Kirim untuk mengirim laporan kerusakan</p>
                    <p></p>
                </div><!--topic-content-->
                
            </div><!--topicpanel-->
                    
            <br />
                    
            
                    
                </div><!--col-md-9-->
                
                <div class="col-md-3">
                    
                    
                    <br />
                            
                    <h3 class="subtitle2">Categories</h3>
                    
                               
                <div class="accordion accordion" id="sidemenu">
                            <h3>Login</h3>
                            <div>
                                <p>
                                    <i class="iconfa-angle-right"></i> <a href="#1" class="open">Login </a><br>
                                </p>
                            </div>
                            <h3>Bar Chart per Triwulan</h3>
                            <div>
                                <p>
                                    <i class="iconfa-angle-right"></i> <a href="#2"  >Bar Chart per Triwulan</a><br>
                                </p>
                            </div>
                            <h3>Indikator dan Capaian</h3>
                            <div>
                                <p>
                                    <i class="iconfa-angle-right"></i> <a href="#31"  >Data Indikator</a><br>
                                    <i class="iconfa-angle-right"></i> <a href="#32"  >Capaian Dit. Bina Obat Publik dan Perbekkes</a><br>
                                    <i class="iconfa-angle-right"></i> <a href="#33"  >Capaian Dit. Bina Produksi dan Distribusi Alat Kesehatan</a><br>
                                    <i class="iconfa-angle-right"></i> <a href="#34"  >Capaian Dit. Bina Pelayanan Kefarmasian</a><br>
                                    <i class="iconfa-angle-right"></i> <a href="#35"  >Capaian Dit. Bina Produksi dan Distribusi Kefarmasian</a><br>
                                    <i class="iconfa-angle-right"></i> <a href="#36"  >Capaian Sekretariat Ditjen</a><br>
                                    
                                </p>
                            </div>
                            <h3>RPK & Pemantauannya</h3>
                            <div>
                                <p>
                                    <i class="iconfa-angle-right"></i> <a href="#41"  >Komponen RPK</a><br>
                                    <i class="iconfa-angle-right"></i> <a href="#42"  >Daftar Komponen RPK</a><br>
                                    <i class="iconfa-angle-right"></i> <a href="#43"  >Pemantauan RPK RPD</a><br>
                                    <i class="iconfa-angle-right"></i> <a href="#44"  >Tambah Data RPK</a><br>
                                </p>
                            </div>
                            <h3>Laporan</h3>
                            <div>
                                <p>
                                    <i class="iconfa-angle-right"></i> <a href="#51"  >Laporan Tahunan (LAPTAH)</a><br>
                                    <i class="iconfa-angle-right"></i> <a href="#52"  >Laporan Akuntabilitas Instansi Pemerintah(LAKIP)</a><br>
                                    <i class="iconfa-angle-right"></i> <a href="#53"  >Chart Indikator Capaian</a><br>
                                </p>
                            </div>
                            <h3>Edit Profile User</h3>
                            <div>
                                <p>
                                    <i class="iconfa-angle-right"></i> <a href="#6"  >Edit Profile User</a><br>
                                </p>
                            </div>
                            <h3>Aplikasi dan Pengaturan</h3>
                            <div>
                                <p>
                                    <i class="iconfa-angle-right"></i><a href="#71">Master Menu</a><br>
                                    <i class="iconfa-angle-right"></i> <a href="#72">Tambah atau Hapus Menu</a> <br>
                                    <i class="iconfa-angle-right"></i><a href="#73">Peran Pengguna (Role List)</a> <br>
                                    <i class="iconfa-angle-right"></i><a href="#74">Pengguna (User)  </a><br>
                                    <i class="iconfa-angle-right"></i> <a href="#75">Direktorat </a><br>
                                </p>
                            </div>
                            <h3>Bantuan</h3>
                            <div>
                                <p>
                                    <i class="iconfa-angle-right"></i> <a href="#81"  >Bantuan Pengguna</a><br>
                                    <i class="iconfa-angle-right"></i> <a href="#82"  >Email Blast</a><br>
                                    <i class="iconfa-angle-right"></i> <a href="#83"  >Laporan Kerusakan</a><br>
                                </p>
                            </div>
                            
                        </div><!--#accordion-->
                </div><!--col-md-3-->
                
            </div><!--row-->


					<%@ include file="/common//mainFooter.jsp"%>
					<!--footer-->

				</div>
				<!--maincontentinner-->
			</div>
			<!--maincontent-->

		</div>
		<!--rightpanel-->

	</div>
	<!--mainwrapper-->