<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<style>
.action-role-table:hover {
	-webkit-transform: scale(1.1);
	-moz-transform: scale(1.1);
	transform: scale(1.1);
}

.ereportform-tab>p {
	background: white;
	border: none;
}

.ereportform-tab>p>label {
	font-size: 12px;
	padding-top: 5px;
	padding-left: 8px;
	width: 0px;
}

.ereportform-tab>p>span {
	margin-left: 120px !important;
	padding: 2px !important;
	border: none !important;
}
</style>

<script type="text/javascript">
$('form').validate({
	rules: {
		firstName: "required"
	},
    submitHandler : function(form) {
    	$.ajax({
			url:$('form').attr('action'),
			type:$('form').attr('method'),
			data: $('form').serialize(),
		}).done(function(data){
			var msg = "Proses Berhasil. Email Telah terkirim.";
			jQuery.jGrowl(msg, { life: 3000});
			$('#autoResizeTA').val('');
		}).fail(function(){
			var msg = "Failed";
			jQuery.jGrowl(msg, { life: 3000});
		});
    	return false;
    }
});
</script>

<body>

	<div id="mainwrapper" class="mainwrapper">

		<div class="rightpanel">
			<ul class="breadcrumbs">
				<li><a href="<c:url value="home.page"/>"><i
						class="iconfa-home"></i></a> <span class="separator"></span></li>
				<li>BANTUAN<span class="separator"></span></li>
				<li>Email Blast</li>
			</ul>

			<div class="pageheader">
				<div class="pageicon animate0 fadeInRightBig">
					<span class="iconfa-laptop"></span>
				</div>
				<div class="pagetitle animate1 fadeInRightBig">
					<h5>BANTUAN</h5>
					<h1>Email Blast</h1>
				</div>
			</div>

			<div class="maincontent">
				<div class="maincontentinner">
					<div class="widget">
						<h4 class="widgettitle">Halaman Kirim Email Blast</h4>
						<div class="widgetcontent">

							<form id="frm_lapor_kerusakan" class="stdform stdform2"
								method="post" enctype="multipart/form-data"
								action="<c:url value="/ebc/sendEmailBlast.json" />">

								<div class="form-group">									
								</div>
								
								<p>
									<label>Alamat penerima e-mail</label> 
									<span class="field"> 
										<input 	type="text" name="namaDirektorat" id="namaDirektorat" required
										value="${direktorat.namaDirektorat}" class="form-control"
										placeholder="Pisahkan alamat email dengan tanda koma. Contoh: (abcd@gmail.com , efgh@yahoo.com , xyz@live.com, dst)"  />
									</span>
								</p>

								<br />

								<p class="stdformbutton">
									<button id="btn-save" class="btn btn-success btn-large"
										style="width: 100px;">Kirim</button>
								</p>
							</form>

						</div>
						<!-- widgetcontent-->
					</div>
					<!-- widgetcontent-->

					<%@ include file="/common//mainFooter.jsp"%>
					<!--footer-->

				</div>
				<!--maincontentinner-->
			</div>
			<!--maincontent-->

		</div>
		<!--rightpanel-->

	</div>
	<!--mainwrapper-->
