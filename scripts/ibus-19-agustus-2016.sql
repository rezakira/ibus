-- MySQL dump 10.13  Distrib 5.5.24, for Win64 (x86)
--
-- Host: localhost    Database: i-bis
-- ------------------------------------------------------
-- Server version	5.5.24-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `access`
--

DROP TABLE IF EXISTS `access`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `access` (
  `ROLE_ID` varchar(3) NOT NULL,
  `MENU_ID` varchar(5) NOT NULL,
  KEY `ACCESS_IBFK_1` (`ROLE_ID`),
  KEY `ACCESS_IBFK_2` (`MENU_ID`),
  CONSTRAINT `ACCESS_IBFK_1` FOREIGN KEY (`ROLE_ID`) REFERENCES `role` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `ACCESS_IBFK_2` FOREIGN KEY (`MENU_ID`) REFERENCES `menu` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `access`
--

LOCK TABLES `access` WRITE;
/*!40000 ALTER TABLE `access` DISABLE KEYS */;
INSERT INTO `access` VALUES ('ADM','REPRT'),('ADM','HELPS'),('ADM','MENUS'),('ADM','ROLES'),('ADM','USERS'),('ADM','GNSET'),('MBM','HELPS'),('MBM','REPRT'),('ADM','APSET'),('MBM','APSET'),('MBM','GNSET'),('ADM','DTMTR'),('ADM','LIBUS'),('ADM','LISDS'),('ADM','LISSB'),('ADM','TRMNG'),('ADM','LIORD'),('ADM','BPMNG'),('ADM','LIBPM'),('ADM','LISOT');
/*!40000 ALTER TABLE `access` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `berita_promo`
--

DROP TABLE IF EXISTS `berita_promo`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `berita_promo` (
  `ID_BERITA_PROMO` bigint(20) NOT NULL,
  `TITLE` varchar(128) NOT NULL,
  `CONTENT_TYPE` tinyint(1) NOT NULL,
  `TIME_START` datetime NOT NULL,
  `TIME_END` datetime NOT NULL,
  `CONTENT` text NOT NULL,
  `STATUS` tinyint(1) NOT NULL,
  `IMAGE_URL` varchar(400) NOT NULL,
  `DATE_ADDED` datetime NOT NULL,
  `DATE_MODIFIED` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `berita_promo`
--

LOCK TABLES `berita_promo` WRITE;
/*!40000 ALTER TABLE `berita_promo` DISABLE KEYS */;
INSERT INTO `berita_promo` VALUES (2,'Test',0,'2016-08-01 00:00:00','2016-08-31 00:00:00','<p>Test</p>',1,'image_berita_promo_p9u5785pq3_200x200.jpg','2016-08-15 19:52:44','2016-08-15 19:52:44'),(3,'Test 2',0,'2016-08-01 00:00:00','2016-08-31 00:00:00','<p>Test 2</p>',1,'image_berita_promo_709mcv8fh9_200x200.jpg','2016-08-15 19:55:48','2016-08-15 19:55:48');
/*!40000 ALTER TABLE `berita_promo` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `bus`
--

DROP TABLE IF EXISTS `bus`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `bus` (
  `ID_BUS` bigint(20) NOT NULL,
  `NAME_BUS` varchar(255) NOT NULL,
  `SPECS_BUS` text NOT NULL,
  `COUNT_OF_CHAIR` int(11) NOT NULL,
  `DATE_ADDED` datetime NOT NULL,
  `DATE_MODIFIED` datetime NOT NULL,
  `STATUS` varchar(5) NOT NULL,
  PRIMARY KEY (`ID_BUS`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `bus`
--

LOCK TABLES `bus` WRITE;
/*!40000 ALTER TABLE `bus` DISABLE KEYS */;
INSERT INTO `bus` VALUES (2,'Bus A','Spesifikasi Bus A',53,'2016-08-09 22:40:06','2016-08-12 17:38:30','A');
/*!40000 ALTER TABLE `bus` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `menu`
--

DROP TABLE IF EXISTS `menu`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `menu` (
  `ID` varchar(5) NOT NULL,
  `TITLE` varchar(70) NOT NULL,
  `DESCRIPTION` varchar(250) DEFAULT NULL,
  `LAST_UPDATE` datetime NOT NULL,
  `STATUS` varchar(10) NOT NULL,
  `PARENT_ID` varchar(5) DEFAULT NULL,
  `SORT_NO` int(11) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `FK_have_submenu` (`PARENT_ID`),
  CONSTRAINT `FK_have_submenu` FOREIGN KEY (`PARENT_ID`) REFERENCES `menu` (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `menu`
--

LOCK TABLES `menu` WRITE;
/*!40000 ALTER TABLE `menu` DISABLE KEYS */;
INSERT INTO `menu` VALUES ('ABOUT','Bantuan Pengguna','Bantuan Pengguna','2014-04-02 00:00:00','A','HELPS',NULL),('APSET','Aplikasi & Pengaturan','Aplikasi & Pengaturan','2014-04-02 00:00:00','A',NULL,1),('BPMNG','Berita & Promo Management','Berita & Promo Management','2016-08-11 00:00:00','A',NULL,3),('DTMTR','Data Master','Data Master','2016-08-08 00:00:00','A',NULL,2),('FQA','Laporan Kerusakan','Laporan Kerusakan','2014-04-02 00:00:00','A','HELPS',NULL),('GNSET','Pengaturan','Pengaturan','2014-04-02 00:00:00','A','APSET',1),('HELPS','Bantuan','Bantuan','2014-04-02 00:00:00','A',NULL,5),('LIBPM','Daftar Berita & Promo','Daftar Berita & Promo','2016-08-11 00:00:00','A','BPMNG',1),('LIBUS','Bus Management','Bus Management','2016-08-08 00:00:00','A','DTMTR',1),('LIORD','Order Management','Order Management','2016-08-10 00:00:00','A','TRMNG',1),('LISDS','Rute Management','Rute Management','2016-08-09 00:00:00','A','DTMTR',3),('LISOT','Schedule On Time Management','Schedule On Time Management','2016-08-10 09:54:39','A','DTMTR',4),('LISSB','Schedule Bis Management','Schedule Bis Management','2016-08-09 10:09:08','A','DTMTR',2),('MENUS','Menu','Menu','2014-04-02 00:00:00','A','APSET',2),('REPRT','Laporan','Laporan','2014-04-02 00:00:00','A',NULL,4),('ROLES','Peran Pengguna','Peran Pengguna','2014-04-02 00:00:00','A','APSET',3),('TRMNG','Transaksi Management','Transaksi Management','2016-08-10 00:00:00','A',NULL,3),('USERS','Pengguna','Pengguna','2014-04-02 00:00:00','A','APSET',4);
/*!40000 ALTER TABLE `menu` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `order_transaction`
--

DROP TABLE IF EXISTS `order_transaction`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `order_transaction` (
  `ID_ORDER` varchar(255) NOT NULL,
  `USER_ID` varchar(255) NOT NULL,
  `SCHEDULE_ID` bigint(20) NOT NULL,
  `ID_RUTE_FROM` bigint(20) NOT NULL,
  `ID_RUTE_TO` bigint(20) NOT NULL,
  `PRICE_TICKET_UNIT` int(11) NOT NULL,
  `DISCOUNT` float NOT NULL,
  `TOTAL_PERSON` int(11) NOT NULL,
  `TOTAL_PRICE` int(11) NOT NULL,
  `STATUS` varchar(5) NOT NULL,
  `QRCODE` text NOT NULL,
  `ORDER_DATE` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `DATE_ADDED` datetime NOT NULL,
  `DATE_MODIFIED` datetime NOT NULL,
  PRIMARY KEY (`ID_ORDER`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `order_transaction`
--

LOCK TABLES `order_transaction` WRITE;
/*!40000 ALTER TABLE `order_transaction` DISABLE KEYS */;
INSERT INTO `order_transaction` VALUES ('3igj9tdx','085324295293',1,4,4,100000,0,2,200000,'1','3igj9tdx','2016-08-14 17:00:00','2016-08-11 13:03:44','2016-08-11 13:05:15'),('3or42liz','085324295293',1,4,4,100000,0,1,100000,'1','3or42liz','2016-07-31 17:00:00','2016-08-12 17:39:23','2016-08-12 17:39:23'),('4i7eeqvh','085324295293',1,4,4,100000,0,1,100000,'1','4i7eeqvh','2016-07-31 17:00:00','2016-08-13 08:55:21','2016-08-13 08:55:21'),('5zyu1lj7','085324295293',1,4,4,100000,0,1,100000,'1','5zyu1lj7','2016-07-31 17:00:00','2016-08-13 09:55:38','2016-08-13 09:55:38'),('75epkyuy','085324295293',1,4,4,100000,0,1,100000,'1','75epkyuy','2016-08-18 17:00:00','2016-08-19 18:34:10','2016-08-19 18:34:10'),('7xptls0x','085324295293',1,4,4,100000,0,1,100000,'1','7xptls0x','2016-07-31 17:00:00','2016-08-13 10:03:35','2016-08-13 10:03:35'),('808imi62','085324295293',1,4,4,100000,0,1,100000,'1','808imi62','2016-07-31 17:00:00','2016-08-12 17:48:20','2016-08-12 17:48:20'),('8nyxu22u','085324295293',1,4,4,100000,0,1,100000,'1','8nyxu22u','2016-08-16 17:00:00','2016-08-11 12:32:04','2016-08-11 12:32:04'),('9hbtwwfw','085324295293',1,4,4,100000,0,1,100000,'1','9hbtwwfw','2016-08-01 17:00:00','2016-08-12 17:39:09','2016-08-12 17:39:09'),('bbq3oxz8','085324295293',1,4,4,100000,0,1,100000,'1','bbq3oxz8','2016-07-31 17:00:00','2016-08-15 19:35:48','2016-08-15 19:35:48'),('cwwu1lph','085324295293',1,5,6,80000,0,1,80000,'1','cwwu1lph','2016-07-31 17:00:00','2016-08-12 17:49:56','2016-08-12 17:49:56'),('f2bpr6ha','085324295293',1,5,6,80000,0,1,80000,'1','f2bpr6ha','2016-07-31 17:00:00','2016-08-12 18:43:37','2016-08-12 18:43:37'),('fmqrtw1e','085324295293',1,4,4,100000,0,1,100000,'1','fmqrtw1e','2016-07-31 17:00:00','2016-08-13 09:04:05','2016-08-13 09:04:05'),('gix2o17o','085722722722',1,5,6,80000,0,2,160000,'1','gix2o17o','2016-08-15 17:00:00','2016-08-11 13:21:05','2016-08-11 13:29:04'),('hevoi5xl','085324295293',1,4,4,100000,0,1,100000,'1','hevoi5xl','2016-07-31 17:00:00','2016-08-13 09:37:48','2016-08-13 09:37:48'),('j1kn5j85','085324295293',1,5,6,80000,0,1,80000,'1','j1kn5j85','2016-07-31 17:00:00','2016-08-12 17:37:40','2016-08-12 17:37:40'),('kt6w0veq','085324295293',1,5,6,80000,0,1,80000,'1','kt6w0veq','2016-07-31 17:00:00','2016-08-12 18:46:07','2016-08-12 18:46:07'),('lc3kz7w4','085324295293',1,5,6,80000,0,1,80000,'1','lc3kz7w4','2016-07-31 17:00:00','2016-08-12 17:38:43','2016-08-12 17:38:43'),('n58jqzdx','085324295293',1,5,6,80000,0,1,80000,'1','n58jqzdx','2016-07-31 17:00:00','2016-08-13 09:56:02','2016-08-13 09:56:02'),('n5ww7c9f','085324295293',1,4,4,100000,0,1,100000,'1','n5ww7c9f','2016-07-31 17:00:00','2016-08-13 09:37:17','2016-08-13 09:37:17'),('n6g3b2o1','085324295293',1,4,4,100000,0,1,100000,'1','n6g3b2o1','2016-07-31 17:00:00','2016-08-13 09:09:22','2016-08-13 09:09:22'),('nk23eiux','085324295293',1,5,6,80000,0,1,80000,'1','nk23eiux','2016-07-31 17:00:00','2016-08-13 10:04:09','2016-08-13 10:04:09'),('osi5x9bb','085324295293',1,5,6,80000,0,1,80000,'1','osi5x9bb','2016-07-31 17:00:00','2016-08-13 09:56:18','2016-08-13 09:56:18'),('p98wi5a4','085324295293',1,4,4,100000,0,1,100000,'1','p98wi5a4','2016-07-31 17:00:00','2016-08-13 09:17:23','2016-08-13 09:17:23'),('rp6v9nb8','085324295293',1,5,6,80000,0,1,80000,'1','rp6v9nb8','2016-07-31 17:00:00','2016-08-12 17:43:22','2016-08-12 17:43:22'),('rxm4re80','085324295293',1,5,6,80000,0,1,80000,'1','rxm4re80','2016-07-31 17:00:00','2016-08-12 18:51:12','2016-08-12 18:51:12'),('se70nlf4','085324295293',1,4,4,100000,0,1,100000,'1','se70nlf4','2016-07-31 17:00:00','2016-08-13 09:22:55','2016-08-13 09:22:55'),('udnvbxen','085324295293',1,4,4,100000,0,1,100000,'1','udnvbxen','2016-07-31 17:00:00','2016-08-13 10:03:58','2016-08-13 10:03:58'),('v3u1gaxu','085324295293',1,4,4,100000,0,1,100000,'1','v3u1gaxu','2016-07-31 17:00:00','2016-08-13 09:10:35','2016-08-13 09:10:35'),('vahgo44v','085324295293',1,4,4,100000,0,1,100000,'1','vahgo44v','2016-07-31 17:00:00','2016-08-13 09:14:37','2016-08-13 09:14:37'),('wfc9roz1','085324295293',2,4,4,100000,0,1,100000,'1','wfc9roz1','2016-07-31 17:00:00','2016-08-12 16:39:05','2016-08-12 16:39:05'),('x0k7x3kj','085324295293',1,4,4,100000,0,3,300000,'1','x0k7x3kj','2016-07-31 17:00:00','2016-08-15 19:35:27','2016-08-15 19:35:27'),('yktuu7ul','085324295293',1,4,4,100000,0,1,100000,'1','yktuu7ul','2016-07-31 17:00:00','2016-08-13 10:33:57','2016-08-13 10:33:57'),('yng1ef5n','085324295293',2,5,6,80000,0,1,80000,'1','yng1ef5n','2016-07-31 17:00:00','2016-08-12 18:49:48','2016-08-12 18:49:48'),('yvbblul1','085324295293',1,4,4,100000,0,1,100000,'1','yvbblul1','2016-07-31 17:00:00','2016-08-13 10:37:32','2016-08-13 10:37:32');
/*!40000 ALTER TABLE `order_transaction` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `role`
--

DROP TABLE IF EXISTS `role`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `role` (
  `ID` varchar(3) NOT NULL,
  `TITLE` varchar(70) DEFAULT NULL,
  `DESCRIPTION` varchar(250) DEFAULT NULL,
  `STATUS` varchar(10) NOT NULL,
  `UPDATE_TIME` datetime NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `role`
--

LOCK TABLES `role` WRITE;
/*!40000 ALTER TABLE `role` DISABLE KEYS */;
INSERT INTO `role` VALUES ('ADM','Administrator','FINE System Administrator','A','2014-03-05 00:00:00'),('MBM','Member','Member','A','2016-02-24 00:00:00');
/*!40000 ALTER TABLE `role` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `rute_from`
--

DROP TABLE IF EXISTS `rute_from`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `rute_from` (
  `ID_RUTE_FROM` bigint(20) NOT NULL,
  `NAME_RUTE_FROM` varchar(255) NOT NULL,
  `DESC_RUTE_FROM` text,
  `DATE_ADDED` datetime NOT NULL,
  `DATE_MODIFIED` datetime NOT NULL,
  `STATUS` varchar(5) NOT NULL,
  PRIMARY KEY (`ID_RUTE_FROM`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `rute_from`
--

LOCK TABLES `rute_from` WRITE;
/*!40000 ALTER TABLE `rute_from` DISABLE KEYS */;
INSERT INTO `rute_from` VALUES (4,'Medan','Medan','2016-08-11 10:08:28','2016-08-11 10:08:28','A'),(5,'Aceh','Aceh','2016-08-11 11:38:00','2016-08-11 11:38:00','A');
/*!40000 ALTER TABLE `rute_from` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `rute_to`
--

DROP TABLE IF EXISTS `rute_to`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `rute_to` (
  `ID_RUTE_TO` bigint(20) NOT NULL,
  `ID_RUTE_FROM_REF` bigint(20) NOT NULL,
  `NAME_RUTE_TO` varchar(255) NOT NULL,
  `DESC_RUTE_TO` text,
  `DATE_ADDED` datetime NOT NULL,
  `DATE_MODIFIED` datetime NOT NULL,
  `STATUS` varchar(5) NOT NULL,
  `PRICE_TICKET_UNIT` int(11) NOT NULL,
  PRIMARY KEY (`ID_RUTE_TO`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `rute_to`
--

LOCK TABLES `rute_to` WRITE;
/*!40000 ALTER TABLE `rute_to` DISABLE KEYS */;
INSERT INTO `rute_to` VALUES (4,4,'Pematang Siantar','Pematang Siantar','2016-08-11 10:08:42','2016-08-11 10:08:42','A',100000),(5,4,'Pematang Siantar 2','Pematang Siantar 2','2016-08-11 10:08:59','2016-08-11 10:08:59','A',100000),(6,5,'Pematang Siantar','Pematang Siantar','2016-08-11 11:43:09','2016-08-11 11:43:09','A',80000);
/*!40000 ALTER TABLE `rute_to` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `schedule_bus`
--

DROP TABLE IF EXISTS `schedule_bus`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `schedule_bus` (
  `ID_SCHEDULE` bigint(20) NOT NULL,
  `TIME_START` varchar(5) NOT NULL,
  `TIME_END` varchar(5) NOT NULL,
  `DATE_ADDED` datetime NOT NULL,
  `DATE_MODIFIED` datetime NOT NULL,
  `STATUS` varchar(5) NOT NULL,
  PRIMARY KEY (`ID_SCHEDULE`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `schedule_bus`
--

LOCK TABLES `schedule_bus` WRITE;
/*!40000 ALTER TABLE `schedule_bus` DISABLE KEYS */;
INSERT INTO `schedule_bus` VALUES (1,'07.00','08.00','2016-08-11 10:07:28','2016-08-11 10:07:28','A'),(2,'08.00','09.00','2016-08-11 10:08:07','2016-08-11 10:08:07','A');
/*!40000 ALTER TABLE `schedule_bus` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `schedule_on_time`
--

DROP TABLE IF EXISTS `schedule_on_time`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `schedule_on_time` (
  `ID_ON_TIME` bigint(20) NOT NULL,
  `ID_SCHEDULE` bigint(20) NOT NULL,
  `ID_BUS` bigint(20) NOT NULL,
  `NAME_DRIVER` varchar(255) NOT NULL,
  `DATE_ADDED` datetime NOT NULL,
  `DATE_MODIFIED` datetime NOT NULL,
  `STATUS` varchar(5) NOT NULL,
  `NOTE` text NOT NULL,
  `RUTE_FROM_ID_REF` bigint(20) NOT NULL,
  `RUTE_TO_ID_REF` bigint(20) NOT NULL,
  `ACTUAL_DATE` date NOT NULL,
  PRIMARY KEY (`ID_ON_TIME`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `schedule_on_time`
--

LOCK TABLES `schedule_on_time` WRITE;
/*!40000 ALTER TABLE `schedule_on_time` DISABLE KEYS */;
INSERT INTO `schedule_on_time` VALUES (1,1,2,'Muhammad Yusuf','2016-08-12 17:06:43','2016-08-12 17:06:43','B','berangkat',5,6,'2016-08-01');
/*!40000 ALTER TABLE `schedule_on_time` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `seq`
--

DROP TABLE IF EXISTS `seq`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `seq` (
  `NAME` varchar(30) NOT NULL,
  `INCREMENT` int(11) unsigned NOT NULL DEFAULT '1',
  `MIN_VALUE` int(11) unsigned NOT NULL DEFAULT '1',
  `MAX_VALUE` bigint(20) unsigned NOT NULL DEFAULT '1844674407',
  `CUR_VALUE` bigint(20) unsigned DEFAULT '1',
  `CYCLE` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`NAME`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `seq`
--

LOCK TABLES `seq` WRITE;
/*!40000 ALTER TABLE `seq` DISABLE KEYS */;
INSERT INTO `seq` VALUES ('sq_rpm',1,1,1844674407,10,NULL),('sq_rpk',1,1,1844674407,19,NULL),('sq_rdl',1,1,1844674407,3,NULL),('sq_lt',1,1,1844674407,11,NULL),('sq_kbj',1,1,1844674407,15,NULL),('sq_dl',1,1,1844674407,28,NULL),('sq_pm',1,1,1844674407,408,NULL),('sq_pk',1,1,1844674407,3064,NULL),('sq_bus',1,1,1844674407,3,NULL),('sq_rute_from',1,1,1844674407,6,NULL),('sq_rute_to',1,1,1844674407,7,NULL),('sq_schedule',1,1,1844674407,3,NULL),('sq_schedule_on_time',1,1,1844674407,2,NULL),('sq_berita_promo',1,1,1844674407,4,NULL);
/*!40000 ALTER TABLE `seq` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `setting`
--

DROP TABLE IF EXISTS `setting`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `setting` (
  `ID` char(5) NOT NULL,
  `NAME` varchar(100) DEFAULT NULL,
  `VAL` varchar(500) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `setting`
--

LOCK TABLES `setting` WRITE;
/*!40000 ALTER TABLE `setting` DISABLE KEYS */;
INSERT INTO `setting` VALUES ('ADDRS',NULL,'Jakarta Selatan'),('NAME',NULL,'PT Underwater Jakarta');
/*!40000 ALTER TABLE `setting` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sit_order`
--

DROP TABLE IF EXISTS `sit_order`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sit_order` (
  `ID_SIT_ORDER` varchar(128) NOT NULL,
  `NO_KURSI` int(11) NOT NULL,
  `SCHEDULE_OT_REF_ID` bigint(20) DEFAULT NULL,
  `DATE_ADDED` datetime NOT NULL,
  `DATE_MODIFIED` datetime NOT NULL,
  `ID_ORDER_REF` varchar(128) NOT NULL,
  `STATUS` varchar(5) NOT NULL,
  PRIMARY KEY (`ID_SIT_ORDER`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sit_order`
--

LOCK TABLES `sit_order` WRITE;
/*!40000 ALTER TABLE `sit_order` DISABLE KEYS */;
INSERT INTO `sit_order` VALUES ('1-4-4-2016/08/01-1',1,0,'2016-08-13 09:37:20','2016-08-13 09:37:20','n5ww7c9f','1'),('1-4-4-2016/08/01-2',2,0,'2016-08-13 10:03:38','2016-08-13 10:03:38','7xptls0x','1'),('1-4-4-2016/08/01-3',3,0,'2016-08-13 10:37:57','2016-08-13 10:37:57','yvbblul1','1'),('1-4-4-2016/08/01-6',6,0,'2016-08-15 19:35:30','2016-08-15 19:35:30','x0k7x3kj','1'),('1-4-4-2016/08/01-7',7,0,'2016-08-15 19:35:32','2016-08-15 19:35:32','x0k7x3kj','1'),('1-4-4-2016/08/01-9',9,0,'2016-08-15 19:35:39','2016-08-15 19:35:39','x0k7x3kj','1'),('1-5-6-2016/08/01-1',1,1,'2016-08-13 10:04:11','2016-08-13 10:04:11','nk23eiux','1'),('1-5-6-2016/08/01-2',2,1,'2016-08-13 09:56:07','2016-08-13 09:56:07','n58jqzdx','1');
/*!40000 ALTER TABLE `sit_order` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user`
--

DROP TABLE IF EXISTS `user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user` (
  `USERNAME` varchar(50) NOT NULL,
  `ROLE_ID` varchar(3) NOT NULL,
  `FIRST_NAME` varchar(100) DEFAULT NULL,
  `LAST_NAME` varchar(100) DEFAULT NULL,
  `EMAIL` varchar(100) DEFAULT NULL,
  `PASSWORD` varchar(250) DEFAULT NULL,
  `LAST_LOGIN` datetime DEFAULT NULL,
  `STATUS` varchar(10) DEFAULT NULL,
  `TYPE` varchar(10) DEFAULT NULL,
  `IS_ENABLED` tinyint(1) DEFAULT NULL,
  `IS_ACCOUNT_NON_EXPIRED` tinyint(1) DEFAULT NULL,
  `IS_ACCOUNT_NON_LOCKED` tinyint(1) DEFAULT NULL,
  `IS_CREDENTIALS_NON_EXPIRED` tinyint(1) DEFAULT NULL,
  `USER_NO` varchar(100) DEFAULT NULL,
  `CREATE_DATE` datetime DEFAULT NULL,
  `AUTHOR` varchar(50) DEFAULT NULL,
  `LAST_UPDATE` datetime DEFAULT NULL,
  `EDITOR` varchar(50) DEFAULT NULL,
  `PUBLISH_DATE` datetime DEFAULT NULL,
  `PUBLISHER` varchar(50) DEFAULT NULL,
  `UNPUBLISH_DATE` datetime DEFAULT NULL,
  `UNPUBLISHER` varchar(50) DEFAULT NULL,
  `UNPUBLISH_REASON` varchar(500) DEFAULT NULL,
  `EXPIRED_DATE` date DEFAULT NULL,
  `IMAGE_URL` text,
  PRIMARY KEY (`USERNAME`),
  KEY `LOGIN_IBFK_2` (`ROLE_ID`),
  CONSTRAINT `LOGIN_IBFK_2` FOREIGN KEY (`ROLE_ID`) REFERENCES `role` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user`
--

LOCK TABLES `user` WRITE;
/*!40000 ALTER TABLE `user` DISABLE KEYS */;
INSERT INTO `user` VALUES ('085324295293','MBM','Muhammad','Yusuf','muh.yusuf17@yahoo.com','dfb50cb11a07000fcee726442ff2d270d0712d9b','2016-03-10 14:45:38','A',NULL,1,1,1,1,NULL,'2016-03-10 14:43:48','admin',NULL,NULL,'2016-03-10 14:43:48','admin',NULL,NULL,NULL,NULL,NULL),('085722722722','MBM','Reza','Fauzul','reza.fauzul@gmail.com','7c4a8d09ca3762af61e59520943dc26494f8941b',NULL,'A',NULL,1,1,1,1,NULL,'2016-06-28 13:54:33','admin',NULL,NULL,'2016-06-28 13:54:34','admin',NULL,NULL,NULL,NULL,NULL),('085722722723','MBM','Dadan','Dermawan','dadan.dermawan@gmail.com','7c4a8d09ca3762af61e59520943dc26494f8941b',NULL,'A',NULL,1,1,1,1,NULL,'2016-06-28 15:20:23','admin',NULL,NULL,'2016-06-28 15:20:23','admin',NULL,NULL,NULL,NULL,NULL),('admin','ADM','System','Administrator','admin@happytax.id','dfb50cb11a07000fcee726442ff2d270d0712d9b','2016-08-19 18:33:53','A','ADM',1,1,1,1,NULL,'2014-03-05 00:00:00','admin','2014-03-05 00:00:00','','0000-00-00 00:00:00','','0000-00-00 00:00:00','','','0000-00-00','profile_pic_200x200.png');
/*!40000 ALTER TABLE `user` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user_details`
--

DROP TABLE IF EXISTS `user_details`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user_details` (
  `USER_REF_ID` varchar(128) NOT NULL,
  `NAMA_PERUSAHAAN` varchar(128) NOT NULL,
  `ALAMAT` text NOT NULL,
  `NPWP` varchar(128) NOT NULL,
  `NAMA_DIREKTUR` varchar(128) NOT NULL,
  `NO_TELP` varchar(15) NOT NULL,
  `JENIS_USAHA` varchar(128) NOT NULL,
  `JML_KARYAWAN` int(11) NOT NULL,
  `JML_PK_PERBULAN` int(11) NOT NULL,
  `JML_PM_PERBULAN` int(11) NOT NULL,
  PRIMARY KEY (`USER_REF_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user_details`
--

LOCK TABLES `user_details` WRITE;
/*!40000 ALTER TABLE `user_details` DISABLE KEYS */;
INSERT INTO `user_details` VALUES ('muh.yusuf17@yahoo.com','PT Jaya Agung Purnama','Bandung','35039053095','Muhammad Yusuf','022 39489','Kuliner',20,20,20);
/*!40000 ALTER TABLE `user_details` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Temporary table structure for view `view_sit_order`
--

DROP TABLE IF EXISTS `view_sit_order`;
/*!50001 DROP VIEW IF EXISTS `view_sit_order`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `view_sit_order` (
  `ID_SIT_ORDER` varchar(128),
  `NO_KURSI` int(11),
  `SCHEDULE_OT_REF_ID` bigint(20),
  `DATE_ADDED` datetime,
  `DATE_MODIFIED` datetime,
  `ID_ORDER_REF` varchar(128),
  `STATUS` varchar(5),
  `ID_SCHEDULE` varchar(255),
  `ID_RUTE_FROM` varchar(255),
  `ID_RUTE_TO` varchar(255),
  `ORDER_DATE` varchar(255)
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Final view structure for view `view_sit_order`
--

/*!50001 DROP TABLE IF EXISTS `view_sit_order`*/;
/*!50001 DROP VIEW IF EXISTS `view_sit_order`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = latin1 */;
/*!50001 SET character_set_results     = latin1 */;
/*!50001 SET collation_connection      = latin1_swedish_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `view_sit_order` AS select `sit_order`.`ID_SIT_ORDER` AS `ID_SIT_ORDER`,`sit_order`.`NO_KURSI` AS `NO_KURSI`,`sit_order`.`SCHEDULE_OT_REF_ID` AS `SCHEDULE_OT_REF_ID`,`sit_order`.`DATE_ADDED` AS `DATE_ADDED`,`sit_order`.`DATE_MODIFIED` AS `DATE_MODIFIED`,`sit_order`.`ID_ORDER_REF` AS `ID_ORDER_REF`,`sit_order`.`STATUS` AS `STATUS`,`SPLIT_STR`(`sit_order`.`ID_SIT_ORDER`,'-',1) AS `ID_SCHEDULE`,`SPLIT_STR`(`sit_order`.`ID_SIT_ORDER`,'-',2) AS `ID_RUTE_FROM`,`SPLIT_STR`(`sit_order`.`ID_SIT_ORDER`,'-',3) AS `ID_RUTE_TO`,`SPLIT_STR`(`sit_order`.`ID_SIT_ORDER`,'-',4) AS `ORDER_DATE` from `sit_order` */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2016-08-19 18:35:43
