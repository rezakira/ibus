-- MySQL dump 10.13  Distrib 5.5.24, for Win64 (x86)
--
-- Host: localhost    Database: i-bis
-- ------------------------------------------------------
-- Server version	5.5.24-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `access`
--

DROP TABLE IF EXISTS `access`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `access` (
  `ROLE_ID` varchar(3) NOT NULL,
  `MENU_ID` varchar(5) NOT NULL,
  KEY `ACCESS_IBFK_1` (`ROLE_ID`),
  KEY `ACCESS_IBFK_2` (`MENU_ID`),
  CONSTRAINT `ACCESS_IBFK_1` FOREIGN KEY (`ROLE_ID`) REFERENCES `role` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `ACCESS_IBFK_2` FOREIGN KEY (`MENU_ID`) REFERENCES `menu` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `access`
--

LOCK TABLES `access` WRITE;
/*!40000 ALTER TABLE `access` DISABLE KEYS */;
INSERT INTO `access` VALUES ('ADM','REPRT'),('ADM','HELPS'),('ADM','MENUS'),('ADM','ROLES'),('ADM','USERS'),('ADM','GNSET'),('MBM','HELPS'),('MBM','REPRT'),('ADM','APSET'),('MBM','APSET'),('MBM','GNSET'),('ADM','DTMTR'),('ADM','LIBUS');
/*!40000 ALTER TABLE `access` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `bus`
--

DROP TABLE IF EXISTS `bus`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `bus` (
  `ID_BUS` bigint(20) NOT NULL,
  `NAME_BUS` varchar(255) NOT NULL,
  `SPECS_BUS` text NOT NULL,
  `COUNT_OF_CHAIR` int(11) NOT NULL,
  `DATE_ADDED` datetime NOT NULL,
  `DATE_MODIFIED` datetime NOT NULL,
  `STATUS` varchar(5) NOT NULL,
  PRIMARY KEY (`ID_BUS`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `bus`
--

LOCK TABLES `bus` WRITE;
/*!40000 ALTER TABLE `bus` DISABLE KEYS */;
/*!40000 ALTER TABLE `bus` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `menu`
--

DROP TABLE IF EXISTS `menu`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `menu` (
  `ID` varchar(5) NOT NULL,
  `TITLE` varchar(70) NOT NULL,
  `DESCRIPTION` varchar(250) DEFAULT NULL,
  `LAST_UPDATE` datetime NOT NULL,
  `STATUS` varchar(10) NOT NULL,
  `PARENT_ID` varchar(5) DEFAULT NULL,
  `SORT_NO` int(11) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `FK_have_submenu` (`PARENT_ID`),
  CONSTRAINT `FK_have_submenu` FOREIGN KEY (`PARENT_ID`) REFERENCES `menu` (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `menu`
--

LOCK TABLES `menu` WRITE;
/*!40000 ALTER TABLE `menu` DISABLE KEYS */;
INSERT INTO `menu` VALUES ('ABOUT','Bantuan Pengguna','Bantuan Pengguna','2014-04-02 00:00:00','A','HELPS',NULL),('APSET','Aplikasi & Pengaturan','Aplikasi & Pengaturan','2014-04-02 00:00:00','A',NULL,1),('DTMTR','Data Master','Data Master','2016-08-08 00:00:00','A',NULL,2),('FQA','Laporan Kerusakan','Laporan Kerusakan','2014-04-02 00:00:00','A','HELPS',NULL),('GNSET','Pengaturan','Pengaturan','2014-04-02 00:00:00','A','APSET',1),('HELPS','Bantuan','Bantuan','2014-04-02 00:00:00','A',NULL,5),('LIBUS','Bus Management','Bus Management','2016-08-08 00:00:00','A','DTMTR',1),('MENUS','Menu','Menu','2014-04-02 00:00:00','A','APSET',2),('REPRT','Laporan','Laporan','2014-04-02 00:00:00','A',NULL,4),('ROLES','Peran Pengguna','Peran Pengguna','2014-04-02 00:00:00','A','APSET',3),('USERS','Pengguna','Pengguna','2014-04-02 00:00:00','A','APSET',4);
/*!40000 ALTER TABLE `menu` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `order`
--

DROP TABLE IF EXISTS `order`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `order` (
  `ID_ORDER` varchar(255) NOT NULL,
  `USER_ID` varchar(255) NOT NULL,
  `SCHEDULE_ID` bigint(20) NOT NULL,
  `ID_RUTE_FROM` bigint(20) NOT NULL,
  `ID_RUTE_TO` bigint(20) NOT NULL,
  `PRICE_TICKET_UNIT` int(11) NOT NULL,
  `DISCOUNT` float NOT NULL,
  `TOTAL_PERSON` int(11) NOT NULL,
  `TOTAL_PRICE` int(11) NOT NULL,
  `STATUS` varchar(5) NOT NULL,
  `QRCODE` text NOT NULL,
  PRIMARY KEY (`ID_ORDER`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `order`
--

LOCK TABLES `order` WRITE;
/*!40000 ALTER TABLE `order` DISABLE KEYS */;
/*!40000 ALTER TABLE `order` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `role`
--

DROP TABLE IF EXISTS `role`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `role` (
  `ID` varchar(3) NOT NULL,
  `TITLE` varchar(70) DEFAULT NULL,
  `DESCRIPTION` varchar(250) DEFAULT NULL,
  `STATUS` varchar(10) NOT NULL,
  `UPDATE_TIME` datetime NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `role`
--

LOCK TABLES `role` WRITE;
/*!40000 ALTER TABLE `role` DISABLE KEYS */;
INSERT INTO `role` VALUES ('ADM','Administrator','FINE System Administrator','A','2014-03-05 00:00:00'),('MBM','Member','Member','A','2016-02-24 00:00:00');
/*!40000 ALTER TABLE `role` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `rute_from`
--

DROP TABLE IF EXISTS `rute_from`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `rute_from` (
  `ID_RUTE_FROM` bigint(20) NOT NULL,
  `NAME_RUTE_FROM` varchar(255) NOT NULL,
  `DESC_RUTE_FROM` text,
  `DATE_ADDED` datetime NOT NULL,
  `DATE_MODIFIED` datetime NOT NULL,
  `STATUS` varchar(5) NOT NULL,
  PRIMARY KEY (`ID_RUTE_FROM`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `rute_from`
--

LOCK TABLES `rute_from` WRITE;
/*!40000 ALTER TABLE `rute_from` DISABLE KEYS */;
/*!40000 ALTER TABLE `rute_from` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `rute_to`
--

DROP TABLE IF EXISTS `rute_to`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `rute_to` (
  `ID_RUTE_TO` bigint(20) NOT NULL,
  `ID_RUTE_FROM_REF` bigint(20) NOT NULL,
  `NAME_RUTE_TO` varchar(255) NOT NULL,
  `DESC_RUTE_TO` text,
  `DATE_ADDED` datetime NOT NULL,
  `DATE_MODIFIED` datetime NOT NULL,
  `STATUS` varchar(5) NOT NULL,
  `PRICE_TICKET_UNIT` int(11) NOT NULL,
  PRIMARY KEY (`ID_RUTE_TO`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `rute_to`
--

LOCK TABLES `rute_to` WRITE;
/*!40000 ALTER TABLE `rute_to` DISABLE KEYS */;
/*!40000 ALTER TABLE `rute_to` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `schedule_bus`
--

DROP TABLE IF EXISTS `schedule_bus`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `schedule_bus` (
  `ID_SCHEDULE` bigint(20) NOT NULL,
  `TIME_START` varchar(5) NOT NULL,
  `TIME_END` varchar(5) NOT NULL,
  `DATE_ADDED` datetime NOT NULL,
  `DATE_MODIFIED` datetime NOT NULL,
  `STATUS` varchar(5) NOT NULL,
  PRIMARY KEY (`ID_SCHEDULE`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `schedule_bus`
--

LOCK TABLES `schedule_bus` WRITE;
/*!40000 ALTER TABLE `schedule_bus` DISABLE KEYS */;
/*!40000 ALTER TABLE `schedule_bus` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `schedule_on_time`
--

DROP TABLE IF EXISTS `schedule_on_time`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `schedule_on_time` (
  `ID_ON_TIME` bigint(20) NOT NULL,
  `ID_SCHEDULE` bigint(20) NOT NULL,
  `ID_BUS` bigint(20) NOT NULL,
  `NAME_DRIVER` varchar(255) NOT NULL,
  `DATE_ADDED` datetime NOT NULL,
  `DATE_MODIFIED` datetime NOT NULL,
  `STATUS` varchar(5) NOT NULL,
  `NOTE` text NOT NULL,
  PRIMARY KEY (`ID_ON_TIME`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `schedule_on_time`
--

LOCK TABLES `schedule_on_time` WRITE;
/*!40000 ALTER TABLE `schedule_on_time` DISABLE KEYS */;
/*!40000 ALTER TABLE `schedule_on_time` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `seq`
--

DROP TABLE IF EXISTS `seq`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `seq` (
  `NAME` varchar(30) NOT NULL,
  `INCREMENT` int(11) unsigned NOT NULL DEFAULT '1',
  `MIN_VALUE` int(11) unsigned NOT NULL DEFAULT '1',
  `MAX_VALUE` bigint(20) unsigned NOT NULL DEFAULT '1844674407',
  `CUR_VALUE` bigint(20) unsigned DEFAULT '1',
  `CYCLE` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`NAME`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `seq`
--

LOCK TABLES `seq` WRITE;
/*!40000 ALTER TABLE `seq` DISABLE KEYS */;
INSERT INTO `seq` VALUES ('sq_rpm',1,1,1844674407,10,NULL),('sq_rpk',1,1,1844674407,19,NULL),('sq_rdl',1,1,1844674407,3,NULL),('sq_lt',1,1,1844674407,11,NULL),('sq_kbj',1,1,1844674407,15,NULL),('sq_dl',1,1,1844674407,28,NULL),('sq_pm',1,1,1844674407,408,NULL),('sq_pk',1,1,1844674407,3064,NULL);
/*!40000 ALTER TABLE `seq` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `setting`
--

DROP TABLE IF EXISTS `setting`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `setting` (
  `ID` char(5) NOT NULL,
  `NAME` varchar(100) DEFAULT NULL,
  `VAL` varchar(500) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `setting`
--

LOCK TABLES `setting` WRITE;
/*!40000 ALTER TABLE `setting` DISABLE KEYS */;
INSERT INTO `setting` VALUES ('ADDRS',NULL,'Jakarta Selatan'),('NAME',NULL,'PT Underwater Jakarta');
/*!40000 ALTER TABLE `setting` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user`
--

DROP TABLE IF EXISTS `user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user` (
  `USERNAME` varchar(50) NOT NULL,
  `ROLE_ID` varchar(3) NOT NULL,
  `FIRST_NAME` varchar(100) DEFAULT NULL,
  `LAST_NAME` varchar(100) DEFAULT NULL,
  `EMAIL` varchar(100) DEFAULT NULL,
  `PASSWORD` varchar(250) DEFAULT NULL,
  `LAST_LOGIN` datetime DEFAULT NULL,
  `STATUS` varchar(10) DEFAULT NULL,
  `TYPE` varchar(10) DEFAULT NULL,
  `IS_ENABLED` tinyint(1) DEFAULT NULL,
  `IS_ACCOUNT_NON_EXPIRED` tinyint(1) DEFAULT NULL,
  `IS_ACCOUNT_NON_LOCKED` tinyint(1) DEFAULT NULL,
  `IS_CREDENTIALS_NON_EXPIRED` tinyint(1) DEFAULT NULL,
  `USER_NO` varchar(100) DEFAULT NULL,
  `CREATE_DATE` datetime DEFAULT NULL,
  `AUTHOR` varchar(50) DEFAULT NULL,
  `LAST_UPDATE` datetime DEFAULT NULL,
  `EDITOR` varchar(50) DEFAULT NULL,
  `PUBLISH_DATE` datetime DEFAULT NULL,
  `PUBLISHER` varchar(50) DEFAULT NULL,
  `UNPUBLISH_DATE` datetime DEFAULT NULL,
  `UNPUBLISHER` varchar(50) DEFAULT NULL,
  `UNPUBLISH_REASON` varchar(500) DEFAULT NULL,
  `EXPIRED_DATE` date DEFAULT NULL,
  `IMAGE_URL` text,
  PRIMARY KEY (`USERNAME`),
  KEY `LOGIN_IBFK_2` (`ROLE_ID`),
  CONSTRAINT `LOGIN_IBFK_2` FOREIGN KEY (`ROLE_ID`) REFERENCES `role` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user`
--

LOCK TABLES `user` WRITE;
/*!40000 ALTER TABLE `user` DISABLE KEYS */;
INSERT INTO `user` VALUES ('085324295293','MBM','Muhammad','Yusuf','muh.yusuf17@yahoo.com','dfb50cb11a07000fcee726442ff2d270d0712d9b','2016-03-10 14:45:38','A',NULL,1,1,1,1,NULL,'2016-03-10 14:43:48','admin',NULL,NULL,'2016-03-10 14:43:48','admin',NULL,NULL,NULL,NULL,NULL),('085722722722','MBM','Reza','Fauzul','reza.fauzul@gmail.com','7c4a8d09ca3762af61e59520943dc26494f8941b',NULL,'A',NULL,1,1,1,1,NULL,'2016-06-28 13:54:33','admin',NULL,NULL,'2016-06-28 13:54:34','admin',NULL,NULL,NULL,NULL,NULL),('085722722723','MBM','Dadan','Dermawan','dadan.dermawan@gmail.com','7c4a8d09ca3762af61e59520943dc26494f8941b',NULL,'A',NULL,1,1,1,1,NULL,'2016-06-28 15:20:23','admin',NULL,NULL,'2016-06-28 15:20:23','admin',NULL,NULL,NULL,NULL,NULL),('admin','ADM','System','Administrator','admin@happytax.id','dfb50cb11a07000fcee726442ff2d270d0712d9b','2016-08-08 13:11:13','A','ADM',1,1,1,1,NULL,'2014-03-05 00:00:00','admin','2014-03-05 00:00:00','','0000-00-00 00:00:00','','0000-00-00 00:00:00','','','0000-00-00','profile_pic_200x200.png');
/*!40000 ALTER TABLE `user` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user_details`
--

DROP TABLE IF EXISTS `user_details`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user_details` (
  `USER_REF_ID` varchar(128) NOT NULL,
  `NAMA_PERUSAHAAN` varchar(128) NOT NULL,
  `ALAMAT` text NOT NULL,
  `NPWP` varchar(128) NOT NULL,
  `NAMA_DIREKTUR` varchar(128) NOT NULL,
  `NO_TELP` varchar(15) NOT NULL,
  `JENIS_USAHA` varchar(128) NOT NULL,
  `JML_KARYAWAN` int(11) NOT NULL,
  `JML_PK_PERBULAN` int(11) NOT NULL,
  `JML_PM_PERBULAN` int(11) NOT NULL,
  PRIMARY KEY (`USER_REF_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user_details`
--

LOCK TABLES `user_details` WRITE;
/*!40000 ALTER TABLE `user_details` DISABLE KEYS */;
INSERT INTO `user_details` VALUES ('muh.yusuf17@yahoo.com','PT Jaya Agung Purnama','Bandung','35039053095','Muhammad Yusuf','022 39489','Kuliner',20,20,20);
/*!40000 ALTER TABLE `user_details` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2016-08-08 17:43:18
