-- phpMyAdmin SQL Dump
-- version 3.3.9
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Aug 25, 2016 at 08:00 AM
-- Server version: 5.5.8
-- PHP Version: 5.3.5

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `i-bis`
--

-- --------------------------------------------------------

--
-- Table structure for table `access`
--

CREATE TABLE IF NOT EXISTS `access` (
  `ROLE_ID` varchar(3) NOT NULL,
  `MENU_ID` varchar(5) NOT NULL,
  KEY `ACCESS_IBFK_1` (`ROLE_ID`),
  KEY `ACCESS_IBFK_2` (`MENU_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `access`
--

INSERT INTO `access` (`ROLE_ID`, `MENU_ID`) VALUES
('ADM', 'REPRT'),
('ADM', 'HELPS'),
('ADM', 'MENUS'),
('ADM', 'ROLES'),
('ADM', 'USERS'),
('ADM', 'GNSET'),
('MBM', 'HELPS'),
('MBM', 'REPRT'),
('ADM', 'APSET'),
('MBM', 'APSET'),
('MBM', 'GNSET'),
('ADM', 'DTMTR'),
('ADM', 'LIBUS'),
('ADM', 'LISSB'),
('ADM', 'LISOT'),
('ADM', 'LISDS'),
('ADM', 'TRMNG'),
('ADM', 'LIORD'),
('ADM', 'BPMNG'),
('ADM', 'LIBPM');

-- --------------------------------------------------------

--
-- Table structure for table `berita_promo`
--

CREATE TABLE IF NOT EXISTS `berita_promo` (
  `ID_BERITA_PROMO` bigint(20) NOT NULL,
  `TITLE` varchar(128) NOT NULL,
  `CONTENT_TYPE` varchar(5) NOT NULL,
  `TIME_START` date NOT NULL,
  `TIME_END` date NOT NULL,
  `CONTENT` text NOT NULL,
  `STATUS` varchar(5) NOT NULL,
  `IMAGE_URL` varchar(500) NOT NULL,
  `DATE_ADDED` datetime NOT NULL,
  `DATE_MODIFIED` datetime NOT NULL,
  `POTONGAN_HARGA` int(20) NOT NULL,
  `STATUS_PILIHAN_PROMO` varchar(5) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `berita_promo`
--

INSERT INTO `berita_promo` (`ID_BERITA_PROMO`, `TITLE`, `CONTENT_TYPE`, `TIME_START`, `TIME_END`, `CONTENT`, `STATUS`, `IMAGE_URL`, `DATE_ADDED`, `DATE_MODIFIED`, `POTONGAN_HARGA`, `STATUS_PILIHAN_PROMO`) VALUES
(133, 'Test Berita', '0', '2016-08-24', '2016-08-26', 'asda', '0', '', '2016-08-23 11:47:36', '2016-08-24 14:19:49', 0, '1'),
(134, 'Test Promo', '1', '2016-08-23', '2016-08-24', '<p>asd</p>', '0', '', '2016-08-23 15:24:09', '2016-08-25 12:03:24', 123, '1'),
(135, 'Test Promo 12', '1', '2016-08-31', '2016-09-02', '<p>asdasd</p>', '0', '', '2016-08-25 11:54:49', '2016-08-25 12:33:50', 1231231233, '1');

-- --------------------------------------------------------

--
-- Table structure for table `bus`
--

CREATE TABLE IF NOT EXISTS `bus` (
  `ID_BUS` bigint(20) NOT NULL,
  `NAME_BUS` varchar(255) NOT NULL,
  `SPECS_BUS` text NOT NULL,
  `COUNT_OF_CHAIR` int(11) NOT NULL,
  `DATE_ADDED` datetime NOT NULL,
  `DATE_MODIFIED` datetime NOT NULL,
  `STATUS` varchar(5) NOT NULL,
  PRIMARY KEY (`ID_BUS`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `bus`
--

INSERT INTO `bus` (`ID_BUS`, `NAME_BUS`, `SPECS_BUS`, `COUNT_OF_CHAIR`, `DATE_ADDED`, `DATE_MODIFIED`, `STATUS`) VALUES
(2, 'Bis B', 'Spesifikasi Bis B', 50, '2016-08-09 10:05:06', '2016-08-09 15:27:03', 'A'),
(4, 'Bis C', 'Spesifikasi Bis C', 50, '2016-08-10 16:19:10', '2016-08-10 16:19:10', 'A'),
(5, 'Bis A', 'Bis A', 133, '2016-08-15 15:51:29', '2016-08-15 15:51:29', 'A'),
(6, 'Bis D', 'bis d', 30, '2016-08-25 12:53:01', '2016-08-25 12:53:01', 'A');

-- --------------------------------------------------------

--
-- Table structure for table `menu`
--

CREATE TABLE IF NOT EXISTS `menu` (
  `ID` varchar(5) NOT NULL,
  `TITLE` varchar(70) NOT NULL,
  `DESCRIPTION` varchar(250) DEFAULT NULL,
  `LAST_UPDATE` datetime NOT NULL,
  `STATUS` varchar(10) NOT NULL,
  `PARENT_ID` varchar(5) DEFAULT NULL,
  `SORT_NO` int(11) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `FK_have_submenu` (`PARENT_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `menu`
--

INSERT INTO `menu` (`ID`, `TITLE`, `DESCRIPTION`, `LAST_UPDATE`, `STATUS`, `PARENT_ID`, `SORT_NO`) VALUES
('ABOUT', 'Bantuan Pengguna', 'Bantuan Pengguna', '2014-04-02 00:00:00', 'A', 'HELPS', NULL),
('APSET', 'Aplikasi & Pengaturan', 'Aplikasi & Pengaturan', '2014-04-02 00:00:00', 'A', NULL, 1),
('BPMNG', 'Berita & Promo Management', 'Berita & Promo Management', '2016-08-11 00:00:00', 'A', NULL, 3),
('DTMTR', 'Data Master', 'Data Master', '2016-08-08 00:00:00', 'A', NULL, 2),
('FQA', 'Laporan Kerusakan', 'Laporan Kerusakan', '2014-04-02 00:00:00', 'A', 'HELPS', NULL),
('GNSET', 'Pengaturan', 'Pengaturan', '2014-04-02 00:00:00', 'A', 'APSET', 1),
('HELPS', 'Bantuan', 'Bantuan', '2014-04-02 00:00:00', 'A', NULL, 5),
('LIBPM', 'Daftar Berita & Promo', 'Daftar Berita & Promo', '2016-08-11 00:00:00', 'A', 'BPMNG', 1),
('LIBUS', 'Bus Management', 'Bus Management', '2016-08-08 00:00:00', 'A', 'DTMTR', 1),
('LIORD', 'Order Management', 'Order Management', '2016-08-10 00:00:00', 'A', 'TRMNG', 1),
('LISDS', 'Rute Management', 'Rute Management', '2016-08-09 00:00:00', 'A', 'DTMTR', 3),
('LISOT', 'Schedule On Time Management', 'Schedule On Time Management', '2016-08-10 09:54:39', 'A', 'DTMTR', 4),
('LISSB', 'Schedule Bis Management', 'Schedule Bis Management', '2016-08-09 10:09:08', 'A', 'DTMTR', 2),
('MENUS', 'Menu', 'Menu', '2014-04-02 00:00:00', 'A', 'APSET', 2),
('REPRT', 'Laporan', 'Laporan', '2014-04-02 00:00:00', 'A', NULL, 4),
('ROLES', 'Peran Pengguna', 'Peran Pengguna', '2014-04-02 00:00:00', 'A', 'APSET', 3),
('TRMNG', 'Transaksi Management', 'Transaksi Management', '2016-08-10 00:00:00', 'A', NULL, 3),
('USERS', 'Pengguna', 'Pengguna', '2014-04-02 00:00:00', 'A', 'APSET', 4);

-- --------------------------------------------------------

--
-- Table structure for table `order_transaction`
--

CREATE TABLE IF NOT EXISTS `order_transaction` (
  `ID_ORDER` varchar(255) NOT NULL,
  `USER_ID` varchar(255) NOT NULL,
  `SCHEDULE_ID` bigint(20) NOT NULL,
  `ID_RUTE_FROM` bigint(20) NOT NULL,
  `ID_RUTE_TO` bigint(20) NOT NULL,
  `PRICE_TICKET_UNIT` int(11) NOT NULL,
  `DISCOUNT` float NOT NULL,
  `TOTAL_PERSON` int(11) NOT NULL,
  `TOTAL_PRICE` int(11) NOT NULL,
  `STATUS` varchar(5) NOT NULL,
  `QRCODE` text NOT NULL,
  `ORDER_DATE` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `DATE_ADDED` datetime NOT NULL,
  `DATE_MODIFIED` datetime NOT NULL,
  PRIMARY KEY (`ID_ORDER`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `order_transaction`
--

INSERT INTO `order_transaction` (`ID_ORDER`, `USER_ID`, `SCHEDULE_ID`, `ID_RUTE_FROM`, `ID_RUTE_TO`, `PRICE_TICKET_UNIT`, `DISCOUNT`, `TOTAL_PERSON`, `TOTAL_PRICE`, `STATUS`, `QRCODE`, `ORDER_DATE`, `DATE_ADDED`, `DATE_MODIFIED`) VALUES
('3skidsil', '085722722722', 10, 1, 1, 200000, 0, 2, 400000, '1', '3skidsil', '2016-08-26 00:00:00', '2016-08-25 12:53:49', '2016-08-25 12:53:49'),
('3w2c9zwd', '085722722722', 10, 1, 1, 200000, 0, 1, 200000, '1', '3w2c9zwd', '2016-08-26 00:00:00', '2016-08-25 12:54:12', '2016-08-25 12:54:12'),
('7bam4nnt', '085722722722', 10, 4, 4, 200000, 0, 2, 400000, '1', '7bam4nnt', '2016-08-29 00:00:00', '2016-08-25 12:47:21', '2016-08-25 12:47:21'),
('c19dmd6k', '085722722723', 10, 4, 4, 200000, 0, 1, 200000, '1', 'c19dmd6k', '2016-08-29 00:00:00', '2016-08-25 12:48:16', '2016-08-25 12:48:16'),
('liz2zce1', '085324295293', 10, 4, 4, 200000, 0, 1, 200000, '1', 'liz2zce1', '2016-08-30 00:00:00', '2016-08-23 13:22:23', '2016-08-23 13:22:23'),
('tr9mxydb', '085324295293', 10, 1, 1, 200000, 0, 1, 200000, '1', 'tr9mxydb', '2016-08-08 00:00:00', '2016-08-11 17:12:08', '2016-08-11 17:12:08');

-- --------------------------------------------------------

--
-- Table structure for table `promo_berlaku`
--

CREATE TABLE IF NOT EXISTS `promo_berlaku` (
  `ID_PROMO_BERLAKU` bigint(20) NOT NULL,
  `ID_BERITA_PROMO` bigint(20) NOT NULL,
  `ID_RUTE_AWAL` bigint(20) NOT NULL,
  `ID_RUTE_TUJUAN` bigint(20) NOT NULL,
  `DATE_ADDED` datetime NOT NULL,
  `DATE_MODIFIED` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `promo_berlaku`
--

INSERT INTO `promo_berlaku` (`ID_PROMO_BERLAKU`, `ID_BERITA_PROMO`, `ID_RUTE_AWAL`, `ID_RUTE_TUJUAN`, `DATE_ADDED`, `DATE_MODIFIED`) VALUES
(8, 134, 4, 4, '2016-08-23 15:48:38', '2016-08-23 15:48:38'),
(46, 134, 1, 1, '2016-08-25 11:54:06', '2016-08-25 11:54:06');

-- --------------------------------------------------------

--
-- Table structure for table `role`
--

CREATE TABLE IF NOT EXISTS `role` (
  `ID` varchar(3) NOT NULL,
  `TITLE` varchar(70) DEFAULT NULL,
  `DESCRIPTION` varchar(250) DEFAULT NULL,
  `STATUS` varchar(10) NOT NULL,
  `UPDATE_TIME` datetime NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `role`
--

INSERT INTO `role` (`ID`, `TITLE`, `DESCRIPTION`, `STATUS`, `UPDATE_TIME`) VALUES
('ADM', 'Administrator', 'FINE System Administrator', 'A', '2014-03-05 00:00:00'),
('MBM', 'Member', 'Member', 'A', '2016-02-24 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `rute_from`
--

CREATE TABLE IF NOT EXISTS `rute_from` (
  `ID_RUTE_FROM` bigint(20) NOT NULL,
  `NAME_RUTE_FROM` varchar(255) NOT NULL,
  `DESC_RUTE_FROM` text,
  `DATE_ADDED` datetime NOT NULL,
  `DATE_MODIFIED` datetime NOT NULL,
  `STATUS` varchar(5) NOT NULL,
  PRIMARY KEY (`ID_RUTE_FROM`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `rute_from`
--

INSERT INTO `rute_from` (`ID_RUTE_FROM`, `NAME_RUTE_FROM`, `DESC_RUTE_FROM`, `DATE_ADDED`, `DATE_MODIFIED`, `STATUS`) VALUES
(1, 'Medan', 'Medan', '2016-08-10 10:15:04', '2016-08-10 10:15:04', 'A'),
(4, 'Bandung', 'bandung', '2016-08-11 14:24:52', '2016-08-11 14:24:52', 'A');

-- --------------------------------------------------------

--
-- Table structure for table `rute_to`
--

CREATE TABLE IF NOT EXISTS `rute_to` (
  `ID_RUTE_TO` bigint(20) NOT NULL,
  `ID_RUTE_FROM_REF` bigint(20) NOT NULL,
  `NAME_RUTE_TO` varchar(255) NOT NULL,
  `DESC_RUTE_TO` text,
  `DATE_ADDED` datetime NOT NULL,
  `DATE_MODIFIED` datetime NOT NULL,
  `STATUS` varchar(5) NOT NULL,
  `PRICE_TICKET_UNIT` int(11) NOT NULL,
  PRIMARY KEY (`ID_RUTE_TO`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `rute_to`
--

INSERT INTO `rute_to` (`ID_RUTE_TO`, `ID_RUTE_FROM_REF`, `NAME_RUTE_TO`, `DESC_RUTE_TO`, `DATE_ADDED`, `DATE_MODIFIED`, `STATUS`, `PRICE_TICKET_UNIT`) VALUES
(1, 1, 'Pematang Siantar', 'Pematang Siantar', '2016-08-10 13:23:14', '2016-08-10 13:23:14', 'A', 200000),
(4, 4, 'Cimahi', 'cimahi', '2016-08-11 14:25:12', '2016-08-11 14:25:12', 'A', 200000);

-- --------------------------------------------------------

--
-- Table structure for table `schedule_bus`
--

CREATE TABLE IF NOT EXISTS `schedule_bus` (
  `ID_SCHEDULE` bigint(20) NOT NULL,
  `TIME_START` varchar(5) NOT NULL,
  `TIME_END` varchar(5) NOT NULL,
  `DATE_ADDED` datetime NOT NULL,
  `DATE_MODIFIED` datetime NOT NULL,
  `STATUS` varchar(5) NOT NULL,
  PRIMARY KEY (`ID_SCHEDULE`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `schedule_bus`
--

INSERT INTO `schedule_bus` (`ID_SCHEDULE`, `TIME_START`, `TIME_END`, `DATE_ADDED`, `DATE_MODIFIED`, `STATUS`) VALUES
(10, '09.00', '14.00', '2016-08-09 17:15:25', '2016-08-09 17:15:25', 'A'),
(11, '07.00', '12.00', '2016-08-10 16:18:46', '2016-08-10 16:18:46', 'A');

-- --------------------------------------------------------

--
-- Table structure for table `schedule_on_time`
--

CREATE TABLE IF NOT EXISTS `schedule_on_time` (
  `ID_ON_TIME` bigint(20) NOT NULL,
  `ID_SCHEDULE` bigint(20) NOT NULL,
  `ID_BUS` bigint(20) NOT NULL,
  `RUTE_FROM_ID_REF` bigint(20) NOT NULL,
  `RUTE_TO_ID_REF` bigint(20) NOT NULL,
  `NAME_DRIVER` varchar(255) NOT NULL,
  `DATE_ADDED` datetime NOT NULL,
  `DATE_MODIFIED` datetime NOT NULL,
  `STATUS` varchar(5) NOT NULL,
  `NOTE` text NOT NULL,
  `ACTUAL_DATE` date NOT NULL,
  PRIMARY KEY (`ID_ON_TIME`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `schedule_on_time`
--

INSERT INTO `schedule_on_time` (`ID_ON_TIME`, `ID_SCHEDULE`, `ID_BUS`, `RUTE_FROM_ID_REF`, `RUTE_TO_ID_REF`, `NAME_DRIVER`, `DATE_ADDED`, `DATE_MODIFIED`, `STATUS`, `NOTE`, `ACTUAL_DATE`) VALUES
(29, 10, 2, 1, 1, 'Dimas', '2016-08-16 17:44:31', '2016-08-16 17:44:31', 'B', 'asd', '2016-08-18'),
(30, 10, 2, 4, 4, 'asd', '2016-08-16 17:45:04', '2016-08-16 17:45:04', 'B', 'asd', '2016-08-18'),
(31, 11, 2, 4, 4, 'asd', '2016-08-16 17:58:19', '2016-08-16 17:58:19', 'B', 'asd', '2016-08-18'),
(32, 11, 2, 4, 4, 'asd', '2016-08-16 17:58:45', '2016-08-16 17:58:45', 'B', 'asd', '2016-08-19'),
(33, 10, 2, 1, 1, 'Jajang', '2016-08-22 11:36:01', '2016-08-22 11:36:01', 'B', 'Bis', '2016-08-31'),
(34, 10, 6, 1, 1, 'dimas', '2016-08-25 12:53:30', '2016-08-25 12:53:30', 'B', 'asd', '2016-08-26'),
(35, 10, 2, 1, 1, 'Dimas', '2016-08-25 12:58:00', '2016-08-25 12:58:00', 'B', 'asd', '2016-08-29');

-- --------------------------------------------------------

--
-- Table structure for table `seq`
--

CREATE TABLE IF NOT EXISTS `seq` (
  `NAME` varchar(30) NOT NULL,
  `INCREMENT` int(11) unsigned NOT NULL DEFAULT '1',
  `MIN_VALUE` int(11) unsigned NOT NULL DEFAULT '1',
  `MAX_VALUE` bigint(20) unsigned NOT NULL DEFAULT '1844674407',
  `CUR_VALUE` bigint(20) unsigned DEFAULT '1',
  `CYCLE` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`NAME`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `seq`
--

INSERT INTO `seq` (`NAME`, `INCREMENT`, `MIN_VALUE`, `MAX_VALUE`, `CUR_VALUE`, `CYCLE`) VALUES
('sq_rpm', 1, 1, 1844674407, 10, NULL),
('sq_rpk', 1, 1, 1844674407, 19, NULL),
('sq_rdl', 1, 1, 1844674407, 3, NULL),
('sq_lt', 1, 1, 1844674407, 11, NULL),
('sq_kbj', 1, 1, 1844674407, 15, NULL),
('sq_dl', 1, 1, 1844674407, 28, NULL),
('sq_pm', 1, 1, 1844674407, 408, NULL),
('sq_pk', 1, 1, 1844674407, 3064, NULL),
('sq_bus', 1, 1, 1844674407, 7, NULL),
('sq_schedule', 1, 1, 1844674407, 12, NULL),
('sq_rute_from', 1, 1, 1844674407, 5, NULL),
('sq_rute_to', 1, 1, 1844674407, 5, NULL),
('sq_schedule_on_time', 1, 1, 1844674407, 36, NULL),
('sq_berita_promo', 1, 1, 1844674407, 136, NULL),
('sq_promo_berlaku', 1, 1, 1844674407, 47, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `setting`
--

CREATE TABLE IF NOT EXISTS `setting` (
  `ID` char(5) NOT NULL,
  `NAME` varchar(100) DEFAULT NULL,
  `VAL` varchar(500) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `setting`
--

INSERT INTO `setting` (`ID`, `NAME`, `VAL`) VALUES
('ADDRS', NULL, 'Jakarta Selatan'),
('NAME', NULL, 'PT Underwater Jakarta');

-- --------------------------------------------------------

--
-- Table structure for table `sit_order`
--

CREATE TABLE IF NOT EXISTS `sit_order` (
  `ID_SIT_ORDER` varchar(128) NOT NULL,
  `NO_KURSI` int(11) NOT NULL,
  `SCHEDULE_OT_REF_ID` bigint(20) DEFAULT NULL,
  `DATE_ADDED` datetime NOT NULL,
  `DATE_MODIFIED` datetime NOT NULL,
  `ID_ORDER_REF` varchar(128) NOT NULL,
  `STATUS` varchar(5) NOT NULL,
  PRIMARY KEY (`ID_SIT_ORDER`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `sit_order`
--

INSERT INTO `sit_order` (`ID_SIT_ORDER`, `NO_KURSI`, `SCHEDULE_OT_REF_ID`, `DATE_ADDED`, `DATE_MODIFIED`, `ID_ORDER_REF`, `STATUS`) VALUES
('10-1-1-2016/08/26-12', 12, 34, '2016-08-25 12:53:56', '2016-08-25 12:53:56', '3skidsil', '1'),
('10-1-1-2016/08/26-13', 13, 34, '2016-08-25 12:54:19', '2016-08-25 12:54:19', '3w2c9zwd', '1'),
('10-1-1-2016/08/26-14', 14, 34, '2016-08-25 12:53:57', '2016-08-25 12:53:57', '3skidsil', '1'),
('10-4-4-2016/08/29-2', 2, 0, '2016-08-25 12:47:35', '2016-08-25 12:47:35', '7bam4nnt', '1'),
('10-4-4-2016/08/29-5', 5, 0, '2016-08-25 12:47:51', '2016-08-25 12:47:51', '7bam4nnt', '1');

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE IF NOT EXISTS `user` (
  `USERNAME` varchar(50) NOT NULL,
  `ROLE_ID` varchar(3) NOT NULL,
  `FIRST_NAME` varchar(100) DEFAULT NULL,
  `LAST_NAME` varchar(100) DEFAULT NULL,
  `EMAIL` varchar(100) DEFAULT NULL,
  `PASSWORD` varchar(250) DEFAULT NULL,
  `LAST_LOGIN` datetime DEFAULT NULL,
  `STATUS` varchar(10) DEFAULT NULL,
  `TYPE` varchar(10) DEFAULT NULL,
  `IS_ENABLED` tinyint(1) DEFAULT NULL,
  `IS_ACCOUNT_NON_EXPIRED` tinyint(1) DEFAULT NULL,
  `IS_ACCOUNT_NON_LOCKED` tinyint(1) DEFAULT NULL,
  `IS_CREDENTIALS_NON_EXPIRED` tinyint(1) DEFAULT NULL,
  `USER_NO` varchar(100) DEFAULT NULL,
  `CREATE_DATE` datetime DEFAULT NULL,
  `AUTHOR` varchar(50) DEFAULT NULL,
  `LAST_UPDATE` datetime DEFAULT NULL,
  `EDITOR` varchar(50) DEFAULT NULL,
  `PUBLISH_DATE` datetime DEFAULT NULL,
  `PUBLISHER` varchar(50) DEFAULT NULL,
  `UNPUBLISH_DATE` datetime DEFAULT NULL,
  `UNPUBLISHER` varchar(50) DEFAULT NULL,
  `UNPUBLISH_REASON` varchar(500) DEFAULT NULL,
  `EXPIRED_DATE` date DEFAULT NULL,
  `IMAGE_URL` text,
  PRIMARY KEY (`USERNAME`),
  KEY `LOGIN_IBFK_2` (`ROLE_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`USERNAME`, `ROLE_ID`, `FIRST_NAME`, `LAST_NAME`, `EMAIL`, `PASSWORD`, `LAST_LOGIN`, `STATUS`, `TYPE`, `IS_ENABLED`, `IS_ACCOUNT_NON_EXPIRED`, `IS_ACCOUNT_NON_LOCKED`, `IS_CREDENTIALS_NON_EXPIRED`, `USER_NO`, `CREATE_DATE`, `AUTHOR`, `LAST_UPDATE`, `EDITOR`, `PUBLISH_DATE`, `PUBLISHER`, `UNPUBLISH_DATE`, `UNPUBLISHER`, `UNPUBLISH_REASON`, `EXPIRED_DATE`, `IMAGE_URL`) VALUES
('', 'MBM', '', '', '', '522420c10d4ea840c08946170a90f3ac7a06618e', NULL, 'A', NULL, 1, 1, 1, 1, NULL, '2016-08-20 15:36:26', 'admin', NULL, NULL, '2016-08-20 15:36:26', 'admin', NULL, NULL, NULL, NULL, NULL),
('085324295293', 'MBM', 'Muhammad', 'Yusuf', 'muh.yusuf17@yahoo.com', 'dfb50cb11a07000fcee726442ff2d270d0712d9b', '2016-03-10 14:45:38', 'A', NULL, 1, 1, 1, 1, NULL, '2016-03-10 14:43:48', 'admin', NULL, NULL, '2016-03-10 14:43:48', 'admin', NULL, NULL, NULL, NULL, NULL),
('085722722722', 'MBM', 'Reza', 'Fauzul', 'reza.fauzul@gmail.com', '7c4a8d09ca3762af61e59520943dc26494f8941b', NULL, 'A', NULL, 1, 1, 1, 1, NULL, '2016-06-28 13:54:33', 'admin', NULL, NULL, '2016-06-28 13:54:34', 'admin', NULL, NULL, NULL, NULL, NULL),
('085722722723', 'MBM', 'Dadan', 'Dermawan', 'dadan.dermawan@gmail.com', '7c4a8d09ca3762af61e59520943dc26494f8941b', NULL, 'A', NULL, 1, 1, 1, 1, NULL, '2016-06-28 15:20:23', 'admin', NULL, NULL, '2016-06-28 15:20:23', 'admin', NULL, NULL, NULL, NULL, NULL),
('admin', 'ADM', 'System', 'Administrator', 'admin@happytax.id', 'dfb50cb11a07000fcee726442ff2d270d0712d9b', '2016-08-25 12:04:56', 'A', 'ADM', 1, 1, 1, 1, NULL, '2014-03-05 00:00:00', 'admin', '2014-03-05 00:00:00', '', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00', '', '', '0000-00-00', 'profile_pic_200x200.png'),
('tesstuser', 'MBM', 'test', 'last', 'example@email.com', 'dfb50cb11a07000fcee726442ff2d270d0712d9b', NULL, 'A', NULL, 1, 1, 1, 1, NULL, '2016-08-19 13:01:24', 'admin', NULL, NULL, '2016-08-19 13:01:24', 'admin', NULL, NULL, NULL, NULL, NULL),
('testing', 'MBM', 'Test', '', 'test@a.com', '40bd001563085fc35165329ea1ff5c5ecbdbbeef', NULL, 'A', NULL, 1, 1, 1, 1, NULL, '2016-08-19 13:50:37', 'admin', NULL, NULL, '2016-08-19 13:50:37', 'admin', NULL, NULL, NULL, NULL, NULL),
('testuser', 'MBM', 'test', 'last', 'example@email.com', 'dfb50cb11a07000fcee726442ff2d270d0712d9b', NULL, 'A', NULL, 1, 1, 1, 1, NULL, '2016-08-19 12:55:36', 'admin', NULL, NULL, '2016-08-19 12:55:36', 'admin', NULL, NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `user_details`
--

CREATE TABLE IF NOT EXISTS `user_details` (
  `USER_REF_ID` varchar(128) NOT NULL,
  `NAMA_PERUSAHAAN` varchar(128) NOT NULL,
  `ALAMAT` text NOT NULL,
  `NPWP` varchar(128) NOT NULL,
  `NAMA_DIREKTUR` varchar(128) NOT NULL,
  `NO_TELP` varchar(15) NOT NULL,
  `JENIS_USAHA` varchar(128) NOT NULL,
  `JML_KARYAWAN` int(11) NOT NULL,
  `JML_PK_PERBULAN` int(11) NOT NULL,
  `JML_PM_PERBULAN` int(11) NOT NULL,
  PRIMARY KEY (`USER_REF_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user_details`
--

INSERT INTO `user_details` (`USER_REF_ID`, `NAMA_PERUSAHAAN`, `ALAMAT`, `NPWP`, `NAMA_DIREKTUR`, `NO_TELP`, `JENIS_USAHA`, `JML_KARYAWAN`, `JML_PK_PERBULAN`, `JML_PM_PERBULAN`) VALUES
('muh.yusuf17@yahoo.com', 'PT Jaya Agung Purnama', 'Bandung', '35039053095', 'Muhammad Yusuf', '022 39489', 'Kuliner', 20, 20, 20);

-- --------------------------------------------------------

--
-- Stand-in structure for view `view_sit_order`
--
CREATE TABLE IF NOT EXISTS `view_sit_order` (
`ID_SIT_ORDER` varchar(128)
,`NO_KURSI` int(11)
,`SCHEDULE_OT_REF_ID` bigint(20)
,`DATE_ADDED` datetime
,`DATE_MODIFIED` datetime
,`ID_ORDER_REF` varchar(128)
,`STATUS` varchar(5)
,`ID_SCHEDULE` varchar(255)
,`ID_RUTE_FROM` varchar(255)
,`ID_RUTE_TO` varchar(255)
,`ORDER_DATE` varchar(255)
);
-- --------------------------------------------------------

--
-- Structure for view `view_sit_order`
--
DROP TABLE IF EXISTS `view_sit_order`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `view_sit_order` AS select `sit_order`.`ID_SIT_ORDER` AS `ID_SIT_ORDER`,`sit_order`.`NO_KURSI` AS `NO_KURSI`,`sit_order`.`SCHEDULE_OT_REF_ID` AS `SCHEDULE_OT_REF_ID`,`sit_order`.`DATE_ADDED` AS `DATE_ADDED`,`sit_order`.`DATE_MODIFIED` AS `DATE_MODIFIED`,`sit_order`.`ID_ORDER_REF` AS `ID_ORDER_REF`,`sit_order`.`STATUS` AS `STATUS`,`SPLIT_STR`(`sit_order`.`ID_SIT_ORDER`,'-',1) AS `ID_SCHEDULE`,`SPLIT_STR`(`sit_order`.`ID_SIT_ORDER`,'-',2) AS `ID_RUTE_FROM`,`SPLIT_STR`(`sit_order`.`ID_SIT_ORDER`,'-',3) AS `ID_RUTE_TO`,`SPLIT_STR`(`sit_order`.`ID_SIT_ORDER`,'-',4) AS `ORDER_DATE` from `sit_order`;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `access`
--
ALTER TABLE `access`
  ADD CONSTRAINT `ACCESS_IBFK_1` FOREIGN KEY (`ROLE_ID`) REFERENCES `role` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `ACCESS_IBFK_2` FOREIGN KEY (`MENU_ID`) REFERENCES `menu` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `menu`
--
ALTER TABLE `menu`
  ADD CONSTRAINT `FK_have_submenu` FOREIGN KEY (`PARENT_ID`) REFERENCES `menu` (`ID`);

--
-- Constraints for table `user`
--
ALTER TABLE `user`
  ADD CONSTRAINT `LOGIN_IBFK_2` FOREIGN KEY (`ROLE_ID`) REFERENCES `role` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION;
