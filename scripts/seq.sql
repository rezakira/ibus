SET GLOBAL log_bin_trust_function_creators = 1;

drop function nextval;
drop function currval;

delimiter $$
CREATE FUNCTION nextval (seq_name varchar(100))
RETURNS bigint(20) NOT DETERMINISTIC
BEGIN
    DECLARE cur_val bigint(20);
 
    SELECT
        cur_value INTO cur_val
    FROM
        seq
    WHERE
        name = seq_name
    ;
 
    IF cur_val IS NOT NULL THEN
        UPDATE
            seq
        SET
            cur_value = IF (
                (cur_value + increment) > max_value,
                IF (
                    cycle = TRUE,
                    min_value,
                    NULL
                ),
                cur_value + increment
            )
        WHERE
            name = seq_name
        ;
    END IF;
 
    RETURN cur_val;
END;
$$
delimiter ;

delimiter $$
CREATE FUNCTION currval (seq_name varchar(100))
RETURNS bigint(20) NOT DETERMINISTIC
BEGIN
	
	declare cur_val bigint(20);
	
    SELECT
        cur_value INTO cur_val
    FROM
        seq
    WHERE
        name = seq_name
    ;
    
    RETURN cur_val;
END;
$$
delimiter ;

-- This code will create sequence with default values.
--Sequence for Subject
INSERT INTO seq
    (name)
VALUE
    ('sq_subject')
;
 
select currval('sq_subject');
select nextval('sq_subject');

--Sequence for Content
INSERT INTO seq
    (name)
VALUE
    ('sq_content')
;
